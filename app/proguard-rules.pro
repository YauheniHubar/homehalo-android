# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Program Files (x86)\Android\android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
# keep anything annotated with @JsonCreator

-keepclassmembers public class * {
     @com.fasterxml.jackson.annotation.JsonCreator *;
}
-keep class !android.support.v7.internal.view.menu.MenuBuilder, !android.support.v7.internal.view.menu.SubMenuBuilder, android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-keep class org.apache.http.** { *; }
-keep class com.octo.** {*;}
-keep class com.fasterxml.jackson.** { *; }
-keepnames interface com.fasterxml.jackson.** { *; }
-dontwarn org.apache.http.**
-dontwarn net.kencochrane.**
-dontwarn com.fasterxml.**
-dontwarn com.google.common.**
-dontwarn com.octo.**
-dontwarn org.joda.**
-dontwarn org.apache.**
-dontwarn org.springframework.**
-dontwarn org.slf4j.**