package android.itransition.com.wedge;

import android.itransition.com.wedge.activity.BaseActivity;
import android.itransition.com.wedge.activity.ManagementActivity;
import android.os.Handler;
import android.os.Message;

/**
 * Created by e.kazimirova on 17.11.2014.
 */
public class ActiveMessageHandler extends Handler {

    private static ActiveMessageHandler _instance = new ActiveMessageHandler();
    private BaseActivity _activity = null;

    @Override
    public void handleMessage(Message message) {
        if(message.obj.toString().equals("gcmNewMessage") && _activity != null)
            ((ManagementActivity)_activity).repopulateList();
        super.handleMessage(message);
    }

    public static ActiveMessageHandler instance() {
        return _instance;
    }

    public void setActivity(BaseActivity activity) {
        _activity = activity;
    }

    public BaseActivity getActivity() {
        return _activity;
    }

}
