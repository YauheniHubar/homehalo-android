package android.itransition.com.wedge;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.itransition.com.wedge.activity.ManagementActivity;
import android.itransition.com.wedge.activity.OrderActivity;
import android.itransition.com.wedge.settings.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

/**
 * Created by i.grechishchev on 10.11.2015.
 * itransition 2015
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("grch","alarm " + String.valueOf(Settings.isAlarmCanceled()));
        if (!Settings.isAlarmCanceled()) {
            Intent notificationIntent = new Intent(context, OrderActivity.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(ManagementActivity.class);
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

            Notification notification = builder.setContentTitle("HomeHalo")
                    .setContentText("HomeHalo helps manage your kids internet. Order yours today")
                    .setTicker("New Message Alert!")
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent).build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        }
    }
}
