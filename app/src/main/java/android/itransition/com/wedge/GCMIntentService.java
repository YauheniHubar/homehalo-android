package android.itransition.com.wedge;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.itransition.com.wedge.robospice.ApiService;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.octo.android.robospice.SpiceManager;

import me.leolin.shortcutbadger.ShortcutBadger;

public class GCMIntentService extends IntentService {
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_NOTIFICATION_ID = "notification_id";
    private static String tmp_notification_id = "";
    private static final int REQUEST_CODE = 536;
    public static final int MAIN_NOTIFICATION_ID = 11;
    public SpiceManager contentManager = new SpiceManager(ApiService.class);

    public GCMIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();


//        extras.isEmpty(); // has effect of unparcelling Bundle

        if (Settings.LOG) {
            Log.i("GCMIntentService", extras.toString());
        }

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);


        if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

            if (intent.getExtras().getString(KEY_MESSAGE) != null) {
                //check for multiple push notifications
                if (intent.getExtras().getString(KEY_NOTIFICATION_ID) != null) {
                    if (!tmp_notification_id.equals(intent.getExtras().getString(KEY_NOTIFICATION_ID))) {
                        tmp_notification_id = intent.getExtras().getString(KEY_NOTIFICATION_ID);
                        Settings.setUpdatingAlerts(false);
                        // if ConversationActivity is active, send a message to handler to refresh the conversation
                        if (ActiveMessageHandler.instance().getActivity() != null) {
                            Message msg = Message.obtain(ActiveMessageHandler.instance());
                            msg.obj = "gcmNewMessage";
                            ActiveMessageHandler.instance().sendMessage(msg);
                            Log.d("grch11", "msg: " + intent.getExtras().getString(KEY_NOTIFICATION_ID));
                        }
                        int count = Settings.getCountAlerts();
                        Settings.setCountAlerts(count + 1);
                        ShortcutBadger.with(getApplicationContext()).count(count+1);

                        showNotification(intent);

                    }
            }
            } else {
                String data = intent.getExtras().getString("data");
                if (data != null) {
                    if (data.contains("device")) {
                        Settings.setUpdatingDevices(false);
                    } else if (data.contains("wedge")) {
                        Settings.setUpdatinWedge(false);
                    } else if (data.contains("content_profile")) {
                        Settings.setNeedUpdateContentProfile(true);
                    }
                    Settings.setUpdatingDevices(false);
                    Log.d("grch","need update deviceOwners");
                    Settings.setNeedUpdateDeviceOwners(true);
                }
            }
        }
    }

    private void showNotification(Intent data) {

        NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = MAIN_NOTIFICATION_ID;
        String message;
        int notificationCount = Settings.getNotificationCount();
        // show single notification for one message or single for multiple
        if (notificationCount > 0) {
            if (notificationCount == 1) nm.cancelAll();

            message = getString(R.string.notifications_multiple);
            Settings.saveNotificationCount(++notificationCount);
        } else {
            String nId = data.getStringExtra(KEY_NOTIFICATION_ID);
            if (nId != null) {
                try {
                    notificationId = Integer.parseInt(nId);
                    Settings.saveNotificationCount(1);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            message = data.getExtras().getString("message");
        }

        PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), notificationId, createAlertsIntent(), 0);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setVibrate(new long[] { 1000, 1000})
                .setSound(android.provider.Settings.System.DEFAULT_NOTIFICATION_URI)
                .setSmallIcon(R.drawable.logo_notif)
                .setAutoCancel(true)
                .setContentTitle("HomeHalo")
                .setContentText(message)
                .setContentIntent(pi)
                .setDeleteIntent(createDeleteIntent());

        nm.notify(notificationId, builder.build());
    }

    private static Intent createAlertsIntent() {
        Intent pushIntent = new Intent(NotificationClickReceiver.ACTION_ACTIVITY);
        return pushIntent;
    }

    private PendingIntent createDeleteIntent() {
        Intent intent = new Intent(NotificationClickReceiver.ACTION_DISMISS);
        PendingIntent deletePi = PendingIntent.getBroadcast(getApplicationContext(), REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        return deletePi;
    }

    public static boolean isVisible(int id, Context ctx) {
        return null != PendingIntent.getBroadcast(ctx, id, createAlertsIntent(), PendingIntent.FLAG_NO_CREATE);
    }

    public static void deleteIfVisible(int id, Context ctx) {
        if (isVisible(id, ctx)) {
            NotificationManager nm = (NotificationManager)ctx.getSystemService(Context.NOTIFICATION_SERVICE);
            nm.cancel(id);
            Settings.saveNotificationCount(0);
        }
    }
}
