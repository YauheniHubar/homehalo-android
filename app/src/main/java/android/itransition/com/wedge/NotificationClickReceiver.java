package android.itransition.com.wedge;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.itransition.com.wedge.activity.AlertsActivity;
import android.itransition.com.wedge.settings.Settings;

/**
 * Created by y.drobysh on 29.01.2015.
 */
public class NotificationClickReceiver extends BroadcastReceiver {


    public static final String ACTION_ACTIVITY = "android.itransition.com.wedge.show_activity";
    public static final String ACTION_DISMISS = "android.itransition.com.wedge.delete_notification";


    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case ACTION_DISMISS:
                Settings.saveNotificationCount(0);
                break;
            case ACTION_ACTIVITY:
                Settings.saveNotificationCount(0);
                Intent alertsActivity = new Intent(context, AlertsActivity.class);
                alertsActivity.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                | Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(alertsActivity);
                break;
        }

    }
}
