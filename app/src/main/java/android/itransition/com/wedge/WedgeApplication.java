package android.itransition.com.wedge;

import android.app.Application;
import android.content.Context;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.User;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.splunk.mint.Mint;

import net.danlew.android.joda.JodaTimeAndroid;

import io.branch.referral.Branch;
import roboguice.util.temp.Ln;

/**
 * Created by e.kazimirova on 02.09.2014.
 */
public class WedgeApplication extends Application {

    private static WedgeApplication mInstance;

    public static MainSource mainSource;

    public static User currentUser;

    public static Tracker tracker;

    public static long lastOnStop;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate(){
        super.onCreate();

        Settings.setsPrefs(this);
        Settings.setXSecretToken("2622a9c8314df4eff283969fedbb5ba42b52bd6253591c447d93019e90a8e1a0d5748e892ed5f97ae5c2e1a3a1601f7312f70a41b745d5aa4348313d285bdf88");

        MainSource.init(this);
        mainSource = MainSource.getInstance();

        Utils.init(this);
        Mint.initAndStartSession(this, "f79fffab");
//        Mint.enableLogging(true);
//        Mint.setLogging("*:W");
        Ln.getConfig().setLoggingLevel(Log.INFO);
        Branch.getAutoInstance(this);
        FacebookSdk.sdkInitialize(getApplicationContext());

        if (BuildConfig.ANALYTICS_ENABLED) {
            GoogleAnalytics.getInstance(this).enableAutoActivityReports(this);

            getTracker(); //enable analytics by calling getTracker ??

            GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
            GoogleAnalytics.getInstance(this).enableAutoActivityReports(this);
        }

        lastOnStop = System.currentTimeMillis();
        JodaTimeAndroid.init(this);
    }

    public static synchronized WedgeApplication getInstance() {
        return mInstance;
    }

    public synchronized Tracker getTracker() {
        if (tracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.enableAutoActivityReports(this);
            if (!Settings.isDemoMode())
            tracker = analytics.newTracker(R.xml.global_tracker);
            else
            tracker = analytics.newTracker(R.xml.global_tracker_demo);
        }
        return tracker;
    }

    public synchronized void changeTrackerMode() {
        if (tracker != null && Settings.isDemoMode()) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.enableAutoActivityReports(this);
            tracker = analytics.newTracker(R.xml.global_tracker_demo);
        }
    }

    public void SendEventGoogleAnalytics(Context iCtx, String iCategoryId, String iActionId, String iLabelId) {

        tracker = getTracker();

        // Build and send an Event.
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(iCategoryId)
                .setAction(iActionId)
                .setLabel(iLabelId)
                .build());

    }

}
