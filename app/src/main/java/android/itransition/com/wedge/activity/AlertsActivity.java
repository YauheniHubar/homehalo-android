package android.itransition.com.wedge.activity;

import android.content.Context;
import android.content.Intent;
import android.itransition.com.wedge.GCMIntentService;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.NotificationsEntity;
import android.itransition.com.wedge.entity.TimeExtensions;
import android.itransition.com.wedge.gui.adapters.AlertsAdapter;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.NotificationModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.NotificationRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.UpdateAlertRequest;
import android.itransition.com.wedge.request.listeners.NotificationRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by e.kazimirova on 29.09.2014.
 */
public class AlertsActivity extends BaseActivity implements RequestAction, View.OnClickListener, AdapterView.OnItemClickListener {


    public static final int REQUEST_ASSIGN = 66;
    public static final int REQUEST_EXTEND = 99;
    public static final String NOTIFICATION_EXTRA = "NOTIFICATION";
    public static final int DECLINE_ALERT = 33;
    public static final int ACCEPT_ALERT = 44;
    public static final int IGNORE_ALERT = 55;
    public static final int CLEAR_ALERT = 66;
    public static final int REQUEST_WHITE_LIST = 77;
    public static final String ALERT_RESULT = "ALERT_RESULT";
//    private PullToRefreshListView listView;
    private SwipeRefreshLayout swipeListView;
    private ListView listView;
    private NotificationsEntity[] notifications;
    private AlertsAdapter adapter;
    private NotificationsEntity processedNotification;
    private TextView emptyView;
    private boolean needDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerts);

        setupActionBar();
        setTitle(getString(R.string.alerts));
        setNeedHome(true);
        //setBarColor(getResources().getColor(R.color.alerts_orange));

        LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        emptyView = inflater.inflate(R.layout.no_items, null);
//        emptyView.setMinimumHeight(120);
        emptyView = (TextView) findViewById(R.id.no_alerts);
//        TextView tvName = (TextView) emptyView.findViewById(R.id.textView);
//        tvName.setText("You have no alerts");

        swipeListView = (SwipeRefreshLayout) findViewById(R.id.listViewUserSwipe);
        listView = (ListView) findViewById(R.id.listViewUser);
        listView.setEmptyView(emptyView);
        swipeListView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                performGetAlerts();
            }
        });
        listView.setOnItemClickListener(this);


        performGetAlerts();
        if (savedInstanceState != null) {
            processedNotification = savedInstanceState.getParcelable(Consts.NOTIFICATION_ENTRY);
        }
    }

    private void performGetAlerts(){
        NotificationRequest request = new NotificationRequest(this, Settings.getUserId());
        contentManager.execute(request, new NotificationRequestListener(this, this, BaseRequest.HttpMethodEnum.get,
                request.getUrl(), request));
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        notifications = ((NotificationModel) model).getNotifications();
        Settings.setCountAlerts(notifications.length);
        swipeListView.setRefreshing(false);
        updateList();
        if (needDelete) deleteAlert();
    }

    private void updateList() {
        adapter = new AlertsAdapter(this, notifications);
        listView.setAdapter(adapter);
        updateEmptyText();
    }

    private void updateEmptyText() {
//        if (adapter == null || adapter.getCount() == 0) {
//            emptyView.setVisibility(View.VISIBLE);
//            if (listView.getHeaderViewsCount() == 1) listView.addHeaderView(emptyView);
//        } else if (listView.getHeaderViewsCount() > 1) {
//            listView.removeHeaderView(emptyView);
//        }
        if (adapter == null || adapter.getCount() == 0) {
            findViewById(R.id.no_alerts).setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void performFailRequestAction(int action) {
        swipeListView.setRefreshing(false);
        if (adapter != null) adapter.clear();
        updateEmptyText();
     }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewHome:
            case R.id.textViewBack:
            case R.id.textViewMain:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (processedNotification != null)
            outState.putParcelable(Consts.NOTIFICATION_ENTRY, processedNotification);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && processedNotification != null) {
            if (requestCode == REQUEST_ASSIGN || requestCode == REQUEST_EXTEND) {
                updateNotification(processedNotification);
                needDelete = true;
                deleteAlert();
            } else if (requestCode == REQUEST_WHITE_LIST) {
                int result = data.getIntExtra(ALERT_RESULT, 0);
                NotificationsEntity item = data.getParcelableExtra(NOTIFICATION_EXTRA);
                switch (result) {
                    case DECLINE_ALERT:
                        updateNotification(item);
                        deleteAlert();
                        break;
                    case ACCEPT_ALERT:
                        deleteAlert();
                        updateNotification(item);
                        break;
                    case IGNORE_ALERT:
                        break;
                    case CLEAR_ALERT:
                        updateNotification(item);
                        deleteAlert();
                        break;
                }
            }
        }

    }

    private void updateNotification(NotificationsEntity item) {
        UpdateAlertRequest request = new UpdateAlertRequest(this, Settings.getUserId(),
                item);
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new RequestListener<BaseModel>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) { }
                    @Override
                    public void onRequestSuccess(BaseModel baseModel) { }
                });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position < 0) {
            performGetAlerts();
            return;
        }
        processedNotification = adapter.getItem(position);
        if (processedNotification.getType().trim().toLowerCase().equals("new_device_attached")) {
            Intent intent = new Intent(this, AssignUserActivity.class);
            intent.putExtra(Consts.IS_ALERTS, true);
            intent.putExtra(Consts.CREATED_AT, (Parcelable) processedNotification.getCreatedAt());
            intent.putExtra(Consts.DEVICE_ID, processedNotification.getData().getId());
            intent.putExtra(Consts.DEVICE_TITLE, processedNotification.getData().
                    getTitle());
            intent.putExtra(Consts.MAC_ADDRESS, processedNotification.getData().
                    getMacAddress());
            intent.putExtra(Consts.DEVICE_OWNER_ID, processedNotification.getData().
                    getDataInformationDeviceOwner().getId());
            intent.putExtra(Consts.MANUFACTURER_TITLE, processedNotification.getData().
                    getManufacturerTitle());

            startActivityForResult(intent, REQUEST_ASSIGN);
            adapter.notifyDataSetChanged();
        } else if (processedNotification.getType().trim().toLowerCase().equals("whitelist_request")) {
            addUrl(processedNotification);
        } else if (processedNotification.getType().trim().toLowerCase().equals("time_extension_request")) {
            TimeExtensions extensions = new TimeExtensions();
            extensions.setDuration(processedNotification.getData().getDuration());
            extensions.setId(processedNotification.getData().getId());
            Intent intent = new Intent();
            intent.putExtra(Consts.IS_ALERTS, true);
            intent.putExtra(Consts.TIME_EXTENSION, (Parcelable) extensions);
            intent.putExtra(Consts.DEVICE_OWNER_ID, (long) processedNotification.getData().getDataInformationDeviceOwner().getId());
            intent.putExtra(Consts.DEVICE_TITLE, processedNotification.getData().getDataInformationDeviceOwner().getTitle());
            intent.setClass(this, ExtendTimeActivity.class);
            startActivityForResult(intent, REQUEST_EXTEND);
        }
    }

    private void deleteAlert() {
        if (adapter != null && processedNotification != null) {
            adapter.removeAlert(processedNotification);
            Settings.setCountAlerts(adapter.getCount());
            adapter.notifyDataSetChanged();
            updateEmptyText();
            needDelete = false;


            GCMIntentService.deleteIfVisible((int) processedNotification.getId(), getApplicationContext());

            processedNotification = null;

            if (adapter.getCount() == 0) {
                listView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 300);
            }
        }
    }

    private void startAlertListRequestActivity(NotificationsEntity item){
        Intent intent = new Intent(this, WhiteListRequestActivity.class);
        intent.putExtra(NOTIFICATION_EXTRA, (Parcelable)item);
        startActivityForResult(intent, REQUEST_WHITE_LIST);
    }

    public void addUrl(final NotificationsEntity item) {

        startAlertListRequestActivity(item);
/*
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getString(R.string.accept));
        alert.setMessage("Do you " +
                "want to add " + item.getData().getUrl() + " to " + item.getData().getDataInformationDeviceOwner().getTitle() +
                "'s whitelist?");

        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, int whichButton) {
                String url = item.getData().getUrl();
                long idOwner = item.getData().getDataInformationDeviceOwner().getId();
                long id = item.getData().getId();

                UpdateUrlRequest request = new UpdateUrlRequest(AlertsActivity.this, idOwner, id, url);
                contentManager.execute(request,
                        new BaseRequestListener<WhiteUrlModel>(AlertsActivity.this, request) {
                            @Override public void onRequestSuccess(WhiteUrlModel baseModel) {
                                deleteAlert();
                                super.onRequestSuccess(baseModel);
                                Utils.makeToast("Yoa accepted url");
                                updateNotification(item);
                                dialog.dismiss();
                            }
                        });
            }
        });

        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                updateNotification(item);
                dialog.dismiss();
                deleteAlert();
                adapter.notifyDataSetChanged();
            }
        });
        alert.setNeutralButton(getString(R.string.alerts_btn_preview), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String url = item.getData().getUrl();
                if (!url.startsWith("http://"))
                    url = "http://" + url;

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        });
        alert.show();
        */
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        performGetAlerts();
    }
}
