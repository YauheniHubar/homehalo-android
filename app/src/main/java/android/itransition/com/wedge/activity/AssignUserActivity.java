package android.itransition.com.wedge.activity;

import android.app.Activity;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceDataSource;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.DevicesEntity;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.DeviceOwnersRequest;
import android.itransition.com.wedge.request.DeviceUpdateRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.listeners.DeviceOwnersRequestListener;
import android.itransition.com.wedge.request.listeners.DeviceRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 19.11.2014.
 */
public class AssignUserActivity extends BaseActivity implements RequestAction, View.OnClickListener {

    private ListView listView;
    private BaseActivity activity;
    private Button btnAssign;
    private DeviceOwnersDataSource datasource;
    private TimeProfiles[] time;
    HomeWorkCategories[] homeworkCategories;
    private long deviceId;
    private DeviceDataSource devicesDataSource;

    private List<DeviceOwner> owners;
    private int position = -1;

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign);


        deviceId = getIntent().getExtras().getLong(Consts.DEVICE_ID);

        btnAssign = (Button) findViewById(R.id.buttonSave);
        btnAssign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (position != -1) {
                    assignDevice();
                }
                else if (getIntent().getExtras().getBoolean(Consts.IS_ALERTS)) {
                    position = getOwner(owners);
                    onAssignSuccess();
                }
            }
        });


        listView = (ListView) findViewById(R.id.listView);
        activity = this;

        setInit();

        setupActionBar();
        //setBarColor(getResources().getColor(R.color.main_gray));
        setNeedHome(true);
        setTitle(getString(R.string.assign_device));

        datasource = new DeviceOwnersDataSource(this, WedgeApplication.mainSource);
        devicesDataSource = new DeviceDataSource(WedgeApplication.mainSource);

        String title = getIntent().getExtras().getString(Consts.DEVICE_TITLE);
        String mac = getIntent().getExtras().getString(Consts.MAC_ADDRESS);
        String mfr = getIntent().getExtras().getString(Consts.MANUFACTURER_TITLE);
        if (devicesDataSource != null) {
//            String mac = "n/a";
//            String mfr = "n/a";
//            if (devicesDataSource.getDeviceById(deviceId).getManufacturedTitle()!=null) {
//                mfr = devicesDataSource.getDeviceById(deviceId).getManufacturedTitle();
//            }
//            if (devicesDataSource.getDeviceById(deviceId).getMacAddress()!=null) {
//                mac = devicesDataSource.getDeviceById(deviceId).getMacAddress();
//            }
            StringBuilder header = new StringBuilder();
            header.append("<b>" + title + "</b>");
            if (mfr != null) {
                header.append("<br/>mfr: " + mfr);
            } else {
                header.append("<br/>mfr: n/a");
            }
            if (mac != null) {
                header.append("<br/>mac: " + mac);
            }
//        ((TextView)findViewById(R.id.textView)).setText(title+"\nsadasd");
            ((TextView) findViewById(R.id.textView)).setText(Html.fromHtml(header.toString()));
        } else {
            Log.d("","");
        }
        boolean b = Settings.isNeedUpdateDeviceOwner();
        if(Settings.isNeedUpdateDeviceOwner()) {
            DeviceOwnersRequest request = new DeviceOwnersRequest(this);
            contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                    new DeviceOwnersRequestListener(this, (RequestAction) this, BaseRequest.HttpMethodEnum.get,
                            request.getUrl(), request));
            datasource.deleteDeviceOwnerAll();
        } else {
            owners = datasource.getAllDeviceOwners();

            updateList(owners);
        }
//        datasource.close();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CheckedTextView c = (CheckedTextView) view;
                c.setChecked(true);
                    listView.setItemChecked(i, true);
                    position = i;

            }
        });
    }

    private void assignDevice() {
        long newOwnerId = owners.get(position).getId();
        long currentOwnerId = getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID);
//        if (newOwnerId == currentOwnerId) {
//            return;
//        }
        DeviceUpdateRequest request = new DeviceUpdateRequest(activity, Settings.getUserId(),
                deviceId,
                getIntent().getExtras().getString(Consts.DEVICE_TITLE),
                true, false, true, getIntent().getExtras().getString(Consts.MAC_ADDRESS),
                (CreateTimeUser)getIntent().getExtras().getParcelable(Consts.CREATED_AT),
                newOwnerId);
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new DeviceRequestListener(activity,  (RequestAction)activity, BaseRequest.HttpMethodEnum.put,
                        request.getUrl(), request));
    }

    private void updateList(List<DeviceOwner> owners) {
        ArrayList<String> values = new ArrayList<String>();
        for (int i = 0; i < owners.size(); i++) {
            values.add(owners.get(i).getTitle());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                R.layout.choice_item,
                values);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        int ownerPos = getOwner(owners);
        if (ownerPos != -1 ) {
            listView.setItemChecked(ownerPos, true);
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()){
            case R.id.imageViewHome:
                onBackPressed();
                break;
            case R.id.textViewBack:
                onBackPressed();
                break;
            case R.id.textViewMain:
                onBackPressed();
                break;
            case R.id.imageViewUpdate:
                finish();
                intent.setClass(activity, ManagementActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private void setInit() {
        if(!getIntent().getExtras().getBoolean(Consts.IS_ALERTS)) {
            time = new TimeProfiles[0];
            homeworkCategories = new HomeWorkCategories[0];
            if (getIntent() != null) {
                Bundle bundle = getIntent().getExtras();
                Parcelable[] tv = (Parcelable[]) bundle.getParcelableArray(Consts.TIME_PROFILESES);
                if (tv != null) {
                    time = new TimeProfiles[tv.length];
                    for (int i = 0; i < tv.length; i++) {
                        time[i] = (TimeProfiles) tv[i];
                    }
                }

                tv = (Parcelable[]) bundle.getParcelableArray(Consts.HOMEWORK_CATEGORIES);

                if (tv != null) {
                    homeworkCategories = new HomeWorkCategories[tv.length];
                    for (int i = 0; i < tv.length; i++) {
                        homeworkCategories[i] = (HomeWorkCategories) tv[i];
                    }
                }
            }
        }


    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        if (owners != null) {
            onAssignSuccess();
        } else {
            owners = (((DeviceOwnersModel) model).getDeviceOwners());

            updateList(owners);
        }
    }

    private void onAssignSuccess() {
        Utils.makeToast(R.string.success);
        Intent data = new Intent();
        data.putExtra(Consts.DEVICE_OWNER_ID, owners.get(position).getId());
        data.putExtra(Consts.OWNER_NAME, owners.get(position).getTitle());
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    private int getOwner(List<DeviceOwner> owners) {
        DevicesEntity device = devicesDataSource.getDeviceById(deviceId);
        if (device != null && device.getDeviceOwner() != null) {
            long ownerId = device.getDeviceOwner().getId();
            for (int i = 0; i < owners.size(); i++) {
                DeviceOwner owner = owners.get(i);
                if (owner.getId() == ownerId) {
                    return i;
                }
            }
        }

       return -1;
    }

    @Override
    public void performFailRequestAction(int action) {

    }
}
