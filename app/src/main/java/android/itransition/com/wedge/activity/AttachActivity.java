package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.request.AttachDeviceRequest;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by e.kazimirova on 17.10.2014.
 */
public class AttachActivity extends BaseActivity implements View.OnClickListener {

    private Button btnAttach;
    private EditText editTextId;

    @Override
    protected void onStart(){
        super.onStart();

    }



    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attach);

        btnAttach = (Button) findViewById(R.id.buttonAttach);
        editTextId = (EditText) findViewById(R.id.editTextId);

        setupActionBar();

        btnAttach.setOnClickListener(this);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), ManagementActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAttach:
                AttachDeviceRequest request = new AttachDeviceRequest(getApplicationContext(),
                        Settings.getUserId(),
                        editTextId.getText().toString());
                contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                        new RequestListener<BaseModel>() {
                            @Override
                            public void onRequestFailure(SpiceException spiceException) {
                                Toast.makeText(getApplicationContext(), R.string.invalid_id,
                                        Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onRequestSuccess(BaseModel baseModel) {
                                Toast.makeText(getApplicationContext(), R.string.succssesful_attach,
                                        Toast.LENGTH_LONG).show();
                                Settings.setWedge(true);
                                finish();
                                Intent intent = new Intent();
                                intent.setClass(getApplicationContext(), ManagementActivity.class);
                                startActivity(intent);
                            }
                        });
                break;
            case R.id.imageViewUpdate:
                onBackPressed();
                break;
            case R.id.imageViewHome:
                onBackPressed();
                break;
            case R.id.textViewBack:
                onBackPressed();
                break;
            case R.id.textViewMain:
                onBackPressed();
            default:
                break;
        }
    }
}
