package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.itransition.com.wedge.BuildConfig;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.robospice.ApiService;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.octo.android.robospice.SpiceManager;

import java.util.Calendar;

/**
 * Created by e.kazimirova on 29.08.2014.
 */
public abstract class BaseActivity  extends ActionBarActivity {

    public SpiceManager contentManager = new SpiceManager(ApiService.class);

//    private static final int INTERVAL_IN_BACKGROUND = 5 * 60 * 1000;
    private static final int INTERVAL_IN_BACKGROUND = 1000;

    boolean isSecure = true;

    public void setNeedHome(boolean needHome) {
        this.needHome = needHome;
    }


    private TextView tvTitle;
    private boolean needHome;

    public TextView getTitleView() {return tvTitle;}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("onStart", this.getClass().getSimpleName());
        contentManager.start(this);
        if (BuildConfig.ANALYTICS_ENABLED) {
            GoogleAnalytics.getInstance(this).reportActivityStart(this);
        }


        if (needHome) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (isSecure && !Settings.isDemoMode() && (System.currentTimeMillis() - WedgeApplication.lastOnStop) > INTERVAL_IN_BACKGROUND) {
            Intent intent = new Intent(this, PinActivity.class);
            intent.putExtra(Consts.PIN_FROM_BACKGROUND, true);
            startActivity(intent);
        } else if (isSecure && Settings.isDemoMode() && (System.currentTimeMillis() - WedgeApplication.lastOnStop) > INTERVAL_IN_BACKGROUND) {
            Intent intent = new Intent(this, ManagementActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

    }



    @Override
    protected void onPause(){
        super.onPause();
        Settings.setDate(Calendar.getInstance().getTime().getTime());
        Log.d("onPause", this.getClass().getSimpleName());

        WedgeApplication.lastOnStop = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        contentManager.shouldStop();

        if (BuildConfig.ANALYTICS_ENABLED) {
            GoogleAnalytics.getInstance(this).reportActivityStop(this);
        }

        Log.d("onStop", this.getClass().getSimpleName());
    }


    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public void setTitle(int title) {
        tvTitle.setText(title);
    }

    public void setBarColor(int color) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(color));
    }

    protected ActionBar setupActionBar() {

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.actionbar_custom_title_centered);
        actionBar.setHomeAsUpIndicator(R.drawable.back_button_arrow);
        setBarColor(getResources().getColor(R.color.bar_color));
        View customView = actionBar.getCustomView();
        tvTitle = (TextView) customView.findViewById(R.id.title);

        setNeedHome(true);

        return actionBar;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (needHome) getMenuInflater().inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        if (item.getItemId() == R.id.action_home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
//                getSupportFragmentManager().popBackStack();
                Intent intent = new Intent(this, ManagementActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
//                finish();
                Intent intent = new Intent(this, ManagementActivity.class);
                intent.putExtra("openMain",true);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }


//    @Override
//    protected void onResume() {
//        Intent intent = new Intent(this, ManagementActivity.class);
//        intent.putExtra("openMain",true);
//        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
//        super.onResume();
//    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
            return;
        }
        super.onBackPressed();
    }
}
