package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.graphics.Color;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.database.datesource.UrlListDataSource;
import android.itransition.com.wedge.entity.UrlListEntity;
import android.itransition.com.wedge.gui.adapters.BlackListAdapter;
import android.itransition.com.wedge.gui.com.baoyz.swipemenulistview.SwipeMenu;
import android.itransition.com.wedge.gui.com.baoyz.swipemenulistview.SwipeMenuCreator;
import android.itransition.com.wedge.gui.com.baoyz.swipemenulistview.SwipeMenuItem;
import android.itransition.com.wedge.gui.com.baoyz.swipemenulistview.SwipeMenuListView;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.BlackUrlModel;
import android.itransition.com.wedge.model.WhiteUrlModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.urllists.AddUrlRequest;
import android.itransition.com.wedge.request.urllists.BlackListRequest;
import android.itransition.com.wedge.request.urllists.DeleteUrlRequest;
import android.itransition.com.wedge.request.urllists.UpdateUrlRequest;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.itransition.com.wedge.utils.Utils.dp2px;


public class BlackListActivity extends BaseActivity implements View.OnClickListener, BlackListAdapter.EditUrlListener {

    private List<UrlListEntity> urlList;
    private List<UrlListEntity> altUrlList; //another urlList (black or white) to found duplicates
    public static SwipeMenuListView listViewUrls;
    private BlackListAdapter adapter;
    private ImageView btnAdd;
    private EditText editTextUrl;
    private TextView empty;
    private long deviceOwnerId;
    private UrlListEntity currentListItem;
    private boolean isWhite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black_list);

        btnAdd = (ImageView) findViewById(R.id.buttonAdd);
        editTextUrl = (EditText) findViewById(R.id.editTextUrl);

        btnAdd.setOnClickListener(this);

        setupActionBar();

        Intent intent = getIntent();

        isWhite = intent.getBooleanExtra(Consts.IS_WHITE_LIST, false);
        deviceOwnerId = intent.getLongExtra(Consts.DEVICE_OWNER_ID, -1);
        setTitle(intent.getStringExtra(Consts.DEVICE_TITLE));

        listViewUrls = (SwipeMenuListView) findViewById(android.R.id.list);
        empty = (TextView) findViewById(R.id.tvNoUrls);
        setupListView();
        editTextUrl.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    String url = editTextUrl.getText().toString();
                    if (isValidUrl(url)) addUrl(url);
                }
                return false;
            }
        });
        getUrlList();
    }

    private void styleSwipeMenuItem(SwipeMenuItem item, String title, int resourceBack) {
        item.setWidth(dp2px(50, this));
        item.setBackground(resourceBack);
        item.setTitle(title);
        item.setTitleColor(Color.WHITE);
        item.setTitleSize(14);
    }

    private void setupListView() {
        listViewUrls.setEmptyView(empty);
        listViewUrls.setMenuCreator(new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                switch (menu.getViewType()) {
                    case 0:
                        SwipeMenuItem assign = new SwipeMenuItem(getApplicationContext());
                        styleSwipeMenuItem(assign, getString(R.string.edit), R.color.green);
                        menu.addMenuItem(assign);
                        SwipeMenuItem edit = new SwipeMenuItem(getApplicationContext());
                        styleSwipeMenuItem(edit, getString(R.string.delete), R.color.red_for_edit);
                        menu.addMenuItem(edit);
                        break;
                }
            }
        });
        listViewUrls.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                currentListItem = adapter.getItem(position);
                switch (index) {
                    case 1:
                        onDeleteClick();
                        break;
                    case 0:
                        currentListItem.inEditMode = true;
                        adapter.notifyDataSetChanged();
//                        View v = adapter.getView(position, null, null);
//                        EditText et = (EditText)v.findViewById(R.id.etUrl);
//                        et.requestFocus();
//                        et.setSelection(4);
//                        Log.d("grch12","length:"+et.getText());
                        break;
                }
                return false;
            }
        });
        listViewUrls.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override public void onSwipeStart(int position) {
                if (currentListItem != null && currentListItem.inEditMode) {
                    Utils.hideSoftKeyboard(BlackListActivity.this, editTextUrl);
                    currentListItem.inEditMode = false;

                }
                adapter.notifyDataSetChanged();
            }
            @Override public void onSwipeEnd(int position) {
                adapter.notifyDataSetChanged();}
        });

        listViewUrls.setLongClickable(true);

    }


    @Override
    public void onCancel() {
        currentListItem.inEditMode = false;
        adapter.notifyDataSetChanged();
    }

    private void onDeleteClick() {
//        AlertDialog.Builder alert = new AlertDialog.Builder(this);
//        alert.setTitle(R.string.delete);
//        alert.setMessage(isWhite ? R.string.blacklist_sure_del_white : R.string.blacklist_sure_del_black);
//        alert.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                deleteUrlRequest();
//                dialog.dismiss();
//            }
//        });
//        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                dialog.dismiss();
//            }
//        });
//        alert.show();
        deleteUrlRequest();
    }

    private void getUrlList() {
        if (deviceOwnerId > 0) {
            UrlListDataSource source = new UrlListDataSource();
            urlList = source.getUrlListByOwnerId(deviceOwnerId, !isWhite);
//            altUrlList = source.getUrlListByOwnerId(deviceOwnerId, isWhite);
            updateList();
        }
    }

    private void updateList() {
        sortList();
        adapter = new BlackListAdapter(this, urlList, this);
        listViewUrls.setAdapter(adapter);
    }

    private void sortList() {
        Collections.sort(
                urlList,
                new Comparator<UrlListEntity>() {
                    public int compare(UrlListEntity lhs, UrlListEntity rhs) {
                        if (lhs.getUrl() == null) return -1;
                        if (rhs.getUrl() == null) return 1;
                        return lhs.getUrl().compareTo(rhs.getUrl());
                    }
                }
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewUpdate:
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), ManagementActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.imageViewHome:
                onBackPressed();
                break;
            case R.id.buttonAdd:
                String url = editTextUrl.getText().toString();
                if (isValidUrl(url)) addUrl(url);
                break;
            case R.id.textViewBack:
                onBackPressed();
                break;
        }
    }

    private boolean isValidUrl(String url) {
        if(url.length() > 5) {
            if (url.startsWith("http://")) url = url.substring(7);
            else if (url.startsWith("https://")) url = url.substring(8);
            if (url.startsWith("www.")) url = url.substring(4);
            if (!url.matches(".+\\..{2,}")) {
                Utils.makeToast(R.string.invalid_web_address);
                return false;
            }
            for (int i = 0; i < urlList.size(); i++) {
                if (urlList.get(i).getUrl().trim().toLowerCase().equals(url.toLowerCase().trim())) {
                    if (isWhite)
                        Utils.makeToast(R.string.blacklist_already_white);
                    else Utils.makeToast(R.string.blacklist_already_black);
                    return false;
                }
            }
            return true;
        } else {
            Utils.makeToast(R.string.invalid_web_address);
        }
        return false;
    }


    public void addUrl(final String urlBase){
        if (isWhite) {
            createWhiteUrl(urlBase);
        } else {
            createBlackUrl(urlBase);
        }
        Utils.hideSoftKeyboard(this, editTextUrl);
    }

    private void createWhiteUrl(String urlBase) {
        AddUrlRequest request = new AddUrlRequest(this, deviceOwnerId, urlBase);
        contentManager.execute(request,
                    new BaseRequestListener<WhiteUrlModel>(this,  request) {
                        @Override
                        public void onRequestSuccess(WhiteUrlModel baseModel) {
                            Utils.makeToast(R.string.success);
                            super.onRequestSuccess(baseModel);
                            if (baseModel.isSuccess() && baseModel.whiteUrl != null) {
                                urlList.add(baseModel.whiteUrl);
                                sortList();
                                adapter.notifyDataSetChanged();
                                editTextUrl.setText("");
                                Utils.hideSoftKeyboard(BlackListActivity.this, editTextUrl);
                            }
                        }
                    });
    }

    private void createBlackUrl(String urlBase) {
        BaseRequest request;
        UrlListEntity url = new UrlListEntity();
        url.setUrl(urlBase);
        url.setDeviceOwner(deviceOwnerId);
        request = new BlackListRequest(this, BaseRequest.HttpMethodEnum.post, url);
        contentManager.execute(request,
                new BaseRequestListener<BlackUrlModel>(this, request) {
                    @Override
                    public void onRequestSuccess(BlackUrlModel baseModel) {
//                        Utils.makeToast(R.string.success);
                        super.onRequestSuccess(baseModel);
                        if (baseModel.isSuccess() && baseModel.urlBlacklist != null) {
                            urlList.add(baseModel.urlBlacklist);
                            sortList();
                            adapter.notifyDataSetChanged();
                            editTextUrl.setText("");
                            Utils.hideSoftKeyboard(BlackListActivity.this, editTextUrl);
                        }
                    }
                });
    }

    private void deleteUrlRequest() {
        BaseRequest request;
        if (isWhite) {
            request = new DeleteUrlRequest(this, currentListItem.getDeviceOwner(), currentListItem.getId());
        } else {
            request = new BlackListRequest(this, BaseRequest.HttpMethodEnum.delete, currentListItem);
        }
        contentManager.execute(request,
                new BaseRequestListener<BaseModel>(this, request) {
                    @Override public void onRequestSuccess(BaseModel result) {
                        super.onRequestSuccess(result);
//                        Utils.makeToast(R.string.black_list_deleted);
                        urlList.remove(currentListItem);
                        currentListItem = null;
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    @Override
    public void onEditUrl(final String url) {
        if (!isValidUrl(url)) return;

        final String oldUrl = currentListItem.getUrl();
        currentListItem.setUrl(url);

        BaseRequest request;;
        if (isWhite) {
            request = new UpdateUrlRequest(this, deviceOwnerId, currentListItem.getId() ,url, Consts.STATUS_ACCEPTED);
        } else {
            request = new BlackListRequest(this, BaseRequest.HttpMethodEnum.put, currentListItem);
        }

        contentManager.execute(request,
                new BaseRequestListener<BaseModel>(this, request) {
                    @Override public void onRequestFailure(SpiceException e) {
                        super.onRequestFailure(e);
                        if (currentListItem != null) {
                            currentListItem.setUrl(oldUrl);
                        }
                    }
                    @Override public void onRequestSuccess(BaseModel baseModel) {
//                        Utils.makeToast(R.string.blacklist_url_updated);
                        super.onRequestSuccess(baseModel);
                        if (currentListItem != null) {
                            currentListItem.inEditMode = false;
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
        Utils.hideSoftKeyboard(BlackListActivity.this, editTextUrl);
    }
}
