package android.itransition.com.wedge.activity;

import android.content.Context;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.gui.fragments.ManualBlacklistFragment;
import android.itransition.com.wedge.gui.fragments.PopularBlacklistFragment;
import android.itransition.com.wedge.settings.Consts;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

/**
 * Created by i.grechishchev on 09.02.2016.
 * itransition 2016
 */
public class BlacklistActivityTabs extends BaseActivity implements View.OnClickListener, TabHost.OnTabChangeListener {
    private TabHost mTabHost;
    private TabHost tabHostFilters;
    private TabWidget tabWidget;
    private int colorBlue;
    private String title;

    private long mDeviceOwnerId;
    private boolean isWhite;

    private final String TABS_TAG_1 = "tab1";
    private final String TABS_TAG_2 = "tab2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_blacklist_tabs);

        setupActionBar();
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabWidget = (TabWidget) findViewById(android.R.id.tabs);

        Intent intent = getIntent();

        DeviceOwner deviceOwner = intent.getParcelableExtra("DO");
        isWhite = intent.getBooleanExtra(Consts.IS_WHITE_LIST, false);
        title = intent.getStringExtra(Consts.OWNER_NAME);
        setTitle(title != null ? title : "user");
        mDeviceOwnerId = intent.getLongExtra(Consts.DEVICE_OWNER_ID, 0);
        mTabHost.setup();


        /** Defining tab builder for Visited tab */
        TabHost.TabSpec tSpecVisited = mTabHost.newTabSpec("popular");
        tSpecVisited.setIndicator(getString(R.string.popular));
        tSpecVisited.setContent(new DummyTabContent(getBaseContext()));
        mTabHost.addTab(tSpecVisited);

        /** Defining tab builder for Usage tab */
        TabHost.TabSpec tSpecUsage = mTabHost.newTabSpec("manual");
        tSpecUsage.setIndicator(getString(R.string.Manual));
        tSpecUsage.setContent(new DummyTabContent(getBaseContext()));
        mTabHost.addTab(tSpecUsage);

        mTabHost.setOnTabChangedListener(this);
        mTabHost.setCurrentTabByTag("popular");
        this.onTabChanged("popular");
        initTabsAppearance(tabWidget);

    }

    private void initTabsAppearance(TabWidget tabWidget) {
        // Change background
        for(int i=0; i < tabWidget.getChildCount(); i++) {
            tabWidget.getChildAt(i).setBackgroundResource(R.drawable.tab_brown_selector);
            TextView tv = (TextView) tabWidget.getChildAt(i).findViewById(android.R.id.title);
            tv.setAllCaps(false);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
//            tv.setTextColor(this.getResources().getColorStateList(R.color.reports_blue));
            tv.setTextColor(this.getResources().getColorStateList(R.color.white));
        }
//        tabWidget.getChildTabViewAt(0).setVisibility(View.GONE);
    }

    @Override
    public void onTabChanged(String tabId) {

        Log.d("TAG", "tabId: " + tabId);
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        PopularBlacklistFragment popularFragment = (PopularBlacklistFragment) fm.findFragmentByTag("popular");
        ManualBlacklistFragment manualFragment = (ManualBlacklistFragment) fm.findFragmentByTag("manual");
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment;
        Bundle args = new Bundle();

        if (popularFragment != null)
            ft.detach(popularFragment);

        if (manualFragment != null)
            ft.detach(manualFragment);

        if (tabId.equalsIgnoreCase("popular")) {    /** If current tab is popular */
            if (popularFragment == null) {
                fragment = new PopularBlacklistFragment();
//                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
//                args.putInt("id", radioGroup.getCheckedRadioButtonId());
                args.putLong(Consts.DEVICE_OWNER_ID, mDeviceOwnerId);
                args.putBoolean(Consts.IS_WHITE_LIST, isWhite);
                fragment.setArguments(args);
                ft.add(R.id.realtabcontent, fragment, "popular");
            } else {
                fragment = popularFragment;
//                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
//                fragment.getArguments().putInt("id", radioGroup.getCheckedRadioButtonId());
                ft.attach(fragment);
            }
        }

        /** If current tab is usage */
        else if (tabId.equalsIgnoreCase("manual")) {

            if (manualFragment == null) {
                /** Create fragment and adding to fragmenttransaction */
                fragment = new ManualBlacklistFragment();
//                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
                args.putLong(Consts.DEVICE_OWNER_ID, mDeviceOwnerId);
                args.putBoolean(Consts.IS_WHITE_LIST, isWhite);
                fragment.setArguments(args);
                ft.add(R.id.realtabcontent, fragment, "manual");
            } else {
                /** Bring to the front, if already exists in the fragmenttransaction */
                fragment = manualFragment;
//                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
//                fragment.getArguments().putInt("id", radioGroup.getCheckedRadioButtonId());
                ft.attach(fragment);
            }

        }
        ft.commit();
    }

    @Override
    public void onClick(View v) {

    }

    public class DummyTabContent implements TabHost.TabContentFactory {
        private Context mContext;

        public DummyTabContent(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            return v;
        }
    }
}
