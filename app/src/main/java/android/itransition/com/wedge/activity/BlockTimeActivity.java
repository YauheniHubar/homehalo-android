package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.TimeExtensions;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.gui.CircularSeekBarNew;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.BlockRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.listeners.UpdateDeviceRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by e.kazimirova on 06.10.2014.
 */
public class BlockTimeActivity extends BaseActivity implements View.OnClickListener, RequestAction {

    public static final int DEFAULT_TIME_MIN = 31;
    public static final int TIMER_PERIOD = 15 * 1000;


//    private boolean isBlocking = false;
//    private boolean flag = false;
    private Button buttonBlock, buttonCancel;
    private TimeProfiles[] time;
    HomeWorkCategories[] homeworkCategories;
    private CircularSeekBarNew seekBar;
    TimeExtensions timeExtension;
    private CreateTimeUser blockStartedAt;
    private long timeDuration = 0;
    private Date endAt;
    private TextView tvActive, tvTimeDuration;

    private boolean timerShouldRun;
    private Timer timer;
    private TimerTask updateAdapterTask = new TimerTask() {
        @Override
        public void run() {
            Log.d("Timer", "tick");
            if (timerShouldRun) {
                mHandler.obtainMessage().sendToTarget();
            }
        }
    };

    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (timerShouldRun) {
//                Date now = Utils.getNow(WedgeApplication.currentUser.getWedge().getTimezone()); //old realization
                Date now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
                long remainMin = (endAt.getTime() - now.getTime()) / (60 * 1000);

                updateRemainText(remainMin);
            }
        }
    };
    private boolean isActive;
    private boolean cancelPressed;
    private MainSource mainsource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocktime);
        mainsource = MainSource.getInstance();

        buttonBlock = (Button) findViewById(R.id.buttonBlock);
        buttonCancel  = (Button) findViewById(R.id.buttonCancel);
        seekBar = (CircularSeekBarNew) findViewById(R.id.seekBar);
        tvActive = (TextView) findViewById(R.id.textViewActive);
        tvTimeDuration = ((TextView) findViewById(R.id.tvMins));


        seekBar.setOnSeekBarChangeListener(new CircularSeekBarNew.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBarNew circularSeekBar, int progress, boolean fromUser) {
                updateBlockButtonText(circularSeekBar.getProgressInMins());
                Utils.updateSliderText(BlockTimeActivity.this,circularSeekBar.getProgressInMins(),
                        timeDuration,tvTimeDuration);
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBarNew seekBar) {

            }

            @Override
            public void onStartTrackingTouch(CircularSeekBarNew seekBar) {

            }
        });

//        isBlocking = getIntent().getExtras().getBoolean(Consts.BLOCKED);

        setupActionBar();
        setTitle(getIntent().getExtras().getString(Consts.DEVICE_TITLE));

        parseIntent();
        blockStartedAt = getIntent().getExtras().getParcelable(Consts.BLOCK_STARTED_AT);
        timeDuration = getIntent().getExtras().getLong(Consts.TIME);


        initUi();
    }

    private void initUi() {

        buttonBlock.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);

        tvActive.setVisibility(GONE);
        buttonCancel.setVisibility(GONE);

        if (blockStartedAt == null || timeDuration == 0 || blockStartedAt.getTimeZone() == null) {
            seekBar.setProgressInMins(DEFAULT_TIME_MIN);
        } else {
//            Date now = Utils.getNow(blockStartedAt.getTimeZone());
//            Date now = Utils.getNow(WedgeApplication.currentUser.getWedge().getTimezone()); //old realization
            Date now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
            endAt = new Date(blockStartedAt.getDate().getTime() + timeDuration * 1000);

            if (endAt.before(now)) {
                seekBar.setProgressInMins(DEFAULT_TIME_MIN);
            } else {
                isActive = true;
                long remainMin = (long) Math.ceil((endAt.getTime() - now.getTime()) / (60 * 1000));

                seekBar.setProgressInMins((int) remainMin);
                buttonCancel.setVisibility(VISIBLE);
                tvActive.setVisibility(VISIBLE);
                tvActive.setTextSize(16);

                updateRemainText(remainMin);

                timer = new Timer("remain_min_counter");
                timer.schedule(updateAdapterTask, 0, TIMER_PERIOD);

            }
        }
        updateBlockButtonText(seekBar.getProgressInMins());
        Utils.updateSliderText(BlockTimeActivity.this,seekBar.getProgressInMins(),
                timeDuration,tvTimeDuration);
    }

    @Override
    protected void onResume() {
        super.onResume();
        timerShouldRun = true;
        if (timer != null) {
            mHandler.obtainMessage().sendToTarget();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        timerShouldRun = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) timer.cancel();
    }

    private void updateRemainText(long remainMin) {
        if (remainMin >= 0) {
            String text = getString(R.string.block_mode_active, Utils.getTimeString(remainMin));
            SpannableString ss = new SpannableString(text);
            ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)), 14, text.length(), 0);
            tvActive.setText(ss);
        }
    }

    private void updateBlockButtonText(int progressInMin) {
        if (isActive) {
            buttonBlock.setText(R.string.apply);
        } else {
            int h = progressInMin / 60;
            String buttonText = null;
            if (progressInMin > 0) {
                if (h > 0) {
                    if (h >= 2) {
                        buttonText = getString(R.string.block_for) + " " + h + " hrs";
                    } else if (progressInMin - h * 60 != 0) {
                        buttonText = getString(R.string.block_for) + " "
                                + h + " h " + (progressInMin - h * 60) + " m";
                    } else {
                        buttonText = getString(R.string.block_for) + " " + h + " h";
                    }
                } else {
                    buttonText = getString(R.string.block_for) + " " + progressInMin + " " + getString(R.string.minutes);
                }
            } else if (progressInMin <= 0 && timeDuration >= 0) {
                buttonText = getString(R.string.apply) + " " + 0 + " " + getString(R.string.minutes);
            }
            if (buttonText != null) buttonBlock.setText(buttonText);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewUpdate:
                finish();
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), ManagementActivity.class);
                startActivity(intent);
                break;
            case R.id.imageViewHome:
                onBackPressed();
                break;
            case R.id.textViewBack:
                onBackPressed();
                break;
            case R.id.textViewMain:
                onBackPressed();
                break;
            case R.id.buttonCancel:
                cancelPressed = true;
                performBlockRequest(0);
                break;
            case R.id.buttonBlock:
                int progressInMin = seekBar.getProgressInMins();
                performBlockRequest(progressInMin);
                break;
        }
    }

    private void performBlockRequest(int progressInMin) {
        Date current = getCurrentDateWithTz();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String reportDate = df.format(current);
        blockStartedAt = new CreateTimeUser();
        blockStartedAt.setDateTime(reportDate);
        blockStartedAt.setTimeZone(blockStartedAt.getTimeZone());
//        blockStartedAt.setTimeZone(WedgeApplication.currentUser.getWedge().getTimezone());

        /* GET DEVICE OWNER FOR HOMEWORK*/
        DeviceOwnersDataSource deviceOwnersDataSource = new DeviceOwnersDataSource(mainsource);
        DeviceOwner deviceOwner = deviceOwnersDataSource.getDeviceOwnerById(getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID));
        CreateTimeUser homeWorkStartedAt = deviceOwner.getHomeworkModeStartedAt();
        Long homeworkModeDuration = deviceOwner.getHomeworkModeDuration();
        HomeWorkCategories[] homeWorkCategories = deviceOwner.getHomeworkCategories();


        progressInMin = Utils.roundHours(progressInMin);
        performUpdate(blockStartedAt, progressInMin * 60, homeWorkStartedAt, homeworkModeDuration, homeWorkCategories);
    }

    private Date getCurrentDateWithTz() {
        try {
        Calendar calendarCurrent = Calendar.getInstance();
        Date current = calendarCurrent.getTime();

            Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(((CreateTimeUser)
                    (getIntent().getExtras().getParcelable(Consts.CREATED_AT))).getTimeZone()));
            cal1.setTime(current);
            Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
            cal2.clear();
            cal2.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
            cal2.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
            cal2.set(Calendar.DATE, cal1.get(Calendar.DATE));
            cal2.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
            cal2.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
            current = cal2.getTime();
            return current;
        } catch (NullPointerException ex){}
        return Calendar.getInstance().getTime();
    }

    private void parseIntent()
    {
        time = new TimeProfiles[0];
        homeworkCategories = new HomeWorkCategories[0];
        if(getIntent()!=null) {
            Bundle bundle = getIntent().getExtras();
            Parcelable[] tv = (Parcelable[]) bundle.getParcelableArray(Consts.TIME_PROFILESES);
            if (tv != null) {
                time = new TimeProfiles[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    time[i] = (TimeProfiles) tv[i];
                }
            }

            tv = (Parcelable[]) bundle.getParcelableArray(Consts.HOMEWORK_CATEGORIES);

            if (tv != null) {
                homeworkCategories = new HomeWorkCategories[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    homeworkCategories[i] = (HomeWorkCategories) tv[i];
                }
            }

            timeExtension = bundle.getParcelable(Consts.TIME_EXTENSION);
        }
    }


    private void performUpdate(CreateTimeUser blockStartAt, long timeDuration, CreateTimeUser homeWorkStartedAt, Long homeworkModeDuration, HomeWorkCategories[] homeWorkCategorieses){
        BlockRequest request = new BlockRequest(this,
                getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID),
                getIntent().getExtras().getString(Consts.DEVICE_TITLE),
                timeDuration, blockStartAt,
                getIntent().getExtras().getLong(Consts.CONTENT_PROFILE_ID), time,
                (CreateTimeUser) getIntent().getExtras().getParcelable(Consts.CREATED_AT), homeWorkStartedAt, homeworkModeDuration, homeWorkCategorieses);
        contentManager.execute(request,
                new UpdateDeviceRequestListener(this, this, BaseRequest.HttpMethodEnum.put,
                        request.getUrl(), request));
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        /*if (seekBar.getProgressInMins() == 0 || cancelPressed) {
            Utils.makeToast(R.string.canceled_block);
        } else {
//            Utils.makeToast("Block Activated");
        }*/
        //after block request get DeviceOwner from model and update db field with homeworkDuration (server specific "feature")
        DeviceOwnersDataSource source = new DeviceOwnersDataSource(WedgeApplication.mainSource);
        DeviceOwnersModel deviceOwner = (DeviceOwnersModel) model;
        int orderId = source.getDeviceOwnerById(deviceOwner.getDeviceOwner().getId()).getOrderId();
        source.deleteDeviceOwner(deviceOwner.getDeviceOwner().getId());
        source.createDeviceOwner(deviceOwner.getDeviceOwner(), orderId);
        goToDeviceOwnerActivity();
    }

    private void goToDeviceOwnerActivity() {
        Intent intent = new Intent(getApplicationContext(), DeviceOwnersActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void performFailRequestAction(int action) {
        Utils.makeToast(getString(R.string.unexpected_error));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        goToOneUserActivity();
    }

    private void goToOneUserActivity() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), OneUserActivity.class);
        putExtras(intent);
        startActivity(intent);
        finish();
    }

    private void putExtras(Intent intent) {
        intent.putExtras(getIntent());
    }
}
