package android.itransition.com.wedge.activity;

import android.itransition.com.wedge.R;
import android.itransition.com.wedge.settings.Settings;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by e.kazimirova on 09.10.2014.
 */
public class BuyActivty extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_buy);

        setupActionBar();
        setTitle("Buy HomeHalo");
        setupWebView();

    }

    private void setupWebView() {
        Map<String, String> httpHeaders = new HashMap<String, String>();
        httpHeaders.put("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.put("X-Access-Token", Settings.getXAccessToken());
        final WebView webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setLoadWithOverviewMode(false);
        webView.getSettings().setUseWideViewPort(false);

        webView.setPadding(0, 0, 0, 0);
        webView.setInitialScale(100);

        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(false);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });


        StringBuilder sb = new StringBuilder();
        sb.append("https://www.homehalo.co.uk/service/login?accessToken=")
                .append(Settings.getXAccessToken())
                .append("&redirectUrl=https://www.homehalo.co.uk/homehaloproducts");

        webView.loadUrl(sb.toString(), httpHeaders);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewHome:
               finish();
               break;
            case R.id.textViewBack:
                finish();
                break;
            case R.id.imageViewUpdate:
                finish();
                break;
        }
    }

    @Override
    protected void onStart(){
        super.onStart();

    }



    @Override
    protected void onPause(){
        super.onPause();
    }
}
