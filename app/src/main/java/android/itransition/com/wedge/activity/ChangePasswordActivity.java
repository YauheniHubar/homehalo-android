package android.itransition.com.wedge.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.request.ChangePasswordRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;



/**
 * Created by e.kazimirova on 29.09.2014.
 */
public class ChangePasswordActivity extends BaseActivity implements RequestAction, View.OnClickListener {


    private EditText etConfirm;
    private  EditText etNewPass;
    private EditText etCurrentPassword;
    private Button buttonChange;
    private Button buttonCancel;

    @Override
    protected void onStart(){
        super.onStart();

    }




    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        etConfirm = (EditText) findViewById(R.id.etConfirmPass);
        etNewPass = (EditText) findViewById(R.id.etNewPass);
        etCurrentPassword = (EditText) findViewById(R.id.etOldPass);
        buttonChange = (Button) findViewById(R.id.buttonChange);

        etCurrentPassword.addTextChangedListener(tvWatcher);
        etConfirm.addTextChangedListener(tvWatcher);
        etNewPass.addTextChangedListener(tvWatcher);

        setupActionBar();
        setTitle("Change password");

        buttonChange.setOnClickListener(this);

    }


    TextWatcher tvWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
        @Override
        public void afterTextChanged(Editable s) {
            boolean show = etConfirm.getText().toString().equals(etNewPass.getText().toString());
            show = show && etCurrentPassword.getText().length() > 0;
            buttonChange.setEnabled(show);
        }
    };

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
//        finish();
//        LoginRequest request = new LoginRequest(this, Settings.getName(),
//                Settings.getPasword(), "", "rms_push_notifications.os.android.gcm",true);
//
//        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
//                new LoginRequestListener(this, this, BaseRequest.HttpMethodEnum.post,
//                        request.getUrl(), request));
//
//        Intent intent = new Intent();
//        intent.setClass(getApplicationContext(), LoginActivity.class);
//        Settings.setXAccessToken("");
//        Settings.setName("");
//        startActivity(intent);
    }


    @Override
    public void performFailRequestAction(int action) {

     }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewHome:
            case R.id.textViewBack:
            case R.id.textViewMain:
            case R.id.imageViewUpdate:
                finish();
                Intent intent2 = new Intent();
                intent2.setClass(this, ManagementActivity.class);
                startActivity(intent2);
                break;
            case R.id.buttonChange:
                String currentPass = etCurrentPassword.getText().toString();
                String newPass = etNewPass.getText().toString();
                String confirmPass = etConfirm.getText().toString();
                if(newPass.equals("") || confirmPass.equals("")){
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(getString(R.string.error));
                    alert.setMessage(getString(R.string.empty));

                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                } else if(!confirmPass.equals(newPass)){
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(getString(R.string.error));
                    alert.setMessage("You've entered wrong password");

                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                } else {
                    ChangePasswordRequest request = new ChangePasswordRequest(this, Settings.getUserId(),
                            currentPass, confirmPass);

                    contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                            new BaseRequestListener<BaseModel>(this,  request){

                                @Override
                                public void onRequestSuccess(BaseModel baseModel) {
                                    super.onRequestSuccess(baseModel);
                                    Toast.makeText(ChangePasswordActivity.this, R.string.success, Toast.LENGTH_SHORT).show();

//                                    Settings.setPassword(etConfirm.getText().toString());

//                                    Intent intent = new Intent();
//                                    intent.setClass(getApplicationContext(), LoginActivity.class);
//                                    Settings.setXAccessToken("");
//                                    Settings.setName(""); //todo
//                                    startActivity(intent);

//                                    finish();
                                }
                            });
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent intent = new Intent();
        intent.setClass(this, ConfigActivity.class);
        startActivity(intent);
        finish();
    }
}
