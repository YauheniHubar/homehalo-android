package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.database.datesource.WedgeDataSource;
import android.itransition.com.wedge.entity.WedgeEntity;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.UserModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.ResetWedgeRequest;
import android.itransition.com.wedge.request.UserRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.listeners.UserRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by e.kazimirova on 09.10.2014.
 */
public class ConfigActivity extends BaseActivity implements View.OnClickListener, RequestAction {

    private Button btnRestart;
    private RelativeLayout relChangePassword;
    private RelativeLayout relChangePin;
    private TextView tv14;
    private int i = 0;

    private WedgeDataSource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        setupActionBar();
        setNeedHome(true);
        //setBarColor(getResources().getColor(R.color.main_gray));
        setTitle(getString(R.string.config_title));

        ((TextView)findViewById(R.id.tvUser)).setText(Settings.getName());
        try {
            ((TextView) findViewById(R.id.textView15)).setText
                    (getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TextView tvWebsite = (TextView)findViewById(R.id.tvWebsite);
        tvWebsite.setPaintFlags(tvWebsite.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvWebsite.setText(Html.fromHtml(getString(R.string.go_to_website)));
        tvWebsite.setOnClickListener(this);
//        tvWebsite.setLinksClickable(true);
//        tvWebsite.setMovementMethod(LinkMovementMethod.getInstance());
        relChangePassword = (RelativeLayout)findViewById(R.id.relChangePassword);
        relChangePin = (RelativeLayout)findViewById(R.id.relChangePin);
        btnRestart = (Button) findViewById(R.id.btnRestart);
        tv14 = (TextView) findViewById(R.id.textView14);
        tv14.setOnClickListener(this);
        if (!Settings.isDemoMode()) {
            btnRestart.setOnClickListener(this);
            relChangePassword.setOnClickListener(this);
            relChangePin.setOnClickListener(this);
        }

        datasource = new WedgeDataSource(this, MainSource.getInstance());
        if(!Settings.getUpdatingWedge()) {
            datasource.deleteWedge();
            getUserInfo();
        } else {


                WedgeEntity wedge = datasource.getWedge();
                ((TextView) findViewById(R.id.textView13)).setText(wedge
                        .getFirmware());
                ((TextView) findViewById(R.id.textView8)).setText(wedge
                        .getSsid());
                ((TextView) findViewById(R.id.textView10)).setText(wedge
                        .getPasskey());
                datasource.close();
         }

        LinearLayout.LayoutParams lp1 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv1)).getLayoutParams();
        lp1.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv1)).setLayoutParams(lp1);

        LinearLayout.LayoutParams lp2 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv2)).getLayoutParams();
        lp2.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv2)).setLayoutParams(lp2);

        LinearLayout.LayoutParams lp3 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv3)).getLayoutParams();
        lp3.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv3)).setLayoutParams(lp3);

        LinearLayout.LayoutParams lp4 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv4)).getLayoutParams();
        lp4.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv4)).setLayoutParams(lp1);

        LinearLayout.LayoutParams lp5 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv5)).getLayoutParams();
        lp5.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv5)).setLayoutParams(lp5);

        LinearLayout.LayoutParams lp6 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv6)).getLayoutParams();
        lp6.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv6)).setLayoutParams(lp6);

        LinearLayout.LayoutParams lp7 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv7)).getLayoutParams();
        lp7.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv7)).setLayoutParams(lp7);

        LinearLayout.LayoutParams lp8 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv8)).getLayoutParams();
        lp8.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv8)).setLayoutParams(lp8);

        LinearLayout.LayoutParams lp9 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv9)).getLayoutParams();
        lp9.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv9)).setLayoutParams(lp9);

        LinearLayout.LayoutParams lp10 =
                (LinearLayout.LayoutParams) ((RelativeLayout)findViewById(R.id.relDiv10)).getLayoutParams();
        lp10.height=1;
        ((RelativeLayout)findViewById(R.id.relDiv10)).setLayoutParams(lp10);
    }

    @Override
    protected void onStart(){
        super.onStart();

    }



    @Override
    protected void onPause(){
        super.onPause();
    }

    private void getUserInfo(){
        UserRequest request = new UserRequest(this, Settings.getUserId());
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new UserRequestListener(this, this, BaseRequest.HttpMethodEnum.get,
                        request.getUrl(), request));
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.relChangePin:
                Intent intent = new Intent();
                intent.putExtra(Consts.EDIT_PIN, true);
                intent.setClass(this, PinActivity.class );
                startActivity(intent);
                break;
            case R.id.btnRestart:

                ResetWedgeRequest request = new ResetWedgeRequest(this, Settings.getUserId());
                contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                        new BaseRequestListener<BaseModel>(this,  request){
                            @Override
                            public void onRequestFailure(SpiceException e) {
                                super.onRequestFailure(e);
                                Toast.makeText(getApplicationContext(), R.string.request_unsuccessful,
                                        Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onRequestSuccess(BaseModel o) {
                                super.onRequestSuccess(o);
                                Toast.makeText(getApplicationContext(), R.string.homehalo_restarted,
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                break;
            case R.id.relChangePassword:
                finish();
                Intent intentChangePassword = new Intent();
                intentChangePassword.setClass(this,ChangePasswordActivity.class );
                intentChangePassword.putExtra(Consts.FROM_CONFIG, true);
                startActivity(intentChangePassword);
                break;
            case R.id.textViewMain:
                onBackPressed();
                break;
            case R.id.imageViewHome:
                onBackPressed();
                break;
            case R.id.textViewBack:
                onBackPressed();
                break;
            case R.id.imageViewUpdate:
                onBackPressed();
                break;
            case R.id.textView14:
                break;
            case R.id.tvWebsite:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(Settings.getUrlSettings()));
                startActivity(i);
//                ++i;
//                if (i>3)
//                    i = i/0;
//                Toast.makeText(this, "i:"+i, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
        Intent intent = new Intent(this, ManagementActivity.class);
        startActivity(intent);
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {

        WedgeEntity wedge = ((UserModel) model).getUser().getWedge();

        if (wedge != null) {
            datasource.createWedge(wedge);
            Settings.setUpdatinWedge(true);
            datasource.close();
            ((TextView) findViewById(R.id.textView13)).setText(wedge
                    .getFirmware());
            ((TextView) findViewById(R.id.textView8)).setText(wedge
                    .getSsid());
            ((TextView) findViewById(R.id.textView10)).setText(wedge
                    .getPasskey());
        }
    }

    @Override
    public void performFailRequestAction(int action) {

    }
}
