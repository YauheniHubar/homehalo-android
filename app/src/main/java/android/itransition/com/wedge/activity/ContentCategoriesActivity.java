package android.itransition.com.wedge.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.ContentProfileCategoriesDataSource;
import android.itransition.com.wedge.database.datesource.ContentProfileDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.ContentCategories;
import android.itransition.com.wedge.entity.ContentProfile;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.gui.adapters.CheckBoxAdapter;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.CategoriesContentProfilesModel;
import android.itransition.com.wedge.model.ContentProfileUpdateModel;
import android.itransition.com.wedge.model.OneContentProfileModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.CreateContentProfileRequest;
import android.itransition.com.wedge.request.GetContentCategoriesRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.SaveContentProfilesRequest;
import android.itransition.com.wedge.request.listeners.CreateContentProfileRequestListener;
import android.itransition.com.wedge.request.listeners.GetContentCategoriesRequestListener;
import android.itransition.com.wedge.request.listeners.SaveContentProfilesRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 01.10.2014.
 */
public class ContentCategoriesActivity extends BaseActivity implements View.OnClickListener, RequestAction {

    private ContentCategories[] mListCategories;
    private ArrayList<Integer> mListCategoriesActive = new ArrayList<>();
    private CheckBoxAdapter adapter;
    private ContentCategoriesActivity activity;
    private ListView listView;
    private ContentProfileCategoriesDataSource datasource;
    private ContentProfileDataSource datasourceProfile;
    private MainSource maindatasourse;
    private Button btnSave;

    private long contentProfileId = 0;
    private String contentTitle = "";
    private boolean isDefault = false;
    private boolean isDeletable = false;
    private CreateTimeUser createTimeUser = new CreateTimeUser();
    @Override
    protected void onStart(){
        super.onStart();

    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_categories);

        this.activity = this;
        listView = (ListView) findViewById(R.id.listView);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        maindatasourse =  WedgeApplication.mainSource;
        datasource = new ContentProfileCategoriesDataSource(this, maindatasourse);
        mListCategoriesActive = getIntent().getExtras().getIntegerArrayList(Consts.CONTENT_CATEGORIES);

        setupActionBar();
        if(getIntent().getExtras().getString(Consts.CONTENT_PROFILE_TITLE).equals("")){
            setTitle((R.string.content_profile_managment));
            showDialog();
        } else {
            setTitle(getIntent().getExtras().getString(Consts.CONTENT_PROFILE_TITLE));

        }


        contentProfileId= getIntent().getExtras().getLong(Consts.CONTENT_PROFILE_ID);
        contentTitle    = getIntent().getExtras().getString(Consts.CONTENT_PROFILE_TITLE);
        isDefault =      getIntent().getExtras().getBoolean(Consts.CONTENT_PROFILE_IS_DEFAULT);
        isDeletable =         getIntent().getExtras().getBoolean(Consts.CONTENT_PROFILE_IS_DELETABLE);
        createTimeUser=        (CreateTimeUser) getIntent().getExtras().getParcelable(Consts.CREATED_AT);


        if(!Settings.getUpdatingContentProfileCategories()) {
            performCategories();
            datasource.deleteContentProfileAll();
        } else {
            List<ContentCategories> l = datasource.getAllContentProfile();
            if (l.size() == 0) {
                performCategories();
                datasource.deleteContentProfileAll();
            } else {
                mListCategories = l.toArray(new ContentCategories[l.size()]);
                updateList();
            }
        }
    }

    private void updateList() {
        boolean[] activeChecked = new boolean[mListCategories.length];
        try {
            for (int i = 0; i < mListCategoriesActive.size(); i++) {
                for (int k = 0; k < mListCategories.length; k++) {
                    if (mListCategories[k].getId() == mListCategoriesActive.get(i)) {
                        activeChecked[k] = true;
                    }
                }
            }
        } catch (NullPointerException ex) {
        }

        datasource.close();
        adapter = new CheckBoxAdapter(this, mListCategories, activeChecked);
        listView.setAdapter(adapter);
    }


    private void showDialog() {

        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogview = inflater.inflate(R.layout.create_content_profile, null);
        AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
        dialogbuilder.setTitle(R.string.add_new_content_profile);
        dialogbuilder.setView(dialogview);


        final AlertDialog  dialogDetails = dialogbuilder.create();

        Button loginbutton = (Button) dialogview
                .findViewById(R.id.btnCreate);
        final Button cancelbutton = (Button) dialogview
                .findViewById(R.id.btnCancel);
        final EditText nameEditText = (EditText) dialogview
                .findViewById(R.id.dialog_txt_name);
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!nameEditText.getText().toString().equals("")) {
                    if (checkContentProfile(nameEditText.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "Content rule "+nameEditText.getText().toString()+" already exists", Toast.LENGTH_LONG).show();
                    } else {
                        CreateContentProfileRequest request = new CreateContentProfileRequest(getApplicationContext(),
                                nameEditText.getText().toString(), Settings.getUserId());
                        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                                new CreateContentProfileRequestListener(activity, activity, BaseRequest.HttpMethodEnum.post,
                                        request.getUrl(), request));
                        dialogDetails.dismiss();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.invalid_title, Toast.LENGTH_LONG).show();
                }

            }
        });
        cancelbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                dialogDetails.dismiss();
            }
        });
        dialogDetails.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialogDetails.show();
        colorAlertDialogTitle(dialogDetails, getResources().getColor(R.color.green));

    }

    public static void colorAlertDialogTitle(AlertDialog dialog, int color) {
        int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
        if (dividerId != 0) {
            View divider = dialog.findViewById(dividerId);
//            divider.setBackgroundColor(color);
        }

       int textViewId = dialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
        if (textViewId != 0) {
            TextView tv = (TextView) dialog.findViewById(textViewId);
            tv.setTextColor(color);
        }

    }


    public void performCategories() {
        if (!Utils.isOnline(this)) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            return;
        }
        GetContentCategoriesRequest categoriesRequest = new GetContentCategoriesRequest(this);
        contentManager.execute(categoriesRequest, null, DurationInMillis.ONE_MINUTE,
                new GetContentCategoriesRequestListener(this, this, BaseRequest.HttpMethodEnum.get,
                        categoriesRequest.getUrl(), categoriesRequest));
    }

    public void performSaveCategories(){
        if (!Utils.isOnline(this)) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            return;
        }

        ArrayList<Integer> checked = adapter.getmListActive();

        SaveContentProfilesRequest categoriesRequest = new SaveContentProfilesRequest(this,
                Settings.getUserId(), contentProfileId,
                contentTitle,
                isDefault,
                isDeletable,
                createTimeUser, checked);
        contentManager.execute(categoriesRequest, null, DurationInMillis.ONE_MINUTE,
                new SaveContentProfilesRequestListener(this,  this, BaseRequest.HttpMethodEnum.put,
                        categoriesRequest.getUrl(), categoriesRequest));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSave:
                performSaveCategories();
                break;
            case R.id.textViewBack:
                Intent intent1 = new Intent();
                intent1.setClass(this, ContentProfileManagmentActivity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.textViewMain:
                Intent intent2 = new Intent();
                intent2.setClass(this, ContentProfileManagmentActivity.class);
                startActivity(intent2);
                finish();
                break;
            case R.id.imageViewHome:
                Intent intent3 = new Intent();
                intent3.setClass(this, ContentProfileManagmentActivity.class);
                startActivity(intent3);
                finish();
                break;
            case R.id.imageViewUpdate:
                Intent intent = new Intent();
                intent.setClass(this, ManagementActivity.class);
                startActivity(intent);
                finish();
            default:
                break;
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent intent = new Intent();
        intent.setClass(this, ContentProfileManagmentActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        if(model instanceof OneContentProfileModel){
            contentProfileId= ((OneContentProfileModel)model).getProfiles().getId();
            contentTitle    = ((OneContentProfileModel)model).getProfiles().getTitle();
            isDefault =     ((OneContentProfileModel)model).getProfiles().getDefault();
            isDeletable =      ((OneContentProfileModel)model).getProfiles().getDeletable();
            createTimeUser=       ((OneContentProfileModel)model).getProfiles().getCreateAt();
//            Toast.makeText(getApplicationContext(), "Your profile has been created ", Toast.LENGTH_LONG).show();
            return;
        }
        if (model instanceof CategoriesContentProfilesModel) {
            mListCategories = ((CategoriesContentProfilesModel) model).getContentCategories();
            boolean[] activeChecked = new boolean[mListCategories.length];
            try {
                for (int i = 0; i < mListCategoriesActive.size(); i++) {
                    for (int k = 0; k < mListCategories.length; k++) {
                        if (mListCategories[k].getId() == mListCategoriesActive.get(i)) {
                            activeChecked[k] = true;
                        }
                    }
                }
            } catch (NullPointerException ex) {
            }
            for(int i=0; i<mListCategories.length;i++){
                datasource.createContentProfile(mListCategories[i]);
            }
            datasource.close();
            Settings.setUpdatingContentProfileCategories(true);

            adapter = new CheckBoxAdapter(this, mListCategories, activeChecked);
            listView.setAdapter(adapter);
            return;
        }

        try {
            ContentProfileUpdateModel result = (ContentProfileUpdateModel) model;
            if (result.getSuccess() && result.getContentProfile()  != null) {

                ContentCategories[] activeCategories = result.getContentProfile().getContentCategories();
                mListCategoriesActive.clear();
                if (activeCategories != null) {
                    for (int i = 0; i < activeCategories.length; i++) {
                        mListCategoriesActive.add((int)activeCategories[i].getId());
                    }
                }
               updateList();
                datasource.updateActiveCategories(result.getContentProfile());
                datasource.close();
//                Utils.makeToast(R.string.changes_saved);
                finish();
            } else {
                Toast.makeText(this, R.string.error_profile, Toast.LENGTH_LONG).show();
            }
        } catch (ClassCastException ex){
            Toast.makeText(this,R.string.error_profile, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void performFailRequestAction(int action) {

    }

    public boolean checkContentProfile(String name) {
        datasourceProfile = new ContentProfileDataSource(MainSource.getInstance());
        List<ContentProfile> list = datasourceProfile.getAllContentProfile();

        for (ContentProfile cp : list) {
            if (cp.getTitle().toLowerCase().equals(name.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
}
