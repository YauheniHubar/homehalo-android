package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.ContentProfileDataSource;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.ContentProfile;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.TimeExtensions;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.gui.CircularSeekBar;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.ContentProfilesModel;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.ContentProfilesRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.UpdateDeviceRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.listeners.ContentProfilesRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 05.12.2014.
 */
public class ContentProfileAssign extends BaseActivity implements RequestAction, View.OnClickListener {

    private boolean flag = false;
    private Button buttonBlock;
    private TimeProfiles[] time;
    HomeWorkCategories[] homeworkCategories;
    TimeExtensions timeExtensions;
    private CircularSeekBar seekBar;
    private CreateTimeUser blockStartedAt;
    private long timeDuration = 0;

    private ListView listView;
    private Button btnAssign;
    private ContentProfile[] mList;
    private ContentProfileDataSource dataSource;
    private RequestAction requestAction;
    private long deviceOnwerId;
    private DeviceOwner deviceOwner;
    private String deviceName;

    @Override
    protected void onStart(){
        super.onStart();

    }


    private int position =0;

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign);
        btnAssign = (Button) findViewById(R.id.buttonSave);
        requestAction = this;

        deviceOnwerId = getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID);
        DeviceOwnersDataSource source = new DeviceOwnersDataSource(WedgeApplication.mainSource);
        deviceOwner = source.getDeviceOwnerById(deviceOnwerId);

        setInit();
        deviceName = getIntent().getExtras().getString(Consts.DEVICE_TITLE);

        btnAssign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assignProfile();
            }
        });



        listView = (ListView) findViewById(R.id.listView);


        setupActionBar();
        //setBarColor(getResources().getColor(R.color.main_gray));
        setTitle(deviceName);

        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        dataSource = new ContentProfileDataSource(MainSource.getInstance());
        List<ContentProfile> list = null;
        if(!Settings.needUpdateContentProfile() && (list = dataSource.getAllContentProfile()).size() != 0){
            mList =  list.toArray(new ContentProfile[list.size()]);
            updateList();
        } else {
            performGetContentProfiles();
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CheckedTextView c = (CheckedTextView) view;

                    c.setChecked(true);
                    listView.setItemChecked(i, true);
                    position = i;

            }
        });
    }

    private void assignProfile() {
        UpdateDeviceRequest request = new UpdateDeviceRequest(getApplicationContext(), Settings.getUserId(),
                deviceOnwerId, deviceName,
                getIntent().getExtras().getString(Consts.DEVICE_STATUS), getIntent().getExtras().getLong(Consts.TIME),
                (CreateTimeUser) getIntent().getExtras().getParcelable(Consts.BLOCK_STARTED_AT),
                (CreateTimeUser) getIntent().getExtras().getParcelable(Consts.HOMEWORK_MODE_STARTED_AT),
                getIntent().getExtras().getLong(Consts.HOMEWORK_MODE_DURATION),
                mList[position].getId(), time, homeworkCategories,
                (CreateTimeUser) getIntent().getExtras().getParcelable(Consts.CREATED_AT), null);
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new BaseRequestListener<DeviceOwnersModel>(ContentProfileAssign.this, request) {
                    @Override
                    public void onRequestSuccess(DeviceOwnersModel o) {
                        super.onRequestSuccess(o);
//                        Utils.makeToast(R.string.content_profile_updated);
//                        Intent intent = new Intent(getApplicationContext(), DeviceOwnersActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
                        finish();
                    }
                });
    }

    private void updateList() {
        ArrayList<String> values = new ArrayList<String>();
        for (int i = 0; i < mList.length; i++) {
            values.add(mList[i].getTitle());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                R.layout.choice_item,
                values);
        listView.setAdapter(adapter);
        selectCurrentProfile();
    }

    private void selectCurrentProfile() {
        if (deviceOwner == null || deviceOwner.getContentProfile() == null) return;

        ContentProfile ownerProfile = deviceOwner.getContentProfile();
        for (int i = 0; i < mList.length; i++) {
            ContentProfile cp = mList[i];
            if (ownerProfile.getId() == cp.getId()) {
                listView.setItemChecked(i, true);
                return;
            }
        }

    }

    private void  performGetContentProfiles(){
        if (Utils.isOnline(this)) {
            ContentProfilesRequest request = new ContentProfilesRequest(this, Settings.getUserId());
            contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                    new ContentProfilesRequestListener(this, this, BaseRequest.HttpMethodEnum.get,
                            request.getUrl(), request));
        } else {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewHome:
                onBackPressed();
                break;
            case R.id.textViewBack:
                onBackPressed();
                break;
            case R.id.textViewMain:
                onBackPressed();
                break;
            case R.id.imageViewUpdate:
                finish();
                Intent intent = new Intent(this, ManagementActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
//        Intent intent = new Intent();
//        intent.setClass(getApplicationContext(), OneUserActivity.class);
//        intent.putExtra(Consts.DEVICE_OWNER_ID,getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID));
//        intent.putExtra(Consts.DEVICE_TITLE, getIntent().getExtras().getString(Consts.DEVICE_TITLE));
//        intent.putExtra(Consts.DEVICE_STATUS, getIntent().getExtras().getString(Consts.DEVICE_STATUS));
//
//        intent.putExtra(Consts.HOMEWORK_MODE_DURATION, getIntent().getExtras().getLong(Consts.HOMEWORK_MODE_DURATION));
//        intent.putExtra(Consts.BLOCK_STARTED_AT, getIntent().getExtras().getParcelable(Consts.BLOCK_STARTED_AT));
//        intent.putExtra(Consts.TIME, getIntent().getExtras().getLong(Consts.TIME));
//        intent.putExtra(Consts.HOMEWORK_MODE_STARTED_AT,
//                getIntent().getExtras().getParcelable(Consts.HOMEWORK_MODE_STARTED_AT));
//        intent.putExtra(Consts.CONTENT_PROFILE_ID,
//                getIntent().getExtras().getLong(Consts.CONTENT_PROFILE_ID));
//        intent.putExtra(Consts.CONTENT_PROFILE_TITLE,
//                getIntent().getExtras().getString(Consts.CONTENT_PROFILE_TITLE));
//        intent.putExtra(Consts.TIME_PROFILESES, time);
//
//        intent.putExtra(Consts.TIME_EXTENSIONSES_COUNT, (Parcelable)timeExtensions);
//        intent.putExtra(Consts.HOMEWORK_CATEGORIES, homeworkCategories);
//        intent.putExtra(Consts.CREATED_AT, getIntent().getExtras().getParcelable(Consts.CREATED_AT));
//        intent.putExtra(Consts.IS_UNLIMITED_ACCESS, getIntent().getBooleanExtra(Consts.IS_UNLIMITED_ACCESS, false));
//        startActivity(intent);
//        finish();
    }


    private void setInit()
    {
        time = new TimeProfiles[0];
        homeworkCategories = new HomeWorkCategories[0];
        if(getIntent()!=null){
            Bundle bundle = getIntent().getExtras();
            Parcelable[] tv = (Parcelable[])bundle.getParcelableArray(Consts.TIME_PROFILESES);
            if(tv!=null) {
                time = new TimeProfiles[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    time[i] = (TimeProfiles) tv[i];
                }
            }

            tv = (Parcelable[])bundle.getParcelableArray(Consts.HOMEWORK_CATEGORIES);

            if(tv!=null) {
                homeworkCategories = new HomeWorkCategories[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    homeworkCategories[i] = (HomeWorkCategories) tv[i];
                }
            }

            timeExtensions = bundle.getParcelable(Consts.TIME_EXTENSION);
        }


    }


    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        try {
            mList = ((ContentProfilesModel) model).getProfiles();
            updateList();
        } catch (ClassCastException ex){
            Toast.makeText(this, R.string.success, Toast.LENGTH_LONG).show();
        } catch (NullPointerException ex){
            Toast.makeText(this, R.string.success, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void performFailRequestAction(int action) {

    }
}