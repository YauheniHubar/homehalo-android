package android.itransition.com.wedge.activity;

import android.app.Activity;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.database.datesource.ContentProfileDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.ContentCategories;
import android.itransition.com.wedge.entity.ContentProfile;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.gui.adapters.ContentProfileManagmentAdapter;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.ContentProfilesModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.ContentProfilesRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.listeners.ContentProfilesRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 30.09.2014.
 */
public class ContentProfileManagmentActivity extends BaseActivity implements View.OnClickListener, RequestAction {

    private ListView listView;
    private ContentProfile[] mList;
    private ContentCategories[] mListCategories;
    private ContentProfileManagmentAdapter adapter;
    private ContentProfileDataSource dataSource;

    @Override
    protected void onStart(){
        super.onStart();

    }



    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_profile_managment);

        dataSource = new ContentProfileDataSource(MainSource.getInstance());
        listView = (ListView) findViewById(R.id.listViewContentProfile);

        LinearLayout headerLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.add_new_user_header, null);
        Button btnAddContentProfileButton = (Button)headerLayout.findViewById(R.id.add_user_btn);
        btnAddContentProfileButton.setText(getString(R.string.add_new_content_profile_text));
        btnAddContentProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), ContentCategoriesActivity.class);
                ArrayList<Integer> listId = new ArrayList<Integer>();
                intent.putExtra(Consts.CONTENT_CATEGORIES, listId);
                intent.putExtra(Consts.CONTENT_PROFILE_ID, 0);
                intent.putExtra(Consts.CONTENT_PROFILE_TITLE, "");
                intent.putExtra(Consts.CONTENT_PROFILE_IS_DEFAULT, true);
                intent.putExtra(Consts.CONTENT_PROFILE_IS_DELETABLE, true);
                intent.putExtra(Consts.CREATED_AT, (Parcelable)new CreateTimeUser());
                startActivity(intent);
            }
        });

        listView.addHeaderView(headerLayout);
        setupActionBar();
        //setBarColor(getResources().getColor(R.color.main_gray));
        setTitle(getString(R.string.content_profile));

        List<ContentProfile> list = dataSource.getAllContentProfile();
        if(!Settings.needUpdateContentProfile() && list.size() != 0){
            mList =  list.toArray(new ContentProfile[list.size()]);
            adapter = new ContentProfileManagmentAdapter(this, mList, null);
            listView.setAdapter(adapter);
        } else {
            performGetContentProfiles();
        }
    }


    private void  performGetContentProfiles(){
        if (!Utils.isOnline(this)) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            return;
        }
        ContentProfilesRequest request = new ContentProfilesRequest(this, Settings.getUserId());
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new ContentProfilesRequestListener(this,  this, BaseRequest.HttpMethodEnum.get,
                        request.getUrl(), request));


    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.imageViewHome:
                finish();
                intent = new Intent(this, ManagementActivity.class);
                startActivity(intent);
                break;
            case R.id.textViewBack:
                finish();
                intent = new Intent(this, ManagementActivity.class);
                startActivity(intent);
                break;
            case R.id.textViewMain:
                finish();
                intent = new Intent(this, ManagementActivity.class);
                startActivity(intent);
                break;
            case R.id.imageViewUpdate:
                finish();
                intent = new Intent(this, ManagementActivity.class);
                startActivity(intent);
            default:
                break;
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
        Intent intent = new Intent(this, ManagementActivity.class);
        startActivity(intent);
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        mList = ((ContentProfilesModel) model).getProfiles();
        adapter = new ContentProfileManagmentAdapter(this, mList, null);
        listView.setAdapter(adapter);

    }

    @Override
    public void performFailRequestAction(int action) {

    }
}
