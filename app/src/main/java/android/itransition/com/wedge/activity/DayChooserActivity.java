package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.gui.ItemDayView;
import android.itransition.com.wedge.model.DeviceOwnerModel;
import android.itransition.com.wedge.request.GetDeviceOwnerRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.os.Bundle;
import android.view.View;

import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by i.grechishchev on 09.07.2015.
 * itransition 2015
 */
public class DayChooserActivity extends BaseActivity implements View.OnClickListener{
    ItemDayView monday, tuesday, wednesday, thursday, friday, saturday, sunday;
    private long mDeviceOwnerId;
    private TimeProfiles[] timeProfiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_chooser);

        setupActionBar();
        setTitle(getIntent().getExtras().getString(Consts.DEVICE_TITLE));
        mDeviceOwnerId = getIntent().getLongExtra(Consts.DEVICE_OWNER_ID, -1);

        monday = (ItemDayView) findViewById(R.id.item_monday);
        tuesday = (ItemDayView) findViewById(R.id.item_tuesday);
        wednesday = (ItemDayView) findViewById(R.id.item_wednesday);
        thursday = (ItemDayView) findViewById(R.id.item_thursday);
        friday = (ItemDayView) findViewById(R.id.item_friday);
        saturday = (ItemDayView) findViewById(R.id.item_saturday);
        sunday = (ItemDayView) findViewById(R.id.item_sunday);

        monday.setOnClickListener(this);
        tuesday.setOnClickListener(this);
        wednesday.setOnClickListener(this);
        thursday.setOnClickListener(this);
        friday.setOnClickListener(this);
        saturday.setOnClickListener(this);
        sunday.setOnClickListener(this);

        initDays();
    }

    private void initDays() {
        monday.setDayEng("monday");
        tuesday.setDayEng("tuesday");
        wednesday.setDayEng("wednesday");
        thursday.setDayEng("thursday");
        friday.setDayEng("friday");
        saturday.setDayEng("saturday");
        sunday.setDayEng("sunday");
    }

    @Override
    protected void onResume() {
        super.onResume();
        performGetDeviceOwnerRequest();
    }

    private void performGetDeviceOwnerRequest(){
        GetDeviceOwnerRequest request = new GetDeviceOwnerRequest(this, mDeviceOwnerId);

        contentManager.execute(request, new BaseRequestListener<DeviceOwnerModel>(this, request) {
            @Override
            public void onRequestFailure(SpiceException e) {
                super.onRequestFailure(e);
            }

            @Override
            public void onRequestSuccess(DeviceOwnerModel model) {
                super.onRequestSuccess(model);
                timeProfiles = model.getDeviceOwner().getTimeProfiles();
                if (timeProfiles != null)
                initSchedule(timeProfiles);
            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), TimeProfileActivity.class);
        switch (v.getId()) {
            case R.id.item_monday:
                intent.putExtra(Consts.ID_DAY, 0);
                break;
            case R.id.item_tuesday:
                intent.putExtra(Consts.ID_DAY, 1);
                break;
            case R.id.item_wednesday:
                intent.putExtra(Consts.ID_DAY, 2);
                break;
            case R.id.item_thursday:
                intent.putExtra(Consts.ID_DAY, 3);
                break;
            case R.id.item_friday:
                intent.putExtra(Consts.ID_DAY, 4);
                break;
            case R.id.item_saturday:
                intent.putExtra(Consts.ID_DAY, 5);
                break;
            case R.id.item_sunday:
                intent.putExtra(Consts.ID_DAY, 6);
                break;
        }
        putExtras(intent);
        startActivity(intent);
    }

    private void putExtras(Intent intent) {
        intent.putExtras(getIntent().getExtras());
        intent.putExtra(Consts.TIME_PROFILESES, timeProfiles);
    }

    private void initDay(ItemDayView itemDayView, int startMin, int endMin){
        for (int i = startMin; i < endMin; i = i+15) {
            itemDayView.getQuarterByTime(i).setBackgroundColor(getResources().getColor(R.color.green));
        }

    }

    private void initSchedule(TimeProfiles[] time) {
        resetDays();

        for (int i = 0; i < time.length; i++) {
            TimeProfiles timeProfiles = time[i];
            String startTime = timeProfiles.getStartTime().getTime();
            String endTime = timeProfiles.getEndTime().getTime();
            String startHrString = startTime.substring(0, startTime.indexOf(":", 0));
            String startMinString = startTime.substring(startTime.indexOf(":", 0) + 1, startTime.lastIndexOf(":"));
            String endHrString = endTime.substring(0, startTime.indexOf(":", 0));
            String endMinString = endTime.substring(startTime.indexOf(":", 0) + 1, startTime.lastIndexOf(":"));
            int startHr = Integer.valueOf(startHrString);
            int startMin = Integer.valueOf(startMinString);
            int endHr = Integer.valueOf(endHrString);
            int endMin = Integer.valueOf(endMinString);

            int startInMin = startHr * 60 + startMin;
            int endInMin = endHr * 60 + endMin;

            if (timeProfiles.getDayOfWeek().equalsIgnoreCase(monday.getDayEng())) initDay(monday, startInMin, endInMin);

            if (timeProfiles.getDayOfWeek().equalsIgnoreCase(tuesday.getDayEng())) initDay(tuesday, startInMin, endInMin);

            if (timeProfiles.getDayOfWeek().equalsIgnoreCase(wednesday.getDayEng())) initDay(wednesday, startInMin, endInMin);

            if (timeProfiles.getDayOfWeek().equalsIgnoreCase(thursday.getDayEng())) initDay(thursday, startInMin, endInMin);

            if (timeProfiles.getDayOfWeek().equalsIgnoreCase(friday.getDayEng())) initDay(friday, startInMin, endInMin);

            if (timeProfiles.getDayOfWeek().equalsIgnoreCase(saturday.getDayEng())) initDay(saturday, startInMin, endInMin);

            if (timeProfiles.getDayOfWeek().equalsIgnoreCase(sunday.getDayEng())) initDay(sunday, startInMin, endInMin);
        }
    }

    private void resetDays(){
        monday.resetViews();
        tuesday.resetViews();
        wednesday.resetViews();
        thursday.resetViews();
        friday.resetViews();
        saturday.resetViews();
        sunday.resetViews();
    }
}
