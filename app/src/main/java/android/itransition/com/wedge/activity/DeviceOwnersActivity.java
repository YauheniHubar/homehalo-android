package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.gui.SwipeListView;
import android.itransition.com.wedge.gui.adapters.DeviceOwnerAdapter;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.DeviceOwnerModel;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.DeleteDeviceOwnerRequest;
import android.itransition.com.wedge.request.DeviceOwnersInfoRequest;
import android.itransition.com.wedge.request.DeviceOwnersRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.listeners.DeviceOwnersRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.retry.DefaultRetryPolicy;

import java.util.List;

/**
 * Created by e.kazimirova on 01.09.2014.
 */
public class DeviceOwnersActivity extends BaseActivity implements RequestAction {

    public static final int TIMER_INTERVAL = 30 * 1000;
    private SwipeRefreshLayout swipeList;
    private SwipeListView listView;
    private DeviceOwnerAdapter adapter;
    private DeviceOwnersRequestListener listener;
    private LinearLayout headerLayout;
    private int headerHeight;
    private int itemHeight;
    private boolean timerShouldRun;

    private Runnable updateAdapterTask = new Runnable() {
        @Override
        public void run() {
            if (adapter != null && timerShouldRun) {
                adapter.recalculateRemainTime();
                mHandler.postDelayed(updateAdapterTask, TIMER_INTERVAL);
            }
        }
    };

    public Handler mHandler = new Handler();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        swipeList = (SwipeRefreshLayout) findViewById(R.id.listViewUserSwipe);
        listView = (SwipeListView) findViewById(R.id.listViewUser);
        headerLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.add_new_user_dummy_header, null);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int displayHeight = displaymetrics.heightPixels;
        int statusBarHeight = getStatusBarHeight();
        setupActionBar();
        setNeedHome(true);
        setTitle(getString(R.string.users_title));

        swipeList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utils.isOnline(DeviceOwnersActivity.this)) {
                    performGetDeviceOwners(false);
                } else {
                    Toast.makeText(DeviceOwnersActivity.this, R.string.error_connection, Toast.LENGTH_SHORT).show();
                    swipeList.setRefreshing(false);
                }
            }
        });

        listView.addHeaderView(headerLayout);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("TAG", "Click");
            }
        });

        mHandler.postDelayed(updateAdapterTask, TIMER_INTERVAL);

        if (Settings.isNeedUpdateDeviceOwner()) {
            performGetDeviceOwners(false);
        } else {
            performGetDeviceOwners(true);
        }

        headerLayout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        headerHeight = headerLayout.getMeasuredHeight();
        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        int abHeight=0;
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            abHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }
        itemHeight = displayHeight - (statusBarHeight+abHeight+headerHeight+3*dpToPx(3));
        if (getIntent().getBooleanExtra("refresh", false)) performGetDeviceOwners(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        setNeedHome(false);
        getMenuInflater().inflate(R.menu.add_user, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.action_add_user){
            startAddUserActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startAddUserActivity(){
        Intent intent = new Intent();
        intent.setClass(DeviceOwnersActivity.this, AddNewUserActivity.class);
        intent.putExtra(Consts.DEVICE_OWNER_ID, "asdas");
        startActivity(intent);
    }


    private void startHiddenRequestForAccessTime() {
        final DeviceOwnersInfoRequest request = new DeviceOwnersInfoRequest(this);
        request.setRetryPolicy(new DefaultRetryPolicy(0, 0, 0));
        contentManager.execute(request, new RequestListener<DeviceOwnersModel>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Log.d("grch11","hidden request fail");
            }

            @Override
            public void onRequestSuccess(DeviceOwnersModel o) {
                updateList(o.getDeviceOwners());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Settings.isNeedUpdateDeviceOwner()) {
            performGetDeviceOwners(false);
        }
    }

    private void performGetDeviceOwners(final boolean fromCache) {
        final DeviceOwnersRequest request = new DeviceOwnersRequest(this, fromCache);
        listener = new DeviceOwnersRequestListener(
                this,
                this,
                BaseRequest.HttpMethodEnum.get,
                request.getUrl(),
                request) {
            @Override
            public void onRequestSuccess(DeviceOwnersModel standartModel) {
                super.onRequestSuccess(standartModel);
                if (fromCache)
                    startHiddenRequestForAccessTime();
            }

            @Override
            public void onRequestFailure(SpiceException e) {
                super.onRequestFailure(e);
            }
        };
        contentManager.execute(request, listener);
    }


    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        DeviceOwnersModel devices = (DeviceOwnersModel) model;
        Settings.setNeedUpdateDeviceOwners(false);
        updateList(devices.getDeviceOwners());
    }

    private DeviceOwnerAdapter.AdapterItemListener mAdapterItemListener = new DeviceOwnerAdapter.AdapterItemListener() {
        @Override
        public void onRightBtnClick(int position) {
            listView.hideRightView();
            if (Utils.isOnline(DeviceOwnersActivity.this)) {
                long deviceOwnerId = ((DeviceOwner)adapter.getItem(position)).getId();
                performDeleteDeviceOwnerRequest(deviceOwnerId);
            } else {
                Toast.makeText(DeviceOwnersActivity.this, R.string.error_connection, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void performDeleteDeviceOwnerRequest(long deviceOwnerId){
        DeleteDeviceOwnerRequest request = new DeleteDeviceOwnerRequest(this, deviceOwnerId);

        contentManager.execute(request, new BaseRequestListener<DeviceOwnerModel>(this, request) {
            @Override
            public void onRequestFailure(SpiceException e) {
                super.onRequestFailure(e);
                Toast.makeText(DeviceOwnersActivity.this, getString(R.string.delete_user_error_message), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRequestSuccess(DeviceOwnerModel model) {
                super.onRequestSuccess(model);
                performGetDeviceOwners(false);
            }
        });
    }

    private void updateList(List<DeviceOwner> devices) {
        if (listView.getAdapter() == null) {
            adapter = new DeviceOwnerAdapter(this, devices, itemHeight, listView.getRightViewWidth(),mAdapterItemListener);
            listView.setAdapter(adapter);
        } else {
            //trick to pass new adapter and staying at the position
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter)listView.getAdapter();
            DeviceOwnerAdapter newAdapter = (DeviceOwnerAdapter)headerViewListAdapter.getWrappedAdapter();
            newAdapter.refill(devices);
//            ((DeviceOwnerAdapter)((HeaderViewListAdapter)listView.getAdapter()).getWrappedAdapter()).refill(devices);
        }

            swipeList.setRefreshing(false);
    }

    @Override
    public void performFailRequestAction(int action) {
        swipeList.setRefreshing(false);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onStart() {
        super.onStart();
        timerShouldRun = true;
        mHandler.postDelayed(updateAdapterTask, TIMER_INTERVAL);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacks(updateAdapterTask);
    }

    @Override
    protected void onPause() {
        super.onPause();
        timerShouldRun = false;
    }

}