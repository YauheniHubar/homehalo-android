package android.itransition.com.wedge.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.database.datesource.DeviceDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.DevicesEntity;
import android.itransition.com.wedge.gui.DeviceListItemView;
import android.itransition.com.wedge.gui.adapters.DevicesAdapter;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.DeviceModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.DeviceDeleteRequest;
import android.itransition.com.wedge.request.DeviceRequest;
import android.itransition.com.wedge.request.DeviceUpdateRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.listeners.DeviceRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by e.kazimirova on 01.09.2014.
 */
public class DevicesActivity  extends BaseActivity implements RequestAction,
        DeviceListItemView.DeviceMenuOptionClickListener {

    public static final int REQUEST_ASSIGN = 123;

    private SwipeRefreshLayout swipeList;
    private ListView smList;
    private DevicesEntity currentListItem;

    private DeviceDataSource datasource;

    private DevicesAdapter devicesAdapter;
    private boolean forgetRequest;
    private TextView emptyView;
    private boolean mIsForOwner;
    private String mOwnerName;
    private long mDeviceOwnerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        swipeList = (SwipeRefreshLayout) findViewById(R.id.devicesListSwipe);
        smList = (ListView) findViewById(R.id.devicesList);
        emptyView = (TextView) findViewById(R.id.no_device);

        setupSwipeMenuList();

        setupActionBar();
        //setBarColor(getResources().getColor(R.color.main_gray));
        setNeedHome(true);


        mIsForOwner = getIntent().getExtras().getBoolean(Consts.IS_DEVICE_OWNER);
        mDeviceOwnerId = getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID);
        if(!mIsForOwner) {
            setTitle(getString(R.string.devices_control));
        } else {
            mOwnerName = getIntent().getExtras().getString(Consts.DEVICE_TITLE);
            setTitle(mOwnerName);
        }
        datasource = new DeviceDataSource(MainSource.getInstance());

        if(!Settings.getUpdatingDevices()) {
            performDeviceRequest();
        } else {
            List<DevicesEntity> l = datasource.getAllDevices();
            if(getIntent().getExtras()!=null && mIsForOwner){
                l = filterDevicesByOwner(mDeviceOwnerId, l);
            }

            updateList(l);
        }
    }

    private void setupSwipeMenuList() {
//        smList.setEmptyView(emptyView);
        swipeList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override public void onRefresh() {
                performDeviceRequest();
            }
        });
    }


    private void updateList(List<DevicesEntity>  items) {
        sortBySeenDate(items);
        devicesAdapter = new DevicesAdapter(this, items,
                getIntent().getExtras().getBoolean(Consts.IS_DEVICE_OWNER), this);
        smList.setAdapter(devicesAdapter);
    }

    private void sortBySeenDate(List<DevicesEntity> items) {
        Collections.sort(items, new Comparator<DevicesEntity>() {
            @Override
            public int compare(DevicesEntity lhs, DevicesEntity rhs) {
                if (lhs.getLastOnlineAt() == null && rhs.getLastOnlineAt() == null)
                    return  0;
                if (rhs.getLastOnlineAt() == null)
                    return  1;
                if (lhs.getLastOnlineAt() == null)
                    return  -1;

                return lhs.getLastOnlineAt().getDate().compareTo(rhs.getLastOnlineAt().getDate());
            }
        });
    }

    public List<DevicesEntity> filterDevicesByOwner(long deviceOwner, List<DevicesEntity> devices){
        List<DevicesEntity> devicesEntities = new ArrayList<DevicesEntity>();
        for(int i=0; i<devices.size(); i++){
            if(devices.get(i).getDeviceOwner().getId()==deviceOwner){
                devicesEntities.add(devices.get(i));
            }
        }

        return devicesEntities;
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        swipeList.setRefreshing(false);

        DeviceModel result = ((DeviceModel)model);
        if (result == null || result.getDevices() == null) return;


        if (mIsForOwner) {
            result.setDevices(filterDevicesByOwner(getIntent().getExtras().getLong(
                    Consts.DEVICE_OWNER_ID), result.getDevices()));
        }

        if (result.getDevices().size() > 0) {
            updateList(result.getDevices());
        } else {
            findViewById(R.id.no_device).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void performFailRequestAction(int action) {
        swipeList.setRefreshing(false);
    }

    private boolean validateNewTitle(String newTitle, String oldTitle) {
        if (newTitle.equals("")) {
            Utils.makeToast(R.string.device_name_short);

        } else if (newTitle.length() <= 2) {
            Utils.makeToast(R.string.device_name_short);
        } /*else if (newTitle.length() > 100) {
            Utils.makeToast("Sorry, device name should be maximum 100 characters");
        } else if (oldTitle.toLowerCase().trim().equals(newTitle.toLowerCase())) {
            Utils.makeToast("You put the same name");
        }*/ else {
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ASSIGN && resultCode == RESULT_OK && data != null) {
            long newOwner = data.getLongExtra(Consts.DEVICE_OWNER_ID, mDeviceOwnerId);
            String newOwnerName = data.getStringExtra(Consts.OWNER_NAME);
            if (currentListItem != null && newOwner != mDeviceOwnerId) {
                if (mIsForOwner) {
                    devicesAdapter.remove(currentListItem);
                } else {
                    currentListItem.getDeviceOwner().setId(newOwner);
                    currentListItem.getDeviceOwner().setTitle(newOwnerName);
                    devicesAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void performDeviceRequest(){
        if (!Utils.isOnline(this)) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            return;
        }

        DeviceRequest request = new DeviceRequest(this);
        contentManager.execute(request,
                new DeviceRequestListener(this,  this, BaseRequest.HttpMethodEnum.get,
                        request.getUrl(), request));
    }

    private void performEditRequest(final DevicesEntity entity, String title){
        if (!Utils.isOnline(this)) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            return;
        }
        DeviceUpdateRequest request = new DeviceUpdateRequest(this, Settings.getUserId(),
                entity.getId(),
                title,  entity.getEnabled(),
                entity.getFavourite(),
                entity.isOnline(),
                entity.getMacAddress(),
                entity.getCreateAt(),
                entity.getDeviceOwner().getId());
            contentManager.execute(request,
                new BaseRequestListener<DeviceModel>(this, request) {
                    @Override
                    public void onRequestSuccess(DeviceModel o) {
                        super.onRequestSuccess(o);
//                        entity.inEditMode = false;
                        entity.setTitle(o.getDevice().getTitle());
                        devicesAdapter.notifyDataSetChanged();
                    }
                });
    }



    @Override
    public void onBackPressed(){
//        Bundle extras = getIntent().getExtras();
//        if(extras != null && extras.getBoolean(Consts.IS_DEVICE_OWNER)) {
//            Intent intent = new Intent();
//            intent.setClass(getApplicationContext(), OneUserDesignAcrivity.class);
//            intent.putExtras(extras);
//            startActivity(intent);
//            finish();
//        } else {
//            Intent intent = new Intent();
//            intent.setClass(getApplicationContext(), ManagementActivity.class);
//            startActivity(intent);
//            finish();
//        }
        super.onBackPressed();
    }

    @Override
    public void onSwipeButtonClick(DeviceListItemView view) {
        int position = view.getPosition();
        DevicesEntity item = devicesAdapter.getItem(position);
        item.inEditMode = !item.inEditMode;
    }

    @Override
    public void onInfoExpand(DeviceListItemView view) {
        int position = view.getPosition();
        DevicesEntity item = devicesAdapter.getItem(position);
        item.isExpanded = !item.isExpanded;
    }

    @Override
    public void onForgetClick(final DeviceListItemView view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getString(R.string.delete));
        alert.setMessage(getString(R.string.sure));

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                forgetRequest = true;
                final DevicesEntity item = devicesAdapter.getItem(view.getPosition());
                DeviceDeleteRequest request = new DeviceDeleteRequest(DevicesActivity.this, Settings.getUserId(),
                        item.getId());
                contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                        new BaseRequestListener<DeviceModel>(DevicesActivity.this, request) {
                            @Override
                            public void onRequestSuccess(DeviceModel o) {
                                super.onRequestSuccess(o);
                                devicesAdapter.remove(item);
                            }
                        });
                dialog.dismiss();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();

    }

    @Override
    public void onAssignClick(DeviceListItemView view) {
        currentListItem = devicesAdapter.getItem(view.getPosition());

        Intent intent = new Intent(getApplicationContext(), AssignUserActivity.class);
        intent.putExtra(Consts.IS_ALERTS, false);
        intent.putExtra(Consts.DEVICE_ID, currentListItem.getId());
        intent.putExtra(Consts.DEVICE_TITLE, currentListItem.getTitle());
        intent.putExtra(Consts.MAC_ADDRESS, currentListItem.getMacAddress());
        intent.putExtra(Consts.MANUFACTURER_TITLE, currentListItem.getManufacturedTitle());
        intent.putExtra(Consts.DEVICE_OWNER_ID, currentListItem.getDeviceOwner().getId());
        try {
            if (getIntent().getExtras() != null) {
                intent.putExtra(Consts.IS_DEVICE_OWNER, getIntent().getExtras().getBoolean(Consts.IS_DEVICE_OWNER));

            }
        } catch(NullPointerException ex){
            intent.putExtra(Consts.IS_DEVICE_OWNER, false);
        }

        startActivityForResult(intent, REQUEST_ASSIGN);
    }

    @Override
    public void onEditOkClick(DeviceListItemView view, String newTitle) {
        DevicesEntity item = devicesAdapter.getItem(view.getPosition());
        if (validateNewTitle(newTitle, item.getTitle())) {
                performEditRequest(item, newTitle);
                item.setTitle(newTitle);
                devicesAdapter.notifyDataSetChanged();
            Utils.hideSoftKeyboard(this, view);
        }
    }
}