package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.TimeExtensions;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.gui.CircularSeekBarNew;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.TimeExtensionModel;
import android.itransition.com.wedge.request.GetAccessByExtension;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.timeextension.DeleteTimeExtensionRequest;
import android.itransition.com.wedge.request.timeextension.TimeExtensionsRequest;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.octo.android.robospice.exception.RequestCancelledException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.retry.DefaultRetryPolicy;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by e.kazimirova on 03.09.2014.
 */
public class ExtendTimeActivity extends BaseActivity implements View.OnClickListener {

    public static final int DEFAULT_TIME = 31;

    private TimeExtensions timeExtension;

    private Button buttonBlock, buttonCancel;
    private TextView tvAccessUntil, tvTimeDuration;
    private CircularSeekBarNew seekBar;
    private long deviceOwnerId = 0;
    private TimeProfiles[] time;
    private HomeWorkCategories[] homeworkCategories;
    private boolean isAlerts = false;
    private Date endAt;
    private DeviceOwner deviceOwner;
    private boolean isActive;

    private GetAccessTextListener remainTimeRequestListener = new GetAccessTextListener();
    private DefaultRetryPolicy noRetryPolicy = new DefaultRetryPolicy(0, 0, 0);
    private Runnable getAccessTextRunnable = new Runnable() {
        @Override public void run() {
            updateAccessText();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extendtime);

        isAlerts = getIntent().getExtras().getBoolean(Consts.IS_ALERTS);

        setupActionBar();
        setTitle( getIntent().getExtras().getString(Consts.DEVICE_TITLE));


        buttonBlock = (Button) findViewById(R.id.buttonBlock);
        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        seekBar = (CircularSeekBarNew) findViewById(R.id.seekBar);
        tvAccessUntil = (TextView) findViewById(R.id.tvAccessUntil);
        tvTimeDuration = ((TextView) findViewById(R.id.tvMins));
        buttonBlock.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);

        deviceOwnerId = getIntent().getLongExtra(Consts.DEVICE_OWNER_ID, -1);
        timeExtension = getIntent().getParcelableExtra(Consts.TIME_EXTENSION);


        seekBar.setOnSeekBarChangeListener(new CircularSeekBarNew.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBarNew circularSeekBar, int progress, boolean fromUser) {
                int progressInMins = circularSeekBar.getProgressInMins();
                updateBlockButtonText(progressInMins);
                Utils.updateSliderText(ExtendTimeActivity.this, seekBar.getProgressInMins(),
                        0, tvTimeDuration);
                seekBar.removeCallbacks(getAccessTextRunnable);
                seekBar.postDelayed(getAccessTextRunnable, 1000);
            }
            @Override public void onStopTrackingTouch(CircularSeekBarNew seekBar) {
            }
            @Override public void onStartTrackingTouch(CircularSeekBarNew seekBar) {
            }
        });


        parseIntent();

        setupUi();
    }

    private void setupUi() {
        buttonCancel.setVisibility(GONE);
        tvAccessUntil.setVisibility(VISIBLE);
        int minutes = DEFAULT_TIME;
        if (timeExtension != null && timeExtension.getDuration() > 0 && timeExtension.getStartedAt() != null) {
            CreateTimeUser startedAt = timeExtension.getStartedAt();
//            Date now = Utils.getNow(WedgeApplication.currentUser.getWedge().getTimezone()); //old realization
            Date now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
            endAt = new Date(startedAt.getDate().getTime() + timeExtension.getDuration() * 1000);
            if (endAt.after(now)) {
                isActive = true;
                buttonBlock.setText(R.string.activate);
                buttonCancel.setVisibility(VISIBLE);
                if (startedAt.getDate().after(now)) { //starts in future
                    minutes = (int) (timeExtension.getDuration() / 60);
                } else {
                    minutes = (int) ((endAt.getTime() - now.getTime()) / (60 * 1000));
                }
            }
        }

        seekBar.setProgressInMins(minutes);
        updateBlockButtonText(seekBar.getProgressInMins());
        Utils.updateSliderText(ExtendTimeActivity.this, seekBar.getProgressInMins(),
                0, tvTimeDuration);
    }

    private void updateAccessText() {
        int duration = seekBar.getProgressInMins() * 60;
        long terId = 0;
        if (duration < 0)
            duration = 0;
        Date startAt = null;
        if (isTimeExtensionActive() && timeExtension.getStartedAt() != null) {
            terId = timeExtension.getTerId();
            startAt = timeExtension.getStartedAt().getDateUTC();
        }
        GetAccessByExtension request
                = new GetAccessByExtension(this, deviceOwnerId, duration, terId, startAt);
        request.setRetryPolicy(noRetryPolicy);

//        tvAccessUntil.setText("has access until: (calculating...)");
        tvAccessUntil.setText(getString(R.string.has_access_until, getString(R.string.calculating)));

        remainTimeRequestListener.cancel();
        remainTimeRequestListener.setRequest(request);
        contentManager.execute(request, remainTimeRequestListener);
    }

    private void parseIntent() {
        time = new TimeProfiles[0];
        homeworkCategories = new HomeWorkCategories[0];
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            Parcelable[] tv = (Parcelable[]) bundle.getParcelableArray(Consts.TIME_PROFILESES);
            if (tv != null) {
                time = new TimeProfiles[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    time[i] = (TimeProfiles) tv[i];
                }
            }

            tv = (Parcelable[]) bundle.getParcelableArray(Consts.HOMEWORK_CATEGORIES);

            if (tv != null) {
                homeworkCategories = new HomeWorkCategories[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    homeworkCategories[i] = (HomeWorkCategories) tv[i];
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (deviceOwner == null && deviceOwnerId > 0) {
            getDataFromSqlite(deviceOwnerId);
        }
    }

    private void getDataFromSqlite(final long ownerId) {
        contentManager.execute(new SpiceRequest<DeviceOwner>(DeviceOwner.class) {
            @Override
            public DeviceOwner loadDataFromNetwork() throws Exception {
                DeviceOwnersDataSource source = new DeviceOwnersDataSource(WedgeApplication.mainSource);
                return source.getDeviceOwnerById(ownerId);
            }
        }, new BaseRequestListener<DeviceOwner>(this, null) {
            @Override
            public void onRequestSuccess(DeviceOwner o) {
                super.onRequestSuccess(o);
                deviceOwner = o;
                updateAccessText();
            }
        });
    }

    private void updateBlockButtonText(int progressInMin) {
        if (isActive) {
            buttonBlock.setText(R.string.activate);
        } else {
            int h = progressInMin / 60;
            String buttonText = null;
            if (progressInMin > 0) {
                if (h > 0) {
                    if (h >= 2) {
                        buttonText = getString(R.string.extend_for) + " " + h + " h";
                    } else if (progressInMin - h * 60 != 0) {
                        buttonText = getString(R.string.extend_for) + " "
                                + h + " h " + (progressInMin - h * 60) + " m";
                    } else {
                        buttonText = getString(R.string.extend_for) + " " + h + " h";
                    }
                } else {
                    buttonText = getString(R.string.extend_for) + " " + progressInMin + getString(R.string.minutes);
                }
            } else if (progressInMin <= 0) {
                buttonText = getString(R.string.cancel_now);
            }
            if (buttonText != null) buttonBlock.setText(buttonText);
        }
    }



    public static Date getCurrentDateWithTz(String timezone) {
        Calendar calendarCurrent = Calendar.getInstance();
        Date current = calendarCurrent.getTime();
        Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(
                timezone
        ));
        cal1.setTime(current);
        Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
        cal2.clear();
        cal2.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
        cal2.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
        cal2.set(Calendar.DATE, cal1.get(Calendar.DATE));
        cal2.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
        cal2.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
        current = cal2.getTime();
        return current;
    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()){
            case R.id.imageViewUpdate:
                finish();
                intent.setClass(getApplicationContext(), ManagementActivity.class);
                startActivity(intent);
                break;
            case R.id.imageViewHome:
            case R.id.textViewBack:
            case R.id.textViewMain:
                if(!isAlerts) {
                    onBackPressed();
                } else {
                    super.onBackPressed();
                }
                break;
            case R.id.buttonBlock: {
                if (timeExtension != null) Log.d("extension: ", timeExtension.toString());

                int progressInMin = seekBar.getProgressInMins();

                if (progressInMin <= 0) {
                    if (isAlerts) {
                        deleteTimeExtension(timeExtension.getId(), deviceOwnerId);
                    } else {
                        if (!isTimeExtensionActive()) {
                            onBackPressed();
                        } else {
                            deleteTimeExtension(timeExtension.getId(), deviceOwnerId);
                        }
                    }
                    return;
                }

                if (!isAlerts) {
                    if (isEndTimeInFuture(progressInMin)) {
                        performSaveTimeExtends();
                    } else {
                        deleteTimeExtension(timeExtension.getId(), deviceOwnerId);
                    }
                } else {
                    TimeExtensionsRequest requestTime = new TimeExtensionsRequest(this,
                            deviceOwnerId,
                            progressInMin * 60);
                    contentManager.execute(requestTime,
                            new BaseRequestListener<TimeExtensionModel>(this, requestTime) {
                                @Override
                                public void onRequestSuccess(TimeExtensionModel res) {
                                    super.onRequestSuccess(res);
                                    setResult(RESULT_OK);
                                    finish();
                                }
                            });
                }
            }
                break;
            case R.id.buttonCancel: {
                long teId = getIntent().getExtras().getLong(Consts.TIME_EXTENSIONSES_ID);
                if (isAlerts) {
                    deleteTimeExtension(teId, deviceOwnerId);
                } else {
                    if (!isTimeExtensionActive()) {
                        onBackPressed();
                    } else {
                        deleteTimeExtension(timeExtension.getId(), deviceOwnerId);
                    }
                }
            }
            break;
        }
    }

    void deleteTimeExtension(long teId, long deviceOwnerId) {
        DeleteTimeExtensionRequest request = new DeleteTimeExtensionRequest(this, deviceOwnerId, teId);
        contentManager.execute(request, new BaseRequestListener<BaseModel>(this, request) {
            @Override
            public void onRequestSuccess(BaseModel baseModel) {
                super.onRequestSuccess(baseModel);
//                Utils.makeToast(R.string.canceled_extension);
                if (isAlerts) {
                    setResult(RESULT_OK);
                    finish();
                }
                else goToDeviceOwnerActivity();
            }
            @Override public void onRequestFailure(SpiceException e) {
                if(e.getCause() instanceof HttpClientErrorException) {
                    HttpClientErrorException exception = (HttpClientErrorException)e.getCause();
                    if(exception.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                        Utils.makeToast(R.string.canceled_extension);
                        setResult(RESULT_OK);
                        finish();
                        setShowToast(false);
                    }
                }
                super.onRequestFailure(e);
            }
        });
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
//        if (isAlerts) return;
//
//
//        Intent intent = new Intent();
//        intent.setClass(getApplicationContext(), OneUserActivity.class);
//        intent.putExtras(getIntent());
//        startActivity(intent);
//        finish();
    }



   private void  performSaveTimeExtends() {
        Date current = getCurrentDateWithTz(
                ((CreateTimeUser) getIntent().getExtras().getParcelable(Consts.CREATED_AT)).getTimeZone());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String reportDate = df.format(current);
        CreateTimeUser startedAt = new CreateTimeUser();
        startedAt.setDateTime(reportDate);
       TimeExtensionsRequest request = null;
       int progressInMin = seekBar.getProgressInMins();

       progressInMin = Utils.roundHours(progressInMin);
       if(!isTimeExtensionActive()){
           request = new TimeExtensionsRequest(this,
                   getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID),
                   progressInMin *60);
       } else {
           request = new TimeExtensionsRequest(this,
                   getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID),
                   progressInMin *60,
                   timeExtension.getId()
           );
       }


        contentManager.execute(request,
                new BaseRequestListener<TimeExtensionModel>(this, request) {
                    @Override
                    public void onRequestSuccess(TimeExtensionModel o) {
                        super.onRequestSuccess(o);
                        Utils.makeToast("Extension Applied");
//                        Settings.setNeedUpdateDeviceOwners(false);
                        goToDeviceOwnerActivity();
                    }
                });
    }

    private void goToDeviceOwnerActivity() {
        Intent intent = new Intent(getApplicationContext(), DeviceOwnersActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    boolean isTimeExtensionActive() {
        if (timeExtension == null) return false;

        //extension from notification not started yet
        if (timeExtension.getStartedAt() == null || timeExtension.getStartedAt().getDate() == null)
            return true;

//        Date now = Utils.getNow(WedgeApplication.currentUser.getWedge().getTimezone()); // old realization
        Date now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
        Date date = new Date(timeExtension.getStartedAt().getDate().getTime() + timeExtension.getDuration() * 1000);

        return date.after(now);
    }

    boolean isEndTimeInFuture(long min) {
        if (timeExtension == null) return true;

        if (timeExtension.getStartedAt() == null || timeExtension.getStartedAt().getDate() == null)
            return true;

        Date now = Utils.getNow(timeExtension.getStartedAt().getTimeZone());

        Date date = new Date(timeExtension.getStartedAt().getDate().getTime() + min * 60 * 1000);

        return date.after(now);
    }

    class GetAccessTextListener implements RequestListener<TimeExtensionModel>{
        private  GetAccessByExtension request;

        public void setRequest(GetAccessByExtension request) {
            this.request = request;
        }

        public void cancel() {
            if (request != null && !request.isCancelled()) {
                request.cancel();
                request = null;
            }
        }

        @Override
        public void onRequestFailure(SpiceException e) {
            request = null;
            if (!(e instanceof RequestCancelledException)) {
                tvAccessUntil.setText(R.string.access_unknown);
            }
        }

        @Override
        public void onRequestSuccess(TimeExtensionModel o) {
            request = null;
            if (o.access != null) {
                tvAccessUntil.setText(o.access.getHasAccessUntilLabel());
                buttonBlock.setEnabled(true);
            } else {
                tvAccessUntil.setText(getString(R.string.extension_declined));
                buttonBlock.setEnabled(false);
            }
        }
    }
}