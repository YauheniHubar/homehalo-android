package android.itransition.com.wedge.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.request.HelpRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by e.kazimirova on 29.08.2014.
 */
public class HelpActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private EditText editProblem;
    private EditText editEmail;

    private Button btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);


        setupActionBar();
        setTitle(getString(R.string.contact_us));


        editProblem = (EditText) findViewById(R.id.editTextProblem);

        editEmail = (EditText) findViewById(R.id.editTextEmail);
        editEmail.setText(Settings.getEmail());
        btnCreate = (Button) findViewById(R.id.buttonSubmit);

        btnCreate.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewHome:
                finish();
                break;
            case R.id.textViewBack:
                finish();
                break;
            case R.id.buttonSubmit:
                if(!editProblem.getText().toString().equals("") &&
                !editEmail.getText().toString().equals("")) {
                    performHelpRequest();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(getString(R.string.error));
                    alert.setMessage(getString(R.string.empty));

                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
                break;
            case R.id.imageViewUpdate:
                finish();
                break;
        }

    }
    @Override
    protected void onStart(){
        super.onStart();

    }



    @Override
    protected void onPause(){
        super.onPause();
    }




    private void performHelpRequest(){
        if (!Utils.isOnline(this)) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            return;
        }
        HelpRequest request = new HelpRequest(this, Settings.getUserId(), editProblem.getText().toString(),
                editEmail.getText().toString());
        contentManager.execute(request,
                new BaseRequestListener<String>(this, request) {
                    @Override
                    public void onRequestSuccess(String o) {
                        super.onRequestSuccess(o);
                        Utils.makeToast(R.string.thanks_feedback);
                        finish();
                    }
                });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
