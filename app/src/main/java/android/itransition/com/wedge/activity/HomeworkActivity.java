package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.database.datesource.HomeworkCategoriesDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.TimeExtensions;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.gui.CircularSeekBarHomeWork;
import android.itransition.com.wedge.gui.CircularSeekBarNew;
import android.itransition.com.wedge.gui.com.kyleduo.switchbutton.switchbutton.SwitchButton;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.HomeworkCategoriesModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.HomeworkCategoriesRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.UpdateDeviceRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.listeners.UpdateDeviceRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.view.View.inflate;

/**
 * Created by e.kazimirova on 03.09.2014.
 */
public class HomeworkActivity extends BaseActivity implements View.OnClickListener, RequestAction {

    private static int DEFAULT_TIME = 60;
    public static final int TIMER_PERIOD = 15 * 1000;

    private Button btnSave, buttonCancel;
    private TextView tvRemainTime, tvMins, tvTimeDuration;
    private LinearLayout listViewCategories;
    private CreateTimeUser homeWorkStartedAt;
    private CircularSeekBarHomeWork seekBarHomework;

    private long duration;
    private TimeProfiles[] time;
    private HomeWorkCategories[] homeworkCategoriesList;
    private HomeWorkCategories[] selectedCategories;
    private HomeworkCategoriesDataSource datasource;

    private MainSource mainsource;

    private TimeExtensions timeExtension;
    private boolean[] listActiveBoolean;


    /**
     * *********************************************
     */
    private boolean timerShouldRun;
    private Timer timer;
    private TimerTask updateAdapterTask = new TimerTask() {
        @Override
        public void run() {
            Log.d("Timer", "tick");
            if (timerShouldRun) {
                mHandler.obtainMessage().sendToTarget();
            }
        }
    };

    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (timerShouldRun) {
//                Date now = Utils.getNow(WedgeApplication.currentUser.getWedge().getTimezone()); // old realiztion
                Date now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
                long remainMin = (endAt.getTime() - now.getTime()) / (60 * 1000);

                updateRemainText(remainMin);
            }
        }
    };
    private Date endAt;
    private boolean isActive;

    /**
     * *************************
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework);


        btnSave = (Button) findViewById(R.id.btnSave);
        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        listViewCategories = (LinearLayout) findViewById(R.id.listViewCategories);
        seekBarHomework = (CircularSeekBarHomeWork) findViewById(R.id.seekBar);
        tvRemainTime = (TextView) findViewById(R.id.textViewActive);
        tvMins = (TextView) findViewById(R.id.tvMins);
        tvTimeDuration = ((TextView) findViewById(R.id.tvMins));



        homeWorkStartedAt = getIntent().getExtras().getParcelable(Consts.HOMEWORK_MODE_STARTED_AT);
        duration = getIntent().getExtras().getLong(Consts.HOMEWORK_MODE_DURATION);



        parseIntent();

        mainsource = MainSource.getInstance();
        datasource = new HomeworkCategoriesDataSource(this, mainsource);
        listActiveBoolean = new boolean[selectedCategories.length];

        setupUi();

        if (!Settings.getUpdatingHW()) {
            performGetCategories();
            datasource.deleteHwCategoriesAll();
        } else {
            List<HomeWorkCategories> l = datasource.getAllHwCategories();
            if (l.size() != 0) {
                homeworkCategoriesList = l.toArray(new HomeWorkCategories[l.size()]);
                listViewCategories.removeAllViews();
                createCategoriesListView();
            } else {
                performGetCategories();
                datasource.deleteHwCategoriesAll();
            }

        }

        setupActionBar();
        setTitle(getIntent().getExtras().getString(Consts.DEVICE_TITLE));


    }

    private void setupUi() {
        btnSave.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        buttonCancel.setVisibility(GONE);
        tvRemainTime.setVisibility(GONE);

        seekBarHomework.setOnSeekBarChangeListener(new CircularSeekBarHomeWork.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBarNew circularSeekBar, int progress, boolean fromUser) {
               Utils.updateSliderText(HomeworkActivity.this, circularSeekBar.getProgressInMins(),
                        0, tvTimeDuration);
            }
            @Override
            public void onStopTrackingTouch(CircularSeekBarNew seekBar) { }
            @Override
            public void onStartTrackingTouch(CircularSeekBarNew seekBar) { }
        });

        if (duration == 0 || homeWorkStartedAt == null) {
            seekBarHomework.setProgressInMins(DEFAULT_TIME);
        } else {
//            Date now = Utils.getNow(WedgeApplication.currentUser.getWedge().getTimezone()); //old realization
            Date now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
            endAt = new Date(homeWorkStartedAt.getDate().getTime() + duration * 1000);
            if (endAt.before(now)) {
                seekBarHomework.setProgressInMins(DEFAULT_TIME);
            } else {
                isActive = true;
                btnSave.setText(R.string.apply);
                buttonCancel.setVisibility(VISIBLE);
                tvRemainTime.setVisibility(VISIBLE);
                tvRemainTime.setTextSize(16);

                long remainMin = (endAt.getTime() - now.getTime()) / (60 * 1000);
                seekBarHomework.setProgressInMins((int) remainMin);

                updateRemainText(remainMin);

                timer = new Timer("remain_min_counter");
                timer.schedule(updateAdapterTask, 0, TIMER_PERIOD);
            }
        }
        Utils.updateSliderText(HomeworkActivity.this, seekBarHomework.getProgressInMins(),
                0, tvTimeDuration);
    }

    @Override
    protected void onResume() {
        super.onResume();
        timerShouldRun = true;
        if (timer != null) {
            mHandler.obtainMessage().sendToTarget();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        timerShouldRun = true;
    }

    private void updateRemainText(long remainMin) {
        if (remainMin > 0) {
            String remainText;
            if (remainMin >= 120) {
                remainMin = remainMin / 60;
                remainText = remainMin + " hours";
            }  else {
                remainText = remainMin + " minutes";
            }
            String text = getString(R.string.homework_mode_active, Utils.getTimeString(remainMin));
            SpannableString ss = new SpannableString(text);
            ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)), 14, text.length(), 0);
            tvRemainTime.setText(ss);
        }
    }


    private void performGetCategories() {
        HomeworkCategoriesRequest request = new HomeworkCategoriesRequest(this);
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new BaseRequestListener<HomeworkCategoriesModel>(this, request) {
                    @Override
                    public void onRequestSuccess(HomeworkCategoriesModel baseModel) {
                        super.onRequestSuccess(baseModel);
                        homeworkCategoriesList = new HomeWorkCategories
                                [((HomeworkCategoriesModel) baseModel).getCategorieses().length];
                        homeworkCategoriesList = ((HomeworkCategoriesModel) baseModel).getCategorieses();
                        for (int i = 0; i < homeworkCategoriesList.length; i++) {
                            datasource.createContentProfile(homeworkCategoriesList[i]);
                        }
                        Settings.setUpdatingHW(true);
                        listViewCategories.removeAllViews();
                        createCategoriesListView();


                    }
                });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewUpdate:
                finish();
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), ManagementActivity.class);
                startActivity(intent);
                break;
            case R.id.imageViewHome:
                onBackPressed();
                break;
            case R.id.textViewBack:
                onBackPressed();
                break;
            case R.id.textViewMain:
                onBackPressed();
                break;
            case R.id.btnSave:
                duration = seekBarHomework.getProgressInMins() * 60;

                CreateTimeUser startedAt = getCreateTimeUser();
                homeWorkStartedAt = startedAt;

                List<HomeWorkCategories> hmResult = getCheckedCategories();
                if (hmResult.size() == 0) {
                    Toast.makeText(this, R.string.homework_block_categories_error, Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    selectedCategories = hmResult.toArray(new HomeWorkCategories[hmResult.size()]);
                    performUpdate(duration, startedAt);
                    break;
                }
            case R.id.buttonCancel:
                duration = 0;
                homeWorkStartedAt = getCreateTimeUser();
                performUpdate(0, null);
                break;
        }
    }

    private List<HomeWorkCategories> getCheckedCategories() {
        List<HomeWorkCategories> categories = new ArrayList<>();
        for (int i = 0; i < listActiveBoolean.length; i++) {
            if (listActiveBoolean[i]) {
                categories.add(homeworkCategoriesList[i]);
            }
        }

        return categories;
    }

    private CreateTimeUser getCreateTimeUser() {
        Date current = getCurrentDateWithTz();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String reportDate = df.format(current);
        CreateTimeUser startedAt = new CreateTimeUser();
        startedAt.setDateTime(reportDate);
        startedAt.setTimeZone(((CreateTimeUser) getIntent().getExtras().getParcelable(Consts.CREATED_AT))
                .getTimeZone());
        return startedAt;
    }

    private void performUpdate(long homeworkModeDuration, CreateTimeUser homeWorkStartedAt) {
        if (!Utils.isOnline(this)) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            return;
        }
        UpdateDeviceRequest request = new UpdateDeviceRequest(this, Settings.getUserId(),
                getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID), getIntent().getExtras().getString(Consts.DEVICE_TITLE),
                getIntent().getExtras().getString(Consts.DEVICE_STATUS), getIntent().getExtras().getLong(Consts.TIME),
                (CreateTimeUser) getIntent().getExtras().getParcelable(Consts.BLOCK_STARTED_AT),
                homeWorkStartedAt,
                homeworkModeDuration,
                getIntent().getExtras().getLong(Consts.CONTENT_PROFILE_ID), time, selectedCategories,
                (CreateTimeUser) getIntent().getExtras().getParcelable(Consts.CREATED_AT), null);
        contentManager.execute(request,
                new UpdateDeviceRequestListener(this, this, BaseRequest.HttpMethodEnum.put,
                        request.getUrl(), request));
    }

    private void parseIntent() {
        time = new TimeProfiles[0];
        selectedCategories = new HomeWorkCategories[0];
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            Parcelable[] tv = (Parcelable[]) bundle.getParcelableArray(Consts.TIME_PROFILESES);
            if (tv != null) {
                time = new TimeProfiles[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    time[i] = (TimeProfiles) tv[i];
                }
            }

            tv = (Parcelable[]) bundle.getParcelableArray(Consts.HOMEWORK_CATEGORIES);

            if (tv != null) {
                selectedCategories = new HomeWorkCategories[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    selectedCategories[i] = (HomeWorkCategories) tv[i];
                }
            }

            timeExtension = bundle.getParcelable(Consts.TIME_EXTENSION);

        }


    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        if (duration == 0) {
            DeviceOwnersDataSource hwCategoriesDataSource = new DeviceOwnersDataSource(mainsource);
            int deviceOwnerId = (int) getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID);
            hwCategoriesDataSource.deleteHomework(deviceOwnerId);
            for (int k = 0; k < selectedCategories.length; k++) {
                 hwCategoriesDataSource.setHomeworkCategories(homeworkCategoriesList[k], deviceOwnerId);
            }
//            Utils.makeToast(getString(R.string.canceled_homework));
        } /*else {
            Utils.makeToast(getString(R.string.activated_homework));
        }*/
        goToDeviceOwnerActivity();
    }

    private void goToDeviceOwnerActivity() {
        Intent intent = new Intent(getApplicationContext(), DeviceOwnersActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void performFailRequestAction(int action) {

    }

    private Date getCurrentDateWithTz() {

        Calendar calendarCurrent = Calendar.getInstance();
        Date current = calendarCurrent.getTime();
        try {
            Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(((CreateTimeUser)
                    (getIntent().getExtras().getParcelable(Consts.CREATED_AT))).getTimeZone()));
            cal1.setTime(current);
            Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
            cal2.clear();
            cal2.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
            cal2.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
            cal2.set(Calendar.DATE, cal1.get(Calendar.DATE));
            cal2.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
            cal2.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
            current = cal2.getTime();
        } catch (NullPointerException ex) {
        }
        return current;
    }


    private void createCategoriesListView() {
        if (homeworkCategoriesList != null) {

            listActiveBoolean = new boolean[homeworkCategoriesList.length];
            for (int k = 0; k < homeworkCategoriesList.length; k++) {
                if (isActive) {
                    for (int i = 0; i < selectedCategories.length; i++) {
                        if (homeworkCategoriesList[k].getCategoryId() == selectedCategories[i].getCategoryId()) {
                            listActiveBoolean[k] = true;
                            break;
                        }
                    }
                } else listActiveBoolean[k] = true;
            }


            for (int i = 0; i < homeworkCategoriesList.length; i++) {
                listViewCategories.addView(setupCategoriesViewItem(listActiveBoolean, homeworkCategoriesList, i));
            }

        }
    }

    private View setupCategoriesViewItem(final boolean[] listActiveBoolean, HomeWorkCategories[] homeworkCategoriesList, int i){

//        RelativeLayout linearLayout = (RelativeLayout)inflate(HomeworkActivity.this, R.layout.homework_category_item, null);
//        TextView categoryItemName = (TextView)linearLayout.findViewById(R.id.categoryName);
//        final ImageView categoryCross = (ImageView)linearLayout.findViewById(R.id.categoryDisable);
//
//        categoryItemName.setTextColor(getResources().getColor(R.color.grey));
//        categoryItemName.setText(homeworkCategoriesList[i].getTitle());
//        linearLayout.setTag(i);
//
//        if (isActive) {
//            //linearLayout.setPressed(listActiveBoolean[i]);
//            setupCurrentCategoryLinearLayout(!listActiveBoolean[i], linearLayout, categoryCross);
//        } else {
//            setupCurrentCategoryLinearLayout(true, linearLayout, categoryCross);
//        }
//        linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean previousValue = listActiveBoolean[((int) v.getTag())];
//                setupCurrentCategoryLinearLayout(previousValue, v, categoryCross);
//                listActiveBoolean[((int) v.getTag())] = !previousValue;
//            }
//        });
//
//        return linearLayout;
        final RelativeLayout linearLayout = (RelativeLayout)inflate(HomeworkActivity.this, R.layout.homework_category_new_item, null);
        TextView categoryItemName = (TextView)linearLayout.findViewById(R.id.categoryName);
        final SwitchButton categorySwitch = (SwitchButton)linearLayout.findViewById(R.id.category_switch);

//        categoryItemName.setTextColor(getResources().getColor(R.color.grey));
        categoryItemName.setText(homeworkCategoriesList[i].getTitle());
        linearLayout.setTag(i);

        if (isActive) {
            setupCurrentCategoryLinearLayout(!listActiveBoolean[i], linearLayout, categorySwitch);
        } else {
            categorySwitch.setChecked(false);
        }

        categorySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean previousValue = listActiveBoolean[((int) linearLayout.getTag())];
//                setupCurrentCategoryLinearLayout(previousValue, linearLayout, categorySwitch);
                listActiveBoolean[((int) linearLayout.getTag())] = !previousValue;

                if (areAllTrue(listActiveBoolean)) {
                    btnSave.setEnabled(false);
                    btnSave.setText(R.string.block_at_least_one_category);
                } else {
                    btnSave.setEnabled(true);
                    btnSave.setText(R.string.start_homework);
                }
            }
        });

//        if (!isActive) categorySwitch.setChecked(true);

        return linearLayout;

    }

    public static boolean areAllTrue (boolean[] array) {
        for(boolean b : array) if(b) return false;
        return true;
    }

    private void setupCurrentCategoryLinearLayout(boolean isPressed, View linearLayout, SwitchButton switchButton){
        if (isPressed) {
            switchButton.setChecked(true);
        } else {
            switchButton.setChecked(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent();
//        intent.setClass(getApplicationContext(), OneUserActivity.class);
//        intent.putExtra(Consts.DEVICE_OWNER_ID, getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID));
//        intent.putExtra(Consts.DEVICE_TITLE, getIntent().getExtras().getString(Consts.DEVICE_TITLE));
//        intent.putExtra(Consts.DEVICE_STATUS, getIntent().getExtras().getString(Consts.DEVICE_STATUS));
//        intent.putExtra(Consts.TIME, getIntent().getExtras().getLong(Consts.TIME));
//        intent.putExtra(Consts.HOMEWORK_MODE_DURATION, duration);
//        intent.putExtra(Consts.BLOCK_STARTED_AT, getIntent().
//                getExtras().getParcelable(Consts.BLOCK_STARTED_AT));
//        intent.putExtra(Consts.HOMEWORK_MODE_STARTED_AT,
//                (Parcelable) homeWorkStartedAt);
//
//        intent.putExtra(Consts.CONTENT_PROFILE_ID,
//                getIntent().getExtras().getLong(Consts.CONTENT_PROFILE_ID));
//        intent.putExtra(Consts.CONTENT_PROFILE_TITLE,
//                getIntent().getExtras().getString(Consts.CONTENT_PROFILE_TITLE));
//        intent.putExtra(Consts.TIME_PROFILESES, time);
//        intent.putExtra(Consts.BLOCKED, getIntent().getExtras().getBoolean(Consts.BLOCKED));
////        intent.putExtra(Consts.HOMEWORK_REMAINIG, getHomeworkRemainig());
//        intent.putExtra(Consts.TIME_EXTENSIONSES_COUNT, (Parcelable) timeExtension);
//        intent.putExtra(Consts.HOMEWORK_CATEGORIES, selectedCategories);
//        intent.putExtra(Consts.CREATED_AT, getIntent().getExtras().getParcelable(Consts.CREATED_AT));
//        intent.putExtra(Consts.IS_UNLIMITED_ACCESS, getIntent().getBooleanExtra(Consts.IS_UNLIMITED_ACCESS, false));
//        startActivity(intent);
//        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) timer.cancel();
    }
}

