package android.itransition.com.wedge.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.itransition.com.wedge.BuildConfig;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.LoginModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.LoginRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.demo.LoginDemoRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.listeners.LoginRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;


public class LoginActivity extends BaseActivity implements View.OnClickListener, RequestAction {


    private EditText editPassword;
    private EditText editName;
    private Button btnLogin, btnRegister, btnDemo;
    private TextView tvForgot;
    private TextView tvMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isSecure = false;

        setContentView(R.layout.activity_login);


        editPassword = (EditText) findViewById(R.id.editTextPassword);
        editName = (EditText) findViewById(R.id.editTextName);
        btnLogin = (Button) findViewById(R.id.buttonLogin);
//        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnDemo = (Button) findViewById(R.id.btnDemo);
        tvForgot = (TextView) findViewById(R.id.textViewForgot);
        tvMore = (TextView) findViewById(R.id.textViewMore);

        btnLogin.setOnClickListener(this);
        tvForgot.setOnClickListener(this);
//        tvForgot.setPaintFlags(tvForgot.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        btnRegister.setOnClickListener(this);
        btnDemo.setOnClickListener(this);
        tvMore.setOnClickListener(this);

        editPassword.setTypeface(Typeface.DEFAULT);
        editName.setTypeface(Typeface.DEFAULT);
        editPassword.setTransformationMethod(new PasswordTransformationMethod());


        if (BuildConfig.DEBUG) {
//            editName.setText("mobile@example.com");
//            editPassword.setText("password");
        }

        if (!Settings.getXAccessToken().equals("") && !Settings.isDemoMode()) {
            Intent intent = new Intent();
            intent.setClass(this, ManagementActivity.class);
            startActivity(intent);
            finish();
        } else if (Settings.isDemoMode()) {
            Intent intent = new Intent();
            intent.setClass(this, ManagementActivity.class);
            startActivity(intent);
            finish();
        }

        if (getIntent().getStringExtra("regLogin") != null && getIntent().getStringExtra("regPassword") != null) {
            String login = getIntent().getStringExtra("regLogin");
            String password = getIntent().getStringExtra("regPassword");
            editName.setText(login);
            editPassword.setText(password);
            performLoginRequest();
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.buttonLogin:
                if (!editPassword.getText().toString().equals("") && (!editName.getText().toString().equals(""))) {
                    performLoginRequest();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(getString(R.string.error));
                    alert.setMessage(getString(R.string.empty));

                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
                break;
            /*case R.id.btnRegister:
                intent.setClass(this, RegisterActivity.class);
                startActivity(intent);
                break;*/
            case R.id.textViewForgot:
                intent.setClass(this, ResetPasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.textViewMore:
                Uri uri = Uri.parse(getString(R.string.moreUrl));
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case R.id.btnDemo:
                demoRequest();
                break;
            default:
                break;
        }
    }

    private void demoRequest() {
        if (Settings.isDemoMode() && !TextUtils.isEmpty(Settings.getXAccessToken())) {
            toHomeActivity();
        } else {
            LoginDemoRequest request = new LoginDemoRequest(this);
            contentManager.execute(request, new BaseRequestListener<LoginModel>(this, request) {
                @Override
                public void onRequestSuccess(LoginModel o) {
                    super.onRequestSuccess(o);
                    Settings.setNeedUpdateDeviceOwners(true);
                    toHomeActivity();
                }
            });
        }
    }

    private void toHomeActivity() {
        Intent i = new Intent(this, ManagementActivity.class);
        startActivity(i);
        finish();
    }

    private void performLoginRequest() {
        if (Utils.isOnline(this)) {
            String pass = editPassword.getText().toString();
            String login = editName.getText().toString();

            LoginRequest request = new LoginRequest(this, login,
                    pass, Consts.GCM_TYPE, false);

            contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                    new LoginRequestListener(this, this, BaseRequest.HttpMethodEnum.post,
                            request.getUrl(), request));


        } else {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            Log.e("Network ", "You haven't connection");
        }
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        Settings.setUserId(((LoginModel) model).getUser().getId());


        Settings.setDemoMode(false);
        Settings.setName(editName.getText().toString());
        Settings.setWedge(((LoginModel) model).getUser().isHasFullAccess());
        Settings.setEmail(((LoginModel) model).getUser().getEmail());
        Settings.setUpdatingAlerts(false);

        Settings.setNeedUpdateDeviceOwners(true);
        Settings.setNeedUpdateContentProfile(true);

        Settings.disableDemoScreen();

        Intent intent = new Intent();
        if (getIntent().getBooleanExtra(Consts.NEEDS_NEW_PIN, false)
                || Settings.getPin().length() == 0) {
            Settings.setPin("");
            intent.putExtra(Consts.NEEDS_NEW_PIN, true);
            intent.setClass(this, PinActivity.class);
        } else {
            intent.setClass(this, ManagementActivity.class);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void performFailRequestAction(int action) {

    }



}
