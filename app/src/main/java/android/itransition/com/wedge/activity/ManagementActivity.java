package android.itransition.com.wedge.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.itransition.com.wedge.ActiveMessageHandler;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.gui.fragments.MainFragmentDesign;
import android.itransition.com.wedge.gui.fragments.MoreFragment;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.LoginRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.demo.LogoutDemoRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.listeners.LoginRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by e.kazimirova on 29.08.2014.
 */
public class ManagementActivity extends BaseActivity implements RequestAction, MoreFragment.OnMenuItemClickListener,
        MainFragmentDesign.MainFragmentItemClickListener {

//    String title;
    MainFragmentDesign mainFragment;
    private Fragment moreFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management);
        if (savedInstanceState == null) {
            mainFragment = new MainFragmentDesign();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .add(R.id.content_frame, mainFragment)
                    .commit();
        }

        ActionBar actionBar = setupActionBar();
        setNeedHome(false);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_gray)));

    }

    private void toLoginActivity() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), LoginActivity.class);
        Settings.setXAccessToken("");
        Settings.setName("");
        startActivity(intent);
        finish();
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        toLoginActivity();
       }

    @Override
    public void performFailRequestAction(int action) {
        toLoginActivity();
    }

    private void logout() {
        Settings.setUpdatingAlerts(false);
        Settings.setNeedUpdateContentProfile(true);
        Settings.setUpdatingContentProfileCategories(false);
        Settings.setUpdatingHW(false);
        Settings.setUpdatingDevices(false);
        Settings.setUpdatinWedge(false);
        Settings.setPin("");
        WedgeApplication.mainSource.clearDb();
        WedgeApplication.currentUser = null;
        if (!Utils.isOnline(getApplicationContext())) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            return;
        }
        if (!Settings.isDemoMode()) {
            LoginRequest request = new LoginRequest(this, null, null, null, true);
            contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                    new LoginRequestListener(this, this, BaseRequest.HttpMethodEnum.post,
                            request.getUrl(), request));
        } else {
            endDemo();
        }
    }

    private void endDemo() {
        LogoutDemoRequest request = new LogoutDemoRequest(this);
        contentManager.execute(request, new BaseRequestListener<BaseModel>(this, request) {
            @Override
            public void onRequestSuccess(BaseModel o) {
                super.onRequestSuccess(o);
                toLoginActivity();
            }

            @Override
            public void onRequestFailure(SpiceException e) {
                setShowToast(false);
                super.onRequestFailure(e);
                toLoginActivity();
            }
        });
    }




    public void repopulateList(){
        mainFragment.setCount();
    }


    @Override
    protected void onStart(){
        super.onStart();
        ActiveMessageHandler.instance().setActivity(this);
        if( getIntent().getBooleanExtra("Exit me", false)){
            finish();
        }
        if( getIntent().getBooleanExtra("openMain", false)){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack();
        }
    }


    @Override
    protected void onStop(){
        super.onStop();
        ActiveMessageHandler.instance().setActivity(null);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if( intent.getBooleanExtra("Exit me", false)){
            finish();
        }
        setIntent(intent);
    }

    @Override
    protected void onPause(){
        super.onPause();
    }


    @Override
    public void onMenuSelect(int position) {
        switch (position) {
            case 0:
                Intent intent = new Intent(this, DevicesActivity.class);
                intent.putExtra(Consts.IS_DEVICE_OWNER, false);
                startActivity(intent);
                break;
            case 1: //content rules
                startActivity(new Intent(getApplicationContext(), ContentProfileManagmentActivity.class));
                break;
            case 2: // buy homehalo
//                startActivity(new Intent(getApplicationContext(), OrderActivity.class));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://homehalo.net/androidapp"));
                startActivity(browserIntent);
                break;
            case 3: //rate this app
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    Utils.makeToast("Couldn't launch the market");
                }
                break;
            case 4: //contact us
//                startActivity(new Intent(getApplicationContext(), HelpActivity.class));
                Intent contactUs = new Intent(Intent.ACTION_SEND);
                contactUs.setType("message/rfc822");
                contactUs.putExtra(Intent.EXTRA_EMAIL, new String[]{"appenquiry@homehalo.net"});
                contactUs.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_enquiry));
//                contactUs.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(contactUs, getString(R.string.send_email)));
                break;
            case 5: //Tell a friend
                Intent tellFriend = new Intent(Intent.ACTION_SEND);
                tellFriend.setType("message/rfc822");
                tellFriend.putExtra(Intent.EXTRA_EMAIL, new String[0]);
                tellFriend.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.tryed));
                if (Settings.isDemoMode()) {
                    tellFriend.putExtra(Intent.EXTRA_TEXT, getString(R.string.use_demo));
                } else {
                    tellFriend.putExtra(Intent.EXTRA_TEXT, getString(R.string.use));
                }
                startActivity(Intent.createChooser(tellFriend, getString(R.string.send_email)));
                break;
            case 6:
                startActivity(new Intent(getApplicationContext(), ConfigActivity.class));
                break;
            case 7:
                if (Settings.isDemoMode()) endDemo();
                else logout();
                break;

        }
    }

    @Override
    public void onMainItemClick(int position) {
        switch (position) {
            case 3:
                if (moreFragment == null) moreFragment = new MoreFragment();

                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_in_left,
                                R.anim.fragment_slide_out_left,
                                R.anim.fragment_slide_in_right,
                                R.anim.fragment_slide_out_right)
                        .replace(R.id.content_frame, moreFragment)
                        .addToBackStack(null)
                        .commit();
                break;
        }
    }
}
