package android.itransition.com.wedge.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.reports.ReportsUserActivityTabs;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.DeviceOwnerManager;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by e.kazimirova on 05.12.2014.
 */
public class OneUserActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    private static final int INTERVAL = 30 * 1000;

    private Handler mHandler = new Handler();
    private String ownerName = "";
    private long idOwner = 0;
    private DeviceOwner mDeviceOwner;
    private ListView mListMenu;

    private MenuItem[] mMenuItems;

    private boolean showTimerRun;
    private static int itemsHeight;

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            if (showTimerRun) {
                setMenuByState();
                adapter.notifyDataSetChanged();
                mHandler.postDelayed(this, INTERVAL);
            }
        }
    };
    private Pair<DeviceOwnerManager.OwnerAccessState, Long> stateWithTime;
    private MenuAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_user);

        idOwner = getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID);
        ownerName = getIntent().getExtras().getString(Consts.DEVICE_TITLE);

        setupActionBar();
        setTitle(ownerName);

        DeviceOwnersDataSource source = new DeviceOwnersDataSource(MainSource.getInstance());
        mDeviceOwner = source.getDeviceOwnerById(idOwner);

        //calculating element's height
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int displayHeight = displaymetrics.heightPixels;
        int statusBarHeight = getStatusBarHeight();
        TypedValue tv = new TypedValue();
        int abHeight=0;
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            abHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        itemsHeight = displayHeight - (statusBarHeight+abHeight+(9*dpToPx(3)));
        createMenu();


    }

    private void createMenu() {

        String[] menuTitles = getResources().getStringArray(R.array.owner_menu);
        TypedArray iconResIdArray = getResources().obtainTypedArray(R.array.owner_menu_icons);

        mMenuItems = new MenuItem[menuTitles.length];
        for (int i = 0; i < menuTitles.length; i++) {
            mMenuItems[i] = new MenuItem(iconResIdArray.getResourceId(i, -1), menuTitles[i]);
        }

        if (mDeviceOwner.isUnlimitedAccess()) {
            mMenuItems[1].disabled = true;
        }

        setMenuByState();

        if (!Settings.hasFullAccess() && !Settings.isDemoMode()) {
            for (int i = 0; i < mMenuItems.length; i++) {
                if (i == 6) continue;
                mMenuItems[i].disabled = true;
            }
        }

        mListMenu = (ListView) findViewById(android.R.id.list);
        adapter = new MenuAdapter(this, mMenuItems);
        mListMenu.setAdapter(adapter);

        mListMenu.setOnItemClickListener(this);
    }

    private void setMenuByState() {
        stateWithTime = DeviceOwnerManager.getOwnerAccessStateWithTime(mDeviceOwner);
        switch (stateWithTime.first) {
            case Blocked:
                mMenuItems[0].info = Utils.getTimeString(stateWithTime.second);
                break;
            case HomeWork:
                mMenuItems[2].info = Utils.getTimeString(stateWithTime.second);
                break;
            case Extended:
                mMenuItems[1].info = Utils.getTimeString(stateWithTime.second);
                break;
        }
    }

    private void putExtras(Intent intent) {
        intent.putExtras(getIntent().getExtras());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MenuItem selectedItem = mMenuItems[position];
        if (selectedItem.disabled) return;


        Intent intent = null;
        switch (position) {
            case 0:
                intent = new Intent(getApplicationContext(), BlockTimeActivity.class);
                break;
            case 1:
                intent = new Intent(getApplicationContext(), ExtendTimeActivity.class);
                break;
            case 2:
                intent = new Intent(getApplicationContext(), HomeworkActivity.class);
                break;
            case 3:
                intent = new Intent(getApplicationContext(), BlackListActivity.class);
                intent.putExtra(Consts.IS_WHITE_LIST, true);
                break;
            case 4:
                intent = new Intent(getApplicationContext(), BlacklistActivityTabs.class);
                intent.putExtra(Consts.OWNER_NAME, ownerName);
                break;
            case 5:
                intent = new Intent(getApplicationContext(), DevicesActivity.class);
                intent.putExtra(Consts.IS_DEVICE_OWNER, true);
                break;
            case 6:
                intent = new Intent(getApplicationContext(), ContentProfileAssign.class);
                break;
            case 7:
                intent = new Intent(getApplicationContext(), DayChooserActivity.class);
                break;
            case 8:

//                intent = new Intent(getApplicationContext(), ReportsUserActivity.class);
                intent = new Intent(getApplicationContext(), ReportsUserActivityTabs.class);
                intent.putExtra(Consts.OWNER_NAME, ownerName);
                intent.putExtra(Consts.DEVICE_OWNER_ID, idOwner);
                break;
            default:
                break;
        }

        if (intent != null) {
            putExtras(intent);
            startActivity(intent);
        }
    }

    static class MenuItem {
        int resIcon;
        String title;
        boolean disabled;
        CharSequence info;

        MenuItem(int resIcon, String title) {
            this.resIcon = resIcon;
            this.title = title;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        showTimerRun = true;
        mHandler.postDelayed(mUpdateTimeTask, INTERVAL);
    }

    @Override
    protected void onStop() {
        super.onStop();
        showTimerRun = false;
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    static class MenuAdapter extends ArrayAdapter<MenuItem> {

        private final LayoutInflater inflater;

        public MenuAdapter(Context context, MenuItem[] items) {
            super(context, R.layout.list_item_menu_owner, items);
            inflater = LayoutInflater.from(context);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_menu_owner, parent, false);
                holder = new Holder();
                holder.icon = (ImageView) convertView.findViewById(R.id.ivIcon);
                holder.title = (TextView) convertView.findViewById(R.id.tvTitle);
                holder.info = (TextView) convertView.findViewById(R.id.tvInfo);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            MenuItem item = getItem(position);

            holder.title.setText(item.title);
            holder.info.setText(item.info);

            holder.icon.setImageResource(item.resIcon);

            if (item.disabled) {
                convertView.setBackgroundResource(R.drawable.selector_gray);
            } else if (item.info != null) {
                convertView.setBackgroundResource(R.drawable.selector_owner_menu_active);
            } else {
                convertView.setBackgroundResource(R.drawable.selector_green);
            }
            int itemHeight = itemsHeight/getCount();
            convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, itemHeight));

            return convertView;
        }

        static class Holder {
            TextView title, info;
            ImageView icon;
        }
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

}
