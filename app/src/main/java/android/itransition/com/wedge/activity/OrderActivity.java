package android.itransition.com.wedge.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;

/**
 * Created by i.grechishchev on 10.11.2015.
 * itransition 2015
 */
public class OrderActivity extends BaseActivity implements View.OnClickListener{
    private LinearLayout tvCallNow;
    private LinearLayout tvOrderOnline;
    private boolean needHome;
    private TextView tvTitle;
    static final String PHONE_US = "tel:+1 866 500 7072";
    static final String PHONE_GB = "tel:+44 1794 528 681";
    static final String PHONE_ROW = "tel:+44 1794 528 681";
    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        tvCallNow = (LinearLayout) findViewById(R.id.layoutCallUs);
        tvOrderOnline = (LinearLayout) findViewById(R.id.layoutOrderOnline);
        tvCallNow.setOnClickListener(this);
        tvOrderOnline.setOnClickListener(this);
        setupActionBar();
        setNumberByCountryCode();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layoutCallUs:
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(phoneNumber));
                startActivity(callIntent);
                break;
            case R.id.layoutOrderOnline:
                /*Intent orderIntent = new Intent(getBaseContext(), BuyActivty.class);
                WedgeApplication.lastOnStop = System.currentTimeMillis();
                startActivity(orderIntent);*/
                WedgeApplication.tracker.send(new HitBuilders.EventBuilder()
                        .setCategory("ui_action")
                        .setAction("button_press")
                        .setLabel("Order online")
                        .build());
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://homehalo.net/buy/products"));
                startActivity(browserIntent);
                break;
        }
    }

    public void setNeedHome(boolean needHome) {
        this.needHome = needHome;
    }

    public void setBarColor(int color) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(color));
    }

    protected ActionBar setupActionBar() {

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.actionbar_custom_title_centered);
//        actionBar.setHomeAsUpIndicator(R.drawable.back_button_arrow);
        setBarColor(getResources().getColor(R.color.bar_color));
        View customView = actionBar.getCustomView();
        tvTitle = (TextView) customView.findViewById(R.id.title);
        tvTitle.setText(R.string.order_today);
        tvTitle.setTextColor(getResources().getColor(R.color.green));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.back_button_arrow);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.bar_color)));

//        setNeedHome(true);

        return actionBar;
    }

    public void setNumberByCountryCode() {
        //gets country code based on telephony manager
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        final String simCountry = tm.getSimCountryIso();
        Log.d("grch", simCountry);
        if (simCountry.equalsIgnoreCase("us")) {
            phoneNumber = PHONE_US;
        } else if (simCountry.equalsIgnoreCase("gb")) {
            phoneNumber = PHONE_GB;
        } else {
            phoneNumber = PHONE_ROW;
        }
    }
}
