package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.model.LoginModel;
import android.itransition.com.wedge.request.LoginRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import static android.itransition.com.wedge.utils.Utils.makeToast;

/**
 * Created by e.kazimirova on 01.10.2014.
 */
public class PinActivity extends BaseActivity implements View.OnClickListener {

//    public SpiceManager contentManager = new SpiceManager(ApiService.class);

    private Button btnOne, btnTwo, btnThree, btnFour, btnFive;
    private Button btnSix, btnSeven, btnEight, btnNine, btnZero;
    private TextView textViewLogout, textViewDelete, tvTitle, pinTitle, pinDescription, setYourPinTv;
    private ImageView pinLogo;
    private String pin = "";
    private boolean passed;
    private int count = 0;
    private boolean editMode;

    private String savedPin;
    private boolean needMenu;
    int pinCounter = 0;
    String newPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isSecure = false;

        savedPin = Settings.getPin();
        if (getIntent().getExtras() != null) {
            editMode = getIntent().getExtras().getBoolean(Consts.EDIT_PIN);
        }

        if(editMode && savedPin.length() > 0){
            setNeedHome(true);
            setupActionBar();
            setTitle("Pin");
        } else {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.activity_pin);

        tvTitle = (TextView) findViewById(R.id.textView);
        btnOne = (Button) findViewById(R.id.buttonOne);
        btnTwo = (Button) findViewById(R.id.buttonTwo);
        btnThree = (Button) findViewById(R.id.buttonThree);
        btnFour = (Button) findViewById(R.id.buttonFour);
        btnFive = (Button) findViewById(R.id.buttonFive);
        btnSix = (Button) findViewById(R.id.buttonSix);
        btnSeven = (Button) findViewById(R.id.buttonSeven);
        btnEight = (Button) findViewById(R.id.buttonEight);
        btnNine = (Button) findViewById(R.id.buttonNine);
        btnZero = (Button) findViewById(R.id.buttonZero);
        textViewDelete = (TextView) findViewById(R.id.textViewDelete);
        textViewLogout = (TextView) findViewById(R.id.textViewLogout);
        pinTitle = (TextView) findViewById(R.id.pinTitle);
        pinDescription = (TextView) findViewById(R.id.pinDescription);
        setYourPinTv = (TextView) findViewById(R.id.setYourPinTv);
        pinLogo = (ImageView) findViewById(R.id.pinLogo);

        btnOne.setOnClickListener(digitListener);
        btnTwo.setOnClickListener(digitListener);
        btnThree.setOnClickListener(digitListener);
        btnFour.setOnClickListener(digitListener);
        btnFive.setOnClickListener(digitListener);
        btnSix.setOnClickListener(digitListener);
        btnSeven.setOnClickListener(digitListener);
        btnEight.setOnClickListener(digitListener);
        btnNine.setOnClickListener(digitListener);
        btnZero.setOnClickListener(digitListener);
        textViewDelete.setOnClickListener(this);
        textViewLogout.setOnClickListener(this);

        if (savedPin.length() == 0) editMode = false;

        if(savedPin.length() == 0 || editMode || getIntent().getBooleanExtra(Consts.PIN_FROM_BACKGROUND, false)) {
            textViewLogout.setVisibility(View.INVISIBLE);
        }
        if (savedPin.length() == 0 && !editMode) {
            tvTitle.setText("Set your PIN");
            tvTitle.setVisibility(View.GONE);
            pinLogo.setVisibility(View.GONE);
            pinTitle.setVisibility(View.VISIBLE);
            pinDescription.setVisibility(View.VISIBLE);
            setYourPinTv.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener digitListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            count += 1;
            pin = pin + ((Button)v).getText();
            makeActiveCircle();
            if(count==4){
                startActivity();
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.textViewDelete:
                count = 0;
                pin ="";
                makeActiveCircle();
                break;
            case R.id.textViewLogout:
                performLogout();
                break;
            default:
                break;
        }
    }

    private void performLogout() {
        if (!TextUtils.isEmpty(Settings.getXAccessToken())) {
            LoginRequest request = new LoginRequest(this, null, null, null,true);
            contentManager.execute(request, new BaseRequestListener<LoginModel>(this, request) {
                @Override public void onRequestSuccess(LoginModel o) {
                    super.onRequestSuccess(o);
                    logout();
                }
            });
        } else {
            logout();
        }
    }

    private void logout() {
        Intent intent = new Intent();
        intent.putExtra(Consts.NEEDS_NEW_PIN, true);
        intent.setClass(getApplicationContext(), LoginActivity.class);
        Settings.setXAccessToken("");
        Settings.setName("");
        WedgeApplication.mainSource.clearDb();
        startActivity(intent);
        finish();
    }


    private void makeActiveCircle(){
        switch (count){
            case 0:
                ((ImageView) findViewById(R.id.imageView2)).setImageResource(R.drawable.pin_not_put_circle);
                ((ImageView) findViewById(R.id.imageView1)).setImageResource(R.drawable.pin_not_put_circle);
                ((ImageView) findViewById(R.id.imageView3)).setImageResource(R.drawable.pin_not_put_circle);
                ((ImageView) findViewById(R.id.imageView4)).setImageResource(R.drawable.pin_not_put_circle);
                break;
            case 1:
                ((ImageView) findViewById(R.id.imageView1)).setImageResource(R.drawable.pin_put_circle);
                break;
            case 2:
                ((ImageView) findViewById(R.id.imageView2)).setImageResource(R.drawable.pin_put_circle);
                break;
            case 3:
                ((ImageView) findViewById(R.id.imageView3)).setImageResource(R.drawable.pin_put_circle);
                break;
            case 4:
                ((ImageView) findViewById(R.id.imageView4)).setImageResource(R.drawable.pin_put_circle);
                break;
            default:
                break;
        }
    }
    private void startActivity() {

        if (TextUtils.isEmpty(savedPin)) {
//            makeToast(R.string.pin_save);
            Settings.setPin(pin);
            Intent intent = new Intent();
            if (getIntent().getBooleanExtra(Consts.NEEDS_NEW_PIN, false)) {
                intent.setClass(this, ManagementActivity.class);
            } else {
                intent.setClass(this, LoginActivity.class);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else if (editMode) {
            if (!passed) {
                if (pin.equals(savedPin)) {
                    passed = true;
                    pinCounter = 0;
                    makeToast(R.string.input_new_pin);
                    tvTitle.setText("Set new pin");
                } else {
                    makeToast(R.string.wrong_pin);
                }
                count = 0;
                pin = "";
                makeActiveCircle();
            } else {
                if (pinCounter == 0) {
                    pinCounter++;
                    newPin = pin;
                    makeToast(R.string.repeat_pin);
                    tvTitle.setText("Repeat new pin");
                    count = 0;
                    pin = "";
                    makeActiveCircle();
                } else if (pinCounter == 1 && newPin.equals(pin)){
//                    makeToast("New pin saved");
                    Settings.setPin(pin);
                    finish();
                } else {
                    makeToast(R.string.wrong_pin);
                    passed = false;
                    count = 0;
                    pin = "";
                    tvTitle.setText(getResources().getString(R.string.enter_pin));
                    makeActiveCircle();
                }
            }
        } else {
            if (pin.equals(savedPin)) {

                if (!getIntent().getBooleanExtra(Consts.PIN_FROM_BACKGROUND, false)) {
                    Intent intentManagment = new Intent();
                    intentManagment.setClass(getApplicationContext(), LoginActivity.class);
                    startActivity(intentManagment);
                } else {
                    //always show ManagementActivity after restart
                    Intent intent = new Intent(this, ManagementActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                finish();
            } else {
                makeToast(R.string.wrong_pin);
                pin = "";
                count = 0;
                makeActiveCircle();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(Consts.PIN_FROM_BACKGROUND, false)) {
            Intent intent = new Intent(this, ManagementActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("Exit me", true);
            startActivity(intent);
            finish();
            return;
        }
        super.onBackPressed();
    }
}