package android.itransition.com.wedge.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.UserModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.RegistrationRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.listeners.RegistrationRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;

/**
 * Created by e.kazimirova on 29.08.2014.
 */
public class RegisterActivity extends BaseActivity implements View.OnClickListener, RequestAction {

    private EditText editEmail;
    private EditText editPassword;
    private EditText editVerify;
    private Button btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isSecure = false;
        getSupportActionBar().hide();

        setContentView(R.layout.activity_register);

        editEmail = (EditText) findViewById(R.id.editTextEmail);
        editPassword = (EditText) findViewById(R.id.editTextPassword);
        editVerify = (EditText) findViewById(R.id.editTextVerification);
        btnCreate = (Button) findViewById(R.id.buttonCreate);

        editPassword.setTypeface(Typeface.DEFAULT);
        editPassword.setTransformationMethod(new PasswordTransformationMethod());

        editVerify.setTypeface(Typeface.DEFAULT);
        editVerify.setTransformationMethod(new PasswordTransformationMethod());

        btnCreate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonCreate:
                String email = editEmail.getText().toString().trim();
                String pass = editPassword.getText().toString();
                if(!pass.equals(editVerify.getText().toString())) {
                    showDialog("Verifing password error", "Passwords don't match");
                }  else if (pass.length()<2){
                    showDialog("Error", "Too short some field");
                } else  if(!Utils.isValidEmail(email)){
                    showDialog("Error", "Email address not valid");
                } else {
                    performRegistrationRequest();
                }

                break;
            default:
                break;
        }

    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(title);
        alert.setMessage(message);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void performRegistrationRequest() {
        if (!Utils.isOnline(this)) {
            Toast.makeText(this, R.string.error_connection, Toast.LENGTH_LONG).show();
            Log.e("Network ", "You haven't connection");
            return;
        }
        RegistrationRequest request = new RegistrationRequest(this, editEmail.getText().toString(), editPassword.getText().toString());
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new RegistrationRequestListener(this,  this, BaseRequest.HttpMethodEnum.post,
                        request.getUrl(), request));
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        UserModel user = (UserModel) model;
        Utils.makeToast(R.string.registration_successful);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("regLogin", editEmail.getText().toString());
        intent.putExtra("regPassword", editPassword.getText().toString());
        intent.putExtra(Consts.NEEDS_NEW_PIN, true);
        startActivity(intent);
        finish();
//        onBackPressed();
    }

    @Override
    public void performFailRequestAction(int action) {

    }
}
