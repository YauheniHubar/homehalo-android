package android.itransition.com.wedge.activity;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.ResetRequest;
import android.itransition.com.wedge.request.listeners.ResetRequestListener;
import android.itransition.com.wedge.robospice.ApiService;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;

/**
 * Created by e.kazimirova on 29.08.2014.
 */
public class ResetPasswordActivity extends ActionBarActivity implements View.OnClickListener, RequestAction {

    private EditText editEmail;
    private Button  btnReset;
    private TextView tvTitle;
    private boolean needHome;
    public SpiceManager contentManager = new SpiceManager(ApiService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if(getIntent()!=null && getIntent().getExtras()!=null &&
                getIntent().getExtras().getBoolean(Consts.FROM_CONFIG)){

            setupActionBar();
            setTitle("Reset");

        } else {
            getSupportActionBar().hide();

        }
        setContentView(R.layout.activity_reset_password);

        btnReset = (Button) findViewById(R.id.buttonReset);
        editEmail = (EditText) findViewById(R.id.editTextEmail);

        btnReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonReset:
                if(editEmail.getText().length() > 0 && Utils.isValidEmail(editEmail.getText().toString())){
                    performResetRequest();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(getString(R.string.error));
                    alert.setMessage(getString(R.string.invalid_email));

                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
                break;
            case R.id.textViewMain:
                finish();
                break;
            case R.id.imageViewHome:
                finish();
                break;
            case R.id.textViewBack:
                finish();
                break;
            default:
                break;
        }
    }

    private void performResetRequest() {
        ResetRequest request = new ResetRequest(this,editEmail.getText().toString());
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new ResetRequestListener(this,  this, BaseRequest.HttpMethodEnum.post,
                        request.getUrl(), request));
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        Toast.makeText(this, getString(R.string.check_email), Toast.LENGTH_LONG).show();
        onBackPressed();
    }

    @Override
    public void performFailRequestAction(int action) {

    }

    public void setNeedHome(boolean needHome) {
        this.needHome = needHome;
    }

    public void setBarColor(int color) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(color));
    }

    protected ActionBar setupActionBar() {

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.actionbar_custom_title_centered);
        actionBar.setHomeAsUpIndicator(R.drawable.back_button_arrow);
        setBarColor(getResources().getColor(R.color.bar_color));
        View customView = actionBar.getCustomView();
        tvTitle = (TextView) customView.findViewById(R.id.title);

        setNeedHome(true);

        return actionBar;
    }
}
