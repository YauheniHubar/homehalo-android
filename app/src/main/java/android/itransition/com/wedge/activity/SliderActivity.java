package android.itransition.com.wedge.activity;

import android.app.Activity;
import android.content.Context;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.request.SubscribeEmailRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.robospice.ApiService;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by i.grechishchev on 12.11.2015.
 * itransition 2015
 */
public class SliderActivity extends Activity {
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private ImageView img_page1, img_page2, img_page3, img_page4;
    private LinearLayout layoutDots;
    public SpiceManager contentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);
        contentManager = new SpiceManager(ApiService.class);
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        img_page1 = (ImageView) findViewById(R.id.img_page1);
        img_page2 = (ImageView) findViewById(R.id.img_page2);
        img_page3 = (ImageView) findViewById(R.id.img_page3);
        img_page4 = (ImageView) findViewById(R.id.img_page4);
        layoutDots = (LinearLayout) findViewById(R.id.layoutDots);
        adapter = new ViewPagerAdapter();
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        layoutDots.setVisibility(View.VISIBLE);
                        img_page1.setImageResource(R.drawable.ic_dot_selected);
                        img_page2.setImageResource(R.drawable.ic_dot);
                        img_page3.setImageResource(R.drawable.ic_dot);
                        img_page4.setImageResource(R.drawable.ic_dot);
                        break;
                    case 1:
                        layoutDots.setVisibility(View.VISIBLE);
                        img_page1.setImageResource(R.drawable.ic_dot);
                        img_page2.setImageResource(R.drawable.ic_dot_selected);
                        img_page3.setImageResource(R.drawable.ic_dot);
                        img_page4.setImageResource(R.drawable.ic_dot);
                        break;
                    case 2:
                        layoutDots.setVisibility(View.VISIBLE);
                        img_page1.setImageResource(R.drawable.ic_dot);
                        img_page2.setImageResource(R.drawable.ic_dot);
                        img_page3.setImageResource(R.drawable.ic_dot_selected);
                        img_page4.setImageResource(R.drawable.ic_dot);
                        break;
                    case 3:
                        layoutDots.setVisibility(View.GONE);
                        /*img_page1.setImageResource(R.drawable.ic_dot);
                        img_page2.setImageResource(R.drawable.ic_dot);
                        img_page3.setImageResource(R.drawable.ic_dot);
                        img_page4.setImageResource(R.drawable.ic_dot_selected);*/
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

//    private class SliderPagesAdapter extends FragmentPagerAdapter {
//
//        @Override
//        public Fragment getItem(int position) {
//            return null;
//        }
//
//        @Override
//        public int getCount() {
//            return 0;
//        }
//    }

    private class ViewPagerAdapter extends PagerAdapter {

        int NumberOfPages = 4;
        @Override
        public int getCount() {
            return NumberOfPages;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            if (position < NumberOfPages) {
                LayoutInflater inflater = LayoutInflater.from(SliderActivity.this);
                ViewGroup layout = null;
                switch (position) {
                    case 0:
                        layout = (ViewGroup) inflater.inflate(R.layout.demo_slider_page_1, container, false);
                        break;
                    case 1:
                        layout = (ViewGroup) inflater.inflate(R.layout.demo_slider_page_2, container, false);
                        break;
                    case 2:
                        layout = (ViewGroup) inflater.inflate(R.layout.demo_slider_page_3, container, false);
                        break;
                    case 3:
//                        LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        layout = (ViewGroup) inflater.inflate( R.layout.slider_discount, null );
                        Button btnSubmit = (Button) layout.findViewById(R.id.btnSubmit);
                        TextView tvSkip = (TextView) layout.findViewById(R.id.tvSkip);
                        int i = Settings.getSipButtonFlag();
                        final EditText etEmail = (EditText) layout.findViewById(R.id.etEmail);
                        btnSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                WedgeApplication.tracker.send(new HitBuilders.EventBuilder()
                                        .setCategory("ui_action")
                                        .setAction("button_press")
                                        .setLabel("Skip demo mode")
                                        .build());

                                String email = etEmail.getText().toString();
                                if (!email.isEmpty()) {
                                    sendEmail(email);
                                } else {
                                    Toast.makeText(getBaseContext(), R.string.empty, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        tvSkip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
                        if (i == 0) {
                            tvSkip.setVisibility(View.GONE);
                        }

                }
                /*ImageView imageView = new ImageView(SliderActivity.this);
                imageView.setImageResource(res[position]);
                ViewGroup.LayoutParams imageParams = new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                imageView.setLayoutParams(imageParams);

                LinearLayout layout = new LinearLayout(SliderActivity.this);
                layout.setOrientation(LinearLayout.VERTICAL);
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                layout.setBackgroundColor(0xFF82736E);
                layout.setLayoutParams(layoutParams);
//            layout.addView(textView);
                layout.addView(imageView);*/
//                LayoutInflater inflater = LayoutInflater.from(SliderActivity.this);
//                ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.demo_slider_page_1, container, false);
//                if (position == 3) {
//                    layout.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            finish();
//                        }
//                    });
//                }
                container.addView(layout);
                return layout;
            } else {
                LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate( R.layout.slider_discount, null );
                Button btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
                TextView tvSkip = (TextView) view.findViewById(R.id.tvSkip);
                int i = Settings.getSipButtonFlag();
                final EditText etEmail = (EditText) view.findViewById(R.id.etEmail);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        WedgeApplication.tracker.send(new HitBuilders.EventBuilder()
                                .setCategory("ui_action")
                                .setAction("button_press")
                                .setLabel("Skip demo mode")
                                .build());

                        String email = etEmail.getText().toString();
                        if (!email.isEmpty()) {
                            sendEmail(email);
                        } else {
                            Toast.makeText(getBaseContext(), R.string.empty, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                tvSkip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
                if (i == 0) {
                    tvSkip.setVisibility(View.GONE);
                }
                container.addView(view);
                return view;
            }
//            final int page = position;



        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }

    }

    public void sendEmail(String email) {
        SubscribeEmailRequest request =
                new SubscribeEmailRequest(this, Settings.getUserId(), email);
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new BaseRequestListener<BaseModel>(this, request) {
                    @Override
                    public void onRequestFailure(SpiceException e) {
                        super.onRequestFailure(e);
//                        Toast.makeText(getBaseContext(), R.string.some_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onRequestSuccess(BaseModel o) {
                        super.onRequestSuccess(o);
                        finish();
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        contentManager.start(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        contentManager.shouldStop();
    }
}
