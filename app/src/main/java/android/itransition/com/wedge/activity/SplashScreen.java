package android.itransition.com.wedge.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.GcmHelper;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicBoolean;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;


/**
 * Created by e.kazimirova on 11.06.2014.
 */
public class SplashScreen extends Activity {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    private AtomicBoolean register = new AtomicBoolean(true);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);


        if (checkPlayServices()) {
            String deviceToken = GcmHelper.getRegistrationId(getApplicationContext());
            if (TextUtils.isEmpty(deviceToken)) {
                registerInBackground();
            }
        }
        WedgeApplication.lastOnStop = System.currentTimeMillis();
        showSplash();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Branch branch = Branch.getInstance();
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    // params are the deep linked params associated with the link that the user clicked before showing up
                    Log.i("BranchConfigTest", "deep link data: " + referringParams.toString());
                }
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this, "138672036493912");
        // ... facebook
    }

    private void registerInBackground() {
        register.set(false);
        AsyncTask<Void, Void, String> registerTask  = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                return GcmHelper.registerDevice(getApplicationContext(), 5);
            }

            @Override
            protected void onPostExecute(String id) {
                register.set(true);
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            registerTask.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR);
        else
            registerTask.execute(null, null);
    }

    private void showSplash() {
        AsyncTask<Void, Void, Void> splashTask = new AsyncTask<Void, Void, Void>() {
            private int ms = 0;
            private int sleepTime = 100;

            @Override protected void onProgressUpdate(Void... values) {
                ms += sleepTime;
                if (ms >= 2000 && register.get()) {
                    cancel(false);
                    Intent intent = new Intent();
                    if (Settings.isDemoMode() && Settings.getXAccessToken().length() > 0) {
                        intent.setClass(SplashScreen.this,  ManagementActivity.class);
                    } else if (!TextUtils.isEmpty(Settings.getPin())) {
                        intent.setClass(SplashScreen.this,  PinActivity.class);
                    } else {
                        intent.setClass(SplashScreen.this,  LoginActivity.class);
                    }
                    startActivity(intent);
                    finish();
                }
            }

            @Override protected Void doInBackground(Void... params) {
                while (!isCancelled()) {
                    try {
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {
                    }
                    onProgressUpdate();
                }
                return null;
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            splashTask.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR);
        else
            splashTask.execute(null, null);
    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST);
                errorDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                });
                errorDialog.show();
            } else {
                Log.i("splash", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        //no op
    }
}
