package android.itransition.com.wedge.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.gui.fragments.TimeProfileDayFragment;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.BlockRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.listeners.UpdateDeviceRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TimeProfileActivity extends BaseActivity implements View.OnClickListener, RequestAction{
    private TimeProfileDayFragment dayFragment;
    private TextView dayTitle, dayTitle2;
    private Button btnSave, btnCopyToNextDay, btnCancel, btnClearDay, btnEdit;
    private ImageButton btnPrev, btnNext;
    private LinearLayout bottomMainPanel, bottomEditPanel;
    private int mDayId;
    List<String> daysOfWeekList;
    List<String> daysOfWeekListEn;
    FragmentManager fragmentManager;
    private MainSource mainsource;

    Long deviceOwnerId;
    Long contentProfileId;
    String deviceTitle;
    CreateTimeUser createTimeUser;

    List<TimeProfiles> currentTimeProfiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_profile);
        dayTitle = (TextView)findViewById(R.id.day_title);
        dayTitle2 = (TextView)findViewById(R.id.tvDayTitle);
        btnSave = (Button)findViewById(R.id.btnSave);
        btnPrev = (ImageButton)findViewById(R.id.btn_prev_day);
        btnNext = (ImageButton)findViewById(R.id.btn_next_day);
        btnCopyToNextDay = (Button) findViewById(R.id.btnCopyToNextDay);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnClearDay = (Button) findViewById(R.id.btnClearDay);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        bottomMainPanel = (LinearLayout) findViewById(R.id.bottomMainPanel);
        bottomEditPanel = (LinearLayout) findViewById(R.id.bottomEditPanel);

        mainsource = MainSource.getInstance();
        daysOfWeekList = Arrays.asList(getString(R.string.monday), getString(R.string.tuesday), getString(R.string.wednesday),
                getString(R.string.thursday), getString(R.string.friday), getString(R.string.saturday), getString(R.string.sunday));
        daysOfWeekListEn = Arrays.asList("MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY");
        Intent intent = getIntent();
        mDayId = intent.getIntExtra(Consts.ID_DAY, 0);
        dayTitle.setText(daysOfWeekList.get(mDayId).toUpperCase());

        deviceOwnerId = getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID);
        contentProfileId = getIntent().getExtras().getLong(Consts.CONTENT_PROFILE_ID);
        deviceTitle = getIntent().getExtras().getString(Consts.DEVICE_TITLE);
        createTimeUser = (CreateTimeUser) getIntent().getExtras().getParcelable(Consts.CREATED_AT);

        dayFragment = new TimeProfileDayFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TimeProfileDayFragment.DAY_OF_WEEK_KEY, daysOfWeekList.get(mDayId));
        bundle.putString(TimeProfileDayFragment.DAY_OF_WEEK_KEY_EN, daysOfWeekListEn.get(mDayId));
        Parcelable[] tv = (Parcelable[]) getIntent().getParcelableArrayExtra(Consts.TIME_PROFILESES);
        bundle.putParcelableArray(Consts.TIME_PROFILESES, tv);
        currentTimeProfiles = new ArrayList<>();
        for (int i = 0; i < tv.length; i++) {
            currentTimeProfiles.add((TimeProfiles) tv[i]);
        }
        //other fragments
        dayFragment.setArguments(bundle);
        setupActionBar();
        setTitle(deviceTitle);

        fragmentManager = getFragmentManager();
        if (savedInstanceState == null) {
            fragmentManager.beginTransaction()
                    .add(R.id.content_frame, dayFragment)
                    .commit();
        }

        btnEdit.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnCopyToNextDay.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnClearDay.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle;
        switch (v.getId()) {
            case R.id.btnSave:
                List<TimeProfiles> timeProfiles = dayFragment.getTimeProfilesList();
                updateTimeProfiles(getArray(timeProfiles));
                setEditModeEnabled(false);
                break;

            case R.id.btn_prev_day:
                mDayId = getPrevDayId(mDayId);

                DisplayMetrics dm = new DisplayMetrics();
                this.getWindowManager().getDefaultDisplay().getMetrics(dm);
                float screenWidth = dm.widthPixels;
                float dayWidth = dayTitle.getWidth();
                dayTitleMoveAnimation(-(screenWidth + dayWidth)/2, (screenWidth + dayWidth)/2);

                dayFragment = new TimeProfileDayFragment();
                bundle = new Bundle();
                bundle.putString(TimeProfileDayFragment.DAY_OF_WEEK_KEY, daysOfWeekList.get(mDayId));
                bundle.putString(TimeProfileDayFragment.DAY_OF_WEEK_KEY_EN, daysOfWeekListEn.get(mDayId));
                bundle.putParcelableArray(Consts.TIME_PROFILESES, getArray(currentTimeProfiles));
                dayFragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.animator.slide_in_left,
                                R.animator.slide_in_left_out)
                        .replace(R.id.content_frame, dayFragment)
                        .addToBackStack(null)
                        .commit();

                break;

            case R.id.btn_next_day:
                mDayId = getNextDayId(mDayId);

                DisplayMetrics dm2 = new DisplayMetrics();
                this.getWindowManager().getDefaultDisplay().getMetrics(dm2);
                float screenWidth2 = dm2.widthPixels;
                float dayWidth2 = dayTitle.getWidth();
                dayTitleMoveAnimation((screenWidth2 + dayWidth2)/2, -(screenWidth2 + dayWidth2)/2);

                dayFragment = new TimeProfileDayFragment();
                bundle = new Bundle();
                bundle.putString(TimeProfileDayFragment.DAY_OF_WEEK_KEY, daysOfWeekList.get(mDayId));
                bundle.putString(TimeProfileDayFragment.DAY_OF_WEEK_KEY_EN, daysOfWeekListEn.get(mDayId));
                bundle.putParcelableArray(Consts.TIME_PROFILESES, getArray(currentTimeProfiles));
                dayFragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.animator.slide_in_right,
                                R.animator.slide_in_right_out)
                        .replace(R.id.content_frame, dayFragment)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.btnCancel:
                setEditModeEnabled(false);
                dayFragment.clearTimeProfileUI(false);
                dayFragment.initTimeProfileUI(getArray(currentTimeProfiles));
                break;
            case R.id.btnClearDay:
                dayFragment.clearTimeProfileUI(true);
                break;
            case R.id.btnEdit:
                currentTimeProfiles = dayFragment.getTimeProfilesList();
                setEditModeEnabled(true);
                break;
            case R.id.btnCopyToNextDay:
                mDayId = getNextDayId(mDayId);

                List<TimeProfiles> timeProfilesList = new ArrayList<>();
                int prevDayId = getPrevDayId(mDayId);
                for (TimeProfiles profile:currentTimeProfiles){
                    if (profile.getDayOfWeek().equalsIgnoreCase(daysOfWeekListEn.get(mDayId))) continue;

                    if (profile.getDayOfWeek().equalsIgnoreCase(daysOfWeekListEn.get(prevDayId))){
                        TimeProfiles newProfile = new TimeProfiles();
                        newProfile.setDayOfWeek(daysOfWeekListEn.get(mDayId).toLowerCase());
                        newProfile.setStartTime(profile.getStartTime());
                        newProfile.setEndTime(profile.getEndTime());
                        timeProfilesList.add(newProfile);
                    }

                    timeProfilesList.add(profile);
                }

                dayFragment = new TimeProfileDayFragment();
                bundle = new Bundle();
                bundle.putString(TimeProfileDayFragment.DAY_OF_WEEK_KEY, daysOfWeekList.get(mDayId));
                bundle.putString(TimeProfileDayFragment.DAY_OF_WEEK_KEY_EN, daysOfWeekListEn.get(mDayId));
                bundle.putParcelableArray(Consts.TIME_PROFILESES, getArray(timeProfilesList));
                dayFragment.setArguments(bundle);

                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.animator.slide_in_right,
                                R.animator.slide_in_right_out)
                        .replace(R.id.content_frame, dayFragment)
                        .addToBackStack(null)
                        .commit();
                dayTitle.setText(daysOfWeekList.get(mDayId).toUpperCase());
                
                updateTimeProfiles(getArray(timeProfilesList));
                break;
        }
    }

    private void dayTitleMoveAnimation(final float fromDist, final float toDist){


        TranslateAnimation animDayTitle = new TranslateAnimation(0, toDist, 0, 0);
        animDayTitle.setDuration(300);

        animDayTitle.setAnimationListener(new TranslateAnimation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) dayTitle.getLayoutParams();
                params.leftMargin += toDist;
                dayTitle.setLayoutParams(params);
                dayTitle.setText(daysOfWeekList.get(mDayId).toUpperCase());
            }
        });



        TranslateAnimation animDayTitle2 = new TranslateAnimation(fromDist, 0, 0, 0);
        animDayTitle2.setDuration(300);


        animDayTitle2.setAnimationListener(new TranslateAnimation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                dayTitle2.setText(daysOfWeekList.get(mDayId).toUpperCase());
                dayTitle2.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) dayTitle2.getLayoutParams();
                params.leftMargin += fromDist;
                dayTitle2.setLayoutParams(params);
                dayTitle2.setVisibility(View.INVISIBLE);
            }
        });



        dayTitle2.startAnimation(animDayTitle2);
        dayTitle.startAnimation(animDayTitle);
    }

    private int getNextDayId(int dayId){
        dayId++;
        if (dayId >= daysOfWeekList.size()) {
            dayId = 0;
        }
        return dayId;
    }

    private int getPrevDayId(int dayId){
        dayId--;
        if (dayId < 0) {
            dayId = daysOfWeekList.size()-1;
        }
        return dayId;
    }

    private TimeProfiles[] getArray(List<TimeProfiles> list){
        TimeProfiles[] array = new TimeProfiles[list.size()];
        list.toArray(array);
        return array;
    }

    private void setEditModeEnabled(boolean isEnabled){
        if (isEnabled){
            bottomMainPanel.setVisibility(View.GONE);
            bottomEditPanel.setVisibility(View.VISIBLE);
        } else {
            bottomMainPanel.setVisibility(View.VISIBLE);
            bottomEditPanel.setVisibility(View.GONE);
        }
        dayFragment.setEditModeEnabled(isEnabled);
        btnNext.setEnabled(!isEnabled);
        btnPrev.setEnabled(!isEnabled);
    }

    public void updateTimeProfiles(TimeProfiles[] timeProfiles) {
        DeviceOwnersDataSource deviceOwnersDataSource = new DeviceOwnersDataSource(mainsource);
        DeviceOwner deviceOwner = deviceOwnersDataSource.getDeviceOwnerById(getIntent().getExtras().getLong(Consts.DEVICE_OWNER_ID));
        CreateTimeUser homeWorkStartedAt = deviceOwner.getHomeworkModeStartedAt();
        Long homeworkModeDuration = deviceOwner.getHomeworkModeDuration();
        if (homeworkModeDuration == null)
            homeworkModeDuration = 0l;
        HomeWorkCategories[] homeWorkCategories = deviceOwner.getHomeworkCategories();
        Long timeDuration = deviceOwner.getHomeworkModeDuration();
        if (timeDuration == null)
            timeDuration = 0l;
        CreateTimeUser blockStartAt = deviceOwner.getBlockStartedAt();

        BlockRequest request = new BlockRequest(this, deviceOwnerId, deviceTitle, timeDuration, blockStartAt, contentProfileId, timeProfiles,
                createTimeUser, homeWorkStartedAt, homeworkModeDuration, homeWorkCategories);

        contentManager.execute(request,
                new UpdateDeviceRequestListener(this, this, BaseRequest.HttpMethodEnum.put,
                        request.getUrl(), request));
    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {
        Log.d("grecha11","uspeh");
        DeviceOwnersDataSource source = new DeviceOwnersDataSource(WedgeApplication.mainSource);
        DeviceOwnersModel deviceOwner = (DeviceOwnersModel) model;
        int orderId = source.getDeviceOwnerById(deviceOwner.getDeviceOwner().getId()).getOrderId();
        source.deleteDeviceOwner(deviceOwner.getDeviceOwner().getId());
        source.createDeviceOwner(deviceOwner.getDeviceOwner(), orderId);
        currentTimeProfiles = new ArrayList<>();
        TimeProfiles[] timeProfileArray = ((DeviceOwnersModel) model).getDeviceOwner().getTimeProfiles();
        for (int i = 0; i < timeProfileArray.length; i++) {
            currentTimeProfiles.add(timeProfileArray[i]);
        }
        dayFragment.clearTimeProfileUI(false);
        dayFragment.initTimeProfileUI(getArray(currentTimeProfiles));
        Settings.setNeedUpdateDeviceOwners(true);
    }

    @Override
    public void performFailRequestAction(int action) {
        Log.d("grecha11","fail");
        dayFragment.clearTimeProfileUI(false);
        dayFragment.initTimeProfileUI(getArray(currentTimeProfiles));
    }
}
