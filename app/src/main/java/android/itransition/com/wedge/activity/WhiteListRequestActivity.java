package android.itransition.com.wedge.activity;

import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.NotificationsEntity;
import android.itransition.com.wedge.model.WhiteUrlModel;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.urllists.UpdateUrlRequest;
import android.itransition.com.wedge.settings.Consts;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class WhiteListRequestActivity extends BaseActivity implements View.OnClickListener {

    private Button btnAccept, btnDecline, btnPreview, btnIgnore, btnClear;
    private WebView webView;
    private NotificationsEntity item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_white_list_request);

        setupActionBar();
        setTitle(getString(R.string.alerts));
        setNeedHome(true);
        //setBarColor(getResources().getColor(R.color.alerts_orange));

        Intent intent = getIntent();
        if (intent!=null)
            item = intent.getParcelableExtra(AlertsActivity.NOTIFICATION_EXTRA);

        btnAccept = (Button)findViewById(R.id.buttonAccept);
        btnDecline = (Button)findViewById(R.id.buttonDecline);
//        btnPreview = (Button)findViewById(R.id.buttonPreview);
        btnIgnore = (Button)findViewById(R.id.buttonIgnore);
        btnClear = (Button)findViewById(R.id.buttonClear);
        webView = (WebView)findViewById(R.id.webView);
        btnAccept.setOnClickListener(this);
//        btnPreview.setOnClickListener(this);
        btnDecline.setOnClickListener(this);
        btnIgnore.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        webView.setWebViewClient(new WebViewOverrideUrl());
        webView.getSettings().setJavaScriptEnabled(true);
        String url = item.getData().getUrl();
        if (!url.startsWith("http://"))
            url="http://"+url;
//        webView.loadUrl("http://"+item.getData().getUrl());
        webView.loadUrl(url);

    }
    private class WebViewOverrideUrl extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.buttonAccept:
                acceptAlert();
                break;
            case R.id.buttonDecline:
                declineAlert();
                break;
//            case R.id.buttonPreview:
//                previewAlert();
//                break;
            case R.id.buttonIgnore:
                ignoreAlert();
                break;
            case R.id.buttonClear:
                clearAlert();
                break;
        }

    }

    private void acceptAlert(){
        String url = item.getData().getUrl();
        long idOwner = item.getData().getDataInformationDeviceOwner().getId();
        long id = item.getData().getId();
        item.getData().setStatus(Consts.STATUS_ACCEPTED);

        UpdateUrlRequest request = new UpdateUrlRequest(WhiteListRequestActivity.this, idOwner, id, url, Consts.STATUS_ACCEPTED);
        contentManager.execute(request,
                new BaseRequestListener<WhiteUrlModel>(WhiteListRequestActivity.this, request) {
                    @Override public void onRequestSuccess(WhiteUrlModel baseModel) {
                        super.onRequestSuccess(baseModel);
                        backToAlertActivity(AlertsActivity.ACCEPT_ALERT, item);
                    }
                });

    }

    private void previewAlert(){
        String url = item.getData().getUrl();
        if (!url.startsWith("http://"))
            url = "http://" + url;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    private void ignoreAlert(){
        backToAlertActivity(AlertsActivity.IGNORE_ALERT, item);
    }
    private void clearAlert(){
        backToAlertActivity(AlertsActivity.CLEAR_ALERT, item);
    }

    private void declineAlert(){
        String url = item.getData().getUrl();
        long idOwner = item.getData().getDataInformationDeviceOwner().getId();
        long id = item.getData().getId();
        item.getData().setStatus(Consts.STATUS_DENIED);

        UpdateUrlRequest request = new UpdateUrlRequest(WhiteListRequestActivity.this, idOwner, id, url, Consts.STATUS_DENIED);
        contentManager.execute(request,
                new BaseRequestListener<WhiteUrlModel>(WhiteListRequestActivity.this, request) {
                    @Override public void onRequestSuccess(WhiteUrlModel baseModel) {
                        super.onRequestSuccess(baseModel);
                        backToAlertActivity(AlertsActivity.DECLINE_ALERT, item);
                    }
                });
    }

    /**
     *
     * @param result
     * @param item
     */
    private void backToAlertActivity(int result, NotificationsEntity item){
        Intent intent = new Intent();
        intent.putExtra(AlertsActivity.ALERT_RESULT, result);
        intent.putExtra(AlertsActivity.NOTIFICATION_EXTRA, (Parcelable)item);
        setResult(RESULT_OK, intent);
        finish();
    }
}
