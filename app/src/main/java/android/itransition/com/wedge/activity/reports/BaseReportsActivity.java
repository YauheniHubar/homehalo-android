package android.itransition.com.wedge.activity.reports;

import android.graphics.Color;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.BaseActivity;
import android.itransition.com.wedge.gui.EmptyTabFactory;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

/**
 * Created by y.drobysh on 10.01.2015.
 */
public abstract class BaseReportsActivity extends BaseActivity {


    static final String LAST_TAB = "selected_tab";
    private int colorBlue;
    private boolean isDemo;

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.imageViewHome:
//                finish();
//                break;
//            case R.id.textViewBack:
//                finish();
//                break;
//            case R.id.textViewMain:
//                finish();
//                break;
//            case R.id.imageViewUpdate:
//                finish();
//                break;
//            case R.id.buttonChange:
//                break;
//            default:
//                break;
//        }
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        colorBlue = getResources().getColor(R.color.reports_blue);

        //setBarColor(colorBlue);
        isDemo = Settings.isDemoMode();

        setNeedHome(true);
    }

    protected void setupTabs(final TabHost tabHost, Bundle state) {
        tabHost.setup();

        EmptyTabFactory emptyTabFactory = new EmptyTabFactory(this);

        TabHost.TabSpec spec = tabHost.newTabSpec("tab1");
        spec.setIndicator(getString(R.string.one_hour));
        spec.setContent(emptyTabFactory);
        tabHost.addTab(spec);
        spec = tabHost.newTabSpec("tab2");
        spec.setIndicator(getString(R.string.twenty_four_hours));
        spec.setContent(emptyTabFactory);
        tabHost.addTab(spec);
        spec = tabHost.newTabSpec("tab3");
        spec.setContent(emptyTabFactory);
        spec.setIndicator(getString(R.string.seven_days));
        tabHost.addTab(spec);
        spec = tabHost.newTabSpec("tab4");
        spec.setContent(emptyTabFactory);
        spec.setIndicator(getString(R.string.thirty_days));
        tabHost.addTab(spec);

        if (state != null) {
            tabHost.setCurrentTab(state.getInt(LAST_TAB));
        }

        final TabWidget widget = tabHost.getTabWidget();
        for(int i = 0; i < widget.getChildCount(); i++) {
            View v = widget.getChildAt(i);
            TextView tv = (TextView)v.findViewById(android.R.id.title);
            if(tv == null) {
                continue;
            }
            tv.setTextSize(12);
            tv.setAllCaps(false);
            v.setBackgroundResource(R.drawable.tab_selector);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) v.getLayoutParams();
            params.bottomMargin = params.leftMargin = params.rightMargin = params.topMargin = 2;
            if (i == 0) tv.setTextColor(Color.WHITE);
            else tv.setTextColor(colorBlue);

            v.getLayoutParams().height = Utils.dp2px(36, this);
        }

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                for(int i=0;i<widget.getChildCount();i++)
                {
                    View v = widget.getChildAt(i);
                    TextView tv = (TextView)v.findViewById(android.R.id.title);
                    if(tv == null)  continue;
                    tv.setTextColor(colorBlue);
                }

                ((TextView) widget.getChildAt(tabHost.getCurrentTab()).
                        findViewById(android.R.id.title)).setTextColor(Color.WHITE);

                onTabIntervalChanged(tabHost.getCurrentTab());
            }
        });
    }


    protected void onTabIntervalChanged(int i) {
        switch (i) {
            case 0:
                if (isDemo)
                getData(60 * 60);
                break;
            case 1:
                if (isDemo)
                getData(24 * 60 * 60);
                break;
            case 2:
                if (isDemo)
                getData(7 * 24 * 60 * 60);
                break;
            case 3:
                if (isDemo)
                getData(30 * 24 * 60 * 60);
                break;
        }
    }

    abstract void getData(long seconds);
}
