package android.itransition.com.wedge.activity.reports;

/**
 * Created by e.kazimirova on 18.10.2014.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.BrowsingHistory;
import android.itransition.com.wedge.entity.TrafficEntity;
import android.itransition.com.wedge.entity.TrafficsEntity;
import android.itransition.com.wedge.model.TrafficsModel;
import android.itransition.com.wedge.request.GetTrafficRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Legend;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


public class ChartReportsActivity extends BaseReportsActivity implements  View.OnClickListener {

    private static final int M_BYTES = 1024 * 1024;

    private LinearLayout listView;
    private PieChart pieChart;
    private TextView tvEmpty;

    private TabHost tabHost;
    private ArrayList<String> users;
    private BrowsingHistory[] browsingTraffic;
    private Size size;
    private boolean isDemo;



    public static int[] PIE_COLORS = new int[]{Color.parseColor("#673AB7"),//blue - the biggest
            Color.parseColor("#3F51B5"), Color.parseColor("#2196F3"), Color.parseColor("#00BCD4"),
            Color.parseColor("#009688"), Color.parseColor("#304FFE"),
            Color.parseColor("#6200EA"), Color.parseColor("#006064")};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pie_chart);


        setupActionBar();


        setTitle(getString(R.string.activity_reports_title));


        listView = (LinearLayout)findViewById(R.id.listView);
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        pieChart = (PieChart) findViewById(R.id.pieChart);
        tvEmpty = (TextView) findViewById(R.id.tvEmpty);

        int displayHeight = Utils.getDisplayHeight(this);
        pieChart.getLayoutParams().height = (int) (displayHeight * 0.45);
        pieChart.invalidate();


        setupTabs(tabHost, savedInstanceState);


        onTabIntervalChanged(tabHost.getCurrentTab());

        ////////

        isDemo = Settings.isDemoMode();
        if (!isDemo) {
            pieChart.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(LAST_TAB, tabHost.getCurrentTab());
    }

    @Override
    protected void getData(long seconds){
//        GetBrowsingHistoryRequest request =
//                new GetBrowsingHistoryRequest(this, Settings.getUserId(), seconds);
//        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
//                new BaseRequestListener<DeviceModel>(this, request) {
//                    @Override public void onRequestFailure(SpiceException e) {
//                        super.onRequestFailure(e);
//                    }
//                    @Override public void onRequestSuccess(DeviceModel o) {
//                        super.onRequestSuccess(o);
//                        browsingTraffic = o.getBrowsingTraffic();
//                        updatePieChart();
//                    }
//                });
        GetTrafficRequest request =
                new GetTrafficRequest(this, Settings.getUserId(), seconds);
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new BaseRequestListener<TrafficsModel>(this, request) {
                    @Override public void onRequestFailure(SpiceException e) {
                        super.onRequestFailure(e);
                    }
                    @Override public void onRequestSuccess(TrafficsModel o) {
                        super.onRequestSuccess(o);
                        List<TrafficsEntity> trafficsEntities = o.getTraffics();
                        browsingTraffic = new BrowsingHistory[trafficsEntities.size()];
                        for (int i = 0; i<trafficsEntities.size(); i++) {
                            BrowsingHistory history = new BrowsingHistory();
                            history.setDeviceOwnerId(trafficsEntities.get(i).getDeviceOwnerId());
                            history.setDeviceOwner(trafficsEntities.get(i).getDeviceOwner());
                            long traffic = 0;
                            List<TrafficEntity> traffics = trafficsEntities.get(i).getTraffic();
                            for (TrafficEntity entity : traffics) {
                                traffic += entity.getTraffic();
                            }
                            history.setTraffic(traffic);
                            browsingTraffic[i] = history;
                            Log.d("","");
                        }
                        updatePieChart();
//                        browsingTraffic = o.getBrowsingTraffic();
//                        updatePieChart();
                    }
                });
    }



    private void updatePieChart() {
        PieData pieData = getDataSetForPie(browsingTraffic);


        updateList(browsingTraffic);

        if (pieData ==  null) {
            pieChart.clear();
        } else {
            pieChart.setData(pieData);

            pieChart.setDescription("");
            pieChart.setHoleRadius(0f);
            pieChart.setTransparentCircleRadius(0f);
            pieChart.setDrawXValues(true);
            pieChart.setValueTextColor(Color.WHITE);
//            pieChart.setValueTextSize(12);
//            pieChart.setUnit(size.toString());
//            pieChart.setDrawUnitsInChart(true);

//            pieChart.setCenterTextSize(18f);
//            pieChart.setUsePercentValues(false);

            pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

                @Override public void onValueSelected(Entry entry, int i) {
                    if (entry != null)
                        openSingleUserChartActivity((BrowsingHistory)entry.getData());
                    else Utils.makeToast(getString(R.string.unexpected_error));
                }

                @Override public void onNothingSelected() { }
            });
            pieChart.invalidate();
            Legend l = pieChart.getLegend();
            l.setForm(Legend.LegendForm.CIRCLE);
            l.setFormSize(10f);
            l.setTextSize(13f);
            l.setTextColor(Color.rgb(240, 240, 240));

            l.setPosition(Legend.LegendPosition.NONE);
        }
    }

    private void updateList(final BrowsingHistory[] browsingTraffic) {
        LayoutInflater inflaterListView = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Integer[] indexes = new Integer[browsingTraffic.length];
        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = i;
        }

        Arrays.sort(indexes, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                if (browsingTraffic[rhs].getTraffic() > browsingTraffic[lhs].getTraffic()) return 1;
                else if (browsingTraffic[rhs].getTraffic() < browsingTraffic[lhs].getTraffic()) return -1;
                return 0;
            }
        });

        listView.removeAllViews();

        for (Integer i : indexes) {
//            long data = browsingTraffic[i].getTraffic();

            final View mLinearView = inflaterListView.inflate(R.layout.list_item_chart_reports, listView, false);
            TextView tvName = (TextView) mLinearView.findViewById(R.id.textViewTitle);
            TextView tvTraffic = (TextView) mLinearView.findViewById(R.id.textViewDownload);

//            ((TextView) mLinearView.findViewById(R.id.textViewDownload)).setText(toBytes(data));
            tvName.setText(browsingTraffic[i].getDeviceOwner());
            tvTraffic.setText(toBytes(browsingTraffic[i].getTraffic()));
//            RingView ringView = (RingView) mLinearView.findViewById(R.id.ringView);
//            ringView.setColor(PIE_COLORS[colorsCount % i]);
            listView.addView(mLinearView);
            /*add color background to list from PIE_COLORS array*/
            mLinearView.setBackgroundColor(PIE_COLORS[i%PIE_COLORS.length]);
            mLinearView.setTag(i);
            mLinearView.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
//        super.onClick(view);
        if (view.getId() == R.id.itemUserInfo) {
            int index = (int) view.getTag();
            showSingleUserChart(index);
        }
    }

    private void showSingleUserChart(int position) {

        BrowsingHistory history = browsingTraffic[position];
        openSingleUserChartActivity(history);
    }

    private void openSingleUserChartActivity(BrowsingHistory history){
        long id = history.getDeviceOwnerId();
//        Intent intent = new Intent(this, ChartReportUserActivity.class);
        Intent intent = new Intent(this, ReportsUserActivityTabs.class);
        intent.putExtra(Consts.OWNER_NAME, history.getDeviceOwner());
        intent.putExtra(Consts.DEVICE_OWNER_ID, id);
        intent.putExtra(LAST_TAB, tabHost.getCurrentTab());
        startActivity(intent);
    }

    private PieData getDataSetForPie(BrowsingHistory[] browsingTraffic) {
        ArrayList<Entry> entries = new ArrayList<>();
        long maxVal = -1;
        for (int counter = 0; counter < browsingTraffic.length; counter++)
        {
            if (browsingTraffic[counter].getTraffic() > maxVal)
            {
                maxVal = browsingTraffic[counter].getTraffic();
            }
        }

        size = getSize(maxVal);
//        pieChart.setUnit(size.toString());
        users = new ArrayList<>();
        int j = 0;

        for (int i = 0; i < browsingTraffic.length; i++) {
            long v = browsingTraffic[i].getTraffic();
            if (v == 0) continue;
            {
                Entry newEntry = new Entry(v, j);
                newEntry.setData(browsingTraffic[i]);
                entries.add(newEntry);
                j++;
            }

//            users.add(browsingTraffic[i].getDeviceOwner() + "\n" + toBytes(v));
            users.add(browsingTraffic[i].getDeviceOwner().substring(0, 3));
            //users.add(String.valueOf(browsingTraffic[i].getDeviceOwner()));
        }

        if (entries.size() > 0) {
            PieDataSet d = new PieDataSet(entries, "");
            d.setSliceSpace(3f);
            d.setColors(PIE_COLORS);
            //getUserList(users,browsingTraffic);
            PieData pieData = new PieData(users, d);

            return pieData;
        } else {
            return null;
        }
    }

    private void getUserList(ArrayList<String> users, BrowsingHistory[] browsingTraffic){
        for (int i = 0; i < browsingTraffic.length; i++) {
            users.add(browsingTraffic[i].getDeviceOwner());
        }
    }

    private String toBytes(long v) {
        if (v > M_BYTES) {
            v /= M_BYTES;
            return "\n " + v + " MB";
        }
        if (v > 1024) {
            v /= 1024;
            return "\n " + v + " KB";
        }
        return v + " B";
    }

//    float formatSize(long value) {
//        Log.d("grch11", "v:" + value);
//        float formattedValue = -1;
//        if (size.toString().equals(Size.Mb.string)) {
//            formattedValue = (float)value/(1024 * 1024);
//        } else if (size.toString().equals(Size.Kb.string)) {
//            formattedValue = (float)value/1024;
//        } else if (size.toString().equals(Size.b.string)) {
//            formattedValue = (float)value;
//        }
//        return formattedValue;
//    }

//    long formatSize(long value) {
//        if (value)
//    }

    private Size getSize(long maxVal) {
        if (maxVal > M_BYTES) {
            return Size.Mb;
        } else if (maxVal > 1024) {
            return Size.Kb;
        }
        return Size.b;
    }

    public enum Size {
        Mb("MB", M_BYTES), Kb("KB", 1024), b("B", 1);

        String string;
        int base;

        Size(String str, int size) {
            string = str;
            base = size;
        }

        public float getSize(float value) {
            return value / base;
        }

        @Override public String toString() {
            return string;
        }

        public String valueToString(float value) {
            if (this == b)
                return String.format("%.0f", getSize(value));
            return String.format("%.2f", getSize(value));
        }
    }
}