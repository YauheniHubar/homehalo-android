package android.itransition.com.wedge.activity.reports;


import android.app.Activity;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.BaseActivity;
import android.itransition.com.wedge.entity.UrlTrafficEntity;
import android.itransition.com.wedge.gui.adapters.ExpandableListBlockedAdapter;
import android.itransition.com.wedge.gui.fragments.BaseFragment;
import android.itransition.com.wedge.model.UrlTrafficModel;
import android.itransition.com.wedge.request.GetUrlsBrowsingTrafficRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by i.grechishchev on 30.07.2015.
 * itransition 2015
 */
public class ReportsBlockedFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener, ExpandableListView.OnGroupExpandListener{

    protected SpiceManager contentManager;
    private long mDeviceOwnerId;
    private int lastExpandedPosition = -1;
    int period;
    String status = "1,2";
    private ExpandableListView listView;
    private List<UrlTrafficEntity> urlTraffic;
    private List<UrlTrafficEntity> urlTrafficDay;
    private List<UrlTrafficEntity> urlTrafficWeek;
    private List<UrlTrafficEntity> urlTrafficMonth;
    private TextView tvEmpty;
    private boolean isDemo;
    ExpandableListBlockedAdapter listAdapter;
    List<UrlTrafficEntity> listDataHeader;
    HashMap<String, UrlTrafficEntity> listDataChild;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reports_blocked, container, false);
        listView = (ExpandableListView) rootView.findViewById(R.id.listViewBlocked);
        tvEmpty = (TextView)rootView.findViewById(R.id.tvEmpty);
        Intent intent = getActivity().getIntent();
        mDeviceOwnerId = intent.getLongExtra(Consts.DEVICE_OWNER_ID, 0);
        /*Bundle args = getArguments();
        if (args != null) {
            onCheckedChanged(null, args.getInt("id"));
        }*/
        listView.setOnGroupExpandListener(this);
        isDemo = Settings.isDemoMode();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            onCheckedChanged(null, args.getInt("id"));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.contentManager = ((BaseActivity) activity).contentManager;
    }

    private void getUrlTraffic(final int period) {
        GetUrlsBrowsingTrafficRequest request = new GetUrlsBrowsingTrafficRequest(getActivity(), mDeviceOwnerId, period, status);

        contentManager.execute(request, new BaseRequestListener<UrlTrafficModel>(getActivity(), request) {
            @Override
            public void onRequestFailure(SpiceException e) {
                super.onRequestFailure(e);
            }

            @Override
            public void onRequestSuccess(UrlTrafficModel model) {
                super.onRequestSuccess(model);
                switch (period) {
                    case 0:
                        if (urlTrafficDay == null) {
                            urlTrafficDay = model.getResults();
                        }
                        urlTraffic = urlTrafficDay;
                        break;
                    case 1:
                        if (urlTrafficWeek == null) {
                            urlTrafficWeek = model.getResults();
                        }
                        urlTraffic = urlTrafficWeek;
                        break;
                    case 2:
                        if (urlTrafficMonth == null) {
                            urlTrafficMonth = model.getResults();
                        }
                        urlTraffic = urlTrafficMonth;
                        break;
                }
                initTraffic(urlTraffic);
            }
        });
    }

    private void initTraffic(List<UrlTrafficEntity> urlTrafficEntity) {
        listDataHeader = new ArrayList<UrlTrafficEntity>();
        listDataChild = new HashMap<String, UrlTrafficEntity>();
        for (UrlTrafficEntity traffic : urlTrafficEntity) {
            listDataHeader.add(traffic);
            listDataChild.put(traffic.getUrl(), traffic);
        }
        listAdapter = new ExpandableListBlockedAdapter(listDataChild, listDataHeader, getActivity(), contentManager, mDeviceOwnerId);
        listView.setAdapter(listAdapter);
        if (urlTrafficEntity.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            tvEmpty.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.btn_day:
                period = 0;
                if (urlTrafficDay == null) {
                    getUrlTraffic(period);
                } else {
                    initTraffic(urlTrafficDay);
                }
                break;
            case R.id.btn_week:
                period = 1;
                if (urlTrafficWeek == null) {
                    getUrlTraffic(period);
                } else {
                    initTraffic(urlTrafficWeek);
                }
                break;
            case R.id.btn_month:
                period = 2;
                if (urlTrafficMonth == null) {
                    getUrlTraffic(period);
                } else {
                    initTraffic(urlTrafficMonth);
                }
                break;
        }
    }

    @Override
    public void onGroupExpand(int groupPosition) {
        if (lastExpandedPosition != -1
                && groupPosition != lastExpandedPosition) {
            listView.collapseGroup(lastExpandedPosition);
        }
        lastExpandedPosition = groupPosition;
    }
}
