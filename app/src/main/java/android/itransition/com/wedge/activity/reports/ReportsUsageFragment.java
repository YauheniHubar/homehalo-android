package android.itransition.com.wedge.activity.reports;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.BaseActivity;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.TrafficEntity;
import android.itransition.com.wedge.gui.fragments.BaseFragment;
import android.itransition.com.wedge.model.TrafficModel;
import android.itransition.com.wedge.request.GetBrowsingHistoryForOwnerRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.github.mikephil.charting.utils.XLabels;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by i.grechishchev on 29.07.2015.
 * itransition 2015
 */
public class ReportsUsageFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener{

    protected SpiceManager contentManager;
    private static final int M_BYTES = 1024 * 1024;
    private LineChart chart;
    private TextView tvEmpty;
    private ChartReportsActivity.Size size;
    private TrafficModel mTrafficModel;
    private long mDeviceOwnerId;
    private boolean isDemo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reports_usage, container, false);
        chart = (LineChart)rootView.findViewById(R.id.chart);
        tvEmpty = (TextView)rootView.findViewById(R.id.tvEmpty);
        Intent intent = getActivity().getIntent();
        mDeviceOwnerId = intent.getLongExtra(Consts.DEVICE_OWNER_ID, 0);
        Bundle args = getArguments();
        if (args != null) {
            onCheckedChanged(null, args.getInt("id"));
        }

        isDemo = Settings.isDemoMode();
        if (!isDemo) {
            chart.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.contentManager = ((BaseActivity)activity).contentManager;
    }

    private void updateChart() {
        LineData dataSet = getChartData();

        if (dataSet == null) return;

        chart.setData(dataSet);

        chart.setTouchEnabled(true);
        chart.setHighlightEnabled(false);
        chart.setPinchZoom(true);
        chart.setDrawYValues(false);
        chart.setDescription("");


        chart.setDrawHorizontalGrid(true);
        chart.setDrawVerticalGrid(true);
        chart.setDrawBorder(false);
        chart.setGridColor(Color.GRAY & 0x70FFFFFF);
        chart.setDrawGridBackground(false);

        chart.setUnit(size.toString());
        chart.setDrawLegend(false);

        chart.getYLabels().setFormatter(new ValueFormatter() {
            @Override public String getFormattedValue(float value) {
                return size.valueToString(value);
            }
        });

        XLabels xl = chart.getXLabels();
        xl.setCenterXLabelText(true);
        xl.setPosition(XLabels.XLabelPosition.BOTTOM);
        xl.setTextSize(8);

        chart.invalidate();
    }

    private LineData getChartData() {

        List<TrafficEntity> receivedData = mTrafficModel.traffic;

        if (receivedData == null || receivedData.size() < 1)
            return null;

        ArrayList<String> labels = new ArrayList<>(receivedData.size());
        ArrayList<Entry> user1Data = new ArrayList<>(receivedData.size());

        long maxVal = -1;

        for (int i = receivedData.size() - 1, k = 0; i >= 0; i--, k++) {
            TrafficEntity entity = receivedData.get(i);
            CreateTimeUser start = entity.getStart();
            if (start == null) continue;

            String label;
            /*int tab = tabHost.getCurrentTab();

            if (tab == 0) {
                label = start.getDateTime().substring(11, 16);
            } else if (tab == 1) {
                label = start.getDateTime().substring(5, 10) + "\n\n" + start.getDateTime().substring(11, 16);
            } else {
                label = start.getDateTime().substring(5, 10);
            }*/
            label = start.getDateTime().substring(11, 16);
            labels.add(label);

            long traffic = entity.getTraffic();

            Entry e = new Entry(traffic, k);
            user1Data.add(e);

            if (maxVal < traffic) {
                maxVal = traffic;
            }
        }

        size = getSize(maxVal);

        LineDataSet user1Set = new LineDataSet(user1Data, "");
        user1Set.setCircleColor(getResources().getColor(R.color.reports_blue));
        user1Set.setLineWidth(3f);
        user1Set.setCircleSize(0f);
        user1Set.setDrawCubic(true);
        user1Set.setCubicIntensity(0.05f);
        user1Set.setDrawFilled(true);
        user1Set.setColor(getResources().getColor(R.color.reports_blue));
        user1Set.setFillColor(getResources().getColor(R.color.reports_blue));

        ArrayList<LineDataSet> allData = new ArrayList<>(Arrays.asList(user1Set));

        LineData chartData = new LineData(labels, allData);
        return chartData;
    }

    private ChartReportsActivity.Size getSize(long maxVal) {
        if (maxVal > M_BYTES) {
            return ChartReportsActivity.Size.Mb;
        } else if (maxVal > 1024) {
            return ChartReportsActivity.Size.Kb;
        }
        return ChartReportsActivity.Size.b;
    }

    void getData (long seconds) {
        GetBrowsingHistoryForOwnerRequest request =
                new GetBrowsingHistoryForOwnerRequest(getActivity(), mDeviceOwnerId, seconds);
        contentManager.execute(request,
                new BaseRequestListener<TrafficModel>(getActivity(), request) {
                    @Override
                    public void onRequestSuccess(TrafficModel o) {
                        super.onRequestSuccess(o);
                        mTrafficModel = o;
                        updateChart();
                    }
                });
    }

//    protected void onTabIntervalChanged(int i) {
//        switch (i) {
//            case 0:
//                getData(24 * 60 * 60);
//                break;
//            case 1:
//                getData(7 * 24 * 60 * 60);
//                break;
//            case 2:
//                getData(30 * 24 * 60 * 60);
//                break;
//        }
//    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.btn_day:
                if (isDemo)
                getData(24 * 60 * 60);
                break;
            case R.id.btn_week:
                if (isDemo)
                getData(7 * 24 * 60 * 60);
                break;
            case R.id.btn_month:
                if (isDemo)
                getData(30 * 24 * 60 * 60);
                break;
        }
    }
}
