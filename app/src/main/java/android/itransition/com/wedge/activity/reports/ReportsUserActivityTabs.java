package android.itransition.com.wedge.activity.reports;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.TrafficEntity;
import android.itransition.com.wedge.gui.CheckableButton;
import android.itransition.com.wedge.model.TrafficModel;
import android.itransition.com.wedge.request.GetBrowsingHistoryForOwnerRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by i.grechishchev on 11.08.2015.
 * itransition 2015
 */
public class ReportsUserActivityTabs extends BaseReportsActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, TabHost.OnTabChangeListener {

    private static final int M_BYTES = 1024 * 1024;
    //    private LineChart chart;
    private TabHost mTabHost;
    private TabHost tabHostFilters;
    private RadioGroup radioGroup;
    private RadioButton rbDay;
    private CheckableButton btnDay, btnWeek, btnMonth;
    private TabWidget tabWidget;
    private int colorBlue;

    private long mDeviceOwnerId;
    private TrafficModel mTrafficModel;

    private Size size;
    private final String TABS_TAG_1 = "tab1";
    private final String TABS_TAG_2 = "tab2";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reports_user_tabs);

        setupActionBar();
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group_reports);
        rbDay = (RadioButton) findViewById(R.id.btn_day);
        tabWidget = (TabWidget) findViewById(android.R.id.tabs);

        Intent intent = getIntent();
        String title = intent.getStringExtra(Consts.OWNER_NAME);
        setTitle(title != null ? title : "user");
        mDeviceOwnerId = intent.getLongExtra(Consts.DEVICE_OWNER_ID, 0);
        mTabHost.setup();

        rbDay.setChecked(true);

        /** Defining tab builder for Usage tab */
        TabHost.TabSpec tSpecUsage = mTabHost.newTabSpec("usage");
        tSpecUsage.setIndicator(getString(R.string.usage));
        tSpecUsage.setContent(new DummyTabContent(getBaseContext()));
        mTabHost.addTab(tSpecUsage);

        /** Defining tab builder for Visited tab */
        TabHost.TabSpec tSpecVisited = mTabHost.newTabSpec("visited");
        tSpecVisited.setIndicator(getString(R.string.visited));
        tSpecVisited.setContent(new DummyTabContent(getBaseContext()));
        mTabHost.addTab(tSpecVisited);


        /** Defining tab builder for Blocked tab */
        TabHost.TabSpec tSpecBlocked = mTabHost.newTabSpec("blocked");
        tSpecBlocked.setIndicator(getString(R.string.blocked));
        tSpecBlocked.setContent(new DummyTabContent(getBaseContext()));
        mTabHost.addTab(tSpecBlocked);
        mTabHost.setOnTabChangedListener(this);
        mTabHost.setCurrentTabByTag("visited");
        initTabsAppearance(tabWidget);

    }

    private void initTabsAppearance(TabWidget tabWidget) {
        // Change background
        for(int i=0; i < tabWidget.getChildCount(); i++) {
            tabWidget.getChildAt(i).setBackgroundResource(R.drawable.tab_selector_reps);
            TextView tv = (TextView) tabWidget.getChildAt(i).findViewById(android.R.id.title);
//            tv.setTextColor(this.getResources().getColorStateList(R.color.reports_blue));
            tv.setTextColor(this.getResources().getColorStateList(R.color.text_color_indicator));
        }
//        tabWidget.getChildTabViewAt(0).setVisibility(View.GONE);
    }

    @Override
    public void onTabChanged(String tabId) {

        Log.d("TAG","tabId: " + tabId);
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        ReportsUsageFragment usageFragment = (ReportsUsageFragment) fm.findFragmentByTag("usage");
        ReportsVisitedFragment visitedFragment = (ReportsVisitedFragment) fm.findFragmentByTag("visited");
        ReportsBlockedFragment blockedFragment = (ReportsBlockedFragment) fm.findFragmentByTag("blocked");
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment;
        Bundle args = new Bundle();

        if (usageFragment != null)
            ft.detach(usageFragment);

        if (visitedFragment != null)
            ft.detach(visitedFragment);

        if (blockedFragment != null)
            ft.detach(blockedFragment);

        /** If current tab is android */
        if (tabId.equalsIgnoreCase("usage")) {

            if (usageFragment == null) {
                /** Create fragment and adding to fragmenttransaction */
                fragment = new ReportsUsageFragment();
                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
                args.putInt("id", radioGroup.getCheckedRadioButtonId());
                fragment.setArguments(args);
                ft.add(R.id.realtabcontent, fragment, "usage");
            } else {
                /** Bring to the front, if already exists in the fragmenttransaction */
                fragment = usageFragment;
                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
                fragment.getArguments().putInt("id", radioGroup.getCheckedRadioButtonId());
                ft.attach(fragment);
            }

        } else if (tabId.equalsIgnoreCase("visited")) {    /** If current tab is apple */
            if (visitedFragment == null) {
                fragment = new ReportsVisitedFragment();
                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
                args.putInt("id", radioGroup.getCheckedRadioButtonId());
                fragment.setArguments(args);
                ft.add(R.id.realtabcontent, fragment, "visited");
            } else {
                fragment = visitedFragment;
                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
                fragment.getArguments().putInt("id", radioGroup.getCheckedRadioButtonId());
                ft.attach(fragment);
            }
        } else if (tabId.equalsIgnoreCase("blocked")) {
            if (blockedFragment == null) {
                fragment = new ReportsBlockedFragment();
                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
                args.putInt("id", radioGroup.getCheckedRadioButtonId());
                fragment.setArguments(args);
                ft.add(R.id.realtabcontent, fragment, "blocked");
            } else {
                fragment = blockedFragment;
                radioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener) fragment);
                fragment.getArguments().putInt("id", radioGroup.getCheckedRadioButtonId());
                ft.attach(fragment);
            }
        }
        ft.commit();
    }

    public class DummyTabContent implements TabHost.TabContentFactory {
        private Context mContext;

        public DummyTabContent(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            return v;
        }
    }


    private LineData getChartData() {

        List<TrafficEntity> receivedData = mTrafficModel.traffic;

        if (receivedData == null || receivedData.size() < 1)
            return null;

        ArrayList<String> labels = new ArrayList<>(receivedData.size());
        ArrayList<Entry> user1Data = new ArrayList<>(receivedData.size());

        long maxVal = -1;

        for (int i = receivedData.size() - 1, k = 0; i >= 0; i--, k++) {
            TrafficEntity entity = receivedData.get(i);
            CreateTimeUser start = entity.getStart();
            if (start == null) continue;

            String label;
            int tab = mTabHost.getCurrentTab();

            if (tab == 0) {
                label = start.getDateTime().substring(11, 16);
            } else if (tab == 1) {
                label = start.getDateTime().substring(5, 10) + "\n\n" + start.getDateTime().substring(11, 16);
            } else {
                label = start.getDateTime().substring(5, 10);
            }
            labels.add(label);

            long traffic = entity.getTraffic();

            Entry e = new Entry(traffic, k);
            user1Data.add(e);

            if (maxVal < traffic) {
                maxVal = traffic;
            }
        }

        size = getSize(maxVal);

        LineDataSet user1Set = new LineDataSet(user1Data, "");
        user1Set.setColor(getResources().getColor(R.color.reports_blue));
        user1Set.setCircleColor(getResources().getColor(R.color.reports_blue));
        user1Set.setLineWidth(3f);
        user1Set.setCircleSize(0f);

        ArrayList<LineDataSet> allData = new ArrayList<>(Arrays.asList(user1Set));

        LineData chartData = new LineData(labels, allData);
        return chartData;
    }

    private Size getSize(long maxVal) {
        if (maxVal > M_BYTES) {
            return Size.Mb;
        } else if (maxVal > 1024) {
            return Size.Kb;
        }
        return Size.b;
    }

    @Override
    void getData(long seconds) {
        GetBrowsingHistoryForOwnerRequest request =
                new GetBrowsingHistoryForOwnerRequest(this, mDeviceOwnerId, seconds);
        contentManager.execute(request,
                new BaseRequestListener<TrafficModel>(this, request) {
                    @Override
                    public void onRequestSuccess(TrafficModel o) {
                        super.onRequestSuccess(o);
                        mTrafficModel = o;
//                        updateChart();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        /*switch (v.getId()) {
            case R.id.btn_day:
                Log.d("TAG", "BTN 1");
                break;
            case R.id.btn_week:
                break;
            case R.id.btn_month:
                break;
        }*/
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.btn_day:
                break;
            case R.id.btn_week:
                break;
            case R.id.btn_month:
                break;
        }

    }


    static enum Size {
        Mb("MB", M_BYTES), Kb("KB", 1024), b("b", 1);

        String string;
        int base;

        Size(String str, int size) {
            string = str;
            base = size;
        }

        float getSize(float value) {
            return value / base;
        }

        @Override
        public String toString() {
            return string;
        }

        public String valueToString(float value) {
            if (this == b)
                return String.format("%.0f", getSize(value));
            return String.format("%.2f", getSize(value));
        }
    }

    protected void setupTheTabs(final TabHost tabHost, Bundle state) {
        tabHost.setup();

        TabHost.TabContentFactory TabFactory = new TabHost.TabContentFactory() {

            @Override
            public View createTabContent(String tag) {
                if (tag == TABS_TAG_1) {
                    return getLayoutInflater().inflate(R.layout.fragment_reports_usage, null);
                } else if (tag == TABS_TAG_2) {
                    return getLayoutInflater().inflate(R.layout.fragment_reports_usage, null);
                }
                return null;
            }
        };

        TabHost.TabSpec spec = tabHost.newTabSpec("tab1");
        spec.setIndicator("Usage");
        spec.setContent(new Intent(this, ReportsUsageFragment.class));
//        spec.setContent(emptyTabFactory);
        tabHost.addTab(spec);
        spec = tabHost.newTabSpec("tab2");
        spec.setIndicator("Visited");
        spec.setContent(new Intent(this, ReportsUsageFragment.class));
        tabHost.addTab(spec);
        spec = tabHost.newTabSpec("tab3");
        spec.setContent(new Intent(this, ReportsUsageFragment.class));
        spec.setIndicator("Blocked");
        tabHost.addTab(spec);

//        if (state != null) {
//            tabHost.setCurrentTab(state.getInt(LAST_TAB));
//        }

        final TabWidget widget = tabHost.getTabWidget();
        for (int i = 0; i < widget.getChildCount(); i++) {
            View v = widget.getChildAt(i);
            TextView tv = (TextView) v.findViewById(android.R.id.title);
            if (tv == null) {
                continue;
            }
            tv.setTextSize(12);
            tv.setAllCaps(false);
//            v.setBackgroundResource(R.drawable.tab_selector);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) v.getLayoutParams();
            params.bottomMargin = params.leftMargin = params.rightMargin = params.topMargin = 2;
            if (i == 0) tv.setTextColor(Color.WHITE);
            else tv.setTextColor(colorBlue);

            v.getLayoutParams().height = Utils.dp2px(36, this);
        }

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                for (int i = 0; i < widget.getChildCount(); i++) {
                    View v = widget.getChildAt(i);
                    TextView tv = (TextView) v.findViewById(android.R.id.title);
                    if (tv == null) continue;
                    tv.setTextColor(colorBlue);
                }

                ((TextView) widget.getChildAt(tabHost.getCurrentTab()).
                        findViewById(android.R.id.title)).setTextColor(Color.WHITE);
//TODO: add in checkbuttons later
//                onTabIntervalChanged(tabHost.getCurrentTab());
            }
        });
    }
}