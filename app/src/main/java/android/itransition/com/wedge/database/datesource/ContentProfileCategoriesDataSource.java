package android.itransition.com.wedge.database.datesource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.itransition.com.wedge.database.dbhelpers.ContentCategoriesStaticDbHelper;
import android.itransition.com.wedge.database.dbhelpers.ContentProfilesCategoriesDbHelper;
import android.itransition.com.wedge.entity.ContentCategories;
import android.itransition.com.wedge.entity.ContentProfile;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 27.11.2014.
 * Class for content profile entity and content profile categories entity
 */


public class ContentProfileCategoriesDataSource {


        private MainSource source;

        public ContentProfileCategoriesDataSource(Context context, MainSource source) {
           this.source = source;
        }

    public void close(){
        source.close();
    }

       public void createContentProfile(ContentCategories contentProfile) {
           open();
           ContentValues values = new ContentValues();

           values.put(ContentCategoriesStaticDbHelper.COLUMN_CATEGORY_ID, contentProfile
                   .getCategoryId());
           values.put(ContentCategoriesStaticDbHelper.COLUMN_CATEGORY_NAME, contentProfile
                   .getCategoryName());
           values.put(ContentCategoriesStaticDbHelper.COLUMN_ID, contentProfile
                   .getId());
           values.put(ContentCategoriesStaticDbHelper.COLUMN_TITLE, contentProfile
                   .getTitle());

           source.database.insert(ContentCategoriesStaticDbHelper.TABLE,  null,
                   values);

       }

    public void deleteContentProfileAll() {
        open();
        source.database.delete(ContentCategoriesStaticDbHelper.TABLE,
               null, null);

    }
       public void deleteContentProfile(int id) {
           open();
           source.database.delete(ContentCategoriesStaticDbHelper.TABLE,
                    ContentCategoriesStaticDbHelper.COLUMN_ID
                            + " = " + id, null);

        }

       public List<ContentCategories> getAllContentProfile() {
           open();
           List<ContentCategories> contentProfiles = new ArrayList<ContentCategories>();

            Cursor cursorContentProfile = source.database.query(ContentCategoriesStaticDbHelper.TABLE,
                    source.allColumnsCategoriesStatic, null, null, null, null, ContentCategoriesStaticDbHelper.
                            COLUMN_CATEGORY_NAME + " ASC");


            cursorContentProfile.moveToFirst();
            while (!cursorContentProfile.isAfterLast()) {

                contentProfiles.add(cursorToContentProfilesCategories(cursorContentProfile));
                cursorContentProfile.moveToNext();
            }

            cursorContentProfile.close();
            return contentProfiles;
        }

    private   ContentCategories cursorToContentProfilesCategories(Cursor cursor) {
        open();
        ContentCategories contentProfileCategories = new ContentCategories();

        contentProfileCategories.setId(cursor.getLong(cursor.getColumnIndex
                (ContentCategoriesStaticDbHelper.COLUMN_ID)));
        contentProfileCategories.setTitle(cursor.getString(cursor.getColumnIndex
                (ContentCategoriesStaticDbHelper.COLUMN_TITLE)));
        contentProfileCategories.setCategoryId(cursor.getLong(cursor.getColumnIndex
                (ContentCategoriesStaticDbHelper.COLUMN_CATEGORY_ID)));
        contentProfileCategories.setCategoryName(cursor.getString(cursor.getColumnIndex
                (ContentCategoriesStaticDbHelper.COLUMN_CATEGORY_NAME)));


        return contentProfileCategories;
    }

    private void open() {
        if(!source.database.isOpen()){
            try {
                source.open();
            }catch (SQLException ex){}

        }
    }

    public void updateActiveCategories(ContentProfile profile) {
        if (profile == null) return;

        open();
        source.database.delete(ContentProfilesCategoriesDbHelper.TABLE,
                ContentProfilesCategoriesDbHelper.COLUMN_ID_CONTENT_PROFILES + " = " + profile.getId(), null);

        ContentCategories[] activeCategories = profile.getContentCategories();
        if (activeCategories != null) {
            ContentValues values;
            for (ContentCategories activeCategory : activeCategories) {
                        values = new ContentValues();
                        values.put(ContentProfilesCategoriesDbHelper.COLUMN_CATEGORY_ID, activeCategory.getId());
                        values.put(ContentProfilesCategoriesDbHelper.COLUMN_CATEGORY_NAME, activeCategory.getCategoryName());
                        values.put(ContentProfilesCategoriesDbHelper.COLUMN_ID, activeCategory.getId());
                        values.put(ContentProfilesCategoriesDbHelper.COLUMN_ID_CONTENT_PROFILES, profile.getId());
                        values.put(ContentProfilesCategoriesDbHelper.COLUMN_TITLE, activeCategory.getTitle());
                        source.database.insert(ContentProfilesCategoriesDbHelper.TABLE, null,
                                values);
            }
        }

    }
}

