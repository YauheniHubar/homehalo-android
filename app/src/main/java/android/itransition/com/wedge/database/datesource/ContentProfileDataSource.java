package android.itransition.com.wedge.database.datesource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.itransition.com.wedge.database.dbhelpers.ContentProfileDbHelper;
import android.itransition.com.wedge.database.dbhelpers.ContentProfilesCategoriesDbHelper;
import android.itransition.com.wedge.entity.ContentCategories;
import android.itransition.com.wedge.entity.ContentProfile;
import android.itransition.com.wedge.entity.CreateTimeUser;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 27.11.2014.
 * Class for content profile entity and content profile categories entity
 */


public class ContentProfileDataSource {


        private MainSource source;

        public ContentProfileDataSource(Context context, MainSource source) {
           this.source = source;
        }

    public ContentProfileDataSource(MainSource source) {
        this.source = source;
    }

    public void close(){
        source.close();
    }

       public void createContentProfile(ContentProfile contentProfile) {
           if(!source.database.isOpen()){
               try {
                   source.open();
               }catch (SQLException ex){}

           }

           long id = contentProfile.getId();
           source.database.delete(ContentProfileDbHelper.TABLE, ContentProfileDbHelper.COLUMN_ID + " = " + id, null);
           source.database.delete(ContentProfilesCategoriesDbHelper.TABLE,
                   ContentProfilesCategoriesDbHelper.COLUMN_ID_CONTENT_PROFILES + " = " + id, null);

           List<ContentProfile> l =getAllContentProfile();

           if(!l.contains(contentProfile)) {
               ContentValues values = new ContentValues();

               values.put(ContentProfileDbHelper.COLUMN_ID, contentProfile.getId());
               values.put(ContentProfileDbHelper.COLUMN_TITLE, contentProfile.getTitle());
               values.put(ContentProfileDbHelper.COLUMN_USER_ID, contentProfile.getUserId());
               values.put(ContentProfileDbHelper.COLUMN_CREATED_AT_TIME_ZONE,
                       contentProfile.getCreateAt().getTimeZone());
               values.put(ContentProfileDbHelper.COLUMN_CREATED_AT_DATE_TIME,
                       contentProfile.getCreateAt().getDateTime());
               values.put(ContentProfileDbHelper.COLUMN_IS_DEFAULT,
                       contentProfile.getDefault() ? 1 : 0);
               values.put(ContentProfileDbHelper.COLUMN_IS_DELETABLE,
                       contentProfile.getDeletable() ? 1 : 0);
               long insertId = source.database.insert(ContentProfileDbHelper.TABLE, null,
                       values);

               if (contentProfile.getContentCategories() != null) {
                   for (int i = 0; i < contentProfile.getContentCategories().length; i++) {
                       values = new ContentValues();
                       values.put(ContentProfilesCategoriesDbHelper.COLUMN_CATEGORY_ID, contentProfile
                               .getContentCategories()[i].getId());
                       values.put(ContentProfilesCategoriesDbHelper.COLUMN_CATEGORY_NAME, contentProfile
                               .getContentCategories()[i].getCategoryName());
                       values.put(ContentProfilesCategoriesDbHelper.COLUMN_ID, contentProfile
                               .getContentCategories()[i].getId());
                       values.put(ContentProfilesCategoriesDbHelper.COLUMN_ID_CONTENT_PROFILES, contentProfile
                               .getId());
                       values.put(ContentProfilesCategoriesDbHelper.COLUMN_TITLE, contentProfile
                               .getContentCategories()[i].getTitle());
                       source.database.insert(ContentProfilesCategoriesDbHelper.TABLE, null,
                               values);
                   }
               }
           }
        }

    public void deleteContentProfileAll(){
        if(!source.database.isOpen()){
            try {
                source.open();
            }catch (SQLException ex){}

        }
        source.database.delete(ContentProfilesCategoriesDbHelper.TABLE,
                null, null);
        source.database.delete(ContentProfileDbHelper.TABLE,
               null, null);
    }
       public void deleteContentProfile(int id) {
           if(!source.database.isOpen()){
               try {
                   source.open();
               }catch (SQLException ex){}

           }
           source.database.delete(ContentProfilesCategoriesDbHelper.TABLE,
                   ContentProfilesCategoriesDbHelper.COLUMN_ID_CONTENT_PROFILES
                           + " = " + id, null);
           source.database.delete(ContentProfileDbHelper.TABLE,
                    ContentProfileDbHelper.COLUMN_ID
                    + " = " + id, null);
        }

       public List<ContentProfile> getAllContentProfile() {
           if(!source.database.isOpen()){
               try {
                   source.open();
               }catch (SQLException ex){}

           }
            List<ContentProfile> contentProfiles = new ArrayList<ContentProfile>();

            Cursor cursorContentProfile = source.database.query(ContentProfileDbHelper.TABLE,
                    source.allColumnsContentProfile, null, null, null, null, null);


            cursorContentProfile.moveToFirst();
            while (!cursorContentProfile.isAfterLast()) {
                ContentProfile contentProfile = cursorToContentProfiles(cursorContentProfile,
                        source.database,
                        source.allColumnsCategories);
                contentProfiles.add(contentProfile);
                cursorContentProfile.moveToNext();
            }

            cursorContentProfile.close();
            return contentProfiles;
        }

    private   ContentCategories cursorToContentProfilesCategories(Cursor cursor) {
        if(!source.database.isOpen()){
            try {
                source.open();
            }catch (SQLException ex){}

        }
        ContentCategories contentProfileCategories = new ContentCategories();

        contentProfileCategories.setId(cursor.getLong(cursor.getColumnIndex
                (ContentProfilesCategoriesDbHelper.COLUMN_ID)));
        contentProfileCategories.setTitle(cursor.getString(cursor.getColumnIndex
                (ContentProfilesCategoriesDbHelper.COLUMN_TITLE)));
        contentProfileCategories.setCategoryId(cursor.getLong(cursor.getColumnIndex
                (ContentProfilesCategoriesDbHelper.COLUMN_CATEGORY_ID)));
        contentProfileCategories.setCategoryName(cursor.getString(cursor.getColumnIndex
                (ContentProfilesCategoriesDbHelper.COLUMN_CATEGORY_NAME)));
        contentProfileCategories.setIdContentProfile(cursor.getLong(cursor.getColumnIndex
                (ContentProfilesCategoriesDbHelper.COLUMN_ID_CONTENT_PROFILES)));

        return contentProfileCategories;
    }

        public  ContentProfile cursorToContentProfiles(Cursor cursor, SQLiteDatabase database,
                                                             String[]  allColumnsCategories) {

            if (cursor.isAfterLast()) return null;

            if(!source.database.isOpen()){
                try {
                    source.open();
                }catch (SQLException ex){}

            }

            ContentProfile contentProfile = new ContentProfile();
            contentProfile.setId(cursor.getLong(cursor.getColumnIndex(ContentProfileDbHelper.COLUMN_ID)));
            contentProfile.setTitle(cursor.getString
                    (cursor.getColumnIndex(ContentProfileDbHelper.COLUMN_TITLE)));
            contentProfile.setDefault(cursor.getInt(
                    cursor.getColumnIndex(ContentProfileDbHelper.COLUMN_IS_DEFAULT))
                    == 1 ? true : false);
            contentProfile.setDeletable(cursor.getInt(
                    cursor.getColumnIndex(ContentProfileDbHelper.COLUMN_IS_DELETABLE))
                    == 1 ? true : false);

            contentProfile.setUserId(cursor.getColumnIndex(ContentProfileDbHelper.COLUMN_USER_ID));

            CreateTimeUser createTimeUser = new CreateTimeUser();
            createTimeUser.setDateTime(cursor.getString
                    (cursor.getColumnIndex(ContentProfileDbHelper.COLUMN_CREATED_AT_DATE_TIME)));
            createTimeUser.setTimeZone(cursor.getString
                    (cursor.getColumnIndex(ContentProfileDbHelper.COLUMN_CREATED_AT_TIME_ZONE)));

            contentProfile.setCreateAt(createTimeUser);


            Cursor cursorContentProfileCategories = database.query(ContentProfilesCategoriesDbHelper.TABLE,
                    allColumnsCategories, ContentProfilesCategoriesDbHelper.COLUMN_ID_CONTENT_PROFILES
                            + " = " + contentProfile.getId(), null, null, null, null);

            cursorContentProfileCategories.moveToFirst();
            ContentCategories[] categories =
                    new ContentCategories[cursorContentProfileCategories.getCount()];
            int i =0;
            while (!cursorContentProfileCategories.isAfterLast()) {
                ContentCategories contentCategories = cursorToContentProfilesCategories(cursorContentProfileCategories);
                categories[i] = contentCategories;
                cursorContentProfileCategories.moveToNext();
                i+=1;
            }

            cursorContentProfileCategories.close();

            contentProfile.setContentCategories(categories);
            return contentProfile;
        }
    }

