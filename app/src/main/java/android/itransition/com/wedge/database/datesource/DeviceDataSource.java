package android.itransition.com.wedge.database.datesource;

import android.content.ContentValues;
import android.database.Cursor;
import android.itransition.com.wedge.database.dbhelpers.DeviceOwnersDbHelper;
import android.itransition.com.wedge.database.dbhelpers.DevicesDbHelper;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.DevicesEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 27.11.2014.
 * Class for content profile entity and content profile categories entity
 */


public class DeviceDataSource {

    private MainSource source;

    public DeviceDataSource(MainSource source) {
        this.source = source;
    }


    public void createDevices(DevicesEntity device) {
        open();

        ContentValues values = new ContentValues();

        values.put(DevicesDbHelper.COLUMN_ID, device.getId());
        values.put(DevicesDbHelper.COLUMN_TITLE, device.getTitle());
        values.put(DevicesDbHelper.COLUMN_CREATED_AT_DATE_TIME,
                device.getCreateAt().getDateTime());
        values.put(DevicesDbHelper.COLUMN_CREATED_AT_TIME_ZONE,
                device.getCreateAt().getTimeZone());
        values.put(DevicesDbHelper.COLUMN_IS_FAVOURITE,
                device.getFavourite() ? 1 : 0);
        values.put(DevicesDbHelper.COLUMN_IS_ONLINE,
                device.isOnline() ? 1 : 0);
        values.put(DevicesDbHelper.COLUMN_IS_ENABLED,
                device.getEnabled() ? 1 : 0);
        values.put(DevicesDbHelper.COLUMN_MAC_ADDRESS, device.getMacAddress());
        values.put(DevicesDbHelper.COLUMN_MANUFACTURER_TITLE, device.getManufacturedTitle());
        values.put(DevicesDbHelper.COLUMN_DEVICE_OWNER, device.getDeviceOwner().getId());
         /*  DeviceOwnersDataSource ownersDataSource = new DeviceOwnersDataSource(context, source);
           ownersDataSource.createDeviceOwner(device.getDeviceOwner());


           if(device.getLastOnlineAt()!=null) {
               values.put(DevicesDbHelper.COLUMN_LAST_ONLINE_AT_DATE_TIME, device.getLastOnlineAt().getDateTime());
               values.put(DevicesDbHelper.COLUMN_LAST_ONLINE_AT_TIME_ZONE, device.getLastOnlineAt().getTimeZone());
           }
           */
        long insertId = source.database.insert(DevicesDbHelper.TABLE, null,
                values);

    }

    public void deleteDevicesAll() {
        open();
        source.database.delete(DevicesDbHelper.TABLE,
                null, null);
    }

    public void deleteContentProfile(int id) {
        open();
        source.database.delete(DevicesDbHelper.TABLE,
                DevicesDbHelper.COLUMN_ID
                        + " = " + id, null);
    }

    public List<DevicesEntity> getAllDevices() {
        open();
        List<DevicesEntity> devicesEntities = new ArrayList<DevicesEntity>();

        Cursor cursorDevices = source.database.query(DevicesDbHelper.TABLE,
                source.allColumnDevices, null, null, null, null, null);


        cursorDevices.moveToFirst();
        while (!cursorDevices.isAfterLast()) {
            DevicesEntity device = cursorToDevices(cursorDevices);
            devicesEntities.add(device);
            cursorDevices.moveToNext();
        }

        cursorDevices.close();
        return devicesEntities;
    }

    public DevicesEntity getDeviceById(long id) {
        open();

        Cursor cursorDevices = source.database.query(DevicesDbHelper.TABLE,
                source.allColumnDevices, DevicesDbHelper.COLUMN_ID + " = " + id, null, null, null, null);

        DevicesEntity result = null;
        if (cursorDevices.getCount() > 0) {
            cursorDevices.moveToFirst();
            result = cursorToDevices(cursorDevices);
        }
        cursorDevices.close();

        return result;
    }

    public void open() {
        if (!source.database.isOpen()) {
            try {
                source.open();
            } catch (SQLException ex) {
            }

        }
    }

    public void close() {
        source.close();
    }

    private DevicesEntity cursorToDevices(Cursor cursor) {
        open();
        DevicesEntity devicesEntity = new DevicesEntity();
        long deviceId = cursor.getLong(cursor.getColumnIndex(DevicesDbHelper.COLUMN_ID));
        devicesEntity.setId(deviceId);

        CreateTimeUser createTimeUser = new CreateTimeUser();
        createTimeUser.setDateTime(cursor.getString(cursor.getColumnIndex(DevicesDbHelper.COLUMN_CREATED_AT_DATE_TIME)));
        createTimeUser.setTimeZone(cursor.getString(cursor.getColumnIndex(DevicesDbHelper.COLUMN_CREATED_AT_TIME_ZONE)));
        devicesEntity.setCreateAt(createTimeUser);

        long ownerId = cursor.getLong(cursor.getColumnIndex(DevicesDbHelper.COLUMN_DEVICE_OWNER));

        DeviceOwnersDataSource ownersDataSource = new DeviceOwnersDataSource(null, source);

        Cursor cursorDeviceOwner = source.database.query(DeviceOwnersDbHelper.TABLE,
                source.allColumnsDeviceOwners, DeviceOwnersDbHelper.COLUMN_ID + " = " + ownerId, null, null, null, null);

        cursorDeviceOwner.moveToFirst();
        devicesEntity.setDeviceOwner(ownersDataSource.cursorToDeviceOwner(cursorDeviceOwner));


        devicesEntity.setEnabled(cursor.getInt(
                cursor.getColumnIndex(DevicesDbHelper.COLUMN_IS_ENABLED))
                == 1 ? true : false);
        devicesEntity.setFavourite(cursor.getInt(
                cursor.getColumnIndex(DevicesDbHelper.COLUMN_IS_FAVOURITE))
                == 1 ? true : false);
        devicesEntity.setTitle(cursor.getString(cursor.getColumnIndex(DevicesDbHelper.COLUMN_TITLE)));
        devicesEntity.setOnline(cursor.getInt(
                cursor.getColumnIndex(DevicesDbHelper.COLUMN_IS_ONLINE))
                == 1 ? true : false);
        devicesEntity.setMacAddress(cursor.getString(cursor.getColumnIndex(DevicesDbHelper.COLUMN_MAC_ADDRESS)));
        devicesEntity.setManufacturedTitle(cursor.getString(cursor.getColumnIndexOrThrow(DevicesDbHelper.COLUMN_MANUFACTURER_TITLE)));

        CreateTimeUser lastOnline = new CreateTimeUser();
        lastOnline.setDateTime(cursor.getString(cursor.getColumnIndex(DevicesDbHelper.COLUMN_LAST_ONLINE_AT_DATE_TIME)));
        lastOnline.setTimeZone(cursor.getString(cursor.getColumnIndex(DevicesDbHelper.COLUMN_LAST_ONLINE_AT_TIME_ZONE)));

        devicesEntity.setLastOnlineAt(lastOnline);
        return devicesEntity;
    }


    public void updateDevice(DevicesEntity device) {
        open();

        ContentValues values = new ContentValues();

        values.put(DevicesDbHelper.COLUMN_IS_ENABLED, device.getEnabled());
        DeviceOwner deviceOwner = device.getDeviceOwner();
        if (deviceOwner != null) {
            values.put(DevicesDbHelper.COLUMN_DEVICE_OWNER, deviceOwner.getId());
        }
        values.put(DevicesDbHelper.COLUMN_IS_FAVOURITE, device.getFavourite());
        values.put(DevicesDbHelper.COLUMN_IS_ONLINE, device.isOnline());
        values.put(DevicesDbHelper.COLUMN_TITLE, device.getTitle());
        if (device.getLastOnlineAt() != null) {
            values.put(DevicesDbHelper.COLUMN_LAST_ONLINE_AT_DATE_TIME, device.getLastOnlineAt().getTimeZone());
        }
        values.put(DevicesDbHelper.COLUMN_MAC_ADDRESS, device.getMacAddress());
        values.put(DevicesDbHelper.COLUMN_MANUFACTURER_TITLE, device.getManufacturedTitle());

        source.database.update(DevicesDbHelper.TABLE, values, DevicesDbHelper.COLUMN_ID + " = " + device.getId(), null);

    }
}

