package android.itransition.com.wedge.database.datesource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.itransition.com.wedge.database.dbhelpers.BlacklistedCategoriesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.ContentProfileDbHelper;
import android.itransition.com.wedge.database.dbhelpers.DeviceOwnersDbHelper;
import android.itransition.com.wedge.database.dbhelpers.HomeworkCategoriesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.TimeExtensionsDbHelper;
import android.itransition.com.wedge.database.dbhelpers.TimeProfilesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.WhiteListUrlDbHelper;
import android.itransition.com.wedge.entity.ContentProfile;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.Time;
import android.itransition.com.wedge.entity.TimeExtensions;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.entity.UrlListEntity;
import android.itransition.com.wedge.model.PopularBlacklistSimpleItem;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 27.11.2014.
 * Class for device owner entity
 */


public class DeviceOwnersDataSource {
    private MainSource source;

    public  DeviceOwnersDataSource(Context context, MainSource source) {
        this.source = source;
    }

    public  DeviceOwnersDataSource(MainSource source) {
        this.source = source;
    }

       public void createDeviceOwner(DeviceOwner deviceOwner, int orderId) {
           open();
           List<DeviceOwner> list = getAllDeviceOwners();
           if(!list.contains(deviceOwner)) {

               ContentValues values = new ContentValues();

               values.put(DeviceOwnersDbHelper.COLUMN_ID, deviceOwner.getId());
               values.put(DeviceOwnersDbHelper.COLUMN_TITLE, deviceOwner.getTitle());
               if (deviceOwner.
                       getCreateAt() != null) {
                   values.put(DeviceOwnersDbHelper.COLUMN_CREATED_AT_DATE_TIME, deviceOwner.
                           getCreateAt().getDateTime());
                   values.put(DeviceOwnersDbHelper.COLUMN_CREATED_AT_TIME_ZONE, deviceOwner.
                           getCreateAt().getTimeZone());
               }
               values.put(DeviceOwnersDbHelper.COLUMN_ID_CONTENT_PROFILE, deviceOwner.
                       getContentProfile().getId());
               values.put(DeviceOwnersDbHelper.COLUMN_STATUS, deviceOwner.getStatus());
               values.put(DeviceOwnersDbHelper.COLUMN_TIME, deviceOwner.getTime());
               if (deviceOwner.
                       getBlockStartedAt() != null) {
                   values.put(DeviceOwnersDbHelper.COLUMN_BLOCK_STARTED_AT_DATE_TIME, deviceOwner.
                           getBlockStartedAt().getDateTime());
                   values.put(DeviceOwnersDbHelper.COLUMN_BLOCK_STARTED_AT_TIME_ZONE, deviceOwner.
                           getBlockStartedAt().getTimeZone());
               }
               if (deviceOwner.getHomeworkModeDuration() != null) {
                   values.put(DeviceOwnersDbHelper.COLUMN_HOMEWORK_DURATION, deviceOwner.
                           getHomeworkModeDuration());
               }
               if (deviceOwner.
                       getHomeworkModeStartedAt() != null) {
                   values.put(DeviceOwnersDbHelper.COLUMN_HOMEWORK_STARTED_AT_DATE_TIME, deviceOwner.
                           getHomeworkModeStartedAt().getDateTime());
                   values.put(DeviceOwnersDbHelper.COLUMN_HOMEWORK_STARTED_AT_TIME_ZONE, deviceOwner.
                           getHomeworkModeStartedAt().getTimeZone());
               }
               if (deviceOwner.getHasAccessUntil() != null) {
                   values.put(DeviceOwnersDbHelper.COLUMN_HAS_ACCESS_DATE_TIME, deviceOwner.
                           getHasAccessUntil().getDateTime());
                   values.put(DeviceOwnersDbHelper.COLUMN_HAS_ACCESS_TIME_ZONE, deviceOwner.
                           getHasAccessUntil().getTimeZone());
               }

               if (deviceOwner.getHasNoAccessUntil() != null) {
                   values.put(DeviceOwnersDbHelper.COLUMN_HAS_NO_ACCESS_UNTIL_DATE_TIME, deviceOwner.
                           getHasNoAccessUntil().getDateTime());
                   values.put(DeviceOwnersDbHelper.COLUMN_HAS_NO_ACCESS_UNTIL_TIME_ZONE, deviceOwner.
                           getHasNoAccessUntil().getTimeZone());
               }

               values.put(DeviceOwnersDbHelper.COLUMN_DEVICE_COUNT, deviceOwner.getDeviceOnlineCount());
               values.put(DeviceOwnersDbHelper.COLUMN_ACCESS_UNTIL_LABEL, deviceOwner.getHasAccessUntilLabel());
               values.put(DeviceOwnersDbHelper.COLUMN_UNLIMITED_ACCESS, deviceOwner.isUnlimitedAccess());
               values.put(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_LABEL, deviceOwner.getNextAccessUntilLabel());

               if (deviceOwner.getNextAccessUntil() != null) {
                   values.put(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_DATE_TIME, deviceOwner.
                           getNextAccessUntil().getDateTime());
                   values.put(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_TIME_ZONE, deviceOwner.
                           getNextAccessUntil().getTimeZone());
               }

               values.put(DeviceOwnersDbHelper.COLUMN_IS_ACCESS_UNLIMITED, deviceOwner.isAccessUnlimited());
               values.put(DeviceOwnersDbHelper.COLUMN_IS_NO_ACCESS_UNLIMITED, deviceOwner.isNoAccessUnlimited());
               values.put(DeviceOwnersDbHelper.COLUMN_ORDER_ID, orderId);
               values.put(DeviceOwnersDbHelper.COLUMN_IS_DELETABLE, deviceOwner.isDeletable());

               long insertId = source.database.insert(DeviceOwnersDbHelper.TABLE, null,
                       values);
               createContentProfile(deviceOwner);
            if (deviceOwner.getTimeExtension() != null)
                createTimeExtension(deviceOwner.getTimeExtension(), deviceOwner.getId());
               createTimeProfile(deviceOwner);
               createWhiteList(deviceOwner);
               createBlackList(deviceOwner);
               createPopularBlacklist(deviceOwner);

        }
    }

    public void createBlackList(DeviceOwner deviceOwner) {
        ContentValues values;
        if (deviceOwner.getUrlBlackLists() != null) {
            for (int i = 0; i < deviceOwner.getUrlBlackLists().size(); i++) {
                UrlListEntity url = deviceOwner.getUrlBlackLists().get(i);
                url.setBlack(true);
                values = new ContentValues();
                WhiteListUrlDbHelper.toValues(url, values);
                source.database.insert(WhiteListUrlDbHelper.TABLE, null,
                        values);
            }
        }
    }

    public void updatePopularBlacklist(DeviceOwner deviceOwner) {
        long id = deviceOwner.getId();
        ContentValues values;
        source.database.delete(BlacklistedCategoriesDbHelper.TABLE,
                BlacklistedCategoriesDbHelper.COLUMN_DEVICE_OWNER
                        + " = " + id, null);
        if (deviceOwner.getBlacklistedCategories() != null) {
            for (int i = 0; i < deviceOwner.getBlacklistedCategories().size(); i++) {
                PopularBlacklistSimpleItem blacklist = deviceOwner.getBlacklistedCategories().get(i);
                values = new ContentValues();
                BlacklistedCategoriesDbHelper.toValues(blacklist, values);
                source.database.insert(BlacklistedCategoriesDbHelper.TABLE, null,
                        values);
            }
        }
    }


    public void createWhiteList(DeviceOwner deviceOwner) {
        ContentValues values;
        if (deviceOwner.getUrlWhiteLists() != null) {
            for (int i = 0; i < deviceOwner.getUrlWhiteLists().size(); i++) {
                UrlListEntity url = deviceOwner.getUrlWhiteLists().get(i);
                values = new ContentValues();
                WhiteListUrlDbHelper.toValues(url, values);
                source.database.insert(WhiteListUrlDbHelper.TABLE, null,
                        values);
            }
        }
    }

    public void createPopularBlacklist(DeviceOwner deviceOwner) {
        ContentValues values;
        if (deviceOwner.getBlacklistedCategories() != null) {
            for (int i = 0; i < deviceOwner.getBlacklistedCategories().size(); i++) {
                PopularBlacklistSimpleItem blacklist = deviceOwner.getBlacklistedCategories().get(i);
                values = new ContentValues();
                BlacklistedCategoriesDbHelper.toValues(blacklist, values);
                source.database.insert(BlacklistedCategoriesDbHelper.TABLE, null,
                        values);
            }
        }
    }

    public void createTimeProfile(DeviceOwner deviceOwner) {
        ContentValues values;
        if (deviceOwner.getTimeProfiles() != null) {
            for (int i = 0; i < deviceOwner.getTimeProfiles().length; i++) {
                values = new ContentValues();
                values.put(TimeProfilesDbHelper.COLUMN_ID, deviceOwner.getTimeProfiles()[i].getId());
                if (deviceOwner.
                        getTimeProfiles()[i].getStartTime() != null) {
                    values.put(TimeProfilesDbHelper.COLUMN_START_TIME_DATE_TIME, deviceOwner.
                            getTimeProfiles()[i].getStartTime().getTime());
                    values.put(TimeProfilesDbHelper.COLUMN_START_TIME_ZONE, deviceOwner.
                            getTimeProfiles()[i].getStartTime().getTimezone());
                }
                if (deviceOwner.
                        getTimeProfiles()[i].getEndTime() != null) {
                    values.put(TimeProfilesDbHelper.COLUMN_END_TIME_DATE_TIME, deviceOwner.
                            getTimeProfiles()[i].getEndTime().getTime());
                    values.put(TimeProfilesDbHelper.COLUMN_END_TIME_ZONE, deviceOwner.
                            getTimeProfiles()[i].getEndTime().getTimezone());
                }
                values.put(TimeProfilesDbHelper.COLUMN_DAY_OF_WEEK, deviceOwner.getTimeProfiles()[i].
                        getDayOfWeek());
                values.put(TimeProfilesDbHelper.COLUMN_ID_DEVICE_OWNERS, deviceOwner.getId());

                source.database.insert(TimeProfilesDbHelper.TABLE, null,
                        values);
            }
        }
    }

    public void createTimeExtension(TimeExtensions extension, long deviceOwnerId) {
        open();
        source.database.delete(TimeExtensionsDbHelper.TABLE,
                TimeExtensionsDbHelper.COLUMN_ID_DEVICE_OWNERS + " = " + deviceOwnerId, null);

        ContentValues values = new ContentValues();
        if (extension.getCreatedAt() != null) {
            values.put(TimeExtensionsDbHelper.COLUMN_CREATED_AT_TIME_DATE_TIME,
                    extension.getCreatedAt().getDateTime());
            values.put(TimeExtensionsDbHelper.COLUMN_CREATED_AT_TIME_ZONE,
                    extension.getCreatedAt().getTimeZone());
        }
        values.put(TimeExtensionsDbHelper.COLUMN_DURATION,
                extension.getDuration());
        values.put(TimeExtensionsDbHelper.COLUMN_ID,
                extension.getId());
        values.put(TimeExtensionsDbHelper.COLUMN_ID_DEVICE_OWNERS, deviceOwnerId);
        values.put(TimeExtensionsDbHelper.COLUMN_STATUS,
                extension.getStatus());
        values.put(TimeExtensionsDbHelper.COLUMN_TER_ID,
                extension.getTerId());
        if (extension.getStartedAt() != null) {
            values.put(TimeExtensionsDbHelper.COLUMN_UPDATED_AT_TIME_DATE_TIME,
                    extension.getStartedAt().getDateTime());
            values.put(TimeExtensionsDbHelper.COLUMN_UPDATED_AT_TIME_ZONE,
                    extension.getStartedAt().getTimeZone());
        }
        source.database.insert(TimeExtensionsDbHelper.TABLE, null,
                values);

    }

    public void createContentProfile(DeviceOwner deviceOwner) {
            open();
            if (deviceOwner.getHomeworkCategories() != null) {
                for (int i = 0; i < deviceOwner.getHomeworkCategories().length; i++) {
                    setHomeworkCategories(deviceOwner.getHomeworkCategories()[i],
                            (int) deviceOwner.getId());
                }
            }
    }

    public void setHomeworkCategories(HomeWorkCategories homeworkCategories,
                                      int deviceOwnerId) {
        open();
        ContentValues values;
        values = new ContentValues();
        values.put(HomeworkCategoriesDbHelper.COLUMN_CATEGORY_ID,
                homeworkCategories.getCategoryId());
        values.put(HomeworkCategoriesDbHelper.COLUMN_CATEGORY_NAME,
                homeworkCategories.getCategoryName());
        values.put(HomeworkCategoriesDbHelper.COLUMN_ID,
                homeworkCategories.getId());
        values.put(HomeworkCategoriesDbHelper.COLUMN_ID_DEVICE_OWNERS,
                deviceOwnerId);
        values.put(HomeworkCategoriesDbHelper.COLUMN_TITLE,
                homeworkCategories.getTitle());
        source.database.insert(HomeworkCategoriesDbHelper.TABLE, null,
                values);

    }


    public void deleteDeviceOwnerAll(){

        open();

        source.database.delete(TimeExtensionsDbHelper.TABLE,
                null, null);

        source.database.delete(TimeProfilesDbHelper.TABLE,
                null, null);

        source.database.delete(WhiteListUrlDbHelper.TABLE,
                null, null);

        source.database.delete(HomeworkCategoriesDbHelper.TABLE,
                null, null);
        source.database.delete(DeviceOwnersDbHelper.TABLE,
                null, null);
        source.database.delete(BlacklistedCategoriesDbHelper.TABLE,
                null, null);
    }

    public void deleteHomework(int deviceOwner){
        try {
            open();
            source.database.delete(HomeworkCategoriesDbHelper.TABLE,
                    HomeworkCategoriesDbHelper.COLUMN_ID_DEVICE_OWNERS +
                            "=" + deviceOwner, null);
        } catch (IllegalStateException ex){}
    }



    public void deleteDeviceOwner(long id) {
        open();

        source.database.delete(TimeExtensionsDbHelper.TABLE,
                TimeExtensionsDbHelper.COLUMN_ID_DEVICE_OWNERS
                        + " = " + id, null);

        source.database.delete(TimeProfilesDbHelper.TABLE,
                    TimeProfilesDbHelper.COLUMN_ID_DEVICE_OWNERS
                            + " = " + id, null);

        source.database.delete(WhiteListUrlDbHelper.TABLE,
                WhiteListUrlDbHelper.COLUMN_DEVICE_OWNER
                        + " = " + id, null);

        source.database.delete(BlacklistedCategoriesDbHelper.TABLE,
                BlacklistedCategoriesDbHelper.COLUMN_DEVICE_OWNER
                + " = " + id, null);

        source.database.delete(HomeworkCategoriesDbHelper.TABLE,
                    HomeworkCategoriesDbHelper.COLUMN_ID_DEVICE_OWNERS
                            + " = " + id, null);
        source.database.delete(DeviceOwnersDbHelper.TABLE,
                DeviceOwnersDbHelper.COLUMN_ID
                    + " = " + id, null);
        }

//    public void close(){
//        source.close();
//    }

    public DeviceOwner getDeviceOwnerById(long id){
        open();
        DeviceOwner listDeviceOwners = new DeviceOwner();

        Cursor cursorDeviceOwner = source.database.query(DeviceOwnersDbHelper.TABLE,
                source.allColumnsDeviceOwners, DeviceOwnersDbHelper.COLUMN_ID
                        + " = " + id, null, null, null, null);


        cursorDeviceOwner.moveToFirst();
        while (!cursorDeviceOwner.isAfterLast()) {
            DeviceOwner deviceOwner = cursorToDeviceOwner(cursorDeviceOwner);
            listDeviceOwners = deviceOwner;
            cursorDeviceOwner.moveToNext();
            break;
        }

        if(cursorDeviceOwner != null && source.database!= null && !cursorDeviceOwner.isClosed()){
            cursorDeviceOwner.close();

        }
        return listDeviceOwners;
    }

    public List<DeviceOwner> getAllDeviceOwners() {
        open();
        List<DeviceOwner> listDeviceOwners = new ArrayList<DeviceOwner>();

        Cursor cursorDeviceOwner = source.database.query(DeviceOwnersDbHelper.TABLE,
                source.allColumnsDeviceOwners, null, null, null, null, DeviceOwnersDbHelper.COLUMN_ORDER_ID);

            cursorDeviceOwner.moveToFirst();
            while (!cursorDeviceOwner.isAfterLast()) {
                DeviceOwner deviceOwner = cursorToDeviceOwner(cursorDeviceOwner);
                if(!listDeviceOwners.contains(deviceOwner)) {
                    listDeviceOwners.add(deviceOwner);
                }
                cursorDeviceOwner.moveToNext();
            }

            if(cursorDeviceOwner != null && source.database!= null && !cursorDeviceOwner.isClosed()){
                   cursorDeviceOwner.close();

        }
        return listDeviceOwners;
    }

    public int getDeviceOwnersCount(){
        open();
        Cursor cursorDeviceOwner = source.database.query(DeviceOwnersDbHelper.TABLE,
                source.allColumnsDeviceOwners, null, null, null, null, DeviceOwnersDbHelper.COLUMN_ORDER_ID);
        int ownersCount = cursorDeviceOwner.getCount();
        cursorDeviceOwner.close();
        return ownersCount;
    }

    private HomeWorkCategories cursorToHomeWorkCategories(Cursor cursor) {
        open();
        HomeWorkCategories homeWorkCategorieses = new HomeWorkCategories();
        homeWorkCategorieses.setId(cursor.getLong(cursor.getColumnIndex
                (HomeworkCategoriesDbHelper.COLUMN_ID)));
        homeWorkCategorieses.setTitle(cursor.getString(cursor.getColumnIndex
                (HomeworkCategoriesDbHelper.COLUMN_TITLE)));
        homeWorkCategorieses.setCategoryId(cursor.getLong(cursor.getColumnIndex
                (HomeworkCategoriesDbHelper.COLUMN_CATEGORY_ID)));
        homeWorkCategorieses.setCategoryName(cursor.getString(cursor.getColumnIndex
                (HomeworkCategoriesDbHelper.COLUMN_CATEGORY_NAME)));

        return homeWorkCategorieses;
    }

    private TimeExtensions cursorToTimeExtensions(Cursor cursor) {
        open();
        TimeExtensions timeExtensions = new TimeExtensions();
        timeExtensions.setStatus(cursor.getString(cursor.getColumnIndex(TimeExtensionsDbHelper.COLUMN_STATUS)));

        CreateTimeUser createTimeUser = new CreateTimeUser();
        createTimeUser.setDateTime(cursor.getString(cursor.getColumnIndex(TimeExtensionsDbHelper.
                COLUMN_CREATED_AT_TIME_DATE_TIME)));
        createTimeUser.setTimeZone(cursor.getString(cursor.getColumnIndex(TimeExtensionsDbHelper.
                COLUMN_CREATED_AT_TIME_ZONE)));

        timeExtensions.setCreatedAt(createTimeUser);

        timeExtensions.setDuration(cursor.getLong(cursor.getColumnIndex(TimeExtensionsDbHelper.COLUMN_DURATION)));
        timeExtensions.setId(cursor.getLong(cursor.getColumnIndex
                (TimeExtensionsDbHelper.COLUMN_ID)));
        timeExtensions.setTerId(cursor.getLong(cursor.getColumnIndex
                (TimeExtensionsDbHelper.COLUMN_TER_ID)));

        CreateTimeUser startAt = new CreateTimeUser();
        startAt.setDateTime(cursor.getString(cursor.getColumnIndex(TimeExtensionsDbHelper.
                COLUMN_UPDATED_AT_TIME_DATE_TIME)));
        startAt.setTimeZone(cursor.getString(cursor.getColumnIndex(TimeExtensionsDbHelper.
                COLUMN_UPDATED_AT_TIME_ZONE)));
        timeExtensions.setStartedAt(startAt);

        return timeExtensions;
    }


    public static PopularBlacklistSimpleItem cursorToPopularBlacklist(Cursor cursor) {
        PopularBlacklistSimpleItem blacklist = new PopularBlacklistSimpleItem();
        blacklist.setContentCategory(cursor.getInt(cursor.getColumnIndex(BlacklistedCategoriesDbHelper.COLUMN_ID)));
        blacklist.setDeviceOwner(cursor.getInt(cursor.getColumnIndex(BlacklistedCategoriesDbHelper.COLUMN_DEVICE_OWNER)));
        blacklist.setIsParentBlocked((cursor.getInt(cursor.getColumnIndex(BlacklistedCategoriesDbHelper.CLOUMN_IS_PARENT_BLOCKED))) > 0);
        return blacklist;
    }

    private TimeProfiles cursorToTimeProfiles(Cursor cursor) {
        open();
        TimeProfiles timeProfiles = new TimeProfiles();
        timeProfiles.setId(cursor.getLong(cursor.getColumnIndex(TimeProfilesDbHelper.COLUMN_ID)));
        timeProfiles.setDayOfWeek(cursor.getString(cursor.getColumnIndex(TimeProfilesDbHelper.COLUMN_DAY_OF_WEEK)));

        Time endTime = new Time();
        endTime.setTime(cursor.getString(cursor.getColumnIndex(TimeProfilesDbHelper.COLUMN_END_TIME_DATE_TIME)));
        endTime.setTimezone(cursor.getString(cursor.getColumnIndex(TimeProfilesDbHelper.COLUMN_END_TIME_ZONE)));
        timeProfiles.setEndTime(endTime);

        Time startTime = new Time();
        startTime.setTime(cursor.getString(cursor.getColumnIndex(TimeProfilesDbHelper.COLUMN_START_TIME_DATE_TIME)));
        startTime.setTimezone(cursor.getString(cursor.getColumnIndex(TimeProfilesDbHelper.COLUMN_START_TIME_ZONE)));
        timeProfiles.setStartTime(startTime);
        return timeProfiles;
    }

    public DeviceOwner cursorToDeviceOwner(Cursor cursor) {
        try {
            open();
            DeviceOwner deviceOwner = new DeviceOwner();
            deviceOwner.setOrderId(cursor.getInt(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_ORDER_ID)));
            deviceOwner.setDeletable(cursor.getInt(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_IS_DELETABLE)) == 1);
            CreateTimeUser hasAccessUntil = new CreateTimeUser();
            hasAccessUntil.setDateTime(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_HAS_ACCESS_DATE_TIME)));
            hasAccessUntil.setTimeZone(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_HAS_ACCESS_TIME_ZONE)));

            if (hasAccessUntil.getTimeZone() != null && hasAccessUntil.getDateTime() != null) {
                deviceOwner.setHasAccessUntil(hasAccessUntil);
            }

            CreateTimeUser homeworkModeStartedAt = new CreateTimeUser();
            homeworkModeStartedAt.setDateTime(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_HOMEWORK_STARTED_AT_DATE_TIME)));
            homeworkModeStartedAt.setTimeZone(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_HOMEWORK_STARTED_AT_TIME_ZONE)));

            deviceOwner.setHomeworkModeStartedAt(homeworkModeStartedAt);

            if (cursor.getString((cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_HOMEWORK_DURATION))) != null) {
                deviceOwner.setHomeworkModeDuration(cursor.getLong(
                        (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_HOMEWORK_DURATION))));
            }
            CreateTimeUser blockStartedAt = new CreateTimeUser();
            blockStartedAt.setDateTime(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_BLOCK_STARTED_AT_DATE_TIME)));
            blockStartedAt.setTimeZone(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_BLOCK_STARTED_AT_TIME_ZONE)));
            deviceOwner.setBlockStartedAt(blockStartedAt);


            deviceOwner.setTime(cursor.getLong(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_TIME)));

            deviceOwner.setDeviceOnlineCount
                    (cursor.getInt(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_DEVICE_COUNT)));
            deviceOwner.setStatus(cursor.getString(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_STATUS)));
            CreateTimeUser createTimeUser = new CreateTimeUser();
            createTimeUser.setDateTime(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_CREATED_AT_DATE_TIME)));
            createTimeUser.setTimeZone(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_CREATED_AT_TIME_ZONE)));
            deviceOwner.setCreateAt(createTimeUser);

            deviceOwner.setTitle(cursor.getString(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_TITLE)));
            deviceOwner.setId(cursor.getLong(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_ID)));
            deviceOwner.setHasAccessUntilLabel(cursor.getString(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_ACCESS_UNTIL_LABEL)));
            deviceOwner.setNextAccessUntilLabel(cursor.getString(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_LABEL)));
            deviceOwner.setUnlimitedAccess(
                    cursor.getInt(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_UNLIMITED_ACCESS)) == 1);

            deviceOwner.setAccessUnlimited(
                    cursor.getInt(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_IS_ACCESS_UNLIMITED)) == 1);
            deviceOwner.setNoAccessUnlimited(
                    cursor.getInt(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_IS_NO_ACCESS_UNLIMITED)) == 1);

            CreateTimeUser nextAccess = new CreateTimeUser();
            nextAccess.setDateTime(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_DATE_TIME)));
            nextAccess.setTimeZone(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_TIME_ZONE)));

            if (nextAccess.getTimeZone() != null && nextAccess.getDateTime() != null) {
                deviceOwner.setNextAccess(nextAccess);
            }

            CreateTimeUser hasNoNextAccess = new CreateTimeUser();
            hasNoNextAccess.setDateTime(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_HAS_NO_ACCESS_UNTIL_DATE_TIME)));
            hasNoNextAccess.setTimeZone(cursor.getString
                    (cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_HAS_NO_ACCESS_UNTIL_TIME_ZONE)));

            if (hasNoNextAccess.getTimeZone() != null && hasNoNextAccess.getDateTime() != null) {
                deviceOwner.setHasNoAccessUntil(hasNoNextAccess);
            }

            long contentProfileId = cursor.getLong(cursor.getColumnIndex(DeviceOwnersDbHelper.COLUMN_ID_CONTENT_PROFILE));


            Cursor cursorHomeWorkCategories = source.database.query(HomeworkCategoriesDbHelper.TABLE,
                    source.allColumnsHomeworkCategories, HomeworkCategoriesDbHelper.COLUMN_ID_DEVICE_OWNERS
                            + " = " + deviceOwner.getId(), null, null, null, null);

            cursorHomeWorkCategories.moveToFirst();
            HomeWorkCategories[] categories =
                    new HomeWorkCategories[cursorHomeWorkCategories.getCount()];
            int i = 0;
            while (!cursorHomeWorkCategories.isAfterLast()) {
                HomeWorkCategories hwCategories = cursorToHomeWorkCategories(cursorHomeWorkCategories);
                categories[i] = hwCategories;
                cursorHomeWorkCategories.moveToNext();
                i += 1;
            }

            cursorHomeWorkCategories.close();
            try {
                deviceOwner.setHomeworkCategories(categories);
            } catch (NullPointerException ex) {
            }

            Cursor cursorTimeExtensions = source.database.query(TimeExtensionsDbHelper.TABLE,
                    source.allColumnsTimeExtensions, TimeExtensionsDbHelper.COLUMN_ID_DEVICE_OWNERS
                            + " = " + deviceOwner.getId(), null, null, null, null);

            if (cursorTimeExtensions.getCount() > 0) {
                cursorTimeExtensions.moveToFirst();
                TimeExtensions tx = cursorToTimeExtensions(cursorTimeExtensions);
                deviceOwner.setTimeExtension(tx);
            }
            cursorTimeExtensions.close();


            Cursor cursorWhiteList = source.database.query(WhiteListUrlDbHelper.TABLE,
                    source.allColumnsWhiteList, WhiteListUrlDbHelper.COLUMN_DEVICE_OWNER
                            + " = " + deviceOwner.getId(), null, null, null, null);

            cursorWhiteList.moveToFirst();
            ArrayList<UrlListEntity> whiteList = new ArrayList<UrlListEntity>(cursorWhiteList.getCount());
            i = 0;
            while (!cursorWhiteList.isAfterLast()) {
                whiteList.add(UrlListDataSource.cursorToUrlList(cursorWhiteList));
                cursorWhiteList.moveToNext();
                i += 1;
            }
            cursorWhiteList.close();
            deviceOwner.setUrlWhiteLists(whiteList);

            Cursor cursorPopularBlacklist = source.database.query(BlacklistedCategoriesDbHelper.TABLE,
                    source.allColumnsPopularBlacklist, BlacklistedCategoriesDbHelper.COLUMN_DEVICE_OWNER
                    + " = " + deviceOwner.getId(), null, null, null, null);
            cursorPopularBlacklist.moveToFirst();
            List<PopularBlacklistSimpleItem> blacklist = new ArrayList<>();
            while (!cursorPopularBlacklist.isAfterLast()) {
                blacklist.add(cursorToPopularBlacklist(cursorPopularBlacklist));
                cursorPopularBlacklist.moveToNext();
            }
            cursorPopularBlacklist.close();
            deviceOwner.setBlacklistedCategories(blacklist);

            Cursor cursorTimeProfile = source.database.query(TimeProfilesDbHelper.TABLE,
                    source.allColumnsTimeProfiles, TimeProfilesDbHelper.COLUMN_ID_DEVICE_OWNERS
                            + " = " + deviceOwner.getId(), null, null, null, null);

            cursorTimeProfile.moveToFirst();
            TimeProfiles[] listTimeProfile = new TimeProfiles[cursorTimeProfile.getCount()];
            i = 0;
            while (!cursorTimeProfile.isAfterLast()) {
                TimeProfiles profile = cursorToTimeProfiles(cursorTimeProfile);
                listTimeProfile[i] = profile;
                cursorTimeProfile.moveToNext();
                i += 1;
            }
            cursorTimeProfile.close();
            deviceOwner.setTimeProfiles(listTimeProfile);

            Cursor cursorContentProfile = source.database.query(ContentProfileDbHelper.TABLE,
                    source.allColumnsContentProfile, ContentProfileDbHelper.COLUMN_ID + " = " + contentProfileId, null, null, null, null);
            cursorContentProfile.moveToFirst();
            if (cursorContentProfile.getCount() > 0) {
                ContentProfile contentProfile = (new ContentProfileDataSource(source)).cursorToContentProfiles(cursorContentProfile,
                        source.database, source.allColumnsCategories);
                deviceOwner.setContentProfile(contentProfile);
            } else {
                ContentProfile mock = new ContentProfile();
                mock.setId(contentProfileId);
                deviceOwner.setContentProfile(mock); //very bad
            }
            cursorContentProfile.close();
            return deviceOwner;
        } catch (android.database.CursorIndexOutOfBoundsException ex) {
            return null;
        }
    }

    private void open() {
        if (!source.database.isOpen()) {
            try {
                source.open();
            } catch (SQLException ex) {
            }
        }
    }

    public void updateDeviceOwner(DeviceOwner deviceOwner) {
        open();
        ContentValues values = new ContentValues();

        if (deviceOwner.getContentProfile() != null) {
            values.put(DeviceOwnersDbHelper.COLUMN_ID_CONTENT_PROFILE, deviceOwner.getContentProfile().getId());
        }
        if (deviceOwner.getHomeworkModeStartedAt() != null) {
            if (deviceOwner.getHomeworkModeDuration() != null)
            values.put(DeviceOwnersDbHelper.COLUMN_HOMEWORK_DURATION, deviceOwner.getHomeworkModeDuration());
            CreateTimeUser startedAt = deviceOwner.getHomeworkModeStartedAt();
            values.put(DeviceOwnersDbHelper.COLUMN_HOMEWORK_STARTED_AT_DATE_TIME, startedAt.getDateTime());
            values.put(DeviceOwnersDbHelper.COLUMN_HOMEWORK_STARTED_AT_TIME_ZONE, startedAt.getTimeZone());
        } /*else {
            values.put(DeviceOwnersDbHelper.COLUMN_HOMEWORK_DURATION, 0);
        }*/
        if (deviceOwner.getBlockStartedAt() != null) {
            values.put(DeviceOwnersDbHelper.COLUMN_TIME, deviceOwner.getTime());
            values.put(DeviceOwnersDbHelper.COLUMN_BLOCK_STARTED_AT_DATE_TIME, deviceOwner.getBlockStartedAt().getDateTime());
            values.put(DeviceOwnersDbHelper.COLUMN_BLOCK_STARTED_AT_TIME_ZONE, deviceOwner.getBlockStartedAt().getTimeZone());
        } else {
            values.put(DeviceOwnersDbHelper.COLUMN_TIME, 0);
        }
        if (deviceOwner.getHasAccessUntil() != null) {
            CreateTimeUser hasAccessUntil = deviceOwner.getHasAccessUntil();
            values.put(DeviceOwnersDbHelper.COLUMN_HAS_ACCESS_DATE_TIME, hasAccessUntil.getDateTime());
            values.put(DeviceOwnersDbHelper.COLUMN_HAS_ACCESS_TIME_ZONE, hasAccessUntil.getTimeZone());
        }

        if (deviceOwner.getHasNoAccessUntil() != null) {
            CreateTimeUser hasNoAccessUntil = deviceOwner.getHasNoAccessUntil();
            values.put(DeviceOwnersDbHelper.COLUMN_HAS_NO_ACCESS_UNTIL_DATE_TIME, hasNoAccessUntil.getDateTime());
            values.put(DeviceOwnersDbHelper.COLUMN_HAS_NO_ACCESS_UNTIL_TIME_ZONE, hasNoAccessUntil.getTimeZone());
        }
        if (deviceOwner.getTimeExtension() != null) {
            createTimeExtension(deviceOwner.getTimeExtension(), deviceOwner.getId());
        } else {
            source.database.delete(TimeExtensionsDbHelper.TABLE,
                    TimeExtensionsDbHelper.COLUMN_ID_DEVICE_OWNERS + " = " + deviceOwner.getId(), null);
        }
        values.put(DeviceOwnersDbHelper.COLUMN_UNLIMITED_ACCESS, deviceOwner.isUnlimitedAccess());

        values.put(DeviceOwnersDbHelper.COLUMN_ACCESS_UNTIL_LABEL, deviceOwner.getHasAccessUntilLabel());
        values.put(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_LABEL, deviceOwner.getNextAccessUntilLabel());
        if (deviceOwner.getNextAccessUntil() != null) {
            values.put(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_DATE_TIME, deviceOwner.getNextAccessUntil().getDateTime());
            values.put(DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_TIME_ZONE, deviceOwner.getNextAccessUntil().getTimeZone());
        }

        values.put(DeviceOwnersDbHelper.COLUMN_IS_ACCESS_UNLIMITED, deviceOwner.isAccessUnlimited());
        values.put(DeviceOwnersDbHelper.COLUMN_IS_NO_ACCESS_UNLIMITED, deviceOwner.isNoAccessUnlimited());

        source.database.update(DeviceOwnersDbHelper.TABLE, values,
                DeviceOwnersDbHelper.COLUMN_ID + " = " + deviceOwner.getId(), null);

    }
}

