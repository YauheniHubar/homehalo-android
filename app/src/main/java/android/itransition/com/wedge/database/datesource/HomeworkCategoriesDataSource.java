package android.itransition.com.wedge.database.datesource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.itransition.com.wedge.database.dbhelpers.ContentProfileDbHelper;
import android.itransition.com.wedge.database.dbhelpers.ContentProfilesCategoriesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.HomeworkCategoriesStaticDbHelper;
import android.itransition.com.wedge.database.dbhelpers.MainDbHelper;
import android.itransition.com.wedge.entity.ContentCategories;
import android.itransition.com.wedge.entity.ContentProfile;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.HomeWorkCategories;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 27.11.2014.
 * Class for content profile entity and content profile categories entity
 */


public class HomeworkCategoriesDataSource {

    private MainSource source;

    public HomeworkCategoriesDataSource(Context context, MainSource source) {
        this.source = source;
    }


       public void createContentProfile(HomeWorkCategories hwCategories) {
           if(!source.database.isOpen()){
               try {
                   source.open();
               }catch (SQLException ex){}

           }
           ContentValues values = new ContentValues();

           values.put(HomeworkCategoriesStaticDbHelper.COLUMN_CATEGORY_ID, hwCategories.getCategoryId());
           values.put(HomeworkCategoriesStaticDbHelper.COLUMN_TITLE, hwCategories.getTitle());
           values.put(HomeworkCategoriesStaticDbHelper.COLUMN_CATEGORY_NAME,
                   hwCategories.getCategoryName());
           values.put(HomeworkCategoriesStaticDbHelper.COLUMN_ID,
                   hwCategories.getId());

           long insertId = source.database.insert(HomeworkCategoriesStaticDbHelper.TABLE, null,
                    values);

        }

        public void deleteHwCategories(int id) {
            if(!source.database.isOpen()){
                try {
                    source.open();
                }catch (SQLException ex){}

            }
            source.database.delete(HomeworkCategoriesStaticDbHelper.TABLE,
                    HomeworkCategoriesStaticDbHelper.COLUMN_ID
                            + " = " + id, null);

        }

    public void deleteHwCategoriesAll() {
        if(!source.database.isOpen()){
            try {
                source.open();
            }catch (SQLException ex){}

        }
        source.database.delete(HomeworkCategoriesStaticDbHelper.TABLE,
                null, null);

    }

        public List<HomeWorkCategories> getAllHwCategories() {
            if(!source.database.isOpen()){
                try {
                    source.open();
                }catch (SQLException ex){}

            }
            List<HomeWorkCategories> listHWCategories = new ArrayList<HomeWorkCategories>();

            Cursor cursorHW = source.database.query(HomeworkCategoriesStaticDbHelper.TABLE,
                    source.allColumnsHomeworkCategoriesStatic, null, null, null, null, null);


            cursorHW.moveToFirst();
            while (!cursorHW.isAfterLast()) {
                HomeWorkCategories hwCategories = cursorToHWCategories(cursorHW);
                listHWCategories.add(hwCategories);
                cursorHW.moveToNext();
            }

            cursorHW.close();
            return listHWCategories;
        }

    public void close(){
        source.close();
    }

    private  HomeWorkCategories cursorToHWCategories(Cursor cursor) {
        if(!source.database.isOpen()){
            try {
                source.open();
            }catch (SQLException ex){}

        }

        HomeWorkCategories hwCategories = new HomeWorkCategories();

        hwCategories.setId(cursor.getLong(cursor.getColumnIndex
                (HomeworkCategoriesStaticDbHelper.COLUMN_ID)));
        hwCategories.setTitle(cursor.getString(cursor.getColumnIndex
                (HomeworkCategoriesStaticDbHelper.COLUMN_TITLE)));
        hwCategories.setCategoryId(cursor.getLong(cursor.getColumnIndex
                (HomeworkCategoriesStaticDbHelper.COLUMN_CATEGORY_ID)));
        hwCategories.setCategoryName(cursor.getString(cursor.getColumnIndex
                (HomeworkCategoriesStaticDbHelper.COLUMN_CATEGORY_NAME)));

        return hwCategories;
    }

    }

