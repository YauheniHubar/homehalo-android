package android.itransition.com.wedge.database.datesource;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.itransition.com.wedge.database.dbhelpers.BlacklistedCategoriesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.ContentCategoriesStaticDbHelper;
import android.itransition.com.wedge.database.dbhelpers.ContentProfileDbHelper;
import android.itransition.com.wedge.database.dbhelpers.ContentProfilesCategoriesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.DeviceOwnersDbHelper;
import android.itransition.com.wedge.database.dbhelpers.DevicesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.HomeworkCategoriesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.HomeworkCategoriesStaticDbHelper;
import android.itransition.com.wedge.database.dbhelpers.MainDbHelper;
import android.itransition.com.wedge.database.dbhelpers.TimeExtensionsDbHelper;
import android.itransition.com.wedge.database.dbhelpers.TimeProfilesDbHelper;
import android.itransition.com.wedge.database.dbhelpers.WedgeDbHelper;
import android.itransition.com.wedge.database.dbhelpers.WhiteListUrlDbHelper;

import java.sql.SQLException;

/**
 * Created by e.kazimirova on 28.11.2014.
 */
public class MainSource {

    public SQLiteDatabase database;
    public MainDbHelper dbHelper;
    private static MainSource instance;
    private static Context mContext;

    public String[] allColumnsContentProfile = { ContentProfileDbHelper.COLUMN_ID,
            ContentProfileDbHelper.COLUMN_TITLE,
            ContentProfileDbHelper.COLUMN_CREATED_AT_TIME_ZONE,
            ContentProfileDbHelper.COLUMN_CREATED_AT_DATE_TIME,
            ContentProfileDbHelper.COLUMN_IS_DEFAULT,
            ContentProfileDbHelper.COLUMN_IS_DELETABLE,
            ContentProfileDbHelper.COLUMN_USER_ID
    };

    public String[] allColumnsCategories = { ContentProfilesCategoriesDbHelper.COLUMN_ID,
            ContentProfilesCategoriesDbHelper.COLUMN_TITLE,
            ContentProfilesCategoriesDbHelper.COLUMN_CATEGORY_NAME,
            ContentProfilesCategoriesDbHelper.COLUMN_CATEGORY_ID,
            ContentProfilesCategoriesDbHelper.COLUMN_ID_CONTENT_PROFILES};


    public String[] allColumnsCategoriesStatic = { ContentCategoriesStaticDbHelper.COLUMN_ID,
            ContentCategoriesStaticDbHelper.COLUMN_TITLE,
            ContentCategoriesStaticDbHelper.COLUMN_CATEGORY_NAME,
            ContentCategoriesStaticDbHelper.COLUMN_CATEGORY_ID};
    public String[] allColumnDevices = {
            DevicesDbHelper.COLUMN_ID,
            DevicesDbHelper.COLUMN_TITLE,
            DevicesDbHelper.COLUMN_CREATED_AT_DATE_TIME,
            DevicesDbHelper.COLUMN_CREATED_AT_TIME_ZONE,
            DevicesDbHelper.COLUMN_IS_FAVOURITE,
            DevicesDbHelper.COLUMN_IS_ONLINE,
            DevicesDbHelper.COLUMN_IS_ENABLED,
            DevicesDbHelper.COLUMN_MAC_ADDRESS,
            DevicesDbHelper.COLUMN_MANUFACTURER_TITLE,
            DevicesDbHelper.COLUMN_DEVICE_OWNER,
            DevicesDbHelper.COLUMN_LAST_ONLINE_AT_DATE_TIME,
            DevicesDbHelper.COLUMN_LAST_ONLINE_AT_TIME_ZONE
    };

    public String[] allColumnsDeviceOwners = {DeviceOwnersDbHelper.COLUMN_ID,
            DeviceOwnersDbHelper.COLUMN_TITLE,
            DeviceOwnersDbHelper.COLUMN_CREATED_AT_DATE_TIME,
            DeviceOwnersDbHelper.COLUMN_CREATED_AT_TIME_ZONE,
            DeviceOwnersDbHelper.COLUMN_ID_CONTENT_PROFILE,
            DeviceOwnersDbHelper.COLUMN_STATUS,
            DeviceOwnersDbHelper.COLUMN_TIME,
            DeviceOwnersDbHelper.COLUMN_BLOCK_STARTED_AT_DATE_TIME,
            DeviceOwnersDbHelper.COLUMN_BLOCK_STARTED_AT_TIME_ZONE,
            DeviceOwnersDbHelper.COLUMN_HOMEWORK_DURATION,
            DeviceOwnersDbHelper.COLUMN_HOMEWORK_STARTED_AT_DATE_TIME,
            DeviceOwnersDbHelper.COLUMN_HOMEWORK_STARTED_AT_TIME_ZONE,
            DeviceOwnersDbHelper.COLUMN_HAS_ACCESS_DATE_TIME,
            DeviceOwnersDbHelper.COLUMN_HAS_ACCESS_TIME_ZONE,
            DeviceOwnersDbHelper.COLUMN_DEVICE_COUNT,
            DeviceOwnersDbHelper.COLUMN_ACCESS_UNTIL_LABEL,
            DeviceOwnersDbHelper.COLUMN_UNLIMITED_ACCESS,
            DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_LABEL,
            DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_DATE_TIME,
            DeviceOwnersDbHelper.COLUMN_NEXT_ACCESS_TIME_ZONE,
            DeviceOwnersDbHelper.COLUMN_ORDER_ID,
            DeviceOwnersDbHelper.COLUMN_IS_DELETABLE,
            DeviceOwnersDbHelper.COLUMN_IS_ACCESS_UNLIMITED,
            DeviceOwnersDbHelper.COLUMN_IS_NO_ACCESS_UNLIMITED,
            DeviceOwnersDbHelper.COLUMN_HAS_NO_ACCESS_UNTIL_TIME_ZONE,
            DeviceOwnersDbHelper.COLUMN_HAS_NO_ACCESS_UNTIL_DATE_TIME
    };

    public String[] allColumnsHomeworkCategories = {
            HomeworkCategoriesDbHelper.COLUMN_ID,
            HomeworkCategoriesDbHelper.COLUMN_CATEGORY_ID,
            HomeworkCategoriesDbHelper.COLUMN_CATEGORY_NAME,
            HomeworkCategoriesDbHelper.COLUMN_ID_DEVICE_OWNERS,
            HomeworkCategoriesDbHelper.COLUMN_TITLE
    };

    public String[] allColumnsHomeworkCategoriesStatic = {
            HomeworkCategoriesStaticDbHelper.COLUMN_ID,
            HomeworkCategoriesStaticDbHelper.COLUMN_CATEGORY_ID,
            HomeworkCategoriesStaticDbHelper.COLUMN_CATEGORY_NAME,
            HomeworkCategoriesStaticDbHelper.COLUMN_TITLE
    };

    public String[] allColumnsTimeExtensions = {
            TimeExtensionsDbHelper.COLUMN_ID,
            TimeExtensionsDbHelper.COLUMN_CREATED_AT_TIME_DATE_TIME,
            TimeExtensionsDbHelper.COLUMN_CREATED_AT_TIME_ZONE,
            TimeExtensionsDbHelper.COLUMN_DURATION,
            TimeExtensionsDbHelper.COLUMN_ID_DEVICE_OWNERS,
            TimeExtensionsDbHelper.COLUMN_STATUS,
            TimeExtensionsDbHelper.COLUMN_TER_ID,
            TimeExtensionsDbHelper.COLUMN_UPDATED_AT_TIME_DATE_TIME,
            TimeExtensionsDbHelper.COLUMN_UPDATED_AT_TIME_ZONE
    };

    public String[] allColumnsTimeProfiles = {
            TimeProfilesDbHelper.COLUMN_DAY_OF_WEEK,
            TimeProfilesDbHelper.COLUMN_END_TIME_DATE_TIME,
            TimeProfilesDbHelper.COLUMN_END_TIME_ZONE,
            TimeProfilesDbHelper.COLUMN_ID,
            TimeProfilesDbHelper.COLUMN_ID_DEVICE_OWNERS,
            TimeProfilesDbHelper.COLUMN_START_TIME_DATE_TIME,
            TimeProfilesDbHelper.COLUMN_START_TIME_ZONE};

    public String[] allColumnsWhiteList = {
            WhiteListUrlDbHelper.COLUMN_ID,
            WhiteListUrlDbHelper.COLUMN_DEVICE_OWNER,
            WhiteListUrlDbHelper.COLUMN_URL,
            WhiteListUrlDbHelper.COLUMN_STATUS,
            WhiteListUrlDbHelper.COLUMN_CREATED_AT_DATE_TIME,
            WhiteListUrlDbHelper.COLUMN_CREATED_AT_TIME_ZONE,
            WhiteListUrlDbHelper.COLUMN_IS_BLACK
    };

    public String[] allColumnsPopularBlacklist = {
            BlacklistedCategoriesDbHelper.COLUMN_ID,
            BlacklistedCategoriesDbHelper.COLUMN_DEVICE_OWNER,
            BlacklistedCategoriesDbHelper.CLOUMN_IS_PARENT_BLOCKED
    };

    public String[] allColumnsWedge = { WedgeDbHelper.COLUMN_ID,
            WedgeDbHelper.COLUMN_FIRMWARE,
            WedgeDbHelper.COLUMN_SSID,
            WedgeDbHelper.COLUMN_PASSKEY};



    public static MainSource getInstance(){
        if(instance==null){
            instance = new MainSource(mContext);
        }
        return instance;
    }

    public static void init(Context context) {
        mContext = context;
        instance = new MainSource(context);
    }

    private  MainSource(Context context) {
        dbHelper = new MainDbHelper(context);
        if(database==null){
            try {
                open();
            } catch (SQLException ex){}
        }
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        if(database.isOpen()) {
            dbHelper.close();
        }
    }

    public void clearDb(){
        if(database==null || !database.isOpen()){
            try {
                open();
            } catch (SQLException ex){}
        }
        dbHelper.clear(database);
    }
}
