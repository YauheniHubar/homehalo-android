package android.itransition.com.wedge.database.datesource;

import android.content.ContentValues;
import android.database.Cursor;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.dbhelpers.WhiteListUrlDbHelper;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.UrlListEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by y.drobysh on 23.12.2014.
 */
public class UrlListDataSource {

    private MainSource source;

    public UrlListDataSource() {
        this.source = WedgeApplication.mainSource;
    }

    private void open() {
        if (!source.database.isOpen()) {
            try {
                source.open();
            } catch (SQLException ex) {
            }

        }
    }

    public List<UrlListEntity> getUrlListByOwnerId(long deviceOwnerId, boolean isBlack) {
        open();

        ArrayList<UrlListEntity> list = new ArrayList<>();
        String[] args = {String.valueOf(deviceOwnerId), String.valueOf(isBlack ? 1 : 0)};

        Cursor cursor = source.database.query(WhiteListUrlDbHelper.TABLE,
                source.allColumnsWhiteList, WhiteListUrlDbHelper.COLUMN_DEVICE_OWNER
                        + " = ? and " + WhiteListUrlDbHelper.COLUMN_IS_BLACK + " = ?",
                args, null, null, null);

        while (cursor.moveToNext()) {
            UrlListEntity urlListEntity = cursorToUrlList(cursor);
            list.add(urlListEntity);
        }

        return list;
    }

    public static UrlListEntity cursorToUrlList(Cursor cursor) {
        UrlListEntity url = new UrlListEntity();

        url.setId(cursor.getLong(cursor.getColumnIndex(WhiteListUrlDbHelper.COLUMN_ID)));

        CreateTimeUser createTimeUser = new CreateTimeUser();
        createTimeUser.setDateTime(cursor.getString(cursor.getColumnIndex
                (WhiteListUrlDbHelper.COLUMN_CREATED_AT_DATE_TIME)));
        createTimeUser.setTimeZone(cursor.getString(cursor.getColumnIndex
                (WhiteListUrlDbHelper.COLUMN_CREATED_AT_TIME_ZONE)));
        url.setCreatedAt(createTimeUser);
        url.setDeviceOwner((cursor.getLong(cursor.getColumnIndex
                (WhiteListUrlDbHelper.COLUMN_DEVICE_OWNER))));
        url.setStatus((cursor.getString(cursor.getColumnIndex
                (WhiteListUrlDbHelper.COLUMN_STATUS))));
        url.setUrl((cursor.getString(cursor.getColumnIndex
                (WhiteListUrlDbHelper.COLUMN_URL))));
        url.setBlack((cursor.getInt(cursor.getColumnIndex
                (WhiteListUrlDbHelper.COLUMN_IS_BLACK))) > 0);

        return url;
    }

    public static void createUrl(UrlListEntity entity) {
        MainSource source = WedgeApplication.mainSource;
        ContentValues values = new ContentValues();
        WhiteListUrlDbHelper.toValues(entity, values);
        source.database.insert(WhiteListUrlDbHelper.TABLE, null,
                values);
    }

    public static void deleteUrl(UrlListEntity url) {
        MainSource source = WedgeApplication.mainSource;

        source.database.delete(WhiteListUrlDbHelper.TABLE,
                WhiteListUrlDbHelper.COLUMN_ID + " = " + url.getId() +
                        " and " +
                        WhiteListUrlDbHelper.COLUMN_IS_BLACK + " = " + (url.isBlack() ? 1 : 0),
                null);
    }

    public static void updateUrl(UrlListEntity url) {
        MainSource source = WedgeApplication.mainSource;

        ContentValues values = new ContentValues();
        WhiteListUrlDbHelper.toValues(url, values);

        source.database.update(WhiteListUrlDbHelper.TABLE, values,
                WhiteListUrlDbHelper.COLUMN_ID + " = " + url.getId() +
                        " and " +
                        WhiteListUrlDbHelper.COLUMN_IS_BLACK + " = " + (url.isBlack() ? 1 : 0), null);
    }
}
