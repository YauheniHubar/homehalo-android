package android.itransition.com.wedge.database.datesource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.itransition.com.wedge.database.dbhelpers.WedgeDbHelper;
import android.itransition.com.wedge.entity.WedgeEntity;

import java.sql.SQLException;


/**
 * Created by e.kazimirova on 27.11.2014.
 */


public class WedgeDataSource {

    private MainSource source;

    public WedgeDataSource(Context context, MainSource source) {
        this.source = source;
    }

    public void createWedge(WedgeEntity wedge) {
        if(!source.database.isOpen()){
            try {
                source.open();
            }catch (SQLException ex){}

        }
         ContentValues values = new ContentValues();

         values.put(WedgeDbHelper.COLUMN_FIRMWARE, wedge.getFirmware());
         values.put(WedgeDbHelper.COLUMN_PASSKEY, wedge.getPasskey());
         values.put(WedgeDbHelper.COLUMN_SSID, wedge.getSsid());
         long insertId = source.database.insert(WedgeDbHelper.TABLE, null,
                    values);

        }

        public void deleteWedge() {
            if(!source.database.isOpen()){
                try {
                    source.open();
                }catch (SQLException ex){}

            }
            source.database.delete(WedgeDbHelper.TABLE, null, null);

        }

        public void close(){
            source.close();
        }

        public WedgeEntity getWedge() {
            if(!source.database.isOpen()){
                try {
                    source.open();
                }catch (SQLException ex){}

            }

            WedgeEntity wedgeEntity = new WedgeEntity();
            Cursor cursorWedge = source.database.query(WedgeDbHelper.TABLE,
                    source.allColumnsWedge, null, null, null, null, null);


            cursorWedge.moveToFirst();
            while (!cursorWedge.isAfterLast()) {
                wedgeEntity =  cursorToWedge(cursorWedge);
                cursorWedge.moveToNext();
            }

            cursorWedge.close();
            return wedgeEntity;

        }

    private WedgeEntity cursorToWedge(Cursor cursor) {
        if(!source.database.isOpen()){
            try {
                source.open();
            }catch (SQLException ex){}

        }

        WedgeEntity wedge = new WedgeEntity();

        wedge.setFirmware(cursor.getString(cursor.getColumnIndex(WedgeDbHelper.COLUMN_FIRMWARE)));
        wedge.setSsid(cursor.getString(cursor.getColumnIndex(WedgeDbHelper.COLUMN_SSID)));
        wedge.setPasskey(cursor.getString(cursor.getColumnIndex(WedgeDbHelper.COLUMN_PASSKEY)));

        return wedge;
    }

    }

