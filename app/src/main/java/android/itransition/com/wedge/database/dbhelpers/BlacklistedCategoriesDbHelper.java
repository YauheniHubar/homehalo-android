package android.itransition.com.wedge.database.dbhelpers;

import android.content.ContentValues;
import android.itransition.com.wedge.model.PopularBlacklistSimpleItem;

/**
 * Created by i.grechishchev on 17.02.2016.
 * itransition 2016
 */
public class BlacklistedCategoriesDbHelper {


    public static final String TABLE = "blacklistedcategories";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_DEVICE_OWNER = "device_owner";
    public static final String CLOUMN_IS_PARENT_BLOCKED = "is_parent_blocked";

    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_DEVICE_OWNER
            + " integer, " + CLOUMN_IS_PARENT_BLOCKED
            + " integer);";


    public static void toValues(PopularBlacklistSimpleItem blackList, ContentValues values) {
        values.put(BlacklistedCategoriesDbHelper.COLUMN_ID, blackList.getContentCategory());
        values.put(BlacklistedCategoriesDbHelper.COLUMN_DEVICE_OWNER, blackList.
                getDeviceOwner());
        values.put(BlacklistedCategoriesDbHelper.CLOUMN_IS_PARENT_BLOCKED, blackList.isParentBlocked());
    }
}
