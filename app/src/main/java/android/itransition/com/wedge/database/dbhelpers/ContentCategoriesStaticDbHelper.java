package android.itransition.com.wedge.database.dbhelpers;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class ContentCategoriesStaticDbHelper {

    public static final String TABLE = "contentprofilescategoriesstatic";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_CATEGORY_NAME = "category_name";
    public static final String COLUMN_CATEGORY_ID = "category_id";




    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_TITLE
            + " text, " + COLUMN_CATEGORY_NAME
            + " text, " + COLUMN_CATEGORY_ID
            + " integer);";


}