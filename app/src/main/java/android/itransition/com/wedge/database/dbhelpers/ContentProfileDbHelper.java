package android.itransition.com.wedge.database.dbhelpers;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class ContentProfileDbHelper  {

    public static final String TABLE = "contentprofiles";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_CREATED_AT_DATE_TIME = "created_at_date_time";
    public static final String COLUMN_CREATED_AT_TIME_ZONE = "created_at_time_ZONE";
    public static final String COLUMN_IS_DEFAULT = "is_default";
    public static final String COLUMN_IS_DELETABLE = "is_deletable";
    public static final String COLUMN_USER_ID = "id_user";



    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_TITLE
            + " text, " + COLUMN_USER_ID + " integer, "
            + COLUMN_CREATED_AT_DATE_TIME
            + " text, " + COLUMN_CREATED_AT_TIME_ZONE
            + " text, " + COLUMN_IS_DEFAULT
            + " flag INTEGER DEFAULT 0, " + COLUMN_IS_DELETABLE
            + " flag INTEGER DEFAULT 0);";


}