package android.itransition.com.wedge.database.dbhelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class ContentProfilesCategoriesDbHelper{

    public static final String TABLE = "contentprofilescategories";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_CATEGORY_NAME = "category_name";
    public static final String COLUMN_CATEGORY_ID = "category_id";
    public static final String COLUMN_ID_CONTENT_PROFILES = "id_content_categories";




    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_TITLE
            + " text, " + COLUMN_CATEGORY_NAME
            + " text, " + COLUMN_CATEGORY_ID
            + " integer,  " + COLUMN_ID_CONTENT_PROFILES
            + " integer);";


}