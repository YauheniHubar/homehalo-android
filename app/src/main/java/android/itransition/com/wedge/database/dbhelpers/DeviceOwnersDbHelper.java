package android.itransition.com.wedge.database.dbhelpers;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class DeviceOwnersDbHelper {

    public static final String TABLE = "deviceowner";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_CREATED_AT_DATE_TIME = "created_at_date_time";
    public static final String COLUMN_CREATED_AT_TIME_ZONE = "created_at_time_zone";
    public static final String COLUMN_ID_CONTENT_PROFILE = "id_content_profile";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_BLOCK_STARTED_AT_DATE_TIME = "block_started_at_date_time";
    public static final String COLUMN_BLOCK_STARTED_AT_TIME_ZONE = "block_started_at_time_zone";
    public static final String COLUMN_HOMEWORK_DURATION = "homework_duration";
    public static final String COLUMN_HOMEWORK_STARTED_AT_DATE_TIME = "homework_started_at_date_time";
    public static final String COLUMN_HOMEWORK_STARTED_AT_TIME_ZONE = "homework_started_at_time_zone";
    public static final String COLUMN_HAS_ACCESS_DATE_TIME = "has_access_date_time";
    public static final String COLUMN_HAS_ACCESS_TIME_ZONE = "has_access_time_zone";
    public static final String COLUMN_HAS_NO_ACCESS_UNTIL_TIME_ZONE = "has_no_access_until_time_zone";
    public static final String COLUMN_HAS_NO_ACCESS_UNTIL_DATE_TIME = "has_no_access_until_date_time";
    public static final String COLUMN_DEVICE_COUNT = "device_count";
    public static final String COLUMN_ACCESS_UNTIL_LABEL = "device_access_until_label";
    public static final String COLUMN_UNLIMITED_ACCESS = "is_unlimited_access";
    public static final String COLUMN_IS_ACCESS_UNLIMITED = "is_access_unlimited";
    public static final String COLUMN_IS_NO_ACCESS_UNLIMITED = "is_no_access_unlimited";
    public static final String COLUMN_NEXT_ACCESS_LABEL = "next_access_label";
    public static final String COLUMN_NEXT_ACCESS_DATE_TIME = "next_access";
    public static final String COLUMN_NEXT_ACCESS_TIME_ZONE = "next_access_time_zone";
    public static final String COLUMN_ORDER_ID = "order_id";
    public static final String COLUMN_IS_DELETABLE = "is_deletable";


    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_TITLE
            + " text, " + COLUMN_CREATED_AT_DATE_TIME
            + " text, " + COLUMN_CREATED_AT_TIME_ZONE
            + " text, " + COLUMN_ID_CONTENT_PROFILE
            + " integer, " + COLUMN_STATUS
            + " integer, " + COLUMN_TIME
            + " integer, " + COLUMN_BLOCK_STARTED_AT_DATE_TIME
            + " text, " + COLUMN_BLOCK_STARTED_AT_TIME_ZONE
            + " text, " + COLUMN_HOMEWORK_DURATION
            + " text, " + COLUMN_HOMEWORK_STARTED_AT_DATE_TIME
            + " text, " + COLUMN_HOMEWORK_STARTED_AT_TIME_ZONE
            + " text, " + COLUMN_HAS_ACCESS_DATE_TIME
            + " text, " + COLUMN_HAS_ACCESS_TIME_ZONE
            + " text, " + COLUMN_HAS_NO_ACCESS_UNTIL_DATE_TIME
            + " text, " + COLUMN_HAS_NO_ACCESS_UNTIL_TIME_ZONE
            + " text, " + COLUMN_DEVICE_COUNT
            + " integer, " + COLUMN_ACCESS_UNTIL_LABEL
            + " text, " + COLUMN_UNLIMITED_ACCESS
            + " text, " + COLUMN_NEXT_ACCESS_LABEL
            + " text, " + COLUMN_NEXT_ACCESS_DATE_TIME
            + " text, " + COLUMN_NEXT_ACCESS_TIME_ZONE
            + " text, " + COLUMN_IS_DELETABLE
            + " text, " + COLUMN_ORDER_ID
            + " integer, " + COLUMN_IS_ACCESS_UNLIMITED
            + " integer, " + COLUMN_IS_NO_ACCESS_UNLIMITED
            + " integer); ";



}