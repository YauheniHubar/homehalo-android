package android.itransition.com.wedge.database.dbhelpers;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class DevicesDbHelper  {

    public static final String TABLE = "devices";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_CREATED_AT_DATE_TIME = "created_at_date_time";
    public static final String COLUMN_CREATED_AT_TIME_ZONE = "created_at_time_zone";
    public static final String COLUMN_IS_ENABLED = "is_enabled";
    public static final String COLUMN_IS_ONLINE = "is_online";
    public static final String COLUMN_IS_FAVOURITE = "is_favourite";
    public static final String COLUMN_MAC_ADDRESS = "mac_address";
    public static final String COLUMN_MANUFACTURER_TITLE = "manufacturerTitle";
    public static final String COLUMN_DEVICE_OWNER = "device_owner";
    public static final String COLUMN_LAST_ONLINE_AT_DATE_TIME = "last_online_at_date_time";
    public static final String COLUMN_LAST_ONLINE_AT_TIME_ZONE = "last_online_at_time_zone";



    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_TITLE
            + " text, " + COLUMN_CREATED_AT_DATE_TIME
            + " text, " + COLUMN_CREATED_AT_TIME_ZONE
            + " text, " + COLUMN_IS_ENABLED
            + " flag INTEGER DEFAULT 0, " + COLUMN_IS_ONLINE
            + " flag INTEGER DEFAULT 0, " + COLUMN_IS_FAVOURITE
            + " flag INTEGER DEFAULT 0, " + COLUMN_MAC_ADDRESS
            + " text, " + COLUMN_MANUFACTURER_TITLE
            + " text, " + COLUMN_DEVICE_OWNER
            + " integer, " + COLUMN_LAST_ONLINE_AT_DATE_TIME
            + " text, " + COLUMN_LAST_ONLINE_AT_TIME_ZONE
            + " text);";


}