package android.itransition.com.wedge.database.dbhelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by e.kazimirova on 28.11.2014.
 */
public class MainDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "deviceowner.db";
    private static final int DATABASE_VERSION = 10;

    public MainDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DeviceOwnersDbHelper.DATABASE_CREATE);
        database.execSQL(ContentProfileDbHelper.DATABASE_CREATE);
        database.execSQL(ContentProfilesCategoriesDbHelper.DATABASE_CREATE);
        database.execSQL(DevicesDbHelper.DATABASE_CREATE);
        database.execSQL(HomeworkCategoriesDbHelper.DATABASE_CREATE);
        database.execSQL(HomeworkCategoriesStaticDbHelper.DATABASE_CREATE);
        database.execSQL(TimeExtensionsDbHelper.DATABASE_CREATE);
        database.execSQL(TimeProfilesDbHelper.DATABASE_CREATE);
        database.execSQL(WedgeDbHelper.DATABASE_CREATE);
        database.execSQL(WhiteListUrlDbHelper.DATABASE_CREATE);
        database.execSQL(ContentCategoriesStaticDbHelper.DATABASE_CREATE);
        database.execSQL(BlacklistedCategoriesDbHelper.DATABASE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DeviceOwnersDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +ContentProfileDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +ContentProfilesCategoriesDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +DevicesDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +HomeworkCategoriesDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +HomeworkCategoriesStaticDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +TimeExtensionsDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +TimeProfilesDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +WedgeDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +WhiteListUrlDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +ContentCategoriesStaticDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +BlacklistedCategoriesDbHelper.TABLE);
        db.execSQL("DROP TABLE IF EXISTS BlackUrl");
        onCreate(db);
    }


    public void clear(SQLiteDatabase db){
        db.execSQL("DELETE FROM " + DeviceOwnersDbHelper.TABLE);
        db.execSQL("DELETE FROM " +ContentProfileDbHelper.TABLE);
        db.execSQL("DELETE FROM " +ContentProfilesCategoriesDbHelper.TABLE);
        db.execSQL("DELETE FROM " +DevicesDbHelper.TABLE);
        db.execSQL("DELETE FROM " +HomeworkCategoriesDbHelper.TABLE);
        db.execSQL("DELETE FROM " +HomeworkCategoriesStaticDbHelper.TABLE);
        db.execSQL("DELETE FROM " +TimeExtensionsDbHelper.TABLE);
        db.execSQL("DELETE FROM " +TimeProfilesDbHelper.TABLE);
        db.execSQL("DELETE FROM " +WedgeDbHelper.TABLE);
        db.execSQL("DELETE FROM " +WhiteListUrlDbHelper.TABLE);
        db.execSQL("DELETE FROM " +ContentCategoriesStaticDbHelper.TABLE);
        db.execSQL("DELETE FROM " +BlacklistedCategoriesDbHelper.TABLE);
    }


}
