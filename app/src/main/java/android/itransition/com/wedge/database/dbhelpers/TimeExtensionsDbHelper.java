package android.itransition.com.wedge.database.dbhelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class TimeExtensionsDbHelper {

    public static final String TABLE = "timeextensions";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CREATED_AT_TIME_DATE_TIME = "created_at_time_date_time";
    public static final String COLUMN_CREATED_AT_TIME_ZONE = "created_at_time_zone";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_DURATION = "duration";
    public static final String COLUMN_TER_ID = "ter_id";
    public static final String COLUMN_UPDATED_AT_TIME_DATE_TIME = "updated_at_time_date_time";
    public static final String COLUMN_UPDATED_AT_TIME_ZONE = "updated_at_time_zone";
    public static final String COLUMN_ID_DEVICE_OWNERS = "id_device_owners";




    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_CREATED_AT_TIME_DATE_TIME
            + " text, " + COLUMN_CREATED_AT_TIME_ZONE
            + " text, " + COLUMN_STATUS
            + " text, " + COLUMN_DURATION
            + " integer, " + COLUMN_TER_ID
            + " integer, "  + COLUMN_UPDATED_AT_TIME_DATE_TIME
            + " text, " + COLUMN_UPDATED_AT_TIME_ZONE
            + " text, " + COLUMN_ID_DEVICE_OWNERS
            + " integer);";



}