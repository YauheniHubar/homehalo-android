package android.itransition.com.wedge.database.dbhelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class TimeProfilesDbHelper {

    public static final String TABLE = "timeprofiles";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_START_TIME_DATE_TIME = "start_time_date_time";
    public static final String COLUMN_START_TIME_ZONE = "start_time_zone";
    public static final String COLUMN_END_TIME_DATE_TIME = "end_time_date_time";
    public static final String COLUMN_END_TIME_ZONE = "endt_time_zone";
    public static final String COLUMN_DAY_OF_WEEK = "day_of_week";
    public static final String COLUMN_ID_DEVICE_OWNERS = "id_device_owners";



    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_START_TIME_DATE_TIME
            + " text, " + COLUMN_START_TIME_ZONE
            + " text, " + COLUMN_END_TIME_DATE_TIME
            + " text, " + COLUMN_END_TIME_ZONE
            + " text, " + COLUMN_DAY_OF_WEEK
            + " text, " + COLUMN_ID_DEVICE_OWNERS
            + " integer);";


}