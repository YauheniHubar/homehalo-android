package android.itransition.com.wedge.database.dbhelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class WedgeDbHelper {

    public static final String TABLE = "browsinghistory";

    public static final String COLUMN_SSID= "ssid";
    public static final String COLUMN_PASSKEY = "passkey";
    public static final String COLUMN_FIRMWARE = "firmware";
    public static final String COLUMN_ID = "_id";



    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE+ "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_SSID
            + " text, " + COLUMN_PASSKEY
            + " text, " + COLUMN_FIRMWARE
            + " text);";


}