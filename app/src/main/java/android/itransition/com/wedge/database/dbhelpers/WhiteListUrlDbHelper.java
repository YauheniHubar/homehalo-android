package android.itransition.com.wedge.database.dbhelpers;

import android.content.ContentValues;
import android.itransition.com.wedge.entity.UrlListEntity;

/**
 * Created by e.kazimirova on 27.11.2014.
 */
public class WhiteListUrlDbHelper {

    public static final String TABLE = "whitelisturl";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_DEVICE_OWNER = "device_owner";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_CREATED_AT_DATE_TIME = "created_at_date_time";
    public static final String COLUMN_CREATED_AT_TIME_ZONE = "created_at_time_zone";
    public static final String COLUMN_IS_BLACK = "is_black";




    // Database creation sql statement
    public static final String DATABASE_CREATE = "create table "
            + TABLE + "(" + COLUMN_ID
            + " integer, " + COLUMN_DEVICE_OWNER
            + " text, " + COLUMN_URL
            + " text, " + COLUMN_STATUS
            + " text, " + COLUMN_CREATED_AT_DATE_TIME
            + " text, " + COLUMN_CREATED_AT_TIME_ZONE
            + " text, " + COLUMN_IS_BLACK
            + " integer);";


    public static void toValues(UrlListEntity whiteList, ContentValues values) {
        values.put(WhiteListUrlDbHelper.COLUMN_ID, whiteList.getId());
        values.put(WhiteListUrlDbHelper.COLUMN_DEVICE_OWNER, whiteList.
                getDeviceOwner());
        values.put(WhiteListUrlDbHelper.COLUMN_URL, whiteList.getUrl());
        values.put(WhiteListUrlDbHelper.COLUMN_STATUS, whiteList.
                getStatus());
        values.put(WhiteListUrlDbHelper.COLUMN_CREATED_AT_DATE_TIME, whiteList.
                getCreatedAt().getDateTime());
        values.put(WhiteListUrlDbHelper.COLUMN_CREATED_AT_TIME_ZONE, whiteList.
                getCreatedAt().getTimeZone());
        values.put(WhiteListUrlDbHelper.COLUMN_IS_BLACK, whiteList.isBlack());
    }
}