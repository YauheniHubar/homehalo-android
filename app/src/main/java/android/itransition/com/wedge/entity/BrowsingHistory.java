package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 05.09.2014.
 */
public  class BrowsingHistory extends BaseModel implements Parcelable {

    @JsonProperty("deviceOwner")
    private String deviceOwner;

    @JsonProperty
    private long deviceOwnerId;

    @JsonProperty("traffic")
    private long traffic;

    public void setDeviceOwnerId(long deviceOwnerId) {
        this.deviceOwnerId = deviceOwnerId;
    }

    public void setDeviceOwner(String deviceOwner){
        this.deviceOwner = deviceOwner;
    }

    public String getDeviceOwner(){
        return deviceOwner;
    }

    public void setTraffic(long traffic){
        this.traffic = traffic;
    }

    public long getDeviceOwnerId() {
        return deviceOwnerId;
    }

    public long getTraffic(){
        return traffic;
    }



    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(deviceOwner);
        parcel.writeLong(traffic);
    }

    public static final Creator<BrowsingHistory> CREATOR = new Creator<BrowsingHistory>() {

        public BrowsingHistory createFromParcel(Parcel in) {

            return new BrowsingHistory(in);
        }

        public BrowsingHistory[] newArray(int size) {
            return new BrowsingHistory[size];
        }
    };

    // constructor for reading data from Parcel
    public BrowsingHistory(Parcel parcel) {
        deviceOwner = parcel.readString();
        traffic = parcel.readLong();
    }

    public BrowsingHistory(){

    }
}