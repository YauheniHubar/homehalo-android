package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class ContentCategories extends BaseModel implements Parcelable{

    @JsonProperty("id")
    private long id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("categoryName")
    private String categoryName;
    @JsonProperty("categoryId")
    private long categoryId;

    private long idContentProfile;

    public void setIdContentProfile(long idContentProfile){
        this.idContentProfile = idContentProfile;
    }

    public long getIdContentProfile(){
        return idContentProfile;
    }

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setCategoryName(String categoryName){
        this.categoryName = categoryName;
    }

    public String getCategoryName(){
        return categoryName;
    }

    public void setCategoryId(long categoryId){
        this.categoryId = categoryId;
    }

    public  long getCategoryId(){
        return categoryId;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(categoryName);
        parcel.writeLong(categoryId);
        parcel.writeLong(idContentProfile);
    }

    public static final Creator<ContentCategories> CREATOR = new Creator<ContentCategories>() {

        public ContentCategories createFromParcel(Parcel in) {

            return new ContentCategories(in);
        }

        public ContentCategories[] newArray(int size) {
            return new ContentCategories[size];
        }
    };

    // constructor for reading data from Parcel
    public ContentCategories(Parcel parcel) {
        id = parcel.readLong();
        title = parcel.readString();
        categoryId = parcel.readLong();
        categoryName = parcel.readString();
        idContentProfile = parcel.readLong();

    }

    public ContentCategories(){

    }
}
