package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 05.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContentProfile extends BaseModel implements Parcelable{

    @JsonProperty("id")
    private long id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("createdAt")
    private CreateTimeUser createAt;
    @JsonProperty("contentCategories")
    private ContentCategories[] contentCategorises;
    @JsonProperty("isDefault")
    private boolean isDefault;
    @JsonProperty("isDeletable")
    private boolean isDeletable;

    private long userId;

    public void setDefault(boolean isDefault){
        this.isDefault = isDefault;
    }

    public boolean getDefault (){
        return isDefault;
    }

    public void setDeletable(boolean isDeletable){
        this.isDeletable = isDeletable;
    }

    public  boolean getDeletable(){
        return isDeletable;
    }

    public void setId(long id){
        this.id= id;
    }

    public long getId(){
        return id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setCreateAt(CreateTimeUser createAt){
        this.createAt = createAt;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public CreateTimeUser getCreateAt(){
        return createAt;
    }

    public void setContentCategories(ContentCategories[] contentCategorieses){
        this.contentCategorises =contentCategorieses;
    }

    public ContentCategories[] getContentCategories(){
        return contentCategorises;
    }
    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeParcelable(createAt, 1);
        parcel.writeTypedArray(contentCategorises, 1);
        parcel.writeByte((byte) (isDefault ? 1 : 0));
        parcel.writeByte((byte) (isDeletable ? 1 : 0));
    }

    public static final Creator<ContentProfile> CREATOR = new Creator<ContentProfile>() {
        public ContentProfile createFromParcel(Parcel in) {
            return new ContentProfile(in);
        }

        public ContentProfile[] newArray(int size) {
            return new ContentProfile[size];
        }
    };

    // constructor for reading data from Parcel
    public ContentProfile(Parcel parcel) {
        id = parcel.readLong();
        title = parcel.readString();
        isDeletable = parcel.readByte() == 1;
        isDefault = parcel.readByte() == 1;
        createAt = parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        contentCategorises = parcel.createTypedArray(ContentCategories.CREATOR);
    }

    public ContentProfile(){

    }
}
