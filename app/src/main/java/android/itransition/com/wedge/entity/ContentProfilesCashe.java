package android.itransition.com.wedge.entity;

/**
 * Created by e.kazimirova on 19.11.2014.
 */
public class ContentProfilesCashe {

    private static ContentProfilesCashe instance = null;
    private ContentProfile[] ct = null;
    private ContentProfilesCashe(){

    }

    public static ContentProfilesCashe getInstance(){
        if(instance==null){
            instance =  new ContentProfilesCashe();

        }
        return instance;
    }

    public ContentProfile[] getCt(){
        return ct;
    }

    public void setCt(ContentProfile[] ct){
        this.ct= ct;
    }
}
