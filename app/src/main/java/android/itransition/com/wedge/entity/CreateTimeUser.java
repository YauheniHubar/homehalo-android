package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.utils.Utils;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public  class CreateTimeUser extends BaseModel implements Parcelable {

    @JsonProperty
    private String dateTime;
    @JsonProperty("timezone")
    private String timeZone;

    public void setDateTime(String dateTime){
        this.dateTime = dateTime;
    }

    public String getDateTime(){
        return dateTime;
    }

    private Date _date;
    private DateTime dt;
    DateTime nowUTC;

    public Date getDate() {
        if (dateTime != null && WedgeApplication.currentUser != null) {
            try {
                _date = Utils.API_DATE_FORMAT.parse(dateTime);
                //w joda time
                DateTimeZone.setDefault(DateTimeZone.UTC);
                DateTimeFormatter dtf = DateTimeFormat.forPattern(Utils.API_DATE_FORMAT.toPattern());
                DateTime dt1 = dtf.parseDateTime(dateTime);
//                nowUTC = dt1.withZone(DateTimeZone.forID(WedgeApplication.currentUser.getWedge().getTimezone())); // old realiztion
                nowUTC = dt1.withZone(DateTimeZone.forID(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge())));
//                DateTimeZone tz = DateTimeZone.forID(WedgeApplication.currentUser.getWedge().getTimezone()); // old realiztion
                DateTimeZone tz = DateTimeZone.forID(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
                dt = dtf.parseDateTime(dateTime).withZone(tz);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (nowUTC == null) {
            // sometimes it crashes with NPE, so it's mockup for safety
            nowUTC = new DateTime();
        }
        return nowUTC.toDate();
    }

    public Date getDateWithOffset() {
        if (_date == null && null != dateTime) {
            try {
                _date = Utils.API_DATE_FORMAT.parse(dateTime);
                //w joda time
                DateTimeZone.setDefault(DateTimeZone.UTC);
                DateTimeFormatter dtf = DateTimeFormat.forPattern(Utils.API_DATE_FORMAT.toPattern());
                DateTime dt1 = dtf.parseDateTime(dateTime);
//                nowUTC = dt1.withZone(DateTimeZone.forID(WedgeApplication.currentUser.getWedge().getTimezone())); // old realization
//                DateTimeZone tz = DateTimeZone.forID(WedgeApplication.currentUser.getWedge().getTimezone()); // old realization
                User user = WedgeApplication.currentUser;
                nowUTC = dt1.withZone(DateTimeZone.forID(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge())));
                DateTimeZone tz = DateTimeZone.forID(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
                dt = dtf.parseDateTime(dateTime).withZone(tz);

            } catch (ParseException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.d("grch12","wedge null");
            }
        }
        if (nowUTC == null) {
            // sometimes it crashes with NPE, so it's mockup for safety
            nowUTC = new DateTime();
        }
        return nowUTC.toLocalDateTime().toDate();
    }

    public Date getDateUTC() {
        if (_date == null && null != dateTime) {
            try {
                _date = Utils.API_DATE_FORMAT.parse(dateTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return _date;
    }

    @Override
    public String toString() {
        return dateTime + timeZone;
    }

    public void setTimeZone(String timeZone){
        this.timeZone = timeZone;
    }

    public String getTimeZone(){
        return timeZone;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(dateTime);
        parcel.writeString(timeZone);
    }

    public static final Creator<CreateTimeUser> CREATOR = new Creator<CreateTimeUser>() {

        public CreateTimeUser createFromParcel(Parcel in) {

            return new CreateTimeUser(in);
        }

        public CreateTimeUser[] newArray(int size) {
            return new CreateTimeUser[size];
        }
    };

    // constructor for reading data from Parcel
    public CreateTimeUser(Parcel parcel) {
        dateTime = parcel.readString();
        timeZone = parcel.readString();
    }

    public CreateTimeUser(){

    }
}