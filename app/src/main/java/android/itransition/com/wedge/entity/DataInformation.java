package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 13.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataInformation extends BaseModel implements Parcelable {

    //DEVISE - TYPE
    @JsonProperty("device_owner")
    private long device_owner;
    @JsonProperty("user")
    private long user;
    @JsonProperty("title")
    private String title;
    @JsonProperty("is_enabled")
    private boolean is_enabled;
    @JsonProperty("is_online")
    private boolean is_online;
    @JsonProperty("is_favorite")
    private boolean is_favorite;
    @JsonProperty("mac_address")
    private String mac_address;
    @JsonProperty("manufacturer_title")
    private String manufacturer_title;
    @JsonProperty("created_at")
    private String  created_at;
    @JsonProperty("id")
    private long id;
    @JsonProperty("deviceOwner")
    private DataInformationDeviceOwner informationDeviceOwner;


    //WHITE LIST _ TYPE
//    @JsonProperty("createdAt")
//    private CreateTimeUser createdAt;
    //id
    //deviceOwner
    @JsonProperty("status")
    private String status;
    @JsonProperty("url")
    private String url;
    @JsonProperty("zveloRating")
    private String zveloRating;


    //TIME EXTENSION - TYPE
    //createdAt
    //deviceOwner
    @JsonProperty("duration")
    private long duration;
    //status
    @JsonProperty("terId")
    private long terId;



    public void setUser(long user){
        this.user = user;
    }

    public long getUser(){
        return user;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setEnabled(boolean is_enabled){
        this.is_enabled = is_enabled;
    }

    public boolean getEnabled(){
        return is_enabled;
    }

    public void setOnline(boolean is_online){
        this.is_online = is_online;
    }

    public boolean getOnline(){
        return is_online;
    }

    public void setFavourite(boolean is_favorite){
        this.is_favorite = is_favorite;
    }

    public boolean getFavourite(){
        return is_favorite;
    }

    public void setMacAddress(String macAddress){
        this.mac_address = macAddress;
    }

    public String getMacAddress(){
        return mac_address;
    }

    public void setManufacturerTitle(String manufacturerTitle){
        this.manufacturer_title = manufacturerTitle;
    }

    public String getManufacturerTitle(){
        return manufacturer_title;
    }

    public void setCreated_At(String createdAt){
        this.created_at = createdAt;
    }

    public String getCreated_At(){
        return created_at;
    }

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }


    public void setDataInformationDeviceOwner(DataInformationDeviceOwner dataInformationDeviceOwner){
        this.informationDeviceOwner = dataInformationDeviceOwner;
    }

    public DataInformationDeviceOwner getDataInformationDeviceOwner(){
        return informationDeviceOwner;
    }

//    public void setCreatedAt(String createdAt){
//        this.createdAt = createdAt;
//    }

    public String getCreatedAt(){
        return created_at;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getUrl(){
        return url;
    }

    public void setZveloRating(String zveloRating){
        this.zveloRating = zveloRating;
    }

    public String getZveloRating(){
        return zveloRating;
    }

    public void setDuration(long duration){
        this.duration = duration;
    }

    public long getDuration(){
        return duration;
    }

    public void setTerId(long terId){
        this.terId = terId;
    }

    public long getTerId(){
        return terId;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(informationDeviceOwner, i);
        parcel.writeLong(device_owner);
        parcel.writeLong(user);
        parcel.writeString(title);
        parcel.writeByte((byte) (is_enabled ? 1 : 0));
        parcel.writeByte((byte) (is_online ? 1 : 0));
        parcel.writeByte((byte) (is_favorite ? 1 : 0));
        parcel.writeString(mac_address);
        parcel.writeString(created_at);
        parcel.writeLong(id);
//        parcel.writeParcelable(createdAt, 1);
        parcel.writeString(status);
        parcel.writeString(url);
        parcel.writeString(zveloRating);
        parcel.writeLong(duration);
        parcel.writeLong(terId);

    }

    public static final Creator<DataInformation> CREATOR = new Creator<DataInformation>() {

        public DataInformation createFromParcel(Parcel in) {

            return new DataInformation(in);
        }

        public DataInformation[] newArray(int size) {
            return new DataInformation[size];
        }
    };

    // constructor for reading data from Parcel
    public DataInformation(Parcel parcel) {
        informationDeviceOwner = parcel.readParcelable(DataInformationDeviceOwner.class.getClassLoader());
        device_owner = parcel.readLong();
        user = parcel.readLong();
        title = parcel.readString();
        is_enabled = parcel.readByte() == 1;
        is_online = parcel.readByte() == 1;
        is_favorite = parcel.readByte() == 1;
        mac_address = parcel.readString();
        created_at = parcel.readString();
        id = parcel.readLong();
//        createdAt = (CreateTimeUser)parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        status = parcel.readString();
        url = parcel.readString();
        zveloRating = parcel.readString();
        duration = parcel.readLong();
        terId = parcel.readLong();
    }

    public DataInformation(){

    }



}



