package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 13.09.2014.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataInformationDeviceOwner extends BaseModel implements Parcelable {

    @JsonProperty("id")
    private long id;
    @JsonProperty("user")
    private int user;
    @JsonProperty("block_started_at")
    private String block_started_at;
    @JsonProperty("homework_mode_started_at")
    private String homework_mode_started_at;
    @JsonProperty("homework_mode_duration")
    private long homework_mode_duration;
    @JsonProperty("contentProfile")
    private ContentProfile content_profile;
    @JsonProperty("title")
    private String title;
//    @JsonProperty("created_at")
//    private String created_at;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("status")
    private String status;
    @JsonProperty("time")
    private long time;
    @JsonProperty("is_default")
    private boolean is_default;
    @JsonProperty("is_deletable")
    private boolean isDeletable;
//    @JsonProperty("createdAt")
//    private CreateTimeUser createdAt;


    public void setHomework_mode_started_at(String homework_mode_started_at){
        this.homework_mode_started_at = homework_mode_started_at;
    }

    public String getHomework_mode_started_at(){
        return homework_mode_started_at;
    }

    public void setHomework_mode_duration(long homework_mode_duration){
        this.homework_mode_duration = homework_mode_duration;
    }

    public long getHomework_mode_duration(){
        return homework_mode_duration;
    }

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public void setUser(int user){
        this.user = user;
    }

    public int getUser(){
        return user;
    }

    public String getBlock_started_at(){
        return block_started_at;
    }

    public void setBlock_started_at(String block_started_at){
        this.block_started_at = block_started_at;
    }

    public void setContentProfile(ContentProfile contentProfile){
        this.content_profile = contentProfile;
    }

    public ContentProfile getContentProfile(){
        return content_profile;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

//    public void setCreatedAt(CreateTimeUser createdAt){
//        this.createdAt = createdAt;
//    }
//
//    public CreateTimeUser getCreatedAt(){
//        return createdAt;
//    }

    public void setUpdatedAt(String updatedAt){
        this.updated_at = updatedAt;
    }

    public String getUpdatedAt(){
        return updated_at;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    public void setTime(long time){
        this.time = time;
    }

    public long getTime(){
        return time;
    }


    public void setDefault(boolean isDefault){
        this.is_default = isDefault;
    }

    public boolean getDefault(){
        return is_default;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
//        parcel.writeParcelable(content_profile, i);
        parcel.writeString(title);
//        parcel.writeString(created_at);
//        parcel.writeString(updated_at);
//        parcel.writeString(status);
//        parcel.writeLong(time);
//        parcel.writeByte((byte) (is_default ? 1 : 0));
//        parcel.writeInt(user);
//        parcel.writeString(block_started_at);
//        parcel.writeString(homework_mode_started_at);
//        parcel.writeLong(homework_mode_duration);
//        parcel.writeParcelable(createdAt, i);

    }

    public static final Creator<DataInformationDeviceOwner> CREATOR = new Creator<DataInformationDeviceOwner>() {
        public DataInformationDeviceOwner createFromParcel(Parcel in) {
            return new DataInformationDeviceOwner(in);
        }
        public DataInformationDeviceOwner[] newArray(int size) {
            return new DataInformationDeviceOwner[size];
        }
    };

    // constructor for reading data from Parcel
    public DataInformationDeviceOwner(Parcel parcel) {
        id = parcel.readLong();
//        content_profile = parcel.readParcelable(ContentProfile.class.getClassLoader());
        title = parcel.readString();
//        created_at = parcel.readString();
//        updated_at = parcel.readString();
//        status = parcel.readString();
//        time = parcel.readLong();
//        is_default = parcel.readByte() == 1;
//        user = parcel.readInt();
//        block_started_at = parcel.readString();
//        homework_mode_started_at = parcel.readString();
//        homework_mode_duration = parcel.readLong();
//        createdAt = (CreateTimeUser)parcel.readParcelable(CreateTimeUser.class.getClassLoader());

    }

    public DataInformationDeviceOwner(){

    }



}



