package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.PopularBlacklistSimpleItem;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
public class DeviceOwner extends BaseModel implements Parcelable{

    @JsonProperty("id")
    private long id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("createdAt")
    private CreateTimeUser createAt;
    @JsonProperty("contentProfile")
    private ContentProfile contentProfile;
    @JsonProperty("status")
    private String status;
    @JsonProperty("timeProfiles")
    private TimeProfiles[] timeProfiles;
    @JsonProperty("urlWhitelists")
    private ArrayList<UrlListEntity> urlWhiteLists;
    @JsonProperty("timeExtension")
    private TimeExtensions timeExtension;
    @JsonProperty("time")
    private long time;
    @JsonProperty("blockStartedAt")
    private CreateTimeUser blockStartedAt;
    @JsonProperty("homeworkModeDuration")
    private Long homeworkModeDuration;
    @JsonProperty("homeworkModeStartedAt")
    private CreateTimeUser homeworkModeStartedAt;
    @JsonProperty("homeworkCategories")
    private HomeWorkCategories[] homeworkCategories;
    @JsonProperty("access")
    private OwnerAccess access;
    @JsonProperty("deviceOnlineCount")
    private int deviceOnlineCount;
    private int orderId;
    @JsonProperty("urlBlacklists")
    private List<UrlListEntity> urlBlacklists;
    @JsonProperty("isDeletable")
    private boolean isDeletable;
    @JsonProperty("blacklistedCategories")
    private List<PopularBlacklistSimpleItem> blacklistedCategories;

    public void setHasAccessUntil(CreateTimeUser hasAccessUntil){
        this.access.hasAccessUntil = hasAccessUntil;
    }

    public CreateTimeUser getHasAccessUntil(){
        return access.hasAccessUntil;
    }
    public CreateTimeUser getNextAccessUntil(){
        return access.nextAccessUntil;
    }

    public CreateTimeUser getHasNoAccessUntil(){
        return access.getHasNoAccessUntil();
    }

    public void setHasNoAccessUntil(CreateTimeUser hasNoAccessUntil){
        this.access.setHasNoAccessUntil(hasNoAccessUntil);
    }

    public void setDeviceOnlineCount(int deviceOnlineCount){
        this.deviceOnlineCount = deviceOnlineCount;
    }

    public int getDeviceOnlineCount(){
        return deviceOnlineCount;
    }

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }


    public void setHomeworkCategories(HomeWorkCategories[] homeworkCategories){
        this.homeworkCategories = homeworkCategories;
    }

    public HomeWorkCategories[] getHomeworkCategories(){
        return homeworkCategories;
    }

    public void setTime(long time){
        this.time = time;
    }

    public long getTime(){
        return time;
    }

    public String getHasAccessUntilLabel() {
        return access.getHasAccessUntilLabel();
    }

    public void setHomeworkModeDuration(long homeworkModeDuration){
        this.homeworkModeDuration = homeworkModeDuration;
    }

    public Long getHomeworkModeDuration(){
            return homeworkModeDuration;
    }

    public void setHomeworkModeStartedAt(CreateTimeUser homeworkModeStartedAt){
        this.homeworkModeStartedAt = homeworkModeStartedAt;
    }

    public CreateTimeUser getHomeworkModeStartedAt(){
        return homeworkModeStartedAt;
    }

    public void setBlockStartedAt(CreateTimeUser blockStartedAt){
        this.blockStartedAt = blockStartedAt;
    }

    public CreateTimeUser getBlockStartedAt(){
        return blockStartedAt;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setCreateAt(CreateTimeUser createAt){
        this.createAt = createAt;
    }

    public CreateTimeUser getCreateAt(){
        return createAt;
    }

    public void setContentProfile(ContentProfile contentProfile){
        this.contentProfile = contentProfile;
    }

    public ContentProfile getContentProfile(){
        return contentProfile;
    }

    public void setStatus(String status){
        this.status =status;
    }

    public String getNextAccessUntilLabel() {
        return access.getNextAccessUntilLabel();
    }

    public void setNextAccessUntilLabel(String value) {
        access.nextAccessUntilLabel = value;
    }

    public String getStatus(){
        return status;
    }

    public void setHasAccessUntilLabel(String hasAccessUntilLabel) {
        this.access.setHasAccessUntilLabel(hasAccessUntilLabel);
    }

    public void setTimeProfiles(TimeProfiles[] timeProfiles){
        this.timeProfiles =timeProfiles;
    }

    public TimeProfiles[] getTimeProfiles(){
        return timeProfiles;
    }

    public void setUrlWhiteLists(ArrayList<UrlListEntity> urlWhiteLists) {
        this.urlWhiteLists = urlWhiteLists;
    }

    public ArrayList<UrlListEntity> getUrlWhiteLists(){
        return urlWhiteLists;
    }

    public void setTimeExtension(TimeExtensions timeExtensionses){
        this.timeExtension =timeExtensionses;
    }

    public TimeExtensions getTimeExtension(){
        return timeExtension;
    }

    public boolean isUnlimitedAccess() {
        return access.isUnlimited();
    }

    public boolean isAccessUnlimited() {
        return access.isAccessUnlimited();
    }

    public boolean isNoAccessUnlimited() {
        return access.isNoAccessUnlimited();
    }

    public void setAccessUnlimited(boolean isAccessUnlimited) {
        access.setAccessUnlimited(isAccessUnlimited);
    }

    public void setNoAccessUnlimited(boolean isNoAccessUnlimited) {
        access.setNoAccessUnlimited(isNoAccessUnlimited);
    }

    public void setUnlimitedAccess(boolean value) {
        access.setUnlimited(value);
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setDeletable(boolean isDeletable) {
        this.isDeletable = isDeletable;
    }

    public boolean isDeletable() {
        return isDeletable;
    }

    public List<PopularBlacklistSimpleItem> getBlacklistedCategories() {
        return blacklistedCategories;
    }

    public void setBlacklistedCategories(List<PopularBlacklistSimpleItem> blacklistedCategories) {
        this.blacklistedCategories = blacklistedCategories;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeInt(deviceOnlineCount);
        parcel.writeLong(time);
        parcel.writeLong(homeworkModeDuration);
        parcel.writeParcelable(homeworkModeStartedAt, 1);
        parcel.writeParcelable(blockStartedAt, 1);
        parcel.writeString(title);
        parcel.writeParcelable(createAt, 1);
        parcel.writeParcelable(access.hasAccessUntil, 1);
        parcel.writeString(status);
        parcel.writeString(access.getHasAccessUntilLabel());
        parcel.writeParcelable(contentProfile, 1);
        parcel.writeParcelableArray(timeProfiles, 1);
        parcel.writeTypedList(urlWhiteLists);
        parcel.writeParcelable(timeExtension, 1);
        parcel.writeParcelableArray(homeworkCategories, 1);
        parcel.writeByte((byte) (access.isAccessUnlimited() ? 1 : 0));
        parcel.writeByte((byte) (access.isNoAccessUnlimited() ? 1 : 0));
        parcel.writeInt(orderId);
        parcel.writeByte((byte) (isDeletable ? 1 : 0));
        parcel.writeTypedList(blacklistedCategories);
    }

    public static final Creator<DeviceOwner> CREATOR = new Creator<DeviceOwner>() {

        public DeviceOwner createFromParcel(Parcel in) {

            return new DeviceOwner(in);
        }

        public DeviceOwner[] newArray(int size) {
            return new DeviceOwner[size];
        }
    };

    // constructor for reading data from Parcel
    public DeviceOwner(Parcel parcel) {
        access = new OwnerAccess();

        id = parcel.readLong();
        time = parcel.readLong();
        deviceOnlineCount = parcel.readInt();
        homeworkModeDuration = parcel.readLong();
        homeworkModeStartedAt = (CreateTimeUser)parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        blockStartedAt = (CreateTimeUser)parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        title = parcel.readString();
        createAt = (CreateTimeUser)parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        access.hasAccessUntil = (CreateTimeUser)parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        status = parcel.readString();
        access.setHasAccessUntilLabel(parcel.readString());
        contentProfile = (ContentProfile)parcel.readParcelable(ContentProfile.class.getClassLoader());
        timeProfiles = parcel.createTypedArray(TimeProfiles.CREATOR);
        parcel.readTypedList(urlWhiteLists, UrlListEntity.CREATOR);
        timeExtension = parcel.readParcelable(TimeExtensions.class.getClassLoader());
        homeworkCategories = parcel.createTypedArray(HomeWorkCategories.CREATOR);
        access.setUnlimited(parcel.readByte() == 1);
        access.setNoAccessUnlimited(parcel.readByte() == 1);
        access.setAccessUnlimited(parcel.readByte() == 1);
        orderId = parcel.readInt();
        isDeletable = (parcel.readByte() == 1);
        parcel.readTypedList(blacklistedCategories, PopularBlacklistSimpleItem.CREATOR);
    }

    public DeviceOwner(){
        access = new OwnerAccess();
//        homeworkModeDuration = null;
    }

    public List<UrlListEntity> getUrlBlackLists() {
        return urlBlacklists;
    }


    @Override
    public String toString() {
        String string = title;

        try {
            ObjectMapper mapper = new ObjectMapper();
            string = mapper.writeValueAsString(this);
        } catch (IOException e) { }
        return string;
    }

    public void setNextAccess(CreateTimeUser nextAccess) {
        this.access.nextAccessUntil = nextAccess;
    }
}
