package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 03.09.2014.
 */

public class DevicesEntity extends BaseModel implements Parcelable {

    @JsonProperty("id")
    private long id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("isEnabled")
    private boolean isEnabled;
    @JsonProperty("isOnline")
    private boolean isOnline;
    @JsonProperty("isFavorite")
    private boolean isFavourite;
    @JsonProperty("macAddress")
    private String macAddress;
    @JsonProperty("manufacturerTitle")
    private String manufacturerTitle;
    @JsonProperty("manufacturerAddress")
    private String manufacturedAddress;
    @JsonProperty("manufacturerCountry")
    private String manufacturedCountry;

    @JsonProperty("createdAt")
    private CreateTimeUser createAt;
    @JsonProperty("deviceOwner")
    private DeviceOwner deviceOwner;//use only ID
    @JsonProperty("lastOnlineAt")//does not use
    private CreateTimeUser lastOnlineAt;

    public transient boolean inEditMode;
    public transient boolean isExpanded;

    public void setId(long id){
        this.id= id;
    }

    public long getId(){
        return id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setEnabled(boolean isEnabled){
        this.isEnabled = isEnabled;
    }

    public boolean getEnabled(){
        return isEnabled;
    }

    public void setOnline(boolean isOnline){
        this.isOnline=isOnline;
    }

    public boolean isOnline(){
        return isOnline;
    }

    public void setFavourite(boolean isFavourite){
        this.isFavourite = isFavourite;
    }

    public boolean getFavourite(){
        return isFavourite;
    }

    public void setMacAddress(String macAddress){
        this.macAddress = macAddress;
    }

    public String getMacAddress(){
        return macAddress;
    }

    public void setCreateAt(CreateTimeUser createAt){
        this.createAt = createAt;
    }

    public CreateTimeUser getCreateAt(){
        return createAt;
    }

    public void setLastOnlineAt(CreateTimeUser lastOnlineAt){
        this.lastOnlineAt = lastOnlineAt;
    }

    public CreateTimeUser getLastOnlineAt(){
        return lastOnlineAt;
    }



    public void setDeviceOwner(DeviceOwner deviceOwner){
        this.deviceOwner = deviceOwner;
    }

    public DeviceOwner getDeviceOwner(){
        return deviceOwner;
    }


    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeByte((byte) (isEnabled ? 1 : 0));
        parcel.writeByte((byte) (isOnline ? 1 : 0));
        parcel.writeByte((byte) (isFavourite ? 1 : 0));
        parcel.writeString(macAddress);
        parcel.writeString(manufacturerTitle);
        parcel.writeParcelable(createAt, 1);
        parcel.writeParcelable(lastOnlineAt, 1);
        parcel.writeParcelable(deviceOwner, 1);


    }

    public static final Creator<DevicesEntity> CREATOR = new Creator<DevicesEntity>() {

        public DevicesEntity createFromParcel(Parcel in) {

            return new DevicesEntity(in);
        }

        public DevicesEntity[] newArray(int size) {
            return new DevicesEntity[size];
        }
    };

    // constructor for reading data from Parcel
    public DevicesEntity(Parcel parcel) {
        id = parcel.readLong();
        title = parcel.readString();
        isEnabled = parcel.readByte() == 1;
        isOnline = parcel.readByte() == 1;
        isFavourite = parcel.readByte() == 1;
        macAddress = parcel.readString();
        manufacturerTitle = parcel.readString();
        createAt =  parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        lastOnlineAt = parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        deviceOwner = parcel.readParcelable(DeviceOwner.class.getClassLoader());


    }

    public DevicesEntity(){

    }

    public String getManufacturedTitle() {
        return manufacturerTitle;
    }

    public void setManufacturedTitle(String manufacturedTitle) {
        this.manufacturerTitle = manufacturedTitle;
    }

    public String getManufacturedAddress() {
        return manufacturedAddress;
    }

    public void setManufacturedAddress(String manufacturedAddress) {
        this.manufacturedAddress = manufacturedAddress;
    }

    public String getManufacturedCountry() {
        return manufacturedCountry;
    }

    public void setManufacturedCountry(String manufacturedCountry) {
        this.manufacturedCountry = manufacturedCountry;
    }
}
