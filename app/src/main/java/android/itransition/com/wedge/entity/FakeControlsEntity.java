package android.itransition.com.wedge.entity;

/**
 * Created by e.kazimirova on 03.09.2014.
 */
public class FakeControlsEntity {


    private int title;
    private String description;

    public FakeControlsEntity(){}



    public void setTitle(int title){
        this.title = title;
    }

    public  int getTitle(){
        return title;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }
}
