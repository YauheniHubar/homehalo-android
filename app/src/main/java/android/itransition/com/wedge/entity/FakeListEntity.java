package android.itransition.com.wedge.entity;

/**
 * Created by e.kazimirova on 01.09.2014.
 */
public class FakeListEntity {

    private String mListOfNames;
    private boolean mWithCircle;
    private int mSize;

    public void setmListOfNames(String listOfNames){
        mListOfNames = listOfNames;
    }

    public String getmListOfNames(){
        return mListOfNames;
    }

    public void setmWithCircle(boolean withCircle){
        this.mWithCircle = withCircle;
    }

    public boolean getmWithCircle(){
        return mWithCircle;
    }

    public void setmSize(int size){
        mSize = size;
    }

    public int getmSize(){
        return mSize;
    }
}
