package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 16.09.2014.
 */
public class HomeWorkCategories extends BaseModel implements Parcelable {

    @JsonProperty("id")
    private long id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("categoryName")
    private String categoryName;
    @JsonProperty("categoryId")
    private long categoryId;


    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public void setTitle(String title){
        this.title= title;
    }

    public String getTitle(){
        return title;
    }

    public void setCategoryName(String categoryName){
        this.categoryName = categoryName;
    }

    public String getCategoryName(){
        return categoryName;
    }

    public void setCategoryId(long categoryId){
        this.categoryId = categoryId;
    }

    public long getCategoryId(){
        return categoryId;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(categoryName);
        parcel.writeLong(categoryId);
    }

    public static final Creator<HomeWorkCategories> CREATOR = new Creator<HomeWorkCategories>() {

        public HomeWorkCategories createFromParcel(Parcel in) {

            return new HomeWorkCategories(in);
        }

        public HomeWorkCategories[] newArray(int size) {
            return new HomeWorkCategories[size];
        }
    };

    // constructor for reading data from Parcel
    public HomeWorkCategories(Parcel parcel) {
        id = parcel.readLong();
        title = parcel.readString();
        categoryName = parcel.readString();
        categoryId = parcel.readLong();

    }

    public HomeWorkCategories(){

    }


}

