package android.itransition.com.wedge.entity;

/**
 * Created by e.kazimirova on 19.11.2014.
 */
public class HomeworkCategoriesCashe {

    private static HomeworkCategoriesCashe instance = null;
    private HomeWorkCategories[] ct = null;
    private HomeworkCategoriesCashe(){

    }

    public static HomeworkCategoriesCashe getInstance(){
        if(instance==null){
            instance =  new HomeworkCategoriesCashe();

        }
        return instance;
    }

    public HomeWorkCategories[] getCt(){
        return ct;
    }

    public void setCt(HomeWorkCategories[] ct){
        this.ct= ct;
    }
}
