package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 01.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationsEntity extends BaseModel implements Parcelable {

    @JsonProperty("id")
    private long id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("text")
    private String text;
    @JsonProperty("type")
    private String type;  //device|url_whitelist|time_extension|default
    @JsonProperty("data")
    private DataInformation data;
    @JsonProperty("isViewed")
    private boolean isViewed;
    @JsonProperty("createdAt")
    private CreateTimeUser  createdAt;




    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setText(String text){
        this.text = text;
    }

    public String getText(){
        return text;
    }

    public void setType(String type){
        this.type = type;
    }

    public String getType(){
        return type;
    }

    public CreateTimeUser getCreatedAt(){
        return createdAt;
    }

    public void setCreatedAt(CreateTimeUser createdAt){
        this.createdAt = createdAt;
    }

    public boolean getViewed(){
        return isViewed;
    }

    public void setViewed(boolean isViewed){
        this.isViewed = isViewed;
    }

    public DataInformation getData(){
        return data;
    }

    public void setDataInformation(DataInformation data){
        this.data = data;
    }


    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(data, i);
        parcel.writeParcelable(createdAt, i);
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(text);
        parcel.writeString(type);
        parcel.writeByte((byte) (isViewed ? 1 : 0));

    }

    public static final Creator<NotificationsEntity> CREATOR = new Creator<NotificationsEntity>() {
        public NotificationsEntity createFromParcel(Parcel in) {
            return new NotificationsEntity(in);
        }
        public NotificationsEntity[] newArray(int size) {
            return new NotificationsEntity[size];
        }
    };

    // constructor for reading data from Parcel
    public NotificationsEntity(Parcel parcel) {
        data = parcel.readParcelable(DataInformation.class.getClassLoader());
        createdAt = parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        id = parcel.readLong();
        title = parcel.readString();
        text = parcel.readString();
        type = parcel.readString();
        isViewed = parcel.readByte() == 1;
    }

    public NotificationsEntity(){

    }


    @Override
    public boolean equals(Object o) {
        if (o == this) return true;

        if (o instanceof NotificationsEntity) {
            return ((NotificationsEntity) o).getId() == this.getId();
        }
        return false;
    }
}

