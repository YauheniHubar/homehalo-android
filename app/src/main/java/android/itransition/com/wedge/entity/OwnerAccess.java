package android.itransition.com.wedge.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by y.drobysh on 16.01.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OwnerAccess {

    @JsonProperty
    private
    String hasAccessUntilLabel;

    @JsonProperty("hasAccessUntil")
    CreateTimeUser hasAccessUntil;
    @JsonProperty("hasNoAccessUntil")
    CreateTimeUser hasNoAccessUntil;

    @JsonProperty
    CreateTimeUser nextAccessUntil;

    @JsonProperty
    private boolean isUnlimited;
    @JsonProperty
    String nextAccessUntilLabel;

    @JsonProperty("isAccessUnlimited")
    private boolean isAccessUnlimited;

    @JsonProperty("isNoAccessUnlimited")
    private boolean isNoAccessUnlimited;


    public CreateTimeUser getHasAccessUntil() {
        return hasAccessUntil;
    }

    public String getHasAccessUntilLabel() {
        return hasAccessUntilLabel;
    }

    public String getNextAccessUntilLabel() {
        return nextAccessUntilLabel;
    }

    public CreateTimeUser getNextAccessUntil() {
        return nextAccessUntil;
    }

    public void setHasAccessUntilLabel(String hasAccessUntilLabel) {
        this.hasAccessUntilLabel = hasAccessUntilLabel;
    }

    public boolean isUnlimited() {
        return isUnlimited;
    }

    public void setUnlimited(boolean isUnlimited) {
        this.isUnlimited = isUnlimited;
    }

    public boolean isNoAccessUnlimited() {
        return isNoAccessUnlimited;
    }

    public void setNoAccessUnlimited(boolean isNoAccessUnlimited) {
        this.isNoAccessUnlimited = isNoAccessUnlimited;
    }

    public boolean isAccessUnlimited() {
        return isAccessUnlimited;
    }

    public void setAccessUnlimited(boolean isAccessUnlimited) {
        this.isAccessUnlimited = isAccessUnlimited;
    }

    public CreateTimeUser getHasNoAccessUntil() {
        return hasNoAccessUntil;
    }

    public void setHasNoAccessUntil(CreateTimeUser hasNoAccessUntil) {
        this.hasNoAccessUntil = hasNoAccessUntil;
    }
}
