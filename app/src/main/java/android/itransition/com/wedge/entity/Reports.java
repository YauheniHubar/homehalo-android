package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 13.09.2014.
 */

public class Reports extends BaseModel implements Parcelable {


    @JsonProperty("id")
    private String id;
    @JsonProperty("url")
    private String url;
    @JsonProperty("traffic")
    private int traffic;
    @JsonProperty("createdAt")
    private CreateTimeUser createdAt;
    @JsonProperty("device")
    private DevicesEntity device;
    @JsonProperty("deviceOwner")
    private DeviceOwner deviceOwner;
    @JsonProperty("contentProfile")
    private ContentProfile contentProfile;
    @JsonProperty("contentCategories")
    private ContentCategories[]  contentCategories;
    @JsonProperty("status")
    private String status;
    @JsonProperty("timeProfiles")
    private TimeProfiles[] timeProfiles;


    public void setTimeProfiles(TimeProfiles[] timeProfiles){
        this.timeProfiles = timeProfiles;
    }

    public TimeProfiles[] getTimeProfiles(){
        return timeProfiles;
    }

    public void setContentCategories(ContentCategories[] contentCategories){
        this.contentCategories = contentCategories;
    }

    public ContentCategories[] getContentCategories(){
        return contentCategories;
    }

    public void setContentProfile(ContentProfile contentProfile){
        this.contentProfile = contentProfile;
    }

    public ContentProfile getContentProfile(){
        return contentProfile;
    }


    public void setDeviceOwner(DeviceOwner deviceOwner){
        this.deviceOwner = deviceOwner;
    }

    public DeviceOwner getDeviceOwner(){
        return deviceOwner;
    }


    public void setDevice(DevicesEntity device){
        this.device = device;
    }

    public DevicesEntity getDevice(){
        return device;
    }

    public void setCreatedAt(CreateTimeUser createdAt){
        this.createdAt = createdAt;
    }

    public CreateTimeUser getCreatedAt(){
        return createdAt;
    }

    public void setTraffic(int traffic){
        this.traffic = traffic;
    }

    public int getTraffic(){
        return traffic;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getUrl(){
        return url;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }


    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeInt(traffic);
        parcel.writeString(url);
        parcel.writeParcelable(createdAt, 1);
        parcel.writeParcelable(device, 1);
        parcel.writeParcelable(deviceOwner, 1);
        parcel.writeParcelable(contentProfile, 1);
        parcel.writeParcelableArray(contentCategories, 1);
        parcel.writeString(status);
        parcel.writeParcelableArray(timeProfiles, 1);

    }

    public static final Creator<Reports> CREATOR = new Creator<Reports>() {

        public Reports createFromParcel(Parcel in) {

            return new Reports(in);
        }

        public Reports[] newArray(int size) {
            return new Reports[size];
        }
    };

    // constructor for reading data from Parcel
    public Reports(Parcel parcel) {
        id = parcel.readString();
        traffic = parcel.readInt();
        url = parcel.readString();
        status = parcel.readString();
        contentCategories = (ContentCategories[]) parcel.readParcelableArray
                (ContentCategories.class.getClassLoader());
        timeProfiles = (TimeProfiles[]) parcel.readParcelableArray(TimeProfiles.class.getClassLoader());
        createdAt = parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        device = parcel.readParcelable(DevicesEntity.class.getClassLoader());
        deviceOwner = parcel.readParcelable(DeviceOwner.class.getClassLoader());
        contentProfile = parcel.readParcelable(ContentProfile.class.getClassLoader());

    }

    public Reports(){

    }



}



