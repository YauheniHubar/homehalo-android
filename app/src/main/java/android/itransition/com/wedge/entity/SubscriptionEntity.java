package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by i.grechishchev on 22.05.2015.
 */
public class SubscriptionEntity extends BaseModel {
    @JsonProperty("isActive")
    private boolean isActive;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }
}
