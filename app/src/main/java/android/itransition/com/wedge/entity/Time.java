package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class Time extends BaseModel implements Parcelable {

    @JsonProperty("time")
    private String time;
    @JsonProperty("timezone")
    private String timezone;

    public  void setTime(String time){
        this.time = time;
    }

    public String getTime(){
        return  time;
    }

    public void setTimezone(String timezone){
        this.timezone = timezone;
    }

    public String getTimezone(){
        return timezone;
    }


    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(time);
        parcel.writeString(timezone);
    }

    public static final Creator<Time> CREATOR = new Creator<Time>() {

        public Time createFromParcel(Parcel in) {

            return new Time(in);
        }

        public Time[] newArray(int size) {
            return new Time[size];
        }
    };

    // constructor for reading data from Parcel
    public Time(Parcel parcel) {
      time= parcel.readString();
      timezone= parcel.readString();

    }

    public Time(){

    }
}
