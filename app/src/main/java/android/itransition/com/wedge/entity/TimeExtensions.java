package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class TimeExtensions extends BaseModel implements Parcelable {

    @JsonProperty("id")
    private long id;
    @JsonProperty("status")
    private String status;
    @JsonProperty("duration")
    private long duration;
    @JsonProperty("createdAt")
    private CreateTimeUser createdAt;
    @JsonProperty("terId")
    private long terId;
    @JsonProperty
    private CreateTimeUser startedAt;


    public  void setId(long id){
        this.id = id;
    }

    public  long getId(){
        return id;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public  String getStatus(){
        return  status;
    }

    public void setDuration(long duration){
        this.duration = duration;
    }

    public long getDuration(){
        return duration;
    }

    public void setCreatedAt(CreateTimeUser createdAt){
        this.createdAt = createdAt;
    }

    public  CreateTimeUser getCreatedAt(){
        return createdAt;
    }

    public void setStartedAt(CreateTimeUser startedAt){
        this.startedAt = startedAt;
    }

    public  CreateTimeUser getStartedAt(){
        return startedAt;
    }

    public void setTerId(long terId){
        this.terId = terId;
    }

    public  long getTerId(){
        return terId;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeLong(duration);
        parcel.writeParcelable(createdAt, i);
        parcel.writeLong(terId);
        parcel.writeString(status);
        parcel.writeParcelable(startedAt, i);

    }

    public static final Creator<TimeExtensions> CREATOR = new Creator<TimeExtensions>() {

        public TimeExtensions createFromParcel(Parcel in) {

            return new TimeExtensions(in);
        }

        public TimeExtensions[] newArray(int size) {
            return new TimeExtensions[size];
        }
    };

    // constructor for reading data from Parcel
    public TimeExtensions(Parcel parcel) {
        id= parcel.readLong();
        duration= parcel.readLong();
        createdAt = (CreateTimeUser)parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        terId= parcel.readLong();
        status= parcel.readString();
        startedAt = (CreateTimeUser)parcel.readParcelable(CreateTimeUser.class.getClassLoader());
    }

    public TimeExtensions(){

    }

    @Override
    public String toString() {
        return String.format("{id = %d, duration = %d, startAT = %s}", id, duration, startedAt != null ? startedAt.toString() : "");
    }
}
