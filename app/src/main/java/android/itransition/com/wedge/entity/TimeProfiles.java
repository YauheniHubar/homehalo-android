package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class TimeProfiles extends BaseModel implements Parcelable {

    @JsonProperty("id")
    private long id;
    @JsonProperty("startTime")
    private Time startTime;
    @JsonProperty("endTime")
    private Time endTime;
    @JsonProperty("dayOfWeek")
    private String dayOfWeek;

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public void setStartTime(Time startTime){
        this.startTime = startTime;
    }

    public Time getStartTime(){
        return startTime;
    }

    public  void setEndTime(Time endTime){
        this.endTime = endTime;
    }

    public Time getEndTime(){
        return endTime;
    }

    public void setDayOfWeek(String dayOfWeek){
        this.dayOfWeek = dayOfWeek;
    }

    public String getDayOfWeek(){
        return dayOfWeek;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeParcelable(startTime, 1);
        parcel.writeParcelable(endTime, 1);
        parcel.writeString(dayOfWeek);
    }

    public static final Creator<TimeProfiles> CREATOR = new Creator<TimeProfiles>() {

        public TimeProfiles createFromParcel(Parcel in) {

            return new TimeProfiles(in);
        }

        public TimeProfiles[] newArray(int size) {
            return new TimeProfiles[size];
        }
    };

    // constructor for reading data from Parcel
    public TimeProfiles(Parcel parcel) {
        id = parcel.readLong();
        startTime = parcel.readParcelable(Time.class.getClassLoader());
        endTime = parcel.readParcelable(Time.class.getClassLoader());
        dayOfWeek= parcel.readString();

    }

    public TimeProfiles(){

    }
}
