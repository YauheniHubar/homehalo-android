package android.itransition.com.wedge.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Created by y.drobysh on 10.01.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrafficEntity {
    @JsonProperty
    private long traffic;

    private CreateTimeUser start;
    private CreateTimeUser end;

    @JsonProperty("period")
    void setStartEnd(Map<String, Object> values) {
        try {
            Map<String, String> start = (Map<String, String>) values.get("start");
            Map<String, String> end = (Map<String, String>) values.get("end");
            if (start != null) {
                this.start = new CreateTimeUser();
                this.start.setDateTime(start.get("dateTime"));
                this.start.setTimeZone(start.get("timezone"));
            }
            if (end != null) {
                this.end = new CreateTimeUser();
                this.end.setDateTime(end.get("dateTime"));
                this.end.setTimeZone(end.get("timezone"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getTraffic() {
        return traffic;
    }

    public CreateTimeUser getStart() {
        return start;
    }

    public CreateTimeUser getEnd() {
        return end;
    }

}
