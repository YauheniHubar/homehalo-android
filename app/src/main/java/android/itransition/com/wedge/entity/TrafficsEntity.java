package android.itransition.com.wedge.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by i.grechishchev on 18.08.2015.
 * itransition 2015
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrafficsEntity {
    @JsonProperty
    private String deviceOwner;

    @JsonProperty
    private long deviceOwnerId;

    @JsonProperty
    public List<TrafficEntity> traffic;

    public String getDeviceOwner() {
        return deviceOwner;
    }

    public void setDeviceOwner(String deviceOwner) {
        this.deviceOwner = deviceOwner;
    }

    public long getDeviceOwnerId() {
        return deviceOwnerId;
    }

    public void setDeviceOwnerId(long deviceOwnerId) {
        this.deviceOwnerId = deviceOwnerId;
    }

    public List<TrafficEntity> getTraffic() {
        return traffic;
    }

    public void setTraffic(List<TrafficEntity> traffic) {
        this.traffic = traffic;
    }
}
