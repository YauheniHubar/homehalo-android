package android.itransition.com.wedge.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by y.drobysh on 23.12.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlListEntity implements Parcelable {

    @JsonProperty
    private long id;
    @JsonProperty
    private long deviceOwner;
    @JsonProperty
    private String url;
    @JsonProperty
    private String zveloRating;
    @JsonProperty
    private String status;
    @JsonProperty
    private CreateTimeUser createdAt;

    private boolean isBlack;

    //field for list adapter
    public transient boolean inEditMode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDeviceOwner() {
        return deviceOwner;
    }


    public void setDeviceOwner(long deviceOwner) {
        this.deviceOwner = deviceOwner;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public CreateTimeUser getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreateTimeUser createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isBlack() {
        return isBlack;
    }

    public void setBlack(boolean isBlack) {
        this.isBlack = isBlack;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZveloRating() {
        return zveloRating;
    }

    public void setZveloRating(String zveloRating) {
        this.zveloRating = zveloRating;
    }


    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeLong(deviceOwner);
        parcel.writeString(url);
        parcel.writeString(zveloRating);
        parcel.writeString(status);
        parcel.writeByte((byte) (isBlack ? 1 : 0));
        parcel.writeParcelable(createdAt, 1);
    }

    public static final Creator<UrlListEntity> CREATOR = new Creator<UrlListEntity>() {

        public UrlListEntity createFromParcel(Parcel in) {

            return new UrlListEntity(in);
        }

        public UrlListEntity[] newArray(int size) {
            return new UrlListEntity[size];
        }
    };

    // constructor for reading data from Parcel
    public UrlListEntity(Parcel parcel) {
        id= parcel.readLong();
        deviceOwner= parcel.readLong();
        url= parcel.readString();
        zveloRating= parcel.readString();
        status= parcel.readString();
        isBlack = parcel.readByte() == 1;
        createdAt = parcel.readParcelable(CreateTimeUser.class.getClassLoader());
    }

    public UrlListEntity(){

    }
}
