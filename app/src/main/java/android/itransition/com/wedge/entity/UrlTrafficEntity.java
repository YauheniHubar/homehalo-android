package android.itransition.com.wedge.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by i.grechishchev on 30.07.2015.
 * itransition 2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlTrafficEntity {
    @JsonProperty
    private String url;
    @JsonProperty
    private long traffic;
    @JsonProperty
    private ContentCategories[] categories;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getTraffic() {
        return traffic;
    }

    public void setTraffic(long traffic) {
        this.traffic = traffic;
    }

    public ContentCategories[] getCategories() {
        return categories;
    }

    public void setCategories(ContentCategories[] categories) {
        this.categories = categories;
    }
}
