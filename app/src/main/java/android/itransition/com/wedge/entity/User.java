package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 01.09.2014.
 */
public class User extends BaseModel implements Parcelable {

    @JsonProperty("hasFullAccess")
    private boolean hasFullAccess;
    @JsonProperty("id")
    private long id;
    @JsonProperty("username")
    private String userName;
    @JsonProperty("createdAt")
    private CreateTimeUser createAt;
    @JsonProperty("settings")
    private UserSettings settings;
    @JsonProperty("wedge")
    private WedgeEntity wedge;
    @JsonProperty("subscription")
    private SubscriptionEntity subscription;
    @JsonProperty
    private String email;
    @JsonProperty("emailStepSkipButtonFlag")
    private int emailStepSkipButtonFlag;


    public void setWedge(WedgeEntity wedge){
        this.wedge = wedge;
    }

    public WedgeEntity getWedge(){
        return wedge;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getEmail(){
        return email;
    }

    public void setUser(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public void setUserName(String userName){
        this.userName = userName;
    }

    public String getUserName(){
        return userName;
    }

    public CreateTimeUser getCreateAt(){
        return createAt;
    }

    public void setCreateAt(CreateTimeUser createAt){
        this.createAt = createAt;
    }

    public UserSettings getSettings(){
        return settings;
    }

    public void setSettings(UserSettings settings){
        this.settings = settings;
    }

    public SubscriptionEntity getSubscription() {
        return subscription;
    }

    public void setSubscription(SubscriptionEntity subscription) {
        this.subscription = subscription;
    }
/*
    public boolean isEmailStepSkipButtonFlag() {
        return (emailStepSkipButtonFlag == 1);
    }

    public void setEmailStepSkipButtonFlag(int emailStepSkipButtonFlag) {
        this.emailStepSkipButtonFlag = emailStepSkipButtonFlag;
    }*/
    public int isEmailStepSkipButtonFlag() {
    return emailStepSkipButtonFlag;
}

    public void setEmailStepSkipButtonFlag(int emailStepSkipButtonFlag) {
        this.emailStepSkipButtonFlag = emailStepSkipButtonFlag;
    }


    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userName);
        parcel.writeLong(id);
        parcel.writeParcelable(createAt, 1);
        parcel.writeParcelable(settings, 1);
        parcel.writeParcelable(wedge, 1);
        parcel.writeString(email);
        parcel.writeInt(emailStepSkipButtonFlag);

    }

    public static final Creator<User> CREATOR = new Creator<User>() {

        public User createFromParcel(Parcel in) {

            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    // constructor for reading data from Parcel
    public User(Parcel parcel) {
        id = parcel.readLong();
        email = parcel.readString();
        userName = parcel.readString();
        wedge = parcel.readParcelable(WedgeEntity.class.getClassLoader());
        createAt = parcel.readParcelable(CreateTimeUser.class.getClassLoader());
        settings = parcel.readParcelable(UserSettings.class.getClassLoader());
        emailStepSkipButtonFlag = parcel.readInt();

    }

    public User(){

    }


    public boolean isHasFullAccess() {
        return hasFullAccess;
    }

    public void setHasFullAccess(boolean hasFullAccess) {
        this.hasFullAccess = hasFullAccess;
    }
}

