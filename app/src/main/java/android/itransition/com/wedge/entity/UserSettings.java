package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class UserSettings extends BaseModel implements Parcelable {

    private boolean email;
    private boolean sms;
    private  boolean push;


    public void setEmail(boolean email){
        this.email = email;
    }

    public boolean getEmail(){
        return email;
    }

    public void setSms(boolean sms){
        this.sms= sms;
    }

    public boolean getSms(){
        return sms;
    }

    public void setPush(boolean push){
        this.push = push;
    }

    public boolean getPush(){
        return push;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (email ? 1 : 0));
        parcel.writeByte((byte) (sms ? 1 : 0));
        parcel.writeByte((byte) (push ? 1 : 0));
    }

    public static final Creator<UserSettings> CREATOR = new Creator<UserSettings>() {

        public UserSettings createFromParcel(Parcel in) {

            return new UserSettings(in);
        }

        public UserSettings[] newArray(int size) {
            return new UserSettings[size];
        }
    };

    // constructor for reading data from Parcel
    public UserSettings(Parcel parcel) {
        sms = parcel.readByte() == 1;
        push = parcel.readByte() == 1;
        email = parcel.readByte() == 1;

    }

    public UserSettings(){

    }


}
