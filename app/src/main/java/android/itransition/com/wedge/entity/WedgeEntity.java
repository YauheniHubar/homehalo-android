package android.itransition.com.wedge.entity;

import android.itransition.com.wedge.model.BaseModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 23.10.2014.
 */
public class WedgeEntity extends BaseModel implements Parcelable {
    @JsonProperty("id")
    private long id;

    @JsonProperty("isOnline")
    private boolean isOnline;

    @JsonProperty("updateStatus")
    private String updateStatus;
    @JsonProperty("ssid")
    private String ssid;
    @JsonProperty("password")
    private String passkey;
    @JsonProperty("firmware")
    private String firmware;
    @JsonProperty("lastOnlineAt")
    private CreateTimeUser lastOnlineAt;
    @JsonProperty("hardwareVersion")
    private String hardwareVersion;
    @JsonProperty("timezone")
    private String timezone;

    public void setId(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public void setLastOnlineAt(CreateTimeUser lastOnlineAt){
        this.lastOnlineAt = lastOnlineAt;
    }

    public CreateTimeUser getLastOnlineAt(){
        return lastOnlineAt;
    }

    public void setHardwareVersion(String hardwareVersion){
        this.hardwareVersion = hardwareVersion;
    }


    public String getHardwareVersion(){
        return hardwareVersion;
    }

    public void setUpdateStatus(String updateStatus){
        this.updateStatus = updateStatus;
    }

    public String getUpdateStatus(){
        return updateStatus;
    }

    public void setSsid(String ssid){
        this.ssid = ssid;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public String getSsid(){
        return ssid;
    }

    public void setPasskey(String passkey){
        this.passkey = passkey;
    }

    public String getPasskey(){
        return passkey;
    }

    public void setFirmware(String firmware){
        this.firmware = firmware;
    }

    public String getFirmware(){
        return firmware;
    }

    public String getTimezone() {
        return timezone;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ssid);
        parcel.writeString(passkey);
        parcel.writeString(firmware);
        parcel.writeString(updateStatus);
        parcel.writeString(hardwareVersion);
        parcel.writeString(timezone);
        parcel.writeParcelable(lastOnlineAt, 1);
        parcel.writeLong(id);

    }

    public static final Creator<WedgeEntity> CREATOR = new Creator<WedgeEntity>() {

        public WedgeEntity createFromParcel(Parcel in) {

            return new WedgeEntity(in);
        }

        public WedgeEntity[] newArray(int size) {
            return new WedgeEntity[size];
        }
    };

    // constructor for reading data from Parcel
    public WedgeEntity(Parcel parcel) {
        ssid = parcel.readString();
        passkey = parcel.readString();
        firmware = parcel.readString();
        updateStatus = parcel.readString();
        hardwareVersion = parcel.readString();
        timezone = parcel.readString();
        id = parcel.readLong();
        lastOnlineAt = parcel.readParcelable(CreateTimeUser.class.getClassLoader());

    }

    public WedgeEntity(){

    }

}
