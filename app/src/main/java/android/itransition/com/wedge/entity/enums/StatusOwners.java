package android.itransition.com.wedge.entity.enums;

/**
 * Created by e.kazimirova on 09.09.2014.
 */
public  enum StatusOwners {
    RESUME("resume"),
    SUSPEND("suspend");

    private String stringValue;

    private StatusOwners(String toString) {
        stringValue = toString;

    }

    public String getStringValue(){
        return  stringValue;
    }
    @Override
    public String toString() {
        return stringValue;
    }
}