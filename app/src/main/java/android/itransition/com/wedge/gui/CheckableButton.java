package android.itransition.com.wedge.gui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.Checkable;


/**
 * Created by i.grechishchev on 07.07.2015.
 * itransition 2015
 */
public class CheckableButton extends Button implements Checkable {
    private long timestamp;

    public CheckableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckableButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean isChecked() {
        return isSelected();
    }

    @Override
    public void setChecked(boolean checked) {
        setSelected(checked);
    }

    @Override
    public void toggle() {
        setChecked(!isChecked());
    }

    public void toggle(long timestamp){
        if (this.timestamp != timestamp){
            setChecked(!isChecked());
            this.timestamp = timestamp;
        }
    }

//    @Override
//    public boolean performClick() {
//        toggle();
//        return super.performClick();
//    }


}
