/*
 * 
 * Copyright 2013 Matt Joseph
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * 
 * This custom view/widget was inspired and guided by:
 * 
 * HoloCircleSeekBar - Copyright 2012 Jes�s Manzano
 * HoloColorPicker - Copyright 2012 Lars Werkman (Designed by Marie Schweiz)
 * 
 * Although I did not used the code from either project directly, they were both used as 
 * reference material, and as a result, were extremely helpful.
 */

package android.itransition.com.wedge.gui;

import android.content.Context;
import android.util.AttributeSet;

public class CircularSeekBarHomeWork extends CircularSeekBarNew {

    final int MAX_HOURS = 3;
    final int MAX_MIN = MAX_HOURS * 60;

    public CircularSeekBarHomeWork(Context context) {
        super(context);
    }

    public CircularSeekBarHomeWork(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircularSeekBarHomeWork(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public int getProgressInMins() {
        if(mProgressDegrees >= green3End){
            return MAX_MIN;
        }
        if (mProgressDegrees <= green1Start) {
            return 0;
        }
        int result = 0;
        if (mProgressDegrees < green1End) {
            result = Math.round(((mProgressDegrees - green1Start - OFFSET) / (circle_width_green - 2 * OFFSET)) * 60);
//            result -= 3;
        } else
        if ((mProgressDegrees >= green2Start) && (mProgressDegrees <= green2End)) {
            result = Math.round((mProgressDegrees - green2Start) / circle_width_green * 6);
            result = result * 10 + 60;
        } else
        if (mProgressDegrees >= green3Start) {
            result = Math.round((mProgressDegrees - green3Start) / circle_width_green * 6);
            result = result * 10 + 120;
        }
        return result;
    }

    @Override
    public void setProgressInMins(int mins) {
        if (mins < 0) mins = 0;
        if (mins > MAX_MIN) mins = MAX_MIN;


        if (mins <= 60) {
            float archWidthForPointer = circle_width_green - 2 * OFFSET;
            mProgressDegrees = (archWidthForPointer / 60) * mins + green1Start + OFFSET;
        } else if (mins > 60 && mins <= 120) {
            mProgressDegrees = (circle_width_green / 60) * (mins - 60) + green2Start;
        } else {
            int hours = (mins / 60) - 2;
            mProgressDegrees = (circle_width_green / 10) * hours + green3Start;
        }
        recalculateAll();
        invalidate();
    }
}
