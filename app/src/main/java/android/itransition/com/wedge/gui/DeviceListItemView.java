package android.itransition.com.wedge.gui;

import android.app.Activity;
import android.content.Context;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.utils.DeviceOwnerManager;
import android.itransition.com.wedge.utils.Utils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by y.drobysh on 23.02.2015.
 */
public class DeviceListItemView extends LinearLayout implements View.OnClickListener, Animation.AnimationListener {

    private final DeviceMenuOptionClickListener listener;

    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public interface DeviceMenuOptionClickListener {
        void onSwipeButtonClick(DeviceListItemView view);
        void onForgetClick(DeviceListItemView view);
        void onAssignClick(DeviceListItemView view);
        void onEditOkClick(DeviceListItemView view, String newTitle);
        void onInfoExpand(DeviceListItemView view);
    }

    ImageView btnSwipe, ivStatus;
    Button btnOk, btnCancel, btnAssign, btnForget, btnEdit;

    TextView tvName, tvInfoTop, tvInfoExpanded, tvInfoBottom;
    EditText etName;

    View paneInfo, paneEdit, paneMenu;
    FrameLayout itemDevice;
    Context mContext;
    private final int displayWidth;

    boolean isMenuShown;
    private final int btnSwipeWidth;
    private boolean isExpanded;

    public DeviceListItemView(Context context, DeviceMenuOptionClickListener listener) {
        super(context);
        this.listener = listener;
        mContext = context;


        displayWidth = Utils.getDisplayWidth((Activity) context);
        btnSwipeWidth = Utils.dp2px(60, context);

        onCreateView();
    }

    private void onCreateView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.list_item_device, this);

        view.setLayoutParams(new AbsListView.LayoutParams(displayWidth, ViewGroup.LayoutParams.MATCH_PARENT));

        itemDevice = (FrameLayout)view.findViewById(R.id.device_item);

        paneInfo = view.findViewById(R.id.paneInfo);
        paneInfo.getLayoutParams().width = displayWidth - btnSwipeWidth;

        paneEdit = view.findViewById(R.id.paneEdit);
        paneEdit.getLayoutParams().width = displayWidth;

        paneMenu = view.findViewById(R.id.paneMenu);
        paneMenu.getLayoutParams().width = displayWidth - btnSwipeWidth;

        btnSwipe = (ImageView) view.findViewById(R.id.btnSwipe);
        btnSwipe.setOnClickListener(this);
        btnSwipe.getLayoutParams().width = btnSwipeWidth;


        tvName = (TextView) paneInfo.findViewById(R.id.tvName);
        tvInfoTop = (TextView) paneInfo.findViewById(R.id.tvInfoTop);
        tvInfoExpanded = (TextView) paneInfo.findViewById(R.id.tvInfoExpanded);
        tvInfoBottom = (TextView) paneInfo.findViewById(R.id.tvInfoBottom);
        ivStatus = (ImageView) paneInfo.findViewById(R.id.ivDeviceStatus);

        btnAssign = (Button) paneMenu.findViewById(R.id.btnAssign);
        btnEdit = (Button) paneMenu.findViewById(R.id.btnEdit);
        btnForget = (Button) paneMenu.findViewById(R.id.btnForget);

        btnOk = (Button) paneEdit.findViewById(R.id.btnOk);
        btnCancel = (Button) paneEdit.findViewById(R.id.btnCancel);
        etName = (EditText) paneEdit.findViewById(R.id.editTextName);


        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnAssign.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnForget.setOnClickListener(this);
        paneInfo.setOnClickListener(this);
    }

    public void setDeviceName(String  name) {
        tvName.setText(name);
    }

    public void setInfoTop(CharSequence info) { tvInfoTop.setText(info); }
    public void setInfoBottom(CharSequence info) { tvInfoBottom.setText(info); }
    public void setInfoExpanded(CharSequence info) { tvInfoExpanded.setText(info); }

    public void setShowInfo(boolean needToShow) {
        tvInfoTop.setVisibility(needToShow ? VISIBLE : GONE    );
    }

    public void setOnline(boolean value) {
        ivStatus.setImageResource(value ? R.drawable.ic_device_online : R.drawable.ic_device_offline);
    }

    public void setAccessIconByOwnerState(DeviceOwnerManager.OwnerAccessState state) {
        switch (state) {

            case HasAccess:
                ivStatus.setImageResource(R.drawable.ic_owner_has_access);
                tvInfoTop.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
            case NoAccess:
                ivStatus.setImageResource(R.drawable.ic_owner_no_access);
                tvInfoTop.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
            case Blocked:
                ivStatus.setImageResource(R.drawable.ic_owner_partial_access);
                tvInfoTop.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_blocked, 0, 0, 0);
                break;
            case HomeWork:
                ivStatus.setImageResource(R.drawable.ic_owner_partial_access);
                tvInfoTop.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_hw, 0, 0, 0);
                break;
            case Extended:
                ivStatus.setImageResource(R.drawable.ic_owner_partial_access);
                tvInfoTop.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_extended, 0, 0, 0);
                break;
            case Online:
                ivStatus.setImageResource(R.drawable.ic_owner_has_access);
                break;
            case Offline:
                ivStatus.setImageResource(R.drawable.ic_owner_no_access);
                break;
            default:
                throw new RuntimeException("you must implement all owner access state cases");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSwipe:
                onSwipeBtnClick();
                listener.onSwipeButtonClick(this);

                break;
            case R.id.btnEdit:
                onEditBtnClick();
                break;
            case R.id.btnCancel:
                hideEditPane();
                Utils.hideSoftKeyboard(getContext(), this);
                break;
            case R.id.btnOk:
                onEditOk();
                break;
            case R.id.btnAssign:
                listener.onAssignClick(this);
                break;
            case R.id.btnForget:
                listener.onForgetClick(this);
                break;
            case R.id.paneInfo:
                listener.onInfoExpand(this);
                if (isExpanded)
                collapseItem();
                else
                expandItem();
        }
    }

    private void hideEditPane() {
        setMenuVisible(true);
        paneEdit.setVisibility(GONE);
        paneInfo.setVisibility(VISIBLE);
        btnSwipe.setVisibility(VISIBLE);
    }

    private void onEditOk() {
        String newTitle = etName.getText().toString().trim();
        listener.onEditOkClick(this, newTitle);
//        hideEditPane();
    }

    private void onEditBtnClick() {
        setMenuVisible(false);
        paneEdit.setVisibility(VISIBLE);
        paneInfo.setVisibility(GONE);
        btnSwipe.setVisibility(GONE);

        etName.setText(tvName.getText());
        etName.setSelection(etName.length());
        etName.requestFocus();
        Utils.showKeyboard(getContext(), etName);
    }

    private void onSwipeBtnClick() {
        if (isMenuShown) {
            hideMenu();
        } else {
            showMenu();
        }
    }

    public void setMenuVisible(boolean isVisible) {


        btnSwipe.setX(isVisible ? 0  : displayWidth - btnSwipeWidth);
        btnSwipe.setImageResource(isVisible ? R.drawable.arrow_right : R.drawable.arrow_left);


        paneMenu.setX(isVisible ? btnSwipeWidth : displayWidth);

        paneEdit.setX(0);

        paneInfo.setX(isVisible ? -paneInfo.getWidth() : 0);

        paneInfo.setVisibility(VISIBLE);
        btnSwipe.setVisibility(VISIBLE);
        paneEdit.setVisibility(GONE);

        isMenuShown = isVisible;

        paneEdit.requestLayout();
        paneMenu.requestLayout();
        btnSwipe.requestLayout();

        requestLayout();
        invalidate();
    }

    public void setExpanded(boolean isExpanded) {
        if (isExpanded) {
            expandItem();
        } else {
            collapseItem();
        }
    }

    public void expandItem() {
        itemDevice.setLayoutParams(new LinearLayout.LayoutParams(displayWidth, Utils.dp2px(150, mContext)));
        tvInfoExpanded.setVisibility(VISIBLE);
        isExpanded = true;
    }

    public void collapseItem() {
        itemDevice.setLayoutParams(new LinearLayout.LayoutParams(displayWidth, Utils.dp2px(100, mContext)));
        tvInfoExpanded.setVisibility(GONE);
        isExpanded = false;
    }

    public void showMenu() {
        moveByX(paneInfo, -paneInfo.getWidth());
        moveByX(btnSwipe, 0);
        moveByX(paneMenu, btnSwipeWidth);
        btnSwipe.setImageResource(R.drawable.arrow_right);

        isMenuShown = true;
    }

    public void hideMenu() {
        if (paneInfo.getWidth() != 0) {
            moveByX(paneInfo, 0);
            moveByX(btnSwipe, displayWidth - btnSwipeWidth);
            moveByX(paneMenu, displayWidth);
            btnSwipe.setImageResource(R.drawable.arrow_left);

            isMenuShown = false;
        }
    }


    private void moveByX(View v, int y) {
        MoveToXAnimation anim = new MoveToXAnimation(v, y);
        anim.setDuration(400);
        v.startAnimation(anim);
        anim.setAnimationListener(this);
    }

    @Override public void onAnimationStart(Animation animation) {
        btnSwipe.setEnabled(false);
    }

    @Override public void onAnimationEnd(Animation animation) {
        btnSwipe.setEnabled(true);
    }

    @Override public void onAnimationRepeat(Animation animation) { }


    private static class MoveToXAnimation extends Animation {
        private final View view;
        private final float startX, y;

        public MoveToXAnimation(View view, float y) {
            this.view = view;
            startX = view.getX();
            this.y = y - startX;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float newPos = startX + (y * interpolatedTime);
            view.setX(newPos);
            view.requestLayout();
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }
    }
}
