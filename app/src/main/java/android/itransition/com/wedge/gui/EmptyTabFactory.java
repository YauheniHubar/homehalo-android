package android.itransition.com.wedge.gui;

import android.content.Context;
import android.view.View;
import android.widget.TabHost;

/**
 * Created by y.drobysh on 31.12.2014.
 */
public class EmptyTabFactory implements TabHost.TabContentFactory {

    private Context ctx;

    public EmptyTabFactory(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public View createTabContent(String tag) {
        return new View(ctx);
    }

}
