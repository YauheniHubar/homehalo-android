package android.itransition.com.wedge.gui;

import android.content.Context;
import android.content.res.TypedArray;
import android.itransition.com.wedge.R;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by i.grechishchev on 09.07.2015.
 * itransition 2015
 */
public class ItemDayView extends LinearLayout {
    private final int COUNT_OF_VIEWS = 12;
    private final int COUNT_OF_HOURS = 2;

    private TextView tvTitle;
    private ItemHourView[] items = new ItemHourView[COUNT_OF_VIEWS];
    private String dayEng;

    public ItemDayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.item_day_profile, this);

        tvTitle = (TextView) findViewById(R.id.tvTitle);

        items[0] =(ItemHourView) findViewById(R.id.view1);
        items[1] =(ItemHourView) findViewById(R.id.view2);
        items[2] =(ItemHourView) findViewById(R.id.view3);
        items[3] =(ItemHourView) findViewById(R.id.view4);
        items[4] =(ItemHourView) findViewById(R.id.view5);
        items[5] =(ItemHourView) findViewById(R.id.view6);
        items[6] =(ItemHourView) findViewById(R.id.view7);
        items[7] =(ItemHourView) findViewById(R.id.view8);
        items[8] =(ItemHourView) findViewById(R.id.view9);
        items[9] =(ItemHourView) findViewById(R.id.view10);
        items[10] =(ItemHourView) findViewById(R.id.view11);
        items[11] =(ItemHourView) findViewById(R.id.view12);

        for (int i = 0; i < COUNT_OF_VIEWS; i++) {
            items[i].setTag(COUNT_OF_HOURS * i);
        }

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SmallHourView, 0, 0);
        CharSequence text = a.getText(R.styleable.SmallHourView_android_text);
        tvTitle.setText(text);
        a.recycle();
    }

    public String getText(){
        return tvTitle.getText().toString();
    }

    public void setDayEng(String dayEng) {
        this.dayEng = dayEng;
    }

    public String getDayEng(){
        return dayEng;
    }

    public View getQuarterByTime(int minutes){
        int hours = minutes / 60;
        int hourViewIndex = hours / COUNT_OF_HOURS;
        int minutesTag = minutes % (COUNT_OF_HOURS * 60);
        return items[hourViewIndex].getQuarterByTag(minutesTag);
    }

    public void resetViews(){
        for (int i = 0; i < items.length; i++) {
            items[i].resetViews();
        }
    }
}
