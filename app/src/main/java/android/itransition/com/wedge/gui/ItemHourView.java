package android.itransition.com.wedge.gui;

import android.content.Context;
import android.itransition.com.wedge.R;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by p.gulevich on 14.07.2015.
 */
public class ItemHourView extends LinearLayout {
    private final int VIEW_COUNT = 8;
    private final int QUARTER_MINUTES = 15;

    private View[] quarters = new View[VIEW_COUNT];

    public ItemHourView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.item_hour_profile_view, this);

        quarters[0] = findViewById(R.id.view00);
        quarters[1] = findViewById(R.id.view15);
        quarters[2] = findViewById(R.id.view30);
        quarters[3] = findViewById(R.id.view45);
        quarters[4] = findViewById(R.id.view60);
        quarters[5] = findViewById(R.id.view75);
        quarters[6] = findViewById(R.id.view90);
        quarters[7] = findViewById(R.id.view105);

        for (int i = 0; i < VIEW_COUNT; i++) {
            quarters[i].setTag(String.valueOf(i * QUARTER_MINUTES));
        }
    }

    public View getQuarterByTag(int tag){
        int index = tag / QUARTER_MINUTES;
        return quarters[index];
    }

    public void resetViews(){
        for (int i = 0; i < quarters.length ; i++) {
            quarters[i].setBackgroundColor(getResources().getColor(R.color.red));
        }
    }
}
