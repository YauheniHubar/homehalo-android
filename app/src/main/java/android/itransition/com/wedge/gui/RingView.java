package android.itransition.com.wedge.gui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by e.kazimirova on 23.10.2014.
 */
public class RingView extends View {


    private static final float STROKE_WIDTH = 7;
    private RectF oval;

    public RingView(Context context) {
        super(context);
        init();
    }
    public RingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public RingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public void setColor(int color){
        myPaint.setColor(color);
    }
    Paint myPaint;

    private void init() {
        myPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        myPaint.setStyle(Paint.Style.STROKE);
        myPaint.setStrokeWidth(STROKE_WIDTH);


    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        oval = new RectF(0 + STROKE_WIDTH, 0 + STROKE_WIDTH, w - STROKE_WIDTH, h - STROKE_WIDTH);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawArc(oval, 0, 360, false, myPaint);
    }



}