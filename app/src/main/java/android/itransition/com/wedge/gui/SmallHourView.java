package android.itransition.com.wedge.gui;

import android.content.Context;
import android.content.res.TypedArray;
import android.itransition.com.wedge.R;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Created by i.grechishchev on 02.07.2015.
 * itransition 2015
 */
public class SmallHourView extends LinearLayout implements CompoundButton.OnCheckedChangeListener, View.OnClickListener{
    private static final float RATIO = 4f / 3f;
    private View view;
    private TextView tvTitle;
    private CheckableButton btn00, btn15, btn30, btn45;
    private String a;
    public SmallHourView(Context context) {
        super(context);
    }

    public SmallHourView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.simple_hour, this);
        tvTitle = (TextView)findViewById(R.id.tvTitle);
        btn00 = (CheckableButton)findViewById(R.id.view00);
        btn15 = (CheckableButton)findViewById(R.id.view15);
        btn30 = (CheckableButton)findViewById(R.id.view30);
        btn45 = (CheckableButton)findViewById(R.id.view45);
        btn00.setTag(R.id.TAG_MINUTES, "00"); // tag with minutes
        btn15.setTag(R.id.TAG_MINUTES, "15");
        btn30.setTag(R.id.TAG_MINUTES, "30");
        btn45.setTag(R.id.TAG_MINUTES, "45");
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SmallHourView, 0, 0);
        int color = a.getColor(R.styleable.SmallHourView_android_textColor, 0);
        int rawSize = a.getDimensionPixelSize(R.styleable.SmallHourView_android_textSize, 0);
        CharSequence text = a.getText(R.styleable.SmallHourView_android_text);
        tvTitle.setText(text);
        a.recycle();
    }

    public void setText(CharSequence text) {
        tvTitle.setText(text);
    }

    public void pressButton() {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d("","");
    }

    @Override
    public void onClick(View v) {
        Log.d("","");
    }

    public void setDefaultState(boolean checked){
        btn00.setChecked(checked);
        btn15.setChecked(checked);
        btn30.setChecked(checked);
        btn45.setChecked(checked);
    }
}
