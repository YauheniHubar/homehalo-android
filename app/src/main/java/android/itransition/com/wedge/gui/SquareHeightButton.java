package android.itransition.com.wedge.gui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by y.drobysh on 17.12.2014.
 */
public class SquareHeightButton extends Button {


    public SquareHeightButton(Context context) {
        super(context);
    }

    public SquareHeightButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareHeightButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SquareHeightButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int min = widthMeasureSpec > 0 && widthMeasureSpec < heightMeasureSpec ?
                widthMeasureSpec : heightMeasureSpec;
        super.onMeasure(min, min);
    }
}
