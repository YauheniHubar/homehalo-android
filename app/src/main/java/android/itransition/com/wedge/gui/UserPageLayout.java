package android.itransition.com.wedge.gui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Created by y.drobysh on 16.01.2015.
 */
public class UserPageLayout extends ViewGroup {

    public UserPageLayout(Context context) {
        super(context);
        init();
    }

    public UserPageLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserPageLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

    }




}
