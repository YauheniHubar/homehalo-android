package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.AlertsActivity;
import android.itransition.com.wedge.entity.DataInformationDeviceOwner;
import android.itransition.com.wedge.entity.NotificationsEntity;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by e.kazimirova on 29.09.2014.
 */
public class AlertsAdapter extends BaseAdapter {


    private Context mContext;
    private ArrayList<NotificationsEntity> mList;
    private final StringBuilder sb;

    public AlertsAdapter(Context c, NotificationsEntity[] list) {

        mContext = c;
        mList = new ArrayList<>(Arrays.asList(list));
        sb = new StringBuilder();
    }

    public void clear() {
        mList.clear();
    }

    public int getCount() {
        return mList.size();
    }


    public NotificationsEntity getItem(int position) {
        return mList.get(position);
    }


    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        final MainHolder holder;
        View view;
        if (convertView == null) {
            // Make up a new view
            LayoutInflater inflater = (LayoutInflater) ((AlertsActivity) mContext)
                    .getLayoutInflater();
            view = inflater.inflate(R.layout.list_item_alerts, parent, false);
            holder = new MainHolder();
            holder.textViewTimeAgo = (TextView) view.findViewById(R.id.textViewTimeAgo);
            holder.textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);

            holder.imageView = (ImageView) view.findViewById(R.id.imageView);

            view.setTag(holder);
        } else {
            // Use convertView if it is available
            view = convertView;
            holder = (MainHolder) convertView.getTag();
        }



        final NotificationsEntity item = getItem(position);
        String text = item.getTitle();
//        if (text.contains("\"")) {
//            text = text.replaceFirst("\"", "<i>");
//            text = text.replaceFirst("\"", "</i>");
//        }
        DataInformationDeviceOwner deviceOwner = item.getData().getDataInformationDeviceOwner();
        if (deviceOwner.getTitle() != null &&
                text.contains(deviceOwner.getTitle())) {
            text = text.replace(deviceOwner.getTitle(),
                    "<b>" + deviceOwner.getTitle() +
                            "</b>");
        }
//        if (item.getData().getUrl() != null && text.contains(item.getData().getUrl())) {
//            text = text.replace(item.getData().getUrl(),
//                    "<a href=\"" + item.getData().getUrl() + "\">" +
//                            item.getData().getUrl() +
//                            "</a>");
//        }

        holder.textViewTitle.setText(Html.fromHtml(text));
//        holder.textViewTitle.setMovementMethod(new LinkMovementMethod());
        String textAgo = "";
        Date parsedDate = new Date();
        PrettyTime p = new PrettyTime();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {

            parsedDate = formatter.parse(item.getCreatedAt().getDateTime());

            Date current = parsedDate;
            Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(
                    TimeZone.getDefault().getID()
            ));

            cal1.setTime(current);
            Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone((item.
                    getCreatedAt().getTimeZone())));
            cal2.clear();
            cal2.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
            cal2.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
            cal2.set(Calendar.DATE, cal1.get(Calendar.DATE));
            cal2.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
            cal2.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
            current = cal2.getTime();
            textAgo = p.format(current);
            if (textAgo.toLowerCase().trim().contains("moments")) {
                holder.textViewTimeAgo.setText(R.string.just_now);
            } else {
                holder.textViewTimeAgo.setText(p.format(current));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (item.getType().trim().toLowerCase().equals("new_device_attached")) {
            holder.imageView.setImageResource(R.drawable.devices_icon);
            view.setBackgroundResource(R.drawable.selector_alerts_device);

            sb.replace(0, sb.length(), "");

            String addText = mContext.getString(R.string.has_been_added);
            String deviceName = item.getData().getTitle();
            String mac = item.getData().getMacAddress();
            sb.append("<b>"+mContext.getString(R.string.new_device)+"</b><br/>");
            sb.append(deviceName).append("<br/>");//.append("\nMac: ");

            if (mac != null) {
                sb.append("MAC: ");
                sb.append(mac).append("\n");
            }

//            sb.append(addText);

            SpannableString spannableString = new SpannableString(sb.toString());
            spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, deviceName.length(), 0);
            spannableString.setSpan(new RelativeSizeSpan(0.8f), deviceName.length(), sb.length(), 0);


            holder.textViewTitle.setText(Html.fromHtml(sb.toString()));


        } else if (item.getType().trim().toLowerCase().equals("whitelist_request")) {
            holder.imageView.setImageResource(R.drawable.link_icon);
            view.setBackgroundResource(R.drawable.selector_alerts_whitelist);

            sb.replace(0, sb.length(), "");
            if (deviceOwner != null) {
                sb.append("<b>").append(deviceOwner.getTitle()).append("</b>\n");
            }
            sb.append(mContext.getString(R.string.please_unblock));
            String formattedUrl = item.getData().getUrl();
            if (formattedUrl.startsWith("http://")) {
                formattedUrl = formattedUrl.replace("http://", "");
            } else if (formattedUrl.startsWith("https://")) {
                formattedUrl = formattedUrl.replace("https://", "");
            }
            if (formattedUrl.startsWith("www.")) {
                formattedUrl = formattedUrl.replace("www.", "");
            }
//            sb.append(item.getData().getUrl());
            sb.append("<br/><u>"+formattedUrl+"</u>");
//            sb.append(" to be whitelisted");
            /*sb.append("white list request <b>");
            if (deviceOwner != null) {
                sb.append(deviceOwner.getTitle()).append("</b>\n");
            }
            sb.append(item.getData().getUrl());*/

            holder.textViewTitle.setText(Html.fromHtml(sb.toString()));
//            Linkify.addLinks(holder.textViewTitle, Linkify.WEB_URLS);


        } else if (item.getType().trim().toLowerCase().equals("time_extension_request")) {
            text = "<b>" + deviceOwner.getTitle() +
                    "</b>" + " <br/>"+mContext.getString(R.string.please_extend);
            holder.textViewTitle.setText(Html.fromHtml(text));
            holder.imageView.setImageResource(R.drawable.ic_time_request);
            view.setBackgroundResource(R.drawable.selector_alerts_timerequest);
            //}
        }
//        holder.textViewTitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("grch11","opa");
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(item.getData().getUrl()));
//                v.getContext().startActivity(i);
//            }
//        });

        return view;
    }

    public void removeAlert(NotificationsEntity item) {
        mList.remove(item);
    }

    protected static class MainHolder {
        TextView textViewTitle;
        TextView textViewTimeAgo;
        ImageView imageView;
    }
}
