package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.UrlListEntity;
import android.itransition.com.wedge.gui.fragments.ManualBlacklistFragment;
import android.itransition.com.wedge.utils.Utils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by y.drobysh on 22.12.2014.
 */
public class BlackListTabsAdapter extends BaseAdapter implements View.OnClickListener {

    public interface EditUrlListener {

        void onCancel();
        void onEditUrl(String s);
    }
    private final Context context;

    private final List<UrlListEntity> list;
    private EditUrlListener listener;
    public BlackListTabsAdapter(Context context, List<UrlListEntity> list, EditUrlListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public UrlListEntity getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position,  View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_black_url, parent, false);
            holder = new Holder(convertView);

            holder.ok.setOnClickListener(this);
            holder.cancel.setOnClickListener(this);
        } else {
            holder = (Holder) convertView.getTag();
        }

        UrlListEntity item = getItem(position);

        holder.layoutEdit.setVisibility(item.inEditMode ? View.VISIBLE : View.GONE);
        holder.urlLinear.setVisibility(item.inEditMode ? View.GONE : View.VISIBLE);

        holder.etUrl.setText(item.getUrl());
        holder.tvUrl.setText(item.getUrl());
        holder.etUrl.requestFocus();

        if (item.inEditMode) {
            holder.etUrl.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    holder.etUrl.requestFocus();
                    Utils.showKeyboard(context, holder.etUrl);
                }
            }, 100);
        }


        holder.btnSwipe.setTag(false);
        holder.btnSwipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(boolean)holder.btnSwipe.getTag()){
                    holder.btnSwipe.setTag(true);
                    holder.btnSwipe.setImageResource(R.drawable.url_left_arrow);
                    ManualBlacklistFragment.listViewUrls.smoothOpenMenu(position);
                } else {
                    holder.btnSwipe.setTag(false);
                    holder.btnSwipe.setImageResource(R.drawable.url_right_arrow);
                    ManualBlacklistFragment.listViewUrls.smoothCloseMenu(position);
                }

            }
        });


        if(ManualBlacklistFragment.listViewUrls.isActivated()){
            holder.btnSwipe.setImageResource(R.drawable.url_left_arrow);
        } else {
            holder.btnSwipe.setImageResource(R.drawable.url_right_arrow);
        }

        holder.tvUrl.setTag(false);
        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(!(boolean) holder.tvUrl.getTag()){
                    holder.tvUrl.setTag(true);
                    holder.btnSwipe.setImageResource(R.drawable.url_left_arrow);
                    ManualBlacklistFragment.listViewUrls.smoothOpenMenu(position);
                } else {
                    holder.tvUrl.setTag(false);
                    holder.btnSwipe.setImageResource(R.drawable.url_right_arrow);
                    ManualBlacklistFragment.listViewUrls.smoothCloseMenu(position);
                }
                return false;
            }
        });
        return convertView;
    }


    @Override
    public void onClick(View v) {
        EditText etUrl = (EditText) v.getTag();
        switch (v.getId()) {
            case R.id.btnOk:
                listener.onEditUrl(etUrl.getText().toString());
                break;
            case R.id.btnCancel:
                Utils.hideSoftKeyboard(context, etUrl);
                listener.onCancel();
                break;
        }
    }

    private static class Holder {
        TextView tvUrl;
        EditText etUrl;
        Button ok, cancel;
        ImageView btnSwipe;
        LinearLayout layoutEdit;
        LinearLayout urlLinear;

        public Holder(View convertView) {
            cancel = (Button) convertView.findViewById(R.id.btnCancel);
            ok = (Button) convertView.findViewById(R.id.btnOk);
            etUrl = (EditText) convertView.findViewById(R.id.etUrl);
            tvUrl = (TextView) convertView.findViewById(R.id.tvUrl);
            layoutEdit = (LinearLayout) convertView.findViewById(R.id.layoutEdit);
            urlLinear = (LinearLayout) convertView.findViewById(R.id.urlLinear);
            btnSwipe = (ImageView) convertView.findViewById(R.id.btnSwipe);
            convertView.setTag(this);
            ok.setTag(etUrl);
            cancel.setTag(etUrl);
        }
    }
}
