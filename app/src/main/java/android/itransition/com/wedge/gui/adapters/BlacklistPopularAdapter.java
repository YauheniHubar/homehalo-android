package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.gui.fragments.OnSwitchChangedListener;
import android.itransition.com.wedge.model.PopularBlacklist;
import android.itransition.com.wedge.model.PopularBlacklistSimpleItem;
import android.itransition.com.wedge.settings.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kyleduo.switchbutton.SwitchButton;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by i.grechishchev on 10.02.2016.
 * itransition 2016
 */
public class BlacklistPopularAdapter extends BaseAdapter {
    private Context context;
    private List<PopularBlacklist> list;
    private List<Integer> checkedIds;
    private List<Integer> blockedIds;
    private List<PopularBlacklistSimpleItem> blacklistItems;
    private ImageLoader imageLoader;
    DisplayImageOptions defaultDisplayImageOptions;
    OnSwitchChangedListener listener;

    public BlacklistPopularAdapter(Context context, List<PopularBlacklist> list, OnSwitchChangedListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        checkedIds = new ArrayList<>();
        blockedIds = new ArrayList<>();
        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        imageLoader.init(config);
        defaultDisplayImageOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).build();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public PopularBlacklist getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        final PopularBlacklist item = getItem(position);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.blacklist_custom_item, parent, false);
            holder = new Holder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.categoryName);
            holder.switchButton = (SwitchButton) convertView.findViewById(R.id.category_switch);
            holder.icon = (ImageView) convertView.findViewById(R.id.website_icon);

            holder.switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        checkedIds.remove(Integer.valueOf(item.getId()));

                    } else {
                        if (!checkedIds.contains(Integer.valueOf(item.getId()))) {
                            checkedIds.add(item.getId());
                        }
                    }
                }
            });
            holder.switchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSwitchChanged();
                    notifyDataSetChanged();
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        if (item != null) {
            holder.switchButton.setChecked(!checkedIds.contains(item.getId()));
            holder.switchButton.setEnabled(!blockedIds.contains(item.getId()));
            holder.tvTitle.setText(item.getTitle());
            imageLoader.displayImage(Settings.getUrlHost().trim() + "/" + item.getIcon(), holder.icon, defaultDisplayImageOptions);
        }
        return convertView;
    }

    public static class Holder {
        ImageView icon;
        TextView tvTitle;
        SwitchButton switchButton;
    }

    public void initSwitchedItems(List<PopularBlacklistSimpleItem> blacklistItems) {
//        checkedIds = ids;
//        notifyDataSetChanged();
//        start
// blacklistItems not implemented
        List<Integer> switchedIds = new ArrayList<>();
        for (PopularBlacklistSimpleItem item : blacklistItems) {
            switchedIds.add(item.getContentCategory());
            checkedIds = switchedIds;
            if (item.isParentBlocked()) {
                blockedIds.add(item.getContentCategory());
            }
            //end
        }
        notifyDataSetChanged();
    }

    public List<Integer> getSwitchedItems() {
        //delete duplicates
        Set<Integer> hs = new HashSet<>();
        hs.addAll(checkedIds);
        checkedIds.clear();
        checkedIds.addAll(hs);
        return checkedIds;
    }
}
