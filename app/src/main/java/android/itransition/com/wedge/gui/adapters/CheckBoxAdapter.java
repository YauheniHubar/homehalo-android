package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.ContentCategories;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by e.kazimirova on 30.09.2014.
 */
public class CheckBoxAdapter extends BaseAdapter {


    private Context mContext;
    private ContentCategories[] mList;
    private boolean[] mListActiveBoolean;


    public CheckBoxAdapter(Context c, ContentCategories[] list,
                           boolean[] listActiveBoolean) {

        mContext = c;
        mList = list;
        mListActiveBoolean = listActiveBoolean;

    }


    public ArrayList<Integer> getmListActive(){
        ArrayList<Integer> checked = new ArrayList<>();
        for (int i = 0; i < mListActiveBoolean.length; i++) {
            if(mListActiveBoolean[i])
                checked.add((int)getItem(i).getCategoryId());

        }
        return checked;
    }
    public int getCount() {
        return mList.length;
    }


    public ContentCategories getItem(int position) {
        return mList[position];
    }


    public long getItemId(int position) {

        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            // Make up a new view
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_checkbox, parent, false);
        } else {
            // Use convertView if it is available
            view = convertView;
        }
        TextView tvCategoriesName = (TextView) view.findViewById(R.id.textViewCategoriesName);
        if(position!=0 && mList[position].getCategoryName().trim().toLowerCase().
                equals(mList[position - 1].getCategoryName().trim().toLowerCase())){
            tvCategoriesName.setVisibility(View.GONE);
        } else {
            tvCategoriesName.setVisibility(View.VISIBLE);
            tvCategoriesName.setText(mList[position].getCategoryName());
        }

        CheckBox check = (CheckBox) view.findViewById(R.id.checkBoxCategories);
        check.setText(mList[position].getTitle());

        check.setTag(Integer.valueOf(position));
        check.setChecked(mListActiveBoolean[position]);
        check.setOnCheckedChangeListener(mListener);


        return view;
    }

    CompoundButton.OnCheckedChangeListener mListener = new CompoundButton.OnCheckedChangeListener() {

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Integer position = (Integer) buttonView.getTag();
            mListActiveBoolean[position] = isChecked;
        }
    };


}
