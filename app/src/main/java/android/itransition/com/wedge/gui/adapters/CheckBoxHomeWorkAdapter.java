package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.ContentCategories;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 30.09.2014.
 */
public class CheckBoxHomeWorkAdapter extends BaseAdapter {


    private Context mContext;
    private ArrayList<Integer> mListActive;
    private HomeWorkCategories[] mList;
    private boolean[] mListActiveBoolean;


    public CheckBoxHomeWorkAdapter(Context c, HomeWorkCategories[] list,
                                   boolean[] listActiveBoolean,
                                   ArrayList<Integer> listActive) {

        mContext = c;
        mList = list;
        mListActive = listActive;
        mListActiveBoolean = listActiveBoolean;

    }


    public HomeWorkCategories[] getmListActive(){
        List<HomeWorkCategories> result = new ArrayList<HomeWorkCategories>();
        for(int i=0; i<mList.length; i++){
            for(int k=0; k<mListActive.size(); k++){
                if(mListActive.get(k)==i){
                    result.add(mList[i]);
                }
            }
        }
        return (HomeWorkCategories[])result.toArray();
    }
    public int getCount() {
        return mList.length;
    }


    public Object getItem(int position) {
        return mList[position];
    }


    public long getItemId(int position) {

        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            // Make up a new view
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_checkbox, null);
        } else {
            // Use convertView if it is available
            view = convertView;
        }
        TextView tvCategoriesName = (TextView) view.findViewById(R.id.textViewCategoriesName);
        if(position!=0 && mList[position].getCategoryName().trim().toLowerCase().
                equals(mList[position - 1].getCategoryName().trim().toLowerCase())){
            tvCategoriesName.setVisibility(View.GONE);
        } else {
            tvCategoriesName.setVisibility(View.VISIBLE);
            tvCategoriesName.setText(mList[position].getCategoryName());
        }

        CheckBox check = (CheckBox) view.findViewById(R.id.checkBoxCategories);
        check.setText(mList[position].getTitle());

        check.setTag(Integer.valueOf(position));
        check.setChecked(mListActiveBoolean[position]);
        check.setOnCheckedChangeListener(mListener);


        return view;
    }

    CompoundButton.OnCheckedChangeListener mListener = new CompoundButton.OnCheckedChangeListener() {

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mListActiveBoolean[(Integer)buttonView.getTag()] = isChecked;
            try {
                if (isChecked && !mListActive.contains((int) mList[(Integer) buttonView.getTag()].getId())) {
                    mListActive.add((int) mList[(Integer) buttonView.getTag()].getId());
                } else if (!isChecked) {
                    int index =  mListActive.indexOf((int) mList[(Integer) buttonView.getTag()].getId());
                    mListActive.remove(index);
                }
            } catch (IndexOutOfBoundsException ex){}
        }
    };


}
