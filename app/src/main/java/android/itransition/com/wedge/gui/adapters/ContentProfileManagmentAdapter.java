package android.itransition.com.wedge.gui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.ContentCategoriesActivity;
import android.itransition.com.wedge.entity.ContentProfile;
import android.itransition.com.wedge.entity.DevicesEntity;
import android.itransition.com.wedge.settings.Consts;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by e.kazimirova on 30.09.2014.
 */
public class ContentProfileManagmentAdapter extends BaseAdapter {


    private Context mContext;
    private ContentProfile[] mList;
    private DevicesEntity[] devicesEntities;


    public ContentProfileManagmentAdapter(Context c, ContentProfile[] list,
                                          DevicesEntity[] devicesEntities) {

        mContext = c;
        mList = list;
        this.devicesEntities = devicesEntities;

    }


    public int getCount() {
        if(mList!=null) {
            return mList.length;
        } else {
            return devicesEntities.length;
        }
    }


    public Object getItem(int position) {
        if(mList!=null){
            return mList[position];
        } else {
            return devicesEntities[position];
        }
    }


    public long getItemId(int position) {

        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            // Make up a new view
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_content, parent, false);
        } else {
            // Use convertView if it is available
            view = convertView;
        }

        TextView tvTitle = (TextView) view.findViewById(R.id.textViewContentProfile);


            tvTitle.setText(mList[position].getTitle());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Activity)mContext).finish();
                    Intent intent = new Intent();
                    intent.setClass(mContext, ContentCategoriesActivity.class);
                    ArrayList<Integer> listId = new ArrayList<Integer>();
                    if (mList[position].getContentCategories() != null) {
                        for (int i = 0; i < mList[position].getContentCategories().length; i++) {
                            listId.add((int) mList[position].getContentCategories()[i].getId());
                        }
                    }
                    intent.putExtra(Consts.CONTENT_CATEGORIES, listId);
                    intent.putExtra(Consts.CONTENT_PROFILE_ID, mList[position].getId());
                    intent.putExtra(Consts.CONTENT_PROFILE_TITLE, mList[position].getTitle());
                    intent.putExtra(Consts.CONTENT_PROFILE_IS_DEFAULT, mList[position].getDefault());
                    intent.putExtra(Consts.CONTENT_PROFILE_IS_DELETABLE, mList[position].getDeletable());
                    intent.putExtra(Consts.CREATED_AT, (Parcelable) mList[position].getCreateAt());
                    mContext.startActivity(intent);
                }
            });

        return view;
    }


}