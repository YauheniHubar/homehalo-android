package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by e.kazimirova on 03.10.2014.
 */
public class CustomSpinnerAdapter  extends ArrayAdapter<String> {

    private Context mContext;
    private int txtResourse;
    private ArrayList<String> spinnerValues;
    private DeviceOwner[] spinnerValuesDevices;

    public CustomSpinnerAdapter(Context ctx, int txtViewResourceId, ArrayList<String> objects,
                                DeviceOwner[] spinnerValuesDevices) {
        super(ctx, txtViewResourceId, objects);
        this.txtResourse = txtViewResourceId;
        this.mContext = ctx;
        if(objects!=null || objects.size()<=0) {
            this.spinnerValues = objects;
        } else {
            this.spinnerValues = new ArrayList<String>();
            for(int i=0; i<spinnerValuesDevices.length; i++) {
                spinnerValues.add(spinnerValuesDevices[i].getTitle());
            }
        }
        this.spinnerValuesDevices = spinnerValuesDevices;
    }

    public DeviceOwner[] getValues(){
        return spinnerValuesDevices;
    }

    public ArrayList<String>  getArray(){
        return spinnerValues;
    }

    public void setValues(DeviceOwner[] values){
        spinnerValuesDevices = values;
    }
    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {

        prnt.setBackgroundColor(Color.WHITE);
        return super.getDropDownView(position, cnvtView, prnt);
    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mySpinner = inflater.inflate(txtResourse, parent, false);
        TextView main_text = (TextView) mySpinner .findViewById(R.id.textViewName);
        if(spinnerValues!=null) {
            main_text.setText(spinnerValues.get(position));
        } else {
            main_text.setText(spinnerValuesDevices[position].getTitle());
        }
        return  mySpinner;
     }


}
