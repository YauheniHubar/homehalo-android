package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.OneUserActivity;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.DeviceOwnerManager;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.itransition.com.wedge.utils.DeviceOwnerManager.getBlockRemainMin;
import static android.itransition.com.wedge.utils.Utils.getTimeString;
import static android.view.View.GONE;

/**
 * Created by e.kazimirova on 01.09.2014.
 */
public class DeviceOwnerAdapter extends BaseAdapter {

    public final String TAG = this.getClass().getSimpleName();

    private Context mContext;
    private List<DeviceOwner> mList;
    private final LayoutInflater inflater;
    private final DateFormat mFormatter;
    private SimpleDateFormat sdf;
    private int mItemHeight;
    private int mItemRightWidth;
    private AdapterItemListener mAdapterItemListener;
    SimpleDateFormat formatter;
    DateFormatSymbols symbols;
    String[] capitalDays; /* = {
            "", "SUN", "MON",
            "TUE", "WED", "THU",
            "FRI", "SAT"
    };*/

    public interface AdapterItemListener {
        void onRightBtnClick(int position);
    }

    public DeviceOwnerAdapter(Context c, List<DeviceOwner> list, int itemHeight, int rightWidth, AdapterItemListener listener) {
        mContext = c;
        mList = list;
        mItemHeight = itemHeight;

        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mFormatter = new SimpleDateFormat("HH:mm");
        sdf = new SimpleDateFormat("EEE");
        symbols = new DateFormatSymbols( new Locale("en", "US"));
        capitalDays = mContext.getResources().getStringArray(R.array.days_short);
        symbols.setShortWeekdays(capitalDays);
        formatter = new SimpleDateFormat("E", symbols);
        mItemRightWidth = rightWidth;
        mAdapterItemListener = listener;
    }

    public void refill(List<DeviceOwner> devices) {
        mList.clear();
        mList.addAll(devices);
        notifyDataSetChanged();
    }
    public int getCount() {
        return mList.size();
    }


    public Object getItem(int position) {
        return mList.get(position);
    }


    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final DeviceOwnersHolder holder;
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_item_users, parent, false);
            holder = new DeviceOwnersHolder(convertView);
        } else {
            holder = (DeviceOwnersHolder) convertView.getTag();
        }
        if (getCount()<=6){
            convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, (mItemHeight-3*getCount())/getCount()));
        }

        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        holder.leftItem.setLayoutParams(lp1);
        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(mItemRightWidth, LinearLayout.LayoutParams.MATCH_PARENT);
        holder.rightItem.setLayoutParams(lp2);
        final DeviceOwner owner = mList.get(position);
        if (!owner.isDeletable()) {
            holder.rightItem.setBackgroundColor(mContext.getResources().getColor(R.color.btn_disabled));
        } else {
            holder.rightItem.setBackgroundColor(mContext.getResources().getColor(R.color.red_for_edit));
        }

        holder.rightItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (owner.isDeletable()) {
                    mAdapterItemListener.onRightBtnClick(position);
                }
            }
        });



        holder.tvUserName.setText(owner.getTitle());

        setViewByBlock(position, holder);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startOwnerActivity(owner);

            }
        });
        return convertView;
    }

    private void startOwnerActivity(DeviceOwner owner) {
        Intent intent = new Intent();
        intent.setClass(mContext, OneUserActivity.class);
        long[] idOwners = new long[mList.size()];
        String[] titleOwners = new String[mList.size()];
        for (int i = 0; i < mList.size(); i++) {
            idOwners[i] = mList.get(i).getId();
            titleOwners[i] = (mList.get(i).getTitle());
        }
        // передаем данные кусками для увеличения скорости прорисовки
        Bundle b = new Bundle();
        b.putStringArray(Consts.USERS_TITLE, titleOwners);
        b.putLongArray(Consts.USERS_ID, idOwners);
        b.putString(Consts.DEVICE_TITLE, owner.getTitle());
        b.putLong(Consts.DEVICE_OWNER_ID, owner.getId());
        b.putString(Consts.DEVICE_STATUS, owner.getStatus());
        b.putParcelable(Consts.CREATED_AT, owner.getCreateAt());
        if (owner.getHomeworkModeDuration() == null) {
            b.putLong(Consts.HOMEWORK_MODE_DURATION, 0L);
        } else {
            b.putLong(Consts.HOMEWORK_MODE_DURATION, owner.getHomeworkModeDuration());
        }
        b.putLong(Consts.TIME, owner.getTime());
        b.putBoolean(Consts.IS_UNLIMITED_ACCESS, owner.isUnlimitedAccess());
        if (owner.getContentProfile() != null) {
            b.putString(Consts.CONTENT_PROFILE_TITLE, owner.getContentProfile().getTitle());
            b.putLong(Consts.CONTENT_PROFILE_ID, owner.getContentProfile().getId());
        } else {
            b.putString(Consts.CONTENT_PROFILE_TITLE, "");
            b.putLong(Consts.CONTENT_PROFILE_ID, 0);
        }
        b.putParcelable(Consts.BLOCK_STARTED_AT, owner.getBlockStartedAt());
        b.putParcelable(Consts.HOMEWORK_MODE_STARTED_AT, owner.getHomeworkModeStartedAt());

        b.putParcelableArray(Consts.TIME_PROFILESES, owner.getTimeProfiles());
        b.putParcelable(Consts.TIME_EXTENSION, owner.getTimeExtension());
        b.putParcelableArray(Consts.HOMEWORK_CATEGORIES, owner.getHomeworkCategories());
        b.putParcelableArrayList(Consts.WHITE_LIST_URL, owner.getUrlWhiteLists());
        b.putBoolean(Consts.BLOCKED, getBlockRemainMin(owner) > 0);
        intent.putExtras(b);
        mContext.startActivity(intent);
    }

    private void setViewByBlock(int position, DeviceOwnersHolder holder) {
        DeviceOwner owner = mList.get(position);

        Pair<DeviceOwnerManager.OwnerAccessState, Long> state = DeviceOwnerManager.getOwnerAccessStateWithTime(owner);

        switch (state.first) {
            case UnlimitedAccess:
                holder.info2.setVisibility(View.INVISIBLE);
                holder.info1.setVisibility(GONE);
                holder.setHasAccessIcon();
                break;
            case HasAccess:
                holder.info1.setVisibility(GONE);
                if (owner.getHasAccessUntil() != null && !owner.isAccessUnlimited()) {
                    holder.info2.setText("> " + mFormatter.format(owner.getHasAccessUntil().getDateWithOffset()));
                    if (owner.getHasAccessUntilLabel()!=null){
                        if (!owner.getHasAccessUntilLabel().contains("today")){
                            holder.info2.append(getDayOfWeekFromDate(owner.getHasAccessUntil().getDateWithOffset()));
                        }
                    }
                    holder.info2.setVisibility(View.VISIBLE);
                } else
                    holder.info2.setVisibility(View.INVISIBLE);
                holder.setHasAccessIcon();
                break;
            case NoAccess:
                holder.info1.setVisibility(GONE);
                holder.setNoAccessIcon(false);
                if (owner.getNextAccessUntil() != null && !owner.isAccessUnlimited()) {
                    holder.info2.setText("< " + mFormatter.format(owner.getNextAccessUntil().getDateWithOffset()));
                    if (!owner.getNextAccessUntilLabel().contains("today")) {
                        holder.info2.append(getDayOfWeekFromDate(owner.getHasAccessUntil().getDate()));
                    }
                    holder.info2.setVisibility(View.VISIBLE);
                } else if (owner.getHasNoAccessUntil()!= null){
                    holder.info2.setText("< " + mFormatter.format(owner.getHasNoAccessUntil().getDateWithOffset()));
                    holder.info2.append(getDayOfWeekFromDate(owner.getHasNoAccessUntil().getDate()));
                    holder.info2.setVisibility(View.VISIBLE);
                }
                else {
                    holder.info2.setVisibility(View.INVISIBLE);
                }
                break;
            case Blocked:
                holder.info1.setVisibility(View.VISIBLE);
                holder.info1.setText(getTimeString(state.second));
                holder.setNoAccessIcon(true);
                if (owner.getNextAccessUntil() != null && owner.isAccessUnlimited()) {
                    holder.info2.setText("< " + mFormatter.format(owner.getNextAccessUntil().getDateWithOffset()));
                    if (!owner.getNextAccessUntilLabel().contains("today")) {
                        holder.info2.append(getDayOfWeekFromDate(owner.getHasAccessUntil().getDate()));
                    }
                    holder.info2.setVisibility(View.VISIBLE);
                }else if (owner.getHasNoAccessUntil()!= null){

                    holder.info2.setText("< " + mFormatter.format(owner.getHasNoAccessUntil().getDateWithOffset()));
                    holder.info2.append(getDayOfWeekFromDate(owner.getHasNoAccessUntil().getDate()));
                    holder.info2.setVisibility(View.VISIBLE);
                } else {
                    holder.info2.setVisibility(View.INVISIBLE);
                }

                break;
            case HomeWork:
                holder.info1.setVisibility(View.VISIBLE);
                holder.info1.setText(getTimeString(state.second));
                if (owner.getHasAccessUntil() != null && !owner.isAccessUnlimited()) {
                    holder.info2.setText("> " + mFormatter.format(owner.getHasAccessUntil().getDateWithOffset()));
                    if (owner.getHasAccessUntilLabel()!=null){
                        if (owner.getHasAccessUntilLabel()!=null){
                            if (!owner.getHasAccessUntilLabel().contains("today")){
                                holder.info2.append(getDayOfWeekFromDate(owner.getHasAccessUntil().getDateWithOffset()));
                            }
                        }
                    }
                    holder.info2.setVisibility(View.VISIBLE);
                } else {
                    holder.info2.setVisibility(View.INVISIBLE);
                }
                holder.setHomeWorkIcon();
                break;
            case Extended:
                holder.info1.setVisibility(View.VISIBLE);
                holder.info1.setText(getTimeString(state.second));
                holder.setExtendedTimeIcon();

                if (owner.getHasAccessUntil() != null && !owner.isAccessUnlimited()) {
                    holder.info2.setText("> " + mFormatter.format(owner.getHasAccessUntil().getDateWithOffset()));
                    if (owner.getHasAccessUntilLabel()!=null){
                        if (owner.getHasAccessUntilLabel()!=null){
                            if (!owner.getHasAccessUntilLabel().contains("today")){
                                holder.info2.append(getDayOfWeekFromDate(owner.getHasAccessUntil().getDateWithOffset()));
                            }
                        }
                    }

                    holder.info2.setVisibility(View.VISIBLE);
                } else {
                    holder.info2.setVisibility(View.INVISIBLE);
                }
                break;
        }
    }

    private String getLastDayFromAccessLabel(String label){
        String lastWord = label.substring(label.lastIndexOf(" ")+1);
        return "\n"+lastWord;
    }

    private String getDayOfWeekFromDate(Date date){

        //return "\n" + sdf.format(date);
        return "\n   " + formatter.format(date);

    }

    public void recalculateRemainTime() {
        Log.i(TAG, "recalculateRemainTime");
        notifyDataSetChanged();
    }

    protected  static class DeviceOwnersHolder {
        public TextView tvUserName;
        public ImageView imageViewCircle;
        public TextView info1;
        public TextView info2;

        public View leftItem, rightItem;

        public View rootView;

        public DeviceOwnersHolder(View view) {
            imageViewCircle = (ImageView) view.findViewById(R.id.imageViewCircle);
            info1 = (TextView) view.findViewById(R.id.info1);
            tvUserName = (TextView) view.findViewById(R.id.tvUserName);
            info2 = (TextView) view.findViewById(R.id.info2);

            leftItem = view.findViewById(R.id.item_left);
            rightItem = view.findViewById(R.id.item_right);

            rootView = view;

            view.setTag(this);
        }

        public void setHasAccessIcon() {
            imageViewCircle.setImageResource(R.drawable.ic_owner_has_access);
            leftItem.setBackgroundResource(R.drawable.selector_green);
        }

        public void setNoAccessIcon(boolean isBlocked) {
            if (isBlocked) {
                info1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_blocked, 0, 0, 0);
                imageViewCircle.setImageResource(R.drawable.ic_owner_no_access);
            } else {
                imageViewCircle.setImageResource(R.drawable.ic_owner_no_access);
            }
            leftItem.setBackgroundResource(R.drawable.selector_gray);
        }

        public void setHomeWorkIcon() {
            imageViewCircle.setImageResource(R.drawable.ic_owner_partial_access);
            info1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_hw, 0, 0, 0);
            leftItem.setBackgroundResource(R.drawable.selector_green);
        }

        public void setExtendedTimeIcon() {
            imageViewCircle.setImageResource(R.drawable.ic_owner_partial_access);
            info1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_extended, 0, 0, 0);
            leftItem.setBackgroundResource(R.drawable.selector_green);
        }


    }


}

