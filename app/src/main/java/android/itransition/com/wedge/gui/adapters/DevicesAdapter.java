package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.DevicesEntity;
import android.itransition.com.wedge.gui.DeviceListItemView;
import android.itransition.com.wedge.utils.DeviceOwnerManager;
import android.itransition.com.wedge.utils.Utils;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class DevicesAdapter extends BaseAdapter {

    private Context context;

    private List<DevicesEntity> items;
    private boolean isDeviceOwner = false;
    private boolean expandedItem;

    private DeviceListItemView.DeviceMenuOptionClickListener listener;

    public DevicesAdapter(Context context, List<DevicesEntity> items, boolean isDeviceOwner, DeviceListItemView.DeviceMenuOptionClickListener listener) {
        this.context = context;
        this.items = items;
        this.isDeviceOwner = isDeviceOwner;
        this.listener = listener;
    }

    public int getCount() {
        return items.size();
    }

    public DevicesEntity getItem(int position) {
        return items.get(position);
    }


    public long getItemId(int position) {
        return position;
    }


    public void remove(DevicesEntity item) {
        items.remove(item);
        notifyDataSetChanged();
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        DeviceListItemView view;
        if (convertView == null) {
            view = new DeviceListItemView(context, listener);
        } else {
            view = (DeviceListItemView) convertView;
        }

        DevicesEntity item = getItem(position);

        view.setDeviceName(item.getTitle());

//        view.setShowInfo(!isDeviceOwner);
        DeviceOwnersDataSource source = new DeviceOwnersDataSource(WedgeApplication.mainSource);
        DeviceOwner owner = source.getDeviceOwnerById(item.getDeviceOwner().getId());
        //DeviceOwner owner = item.getDeviceOwner();
        String manufactureTitle;
        if (item.getManufacturedTitle() == null) {
            manufactureTitle = "n/a";
        } else {
            manufactureTitle = item.getManufacturedTitle();
        }

        String textAgo = "";
        Date parsedDate = new Date();
        PrettyTime p = new PrettyTime();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
        String firstSeen = "";
        Date createdAt = item.getCreateAt().getDate();
        firstSeen = dateFormatter.format(createdAt);
        if (item.getLastOnlineAt() != null) {
            try {
                CreateTimeUser last = item.getLastOnlineAt();
                parsedDate = formatter.parse(item.getLastOnlineAt().getDateTime());

                Date current = parsedDate;
                Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge())));

                cal1.setTime(current);
                Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone((item.
                        getLastOnlineAt().getTimeZone())));
                cal2.clear();
                cal2.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
                cal2.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
                cal2.set(Calendar.DATE, cal1.get(Calendar.DATE));
                cal2.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
                cal2.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
                current = cal1.getTime();
                textAgo = p.format(current);
                if (textAgo.toLowerCase().trim().contains("moments")) {
                    textAgo = context.getString(R.string.just_now);
                } else {
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            textAgo = context.getString(R.string.never);
        }
        SpannableString info = new SpannableString(owner.getTitle() + "\n"+context.getString(R.string.last_seen)+textAgo);
        SpannableString infoExpanded = new SpannableString(context.getString(R.string.first_seen) + firstSeen + "\nMAC: " + item.getMacAddress() + "\nMFR: "+manufactureTitle);
        if (item.getMacAddress() != null && owner.getId()!=0) {
            info.setSpan(new StyleSpan(Typeface.BOLD), owner.getTitle().length(), info.length(), 0);
        }
        view.setInfoTop(owner.getTitle());
        view.setInfoExpanded(infoExpanded);
        view.setInfoBottom(context.getString(R.string.last_seen)+textAgo);

        view.setOnline(item.isOnline());

        view.setMenuVisible(item.inEditMode);
        view.setExpanded(item.isExpanded);

        view.setAccessIconByOwnerState(DeviceOwnerManager.getOwnerNetworkStatus(item));

        view.setPosition(position);

        return view;
    }

}