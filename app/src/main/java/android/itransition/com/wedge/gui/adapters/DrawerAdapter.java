package android.itransition.com.wedge.gui.adapters;

/**
 * Created by e.kazimirova on 11.06.2014.
 */


import android.content.Context;
import android.itransition.com.wedge.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DrawerAdapter extends BaseAdapter {


    private Context mContext;
    private  String[] mListOfNames;

    public DrawerAdapter(Context c, String[] listOfNames) {

        mContext = c;
        mListOfNames = listOfNames;
     }


    public int getCount() {
        return mListOfNames.length;
    }


    public Object getItem(int position) {
        return mListOfNames[position];
    }


    public long getItemId(int position) {

        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view;
        if (convertView == null) {
            // Make up a new view
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (TextView) inflater.inflate(R.layout.list_item_layout, parent, false);
        } else {
            // Use convertView if it is available
            view = (TextView) convertView;
        }

        view.setText(mListOfNames[position]);

        return view;
    }


}