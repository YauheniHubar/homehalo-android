package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.ContentCategories;
import android.itransition.com.wedge.entity.UrlListEntity;
import android.itransition.com.wedge.entity.UrlTrafficEntity;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.BlackUrlModel;
import android.itransition.com.wedge.request.AddCategoriesRequest;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.urllists.BlackListRequest;
import android.itransition.com.wedge.utils.Utils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.octo.android.robospice.SpiceManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by i.grechishchev on 31.07.2015.
 * itransition 2015
 */
public class ExpandableListVisitedAdapter extends BaseExpandableListAdapter {

    private static final int M_BYTES = 1024 * 1024;
    private Context _context;
    private List<UrlTrafficEntity> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, UrlTrafficEntity> _listDataChild;
    private SpiceManager _contentManager;
    private long _deviceOwnerId;
    private boolean _isDemo;

    public ExpandableListVisitedAdapter(HashMap<String, UrlTrafficEntity> _listDataChild, List<UrlTrafficEntity> _listDataHeader,
                                        Context _context, SpiceManager _contentManager, long _deviceOwnerId, boolean _isDemo) {
        this._listDataChild = _listDataChild;
        this._listDataHeader = _listDataHeader;
        this._context = _context;
        this._contentManager = _contentManager;
        this._deviceOwnerId = _deviceOwnerId;
        this._isDemo = _isDemo;
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition).getUrl());
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
//        String headerTitle = (String) getGroup(groupPosition);
        UrlTrafficEntity urlTrafficEntity = (UrlTrafficEntity) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_traffic_parent, null);
        }
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        TextView tvCategory = (TextView) convertView.findViewById(R.id.tvCategory);
        TextView tvTraffic = (TextView) convertView.findViewById(R.id.tvTraffic);
        tvTraffic.setText(toBytes(urlTrafficEntity.getTraffic()));
        ContentCategories[] categories = urlTrafficEntity.getCategories();
        if (_isDemo) {
            tvTraffic.setVisibility(View.VISIBLE);
        } else {
            tvTraffic.setVisibility(View.GONE);
        }

        if (isExpanded) {
            if (categories.length != 0) {
                StringBuilder stringBuilder = new StringBuilder();
                tvCategory.setVisibility(View.VISIBLE);
                for (ContentCategories cc : categories) {
                    stringBuilder.append(cc.getTitle());
                    stringBuilder.append(", ");
                }
                tvCategory.setText(stringBuilder.substring(0, stringBuilder.length() - 2));
            }
            convertView.setBackgroundResource(R.color.reports_blue);
            tvTitle.setTextColor(_context.getResources().getColor(R.color.white));
            tvCategory.setTextColor(_context.getResources().getColor(R.color.white));
            tvTraffic.setTextColor(_context.getResources().getColor(R.color.white));
        } else {
            tvCategory.setVisibility(View.GONE);
            convertView.setBackgroundResource(R.color.main_gray_light);
            tvTitle.setTextColor(_context.getResources().getColor(R.color.black));
            tvCategory.setTextColor(_context.getResources().getColor(R.color.black));
            tvTraffic.setTextColor(_context.getResources().getColor(R.color.black));
        }

        tvTitle.setText(urlTrafficEntity.getUrl());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final UrlTrafficEntity urlTrafficEntity = (UrlTrafficEntity) getChild(groupPosition, childPosition);
        final String url = validateUrl(urlTrafficEntity.getUrl());
        final VisitedUrlViewHolder holder;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_traffic_visited_child, null);
            holder = new VisitedUrlViewHolder();
            holder.webView = (WebView) convertView.findViewById(R.id.webView);
            holder.btnBlockCat = (Button) convertView.findViewById(R.id.btnBlockCategory);
            holder.btnBlackListWebSite = (Button) convertView.findViewById(R.id.btnBlackList);
            holder.webView.loadUrl("about:blank");
            holder.webView.setWebViewClient(new WebViewClient());
            holder.webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            holder.webView.getSettings().setJavaScriptEnabled(true);
            holder.webView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        holder.webView.getParent().requestDisallowInterceptTouchEvent(false);
                    } else
                        holder.webView.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
            convertView.setTag(holder);
        } else

        holder = (VisitedUrlViewHolder)convertView.getTag();
        holder.webView.loadUrl("about:blank");
        holder.webView.loadUrl(url);
        holder.btnBlockCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockCategory(urlTrafficEntity.getCategories());
            }
        });

        holder.btnBlackListWebSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBlackUrl(url);
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private void createBlackUrl(String urlBase) {
        BaseRequest request;
        UrlListEntity url = new UrlListEntity();
        url.setUrl(urlBase);
        url.setDeviceOwner(_deviceOwnerId);
        request = new BlackListRequest(_context, BaseRequest.HttpMethodEnum.post, url);
        _contentManager.execute(request,
                new BaseRequestListener<BlackUrlModel>(_context, request) {
                    @Override
                    public void onRequestSuccess(BlackUrlModel baseModel) {
                        Utils.makeToast(R.string.success);
                        super.onRequestSuccess(baseModel);
                        /*if (baseModel.isSuccess() && baseModel.urlBlacklist != null) {
                            urlList.add(baseModel.urlBlacklist);

                            sortList();
                            adapter.notifyDataSetChanged();
                            editTextUrl.setText("");
                            Utils.hideSoftKeyboard(BlackListActivity.this, editTextUrl);
                        }*/
                    }
                });
    }

    private void blockCategory(ContentCategories[] categories) {
        BaseRequest request;
        request = new AddCategoriesRequest(_context, _deviceOwnerId, categories, true);
        _contentManager.execute(request,
                new BaseRequestListener<BaseModel>(_context, request) {
                    @Override
                    public void onRequestSuccess(BaseModel baseModel) {
                        Utils.makeToast(R.string.success);
                        super.onRequestSuccess(baseModel);
                    }
                });
    }

    private String toBytes(long v) {
        if (v > M_BYTES) {
            v /= M_BYTES;
            return v + " MB";
        }
        if (v > 1024) {
            v /= 1024;
            return v + " KB";
        }
        return v + " B";
    }

    private String validateUrl(String initialUrl) {
        String url = "";
        if (!initialUrl.startsWith("http://") || initialUrl.startsWith("https://")) {
            url = "http://"+initialUrl;
        }
        return url;
    }

    class VisitedUrlViewHolder {

        public WebView webView;
        public Button btnBlockCat;
        public Button btnBlackListWebSite;
    }
}

