package android.itransition.com.wedge.gui.adapters;

import android.content.Context;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.AlertsActivity;
import android.itransition.com.wedge.activity.AttachActivity;
import android.itransition.com.wedge.activity.BaseActivity;
import android.itransition.com.wedge.activity.ConfigActivity;
import android.itransition.com.wedge.activity.ContentProfileManagmentActivity;
import android.itransition.com.wedge.activity.DeviceOwnersActivity;
import android.itransition.com.wedge.activity.DevicesActivity;
import android.itransition.com.wedge.activity.reports.ChartReportsActivity;
import android.itransition.com.wedge.entity.FakeListEntity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by e.kazimirova on 01.09.2014.
 */
public class MainAdapter extends BaseAdapter {


    private Context mContext;
    private  ArrayList<FakeListEntity> mList;

    public MainAdapter(Context c, ArrayList<FakeListEntity> list) {

       mContext = c;
       mList = list;
    }


    public int getCount() {
        return mList.size();
    }


    public Object getItem(int position) {
        return mList.get(position);
    }


    public long getItemId(int position) {

        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        final MainHolder holder = new MainHolder();
        View view;
        if (convertView == null) {
            // Make up a new view
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_main_layout, null);
        } else {
            // Use convertView if it is available
            view = convertView;
        }

        holder.tvName = (TextView) view.findViewById(R.id.tvName);
        holder.imageIcon = (ImageView) view.findViewById(R.id.imageViewCircle);
        holder.tvCircle = (TextView) view.findViewById(R.id.tvCircle);

        holder.tvName.setText(mList.get(position).getmListOfNames());


        if(mList.get(position).getmWithCircle()){
            holder.imageIcon.setVisibility(View.VISIBLE);
            holder.tvCircle.setVisibility(View.VISIBLE);
            holder.tvCircle.setText(String.valueOf(mList.get(position).getmSize()));
        } else {
            holder.imageIcon.setVisibility(View.GONE);
            holder.tvCircle.setVisibility(View.GONE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                String k = holder.tvName.getText().toString();
                if(holder.tvName.getText().toString().trim().toLowerCase().equals(mContext.
                        getResources().getString((R.string.alerts)).trim().toLowerCase())){
                    ((BaseActivity)mContext).finish();
                    intent.setClass(mContext, AlertsActivity.class);
                } else if(holder.tvName.getText().toString().trim().toLowerCase().equals(mContext.
                        getResources().getString((R.string.users)).trim().toLowerCase())){
                    intent.setClass(mContext, DeviceOwnersActivity.class);

                }else if(holder.tvName.getText().toString().trim().toLowerCase().equals(mContext.
                        getResources().getString((R.string.devices)).trim().toLowerCase())){
                    intent.setClass(mContext, DevicesActivity.class);

                }else if(holder.tvName.getText().toString().trim().toLowerCase().equals(mContext.
                        getResources().getString((R.string.content_profile_managment)).trim().toLowerCase())){
                    intent.setClass(mContext, ContentProfileManagmentActivity.class);
                    ((BaseActivity)mContext).finish();

                }else if(holder.tvName.getText().toString().trim().toLowerCase().equals(mContext.
                        getResources().getString((R.string.reports)).trim().toLowerCase())){
                    intent.setClass(mContext, ChartReportsActivity.class);

                }else if(holder.tvName.getText().toString().trim().toLowerCase().equals(mContext.
                        getResources().getString((R.string.config)).trim().toLowerCase())){

                    intent.setClass(mContext, ConfigActivity.class);
                }else if(holder.tvName.getText().toString().trim().toLowerCase().equals(mContext.
                        getResources().getString((R.string.attachHomeHolo_title)).trim().toLowerCase())){
                    intent.setClass(mContext, AttachActivity.class);
                    ((BaseActivity)mContext).finish();
                }

                mContext.startActivity(intent);

            }
        });
        return view;
    }


    protected  static class MainHolder {

        TextView tvName;
        ImageView imageIcon;
        TextView tvCircle;
    }
}
