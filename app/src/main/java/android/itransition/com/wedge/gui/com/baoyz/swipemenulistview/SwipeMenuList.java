package android.itransition.com.wedge.gui.com.baoyz.swipemenulistview;

import android.view.animation.Interpolator;

/**
 * Created by y.drobysh on 08.01.2015.
 */
public interface SwipeMenuList {

    public Interpolator getOpenInterpolator();
    public Interpolator getCloseInterpolator();
    public void setCloseInterpolator(Interpolator interpolator);
    public void setOpenInterpolator(Interpolator interpolator);

}
