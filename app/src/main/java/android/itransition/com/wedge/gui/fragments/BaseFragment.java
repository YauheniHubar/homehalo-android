package android.itransition.com.wedge.gui.fragments;


import android.itransition.com.wedge.activity.BaseActivity;
import android.support.v4.app.Fragment;


public abstract class BaseFragment extends Fragment {


    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public interface PeriodInterface {
        void periodChanged();
    }

}
