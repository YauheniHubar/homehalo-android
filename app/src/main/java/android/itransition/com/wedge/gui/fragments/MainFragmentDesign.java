package android.itransition.com.wedge.gui.fragments;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.activity.AlertsActivity;
import android.itransition.com.wedge.activity.AttachActivity;
import android.itransition.com.wedge.activity.BaseActivity;
import android.itransition.com.wedge.activity.DeviceOwnersActivity;
import android.itransition.com.wedge.activity.OrderActivity;
import android.itransition.com.wedge.activity.SliderActivity;
import android.itransition.com.wedge.activity.reports.ChartReportsActivity;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.User;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.NotificationModel;
import android.itransition.com.wedge.model.UserModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.DeviceOwnersRequest;
import android.itransition.com.wedge.request.NotificationRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.request.UserRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.listeners.DeviceOwnersRequestListener;
import android.itransition.com.wedge.request.listeners.NotificationRequestListener;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.Calendar;
import java.util.Locale;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by e.kazimirova on 05.12.2014.
 */
public class MainFragmentDesign  extends BaseFragment implements RequestAction, View.OnClickListener {

    public interface MainFragmentItemClickListener {
        void onMainItemClick(int position);
    }

    private TextView textViewCount;
    protected SpiceManager contentManager;
    private View btnAlerts, btnReports, btnUsers, btnMore;
    private Button btnAttach;
    private static int count;
    private boolean isDemo;
    private Context mContext;
    private DeviceOwnersDataSource datasource;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mContext = inflater.getContext();
        datasource = new DeviceOwnersDataSource(mContext, WedgeApplication.mainSource);

        btnAlerts =         (RelativeLayout) view.findViewById(R.id.rowAlerts);
        btnReports = (RelativeLayout) view.findViewById(R.id.rowReports);
        btnUsers = (RelativeLayout) view.findViewById(R.id.rowUsers);
        btnAttach = (Button) view.findViewById(R.id.buttonAttach);
        btnMore =  view.findViewById(R.id.rowMore);
        textViewCount = (TextView)view.findViewById(R.id.counterAlerts);
//        tvStatus = (TextView) view.findViewById(R.id.tvStatus);

        isDemo = Settings.isDemoMode();

        if (isDemo) {

        }

        btnAlerts.setOnClickListener(this);
        btnReports.setOnClickListener(this);
        btnUsers.setOnClickListener(this);
        btnMore.setOnClickListener(this);
//        btnAttach.setOnClickListener(this);

        if(!Settings.hasFullAccess() && !isDemo) {
            handleNoWadge(view);
        }
        boolean hasFullAccess = Settings.hasFullAccess();
        boolean getUpdatingAlerts = Settings.getUpdatingAlerts();
        if (Settings.hasFullAccess() && !Settings.getUpdatingAlerts() || isDemo) {
            performGetAlerts();
        } else if (!Settings.hasFullAccess()) {
            count = 0;
        }

        // show notification in 24 hours if first run
        if (isDemo) {
            if (Settings.isFirstRun()) {
                if (getLocationCountry().equals("GB")) {
                    AlarmManager alarmManager = (AlarmManager) getBaseActivity().getSystemService(Context.ALARM_SERVICE);
                    Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
                    notificationIntent.addCategory("android.intent.category.DEFAULT");
                    PendingIntent broadcast = PendingIntent.getBroadcast(getBaseActivity().getBaseContext(), 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.HOUR_OF_DAY, 24);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
                    String country = getBaseActivity().getBaseContext().getResources().getConfiguration().locale.getCountry();

                }
            }
        } else {
            Settings.setCancelAlarmManager(true);
        }

        Log.d("grch15", "locale: " + Locale.getDefault().getLanguage());
        return view;
    }

    public void onResume() {
        super.onResume();
//        if (Settings.getUpdatingAlerts())
//        {
            count = Settings.getCountAlerts();
        if (!Settings.hasFullAccess() && !isDemo) {
            count = 0;
        }
            showAlertsCount(count);
//        }
    }


    private void handleNoWadge(View view) {
//        btnAttach.setVisibility(View.VISIBLE);
//
//        btnAlerts.setVisibility(View.GONE);
//        view.findViewById(R.id.thirdRow).setVisibility(View.VISIBLE);
//        view.findViewById(R.id.homeSecondRow).setVisibility(View.GONE);
//
//
//        btnUsers.setLayoutParams(new LinearLayout.LayoutParams(Utils.dp2px(150, getActivity()),
//                ViewGroup.LayoutParams.WRAP_CONTENT));
//
//        tvStatus.setText(R.string.no_router);
//        tvStatus.setTextColor(getResources().getColor(R.color.red));
    }

    @Override
    public void onStart() {
        super.onStart();
        User user = WedgeApplication.currentUser;
        if (user != null && user.isHasFullAccess() && user.getWedge() != null && !isDemo) {
            TextView tvStatus = getBaseActivity().getTitleView();
            tvStatus.setText(user.getWedge().isOnline() ? R.string.active : R.string.offline);
        }


        UserRequest getUserInfoRequest = new UserRequest(getActivity(), Settings.getUserId());
        contentManager.execute(getUserInfoRequest, new BaseRequestListener<UserModel>() {
            @Override
            public void onRequestSuccess(UserModel o) {
                updateStatus();
            }

            @Override
            public void onRequestFailure(SpiceException e) {
            }
        });

        showAlertsCount(Settings.getCountAlerts());

        prepareActionBar();
    }

    private void performGetDeviceOwnersRequest(){
        if(Settings.isNeedUpdateDeviceOwner()) {
            DeviceOwnersRequest request = new DeviceOwnersRequest(getActivity());
            contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                    new DeviceOwnersRequestListener(mContext, (RequestAction) this, request, true));
        }
    }

    private void prepareActionBar() {

        TextView titleView = getBaseActivity().getTitleView();
        if (isDemo) {
//            titleView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_basket, 0, 0, 0);
//            titleView.setTextColor(getResources().getColor(R.color.green));
//            titleView.setText(R.string.buy_homehalo);
            ActionBar actionBar = getBaseActivity().getSupportActionBar();
            actionBar.setCustomView(R.layout.actionbar_custom_demo);
            TextView btnBuy = (TextView)actionBar.getCustomView().findViewById(R.id.btnBuyHH);
            btnBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://homehalo.net/androidapp"));
                    startActivity(browserIntent);
                }
            });
        } else {
            titleView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_wedge_status, 0, 0, 0);
        }
        ((LinearLayout.LayoutParams)titleView.getLayoutParams()).rightMargin = Utils.dp2px(16, getActivity());
        getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getBaseActivity().getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.bar_color)));


    }

    private void updateStatus() {
        User user = WedgeApplication.currentUser;
//        String tz = WedgeApplication.currentUser.getWedge().getTimezone();
        if (user != null && user.getWedge() != null && !isDemo) {
            TextView tvStatus = getBaseActivity().getTitleView();
            tvStatus.setText(user.getWedge().isOnline() ? R.string.active : R.string.offline);
            checkForPayment(user);
//            int colorRes = user.getWedge().isOnline() ? R.color.green : R.color.red;
//            tvStatus.setTextColor(getResources().getColor(colorRes));
        }/* else {
            checkForPayment(user);
        }*/
    }

    private void checkForPayment(User user) {
//        User user = WedgeApplication.currentUser;
        if (!user.getSubscription().isActive()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(getString(R.string.error));
            alert.setMessage(getString(R.string.subscription_expired));
            alert.setPositiveButton(R.string.renew, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(getActivity(), OrderActivity.class));
                }
            });
            alert.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.contentManager = ((BaseActivity)activity).contentManager;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        contentManager = null;
    }

    private void showAlertsCount(int alertsCount) {
        if (alertsCount > 0) {
            textViewCount.setVisibility(View.VISIBLE);
            textViewCount.setText(String.valueOf(alertsCount));
            ShortcutBadger.with(getActivity().getApplicationContext()).count(alertsCount);
        } else {
            textViewCount.setVisibility(View.GONE);
            ShortcutBadger.with(getActivity().getApplicationContext()).remove();
        }
    }

    private void performGetAlerts(){
        NotificationRequest request = new NotificationRequest(getActivity(), Settings.getUserId());
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE,
                new NotificationRequestListener(getActivity(),  this, BaseRequest.HttpMethodEnum.get,
                        request.getUrl(), request));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void updateViewAfterSuccessfulAction(BaseModel model) {

        if (model instanceof NotificationModel){
            try {
                count = ((NotificationModel) model).getNotifications().length;
                Settings.setCountAlerts(count);
                Settings.setUpdatingAlerts(true);
                showAlertsCount(count);
            } catch(ClassCastException ex){
                ex.printStackTrace();
            }
            performGetDeviceOwnersRequest();
        }
        //show demo slider if first run after updating alerts
        if (isDemo && Settings.isFirstRun()) {
            Settings.setFirstRun(false);
            Intent intent = new Intent(getActivity().getBaseContext(), SliderActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void performFailRequestAction(int action) {
    }

    public void setCount() {
        count = count+1;
//        Settings.setCountAlerts(count);
        showAlertsCount(count);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        Activity activity = getActivity();
        switch (v.getId()){
            case R.id.rowAlerts:
                intent.setClass(activity, AlertsActivity.class);
                break;
            case R.id.rowReports:
                intent.setClass(activity, ChartReportsActivity.class);
                break;
            case R.id.rowUsers:
                intent.setClass(activity, DeviceOwnersActivity.class);
                break;
            case R.id.buttonAttach:
                intent.setClass(activity, AttachActivity.class);
                ((BaseActivity) activity).finish();
                break;
            case R.id.rowMore:
                if (activity instanceof MainFragmentItemClickListener) {
                    ((MainFragmentItemClickListener) activity).onMainItemClick(3);
                }
                return;
            default:
                break;
        }
        startActivity(intent);
    }

    public String getLocationCountry() {
        String country = getBaseActivity().getBaseContext().getResources().getConfiguration().locale.getCountry();
        return country;
    }
}
