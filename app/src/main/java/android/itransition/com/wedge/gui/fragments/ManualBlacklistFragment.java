package android.itransition.com.wedge.gui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.BaseActivity;
import android.itransition.com.wedge.activity.ManagementActivity;
import android.itransition.com.wedge.database.datesource.UrlListDataSource;
import android.itransition.com.wedge.entity.UrlListEntity;
import android.itransition.com.wedge.gui.adapters.BlackListTabsAdapter;
import android.itransition.com.wedge.gui.com.baoyz.swipemenulistview.SwipeMenu;
import android.itransition.com.wedge.gui.com.baoyz.swipemenulistview.SwipeMenuCreator;
import android.itransition.com.wedge.gui.com.baoyz.swipemenulistview.SwipeMenuItem;
import android.itransition.com.wedge.gui.com.baoyz.swipemenulistview.SwipeMenuListView;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.model.BlackUrlModel;
import android.itransition.com.wedge.model.WhiteUrlModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.request.urllists.AddUrlRequest;
import android.itransition.com.wedge.request.urllists.BlackListRequest;
import android.itransition.com.wedge.request.urllists.DeleteUrlRequest;
import android.itransition.com.wedge.request.urllists.UpdateUrlRequest;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.Utils;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.itransition.com.wedge.utils.Utils.dp2px;

/**
 * Created by i.grechishchev on 09.02.2016.
 * itransition 2016
 */
public class ManualBlacklistFragment extends BaseFragment implements View.OnClickListener, BlackListTabsAdapter.EditUrlListener{

    protected SpiceManager contentManager;
    private List<UrlListEntity> urlList;
    private List<UrlListEntity> altUrlList; //another urlList (black or white) to found duplicates
    public static SwipeMenuListView listViewUrls;
    private BlackListTabsAdapter adapter;
    private ImageView btnAdd;
    private EditText editTextUrl;
    private TextView empty;
    private long deviceOwnerId;
    private UrlListEntity currentListItem;
    private boolean isWhite;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_manual_blacklist, container, false);
        btnAdd = (ImageView) rootView.findViewById(R.id.buttonAdd);
        editTextUrl = (EditText) rootView.findViewById(R.id.editTextUrl);
        btnAdd.setOnClickListener(this);
        Bundle args = getArguments();
        /*if (args != null) {
            onCheckedChanged(null, args.getInt("id"));
        }*/
        isWhite = args.getBoolean(Consts.IS_WHITE_LIST, false);
        deviceOwnerId = args.getLong(Consts.DEVICE_OWNER_ID, -1);

//        setTitle(intent.getStringExtra(Consts.DEVICE_TITLE)); in activity

        listViewUrls = (SwipeMenuListView) rootView.findViewById(android.R.id.list);
        empty = (TextView) rootView.findViewById(R.id.tvNoUrls);
        setupListView();
        editTextUrl.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    String url = editTextUrl.getText().toString();
                    if (isValidUrl(url)) addUrl(url);
                }
                return false;
            }
        });
        getUrlList();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.contentManager = ((BaseActivity) activity).contentManager;
    }

    private void styleSwipeMenuItem(SwipeMenuItem item, String title, int resourceBack) {
        item.setWidth(dp2px(50, getActivity().getBaseContext()));
        item.setBackground(resourceBack);
        item.setTitle(title);
        item.setTitleColor(Color.WHITE);
        item.setTitleSize(14);
    }

    private void setupListView() {
        listViewUrls.setEmptyView(empty);
        listViewUrls.setMenuCreator(new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                switch (menu.getViewType()) {
                    case 0:
                        SwipeMenuItem assign = new SwipeMenuItem(getActivity().getApplicationContext());
                        styleSwipeMenuItem(assign, "Edit", R.color.green);
                        menu.addMenuItem(assign);
                        SwipeMenuItem edit = new SwipeMenuItem(getActivity().getApplicationContext());
                        styleSwipeMenuItem(edit, "Delete", R.color.red_for_edit);
                        menu.addMenuItem(edit);
                        break;
                }
            }
        });
        listViewUrls.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                currentListItem = adapter.getItem(position);
                switch (index) {
                    case 1:
                        onDeleteClick();
                        break;
                    case 0:
                        currentListItem.inEditMode = true;
                        adapter.notifyDataSetChanged();
//                        View v = adapter.getView(position, null, null);
//                        EditText et = (EditText)v.findViewById(R.id.etUrl);
//                        et.requestFocus();
//                        et.setSelection(4);
//                        Log.d("grch12","length:"+et.getText());
                        break;
                }
                return false;
            }
        });
        listViewUrls.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override public void onSwipeStart(int position) {
                if (currentListItem != null && currentListItem.inEditMode) {
                    Utils.hideSoftKeyboard(getActivity().getBaseContext(), editTextUrl);
                    currentListItem.inEditMode = false;

                }
                adapter.notifyDataSetChanged();
            }
            @Override public void onSwipeEnd(int position) {
                adapter.notifyDataSetChanged();}
        });

        listViewUrls.setLongClickable(true);

    }


    @Override
    public void onCancel() {
        currentListItem.inEditMode = false;
        adapter.notifyDataSetChanged();
    }

    private void onDeleteClick() {
        deleteUrlRequest();
    }

    private void getUrlList() {
        if (deviceOwnerId > 0) {
            UrlListDataSource source = new UrlListDataSource();
            urlList = source.getUrlListByOwnerId(deviceOwnerId, !isWhite);
//            altUrlList = source.getUrlListByOwnerId(deviceOwnerId, isWhite);
            updateList();
        }
    }

    private void updateList() {
        sortList();
        adapter = new BlackListTabsAdapter(getActivity().getBaseContext(), urlList, this);
        listViewUrls.setAdapter(adapter);
    }

    private void sortList() {
        Collections.sort(
                urlList,
                new Comparator<UrlListEntity>() {
                    public int compare(UrlListEntity lhs, UrlListEntity rhs) {
                        if (lhs.getUrl() == null) return -1;
                        if (rhs.getUrl() == null) return 1;
                        return lhs.getUrl().compareTo(rhs.getUrl());
                    }
                }
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewUpdate:
                Intent intent = new Intent();
                intent.setClass(getActivity().getBaseContext(), ManagementActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.imageViewHome:
                getActivity().onBackPressed();
                break;
            case R.id.buttonAdd:
                String url = editTextUrl.getText().toString();
                if (isValidUrl(url)) addUrl(url);
                break;
            case R.id.textViewBack:
                getActivity().onBackPressed();
                break;
        }
    }

    private boolean isValidUrl(String url) {
        if(url.length() > 5) {
            if (url.startsWith("http://")) url = url.substring(7);
            else if (url.startsWith("https://")) url = url.substring(8);
            if (url.startsWith("www.")) url = url.substring(4);
            if (!url.matches(".+\\..{2,}")) {
                Utils.makeToast(R.string.invalid_web_address);
                return false;
            }
            for (int i = 0; i < urlList.size(); i++) {
                if (urlList.get(i).getUrl().trim().toLowerCase().equals(url.toLowerCase().trim())) {
                    if (isWhite)
                        Utils.makeToast(R.string.blacklist_already_white);
                    else Utils.makeToast(R.string.blacklist_already_black);
                    return false;
                }
            }
            return true;
        } else {
            Utils.makeToast(R.string.invalid_web_address);
        }
        return false;
    }


    public void addUrl(final String urlBase){
        if (isWhite) {
            createWhiteUrl(urlBase);
        } else {
            createBlackUrl(urlBase);
        }
        Utils.hideSoftKeyboard(getActivity().getBaseContext(), editTextUrl);
    }

    private void createWhiteUrl(String urlBase) {
        AddUrlRequest request = new AddUrlRequest(getActivity().getBaseContext(), deviceOwnerId, urlBase);
        contentManager.execute(request,
                new BaseRequestListener<WhiteUrlModel>(getActivity().getBaseContext(), request) {
                    @Override
                    public void onRequestSuccess(WhiteUrlModel baseModel) {
                        Utils.makeToast(R.string.success);
                        super.onRequestSuccess(baseModel);
                        if (baseModel.isSuccess() && baseModel.whiteUrl != null) {
                            urlList.add(baseModel.whiteUrl);
                            sortList();
                            adapter.notifyDataSetChanged();
                            editTextUrl.setText("");
                            Utils.hideSoftKeyboard(getActivity().getBaseContext(), editTextUrl);
                        }
                    }
                });
    }

    private void createBlackUrl(String urlBase) {
        BaseRequest request;
        UrlListEntity url = new UrlListEntity();
        url.setUrl(urlBase);
        url.setDeviceOwner(deviceOwnerId);
        request = new BlackListRequest(getActivity().getBaseContext(), BaseRequest.HttpMethodEnum.post, url);
        contentManager.execute(request,
                new BaseRequestListener<BlackUrlModel>(getActivity(), request) {
                    @Override
                    public void onRequestSuccess(BlackUrlModel baseModel) {
//                        Utils.makeToast(R.string.success);
                        super.onRequestSuccess(baseModel);
                        if (baseModel.isSuccess() && baseModel.urlBlacklist != null) {
                            urlList.add(baseModel.urlBlacklist);
                            sortList();
                            adapter.notifyDataSetChanged();
                            editTextUrl.setText("");
                            Utils.hideSoftKeyboard(getActivity().getBaseContext(), editTextUrl);
                        }
                    }
                });
    }

    private void deleteUrlRequest() {
        BaseRequest request;
        if (isWhite) {
            request = new DeleteUrlRequest(getActivity().getBaseContext(), currentListItem.getDeviceOwner(), currentListItem.getId());
        } else {
            request = new BlackListRequest(getActivity().getBaseContext(), BaseRequest.HttpMethodEnum.delete, currentListItem);
        }
        contentManager.execute(request,
                new BaseRequestListener<BaseModel>(getActivity(), request) {
                    @Override public void onRequestSuccess(BaseModel result) {
                        super.onRequestSuccess(result);
//                        Utils.makeToast(R.string.black_list_deleted);
                        urlList.remove(currentListItem);
                        currentListItem = null;
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    @Override
    public void onEditUrl(final String url) {
        if (!isValidUrl(url)) return;

        final String oldUrl = currentListItem.getUrl();
        currentListItem.setUrl(url);

        BaseRequest request;;
        if (isWhite) {
            request = new UpdateUrlRequest(getActivity().getBaseContext(), deviceOwnerId, currentListItem.getId() ,url, Consts.STATUS_ACCEPTED);
        } else {
            request = new BlackListRequest(getActivity().getBaseContext(), BaseRequest.HttpMethodEnum.put, currentListItem);
        }

        contentManager.execute(request,
                new BaseRequestListener<BaseModel>(getActivity(), request) {
                    @Override public void onRequestFailure(SpiceException e) {
                        super.onRequestFailure(e);
                        if (currentListItem != null) {
                            currentListItem.setUrl(oldUrl);
                        }
                    }
                    @Override public void onRequestSuccess(BaseModel baseModel) {
//                        Utils.makeToast(R.string.blacklist_url_updated);
                        super.onRequestSuccess(baseModel);
                        if (currentListItem != null) {
                            currentListItem.inEditMode = false;
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
        Utils.hideSoftKeyboard(getActivity().getBaseContext(), editTextUrl);
    }
}
