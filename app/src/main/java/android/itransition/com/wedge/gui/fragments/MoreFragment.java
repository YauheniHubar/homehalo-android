package android.itransition.com.wedge.gui.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.ManagementActivity;
import android.itransition.com.wedge.settings.Settings;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class MoreFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    public interface OnMenuItemClickListener {
        void onMenuSelect(int position);
    }

    private ListView listVIew;
    private String[] menuItems;
    private MoreMenuItem[] moreMenuItems;
    int width;
    static int height;
    static int actionbarHeight;
    static int statusbarHeight;
    static int dividerHeight;

    private OnMenuItemClickListener listener;

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        listener = (OnMenuItemClickListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        menuItems = getResources().getStringArray(R.array.drawer_list);
        TypedArray iconResIdArray = getResources().obtainTypedArray(R.array.owner_menu_more_icons);

        moreMenuItems = new MoreMenuItem[menuItems.length];
        for (int i = 0; i < menuItems.length; i++) {
            moreMenuItems[i] = new MoreMenuItem(iconResIdArray.getResourceId(i, -1), menuItems[i]);
        }

        if (Settings.isDemoMode()) {
            menuItems[menuItems.length - 1] = getString(R.string.demo_end);
            moreMenuItems[menuItems.length - 1] = new MoreMenuItem(iconResIdArray.getResourceId(menuItems.length - 1, -1), getString(R.string.demo_end));
        }

        setHasOptionsMenu(true);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionbarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        statusbarHeight = getStatusBarHeight();
        dividerHeight = dpToPx(3);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_more, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listVIew = (ListView) view.findViewById(android.R.id.list);
        MoreMenuAdapter adapter1 = new MoreMenuAdapter(getActivity(), moreMenuItems);
        listVIew.setAdapter(adapter1);
        listVIew.setOnItemClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();

        prepareActionBar();
    }

    private void prepareActionBar() {
        TextView titleView;
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        if (Settings.isDemoMode()) {
            actionBar.setCustomView(R.layout.actionbar_custom_title_centered);
            titleView = (TextView) actionBar.getCustomView().findViewById(R.id.title);
        } else {
            titleView = getBaseActivity().getTitleView();
            titleView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.back_button_arrow);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.bar_color)));
        titleView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        titleView.setText(getString(R.string.more_title));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.action_home) {
//            getActivity().onBackPressed();
            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
//                finish();
                Intent intent = new Intent(getActivity().getApplicationContext(), ManagementActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (listener != null) {
            listener.onMenuSelect(position);
        }
    }

    private static class MoreMenuAdapter extends ArrayAdapter<MoreMenuItem> {

        private final LayoutInflater inflater;

        public MoreMenuAdapter(Context context, MoreMenuItem[] items) {
            super(context, R.layout.list_item_more_menu, items);
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Holder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_more_menu, parent, false);
                holder = new Holder();
                holder.icon = (ImageView) convertView.findViewById(R.id.ivIcon);
                holder.title = (TextView) convertView.findViewById(R.id.tvTitle);
                convertView.setTag(holder);
                int itemHeight = (height - (actionbarHeight + statusbarHeight + ((getCount()) * dividerHeight))) / getCount();
                convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, itemHeight));
            } else {
                holder = (Holder) convertView.getTag();
            }

            MoreMenuItem item = getItem(position);

            holder.title.setText(item.title);
            holder.icon.setImageResource(item.resIcon);
            return convertView;
        }

        static class Holder {
            TextView title;
            ImageView icon;
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    static class MoreMenuItem {
        int resIcon;
        String title;

        MoreMenuItem(int resIcon, String title) {
            this.resIcon = resIcon;
            this.title = title;
        }
    }

}
