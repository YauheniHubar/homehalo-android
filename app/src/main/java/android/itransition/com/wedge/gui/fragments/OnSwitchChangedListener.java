package android.itransition.com.wedge.gui.fragments;

/**
 * Created by i.grechishchev on 22.03.2016.
 * itransition 2016
 */
public interface OnSwitchChangedListener {
    void onSwitchChanged();
}
