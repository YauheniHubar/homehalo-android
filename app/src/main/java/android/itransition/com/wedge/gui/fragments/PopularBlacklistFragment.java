package android.itransition.com.wedge.gui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.activity.BaseActivity;
import android.itransition.com.wedge.activity.DeviceOwnersActivity;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.UrlListEntity;
import android.itransition.com.wedge.gui.adapters.BlacklistPopularAdapter;
import android.itransition.com.wedge.model.PopularBlacklist;
import android.itransition.com.wedge.model.PopularBlacklistModel;
import android.itransition.com.wedge.model.PopularBlacklistSimpleItem;
import android.itransition.com.wedge.model.PopularBlacklistSimpleItemModel;
import android.itransition.com.wedge.request.GetPopularBlacklistRequest;
import android.itransition.com.wedge.request.SendBlacklistCategoriesRequest;
import android.itransition.com.wedge.request.listeners.BaseRequestListener;
import android.itransition.com.wedge.settings.Consts;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i.grechishchev on 10.02.2016.
 * itransition 2016
 */
public class PopularBlacklistFragment extends BaseFragment implements View.OnClickListener, OnSwitchChangedListener {

    protected SpiceManager contentManager;
    private List<Integer> switchedIds;
    private List<PopularBlacklist> blacklistCategories;
    private List<PopularBlacklistSimpleItem> blacklistItems;
    private List<UrlListEntity> altUrlList; //another urlList (black or white) to found duplicates
    private ListView lvPopularBlacklist;
    private Button btnSend;
    private BlacklistPopularAdapter adapter;
    private DeviceOwner deviceOwner;
    private long deviceOwnerId;
    private DeviceOwnersDataSource source;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_popular_blacklist, container, false);
        lvPopularBlacklist = (ListView) rootView.findViewById(R.id.lvPopularBlacklist);
        btnSend = (Button) rootView.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
        Bundle args = getArguments();
        /*if (args != null) {
            onCheckedChanged(null, args.getInt("id"));
        }*/
        deviceOwnerId = args.getLong(Consts.DEVICE_OWNER_ID, -1);
        source = new DeviceOwnersDataSource(MainSource.getInstance());
        deviceOwner = source.getDeviceOwnerById(deviceOwnerId);
        blacklistItems = new ArrayList<>();
        switchedIds = new ArrayList<>();
        blacklistItems = deviceOwner.getBlacklistedCategories();
        for (PopularBlacklistSimpleItem item : blacklistItems) {
            switchedIds.add(item.getContentCategory());
        }
        if (blacklistCategories == null) {
            performGetPopularBlacklist();
        } else {
            updateList(blacklistCategories);
        }
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.contentManager = ((BaseActivity) activity).contentManager;
    }


    /*private void getUrlList() {
        if (deviceOwnerId > 0) {
            UrlListDataSource source = new UrlListDataSource();
            urlList = source.getUrlListByOwnerId(deviceOwnerId, !isWhite);
//            altUrlList = source.getUrlListByOwnerId(deviceOwnerId, isWhite);
            updateList();
        }
    }*/

    private void performGetPopularBlacklist() {
        GetPopularBlacklistRequest request = new GetPopularBlacklistRequest(getActivity());
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE, new BaseRequestListener<PopularBlacklistModel>(getActivity(), request) {
            @Override
            public void onRequestFailure(SpiceException e) {
                super.onRequestFailure(e);
            }

            @Override
            public void onRequestSuccess(PopularBlacklistModel o) {
                super.onRequestSuccess(o);
                blacklistCategories = o.getCategories();
                updateList(o.getCategories());
//                deviceOwner.setBlacklistedCategories(o.getCategories());
//                source.createPopularBlacklist(DeviceOwner deviceOwner);
            }
        });
    }

    private void performSendPopularBlacklist(List<Integer> ids) {

        SendBlacklistCategoriesRequest request = new SendBlacklistCategoriesRequest(getActivity(), deviceOwnerId, ids);
        contentManager.execute(request, null, DurationInMillis.ONE_MINUTE, new BaseRequestListener<PopularBlacklistSimpleItemModel>(getActivity(), request) {
            @Override
            public void onRequestFailure(SpiceException e) {
                super.onRequestFailure(e);
                Log.d("", "");
            }

            @Override
            public void onRequestSuccess(PopularBlacklistSimpleItemModel o) {
                super.onRequestSuccess(o);
                deviceOwner.setBlacklistedCategories(o.getCategories());
                source.updatePopularBlacklist(deviceOwner);
                goToDeviceOwnerActivity();
                Log.d("","");
            }
        });
    }

    private void updateList(List<PopularBlacklist> list) {
        adapter = new BlacklistPopularAdapter(getActivity().getBaseContext(), list, this);
        lvPopularBlacklist.setAdapter(adapter);
        initSwitches();
        adapter.notifyDataSetChanged();
    }

//    private void sortList() {
//        Collections.sort(
//                urlList,
//                new Comparator<UrlListEntity>() {
//                    public int compare(UrlListEntity lhs, UrlListEntity rhs) {
//                        if (lhs.getUrl() == null) return -1;
//                        if (rhs.getUrl() == null) return 1;
//                        return lhs.getUrl().compareTo(rhs.getUrl());
//                    }
//                }
//        );
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSend:
                switchedIds = adapter.getSwitchedItems();
                performSendPopularBlacklist(switchedIds);
                break;

        }
    }

    private void initSwitches() {
       adapter.initSwitchedItems(blacklistItems);
    }

    private void goToDeviceOwnerActivity() {
        Intent intent = new Intent(getActivity(), DeviceOwnersActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onSwitchChanged() {
        btnSend.setVisibility(View.VISIBLE);

    }
}