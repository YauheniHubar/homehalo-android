package android.itransition.com.wedge.gui.fragments;

import android.app.Fragment;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.Time;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.gui.CheckableButton;
import android.itransition.com.wedge.gui.SmallHourView;
import android.itransition.com.wedge.gui.TimeGridLayout;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.utils.TimeDataHelper;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


/**
 * Created by i.grechishchev on 06.07.2015.
 * itransition 2015
 */
public class TimeProfileDayFragment extends Fragment implements View.OnTouchListener {
    public static final String DAY_OF_WEEK_KEY = "dayOfWeek";
    public static final String DAY_OF_WEEK_KEY_EN = "dayOfWeekEn";

    SmallHourView h12am, h01am, h02am, h03am, h04am, h05am, h06am, h07am, h08am, h09am, h10am, h11am;
    SmallHourView h12pm, h01pm, h02pm, h03pm, h04pm, h05pm, h06pm, h07pm, h08pm, h09pm, h10pm, h11pm;
    LinearLayout dayLayout;
    private String dayOfWeek;
    private String dayOfWeekEn;
    int x1, x2, y1, y2;
    List<TimeDataHelper> timeList;
    Map<Integer, List<TimeDataHelper>> treeMap;//TODO try named fields with more info
    Integer interval = 0;
    TimeProfiles[] time;

    private boolean isEditMode = false;

    private long startEventTime;
//    final static int TIME_TAG = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null){
            Bundle args = getArguments();

            dayOfWeek = args.getString(DAY_OF_WEEK_KEY);
            dayOfWeekEn = args.getString(DAY_OF_WEEK_KEY_EN);

            time = new TimeProfiles[0];

            Parcelable[] tv = args.getParcelableArray(Consts.TIME_PROFILESES);
            if (tv != null) {
                time = new TimeProfiles[tv.length];
                for (int i = 0; i < tv.length; i++) {
                    time[i] = (TimeProfiles) tv[i];
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmant_day, container, false);
        dayLayout = (LinearLayout) view.findViewById(R.id.day_layout);

        h12am = (SmallHourView) view.findViewById(R.id.h12am);
        h01am = (SmallHourView) view.findViewById(R.id.h01am);
        h02am = (SmallHourView) view.findViewById(R.id.h02am);
        h03am = (SmallHourView) view.findViewById(R.id.h03am);
        h04am = (SmallHourView) view.findViewById(R.id.h04am);
        h05am = (SmallHourView) view.findViewById(R.id.h05am);
        h06am = (SmallHourView) view.findViewById(R.id.h06am);
        h07am = (SmallHourView) view.findViewById(R.id.h07am);
        h08am = (SmallHourView) view.findViewById(R.id.h08am);
        h09am = (SmallHourView) view.findViewById(R.id.h09am);
        h10am = (SmallHourView) view.findViewById(R.id.h10am);
        h11am = (SmallHourView) view.findViewById(R.id.h11am);

        h12pm = (SmallHourView) view.findViewById(R.id.h12pm);
        h01pm = (SmallHourView) view.findViewById(R.id.h01pm);
        h02pm = (SmallHourView) view.findViewById(R.id.h02pm);
        h03pm = (SmallHourView) view.findViewById(R.id.h03pm);
        h04pm = (SmallHourView) view.findViewById(R.id.h04pm);
        h05pm = (SmallHourView) view.findViewById(R.id.h05pm);
        h06pm = (SmallHourView) view.findViewById(R.id.h06pm);
        h07pm = (SmallHourView) view.findViewById(R.id.h07pm);
        h08pm = (SmallHourView) view.findViewById(R.id.h08pm);
        h09pm = (SmallHourView) view.findViewById(R.id.h09pm);
        h10pm = (SmallHourView) view.findViewById(R.id.h10pm);
        h11pm = (SmallHourView) view.findViewById(R.id.h11pm);

        dayLayout.setOnTouchListener(this);

        h12am.setTag(R.id.TAG_HOUR, "00");
        h01am.setTag(R.id.TAG_HOUR, "01");
        h02am.setTag(R.id.TAG_HOUR, "02");
        h03am.setTag(R.id.TAG_HOUR, "03");
        h04am.setTag(R.id.TAG_HOUR, "04");
        h05am.setTag(R.id.TAG_HOUR, "05");
        h06am.setTag(R.id.TAG_HOUR, "06");
        h07am.setTag(R.id.TAG_HOUR, "07");
        h08am.setTag(R.id.TAG_HOUR, "08");
        h09am.setTag(R.id.TAG_HOUR, "09");
        h10am.setTag(R.id.TAG_HOUR, "10");
        h11am.setTag(R.id.TAG_HOUR, "11");

        h12pm.setTag(R.id.TAG_HOUR, "12");
        h01pm.setTag(R.id.TAG_HOUR, "13");
        h02pm.setTag(R.id.TAG_HOUR, "14");
        h03pm.setTag(R.id.TAG_HOUR, "15");
        h04pm.setTag(R.id.TAG_HOUR, "16");
        h05pm.setTag(R.id.TAG_HOUR, "17");
        h06pm.setTag(R.id.TAG_HOUR, "18");
        h07pm.setTag(R.id.TAG_HOUR, "19");
        h08pm.setTag(R.id.TAG_HOUR, "20");
        h09pm.setTag(R.id.TAG_HOUR, "21");
        h10pm.setTag(R.id.TAG_HOUR, "22");
        h11pm.setTag(R.id.TAG_HOUR, "23");

        initTimeProfileUI(time);
        return view;
    }

    public void setEditModeEnabled(boolean isEnabled){
        this.isEditMode = isEnabled;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (!isEditMode) return false;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = (int) event.getRawX();
                y1 = (int) event.getRawY();
                startEventTime = event.getDownTime();

                break;
            case MotionEvent.ACTION_MOVE:
                x2 = (int) event.getRawX();
                y2 = (int) event.getRawY();
                toggleSelectedButtons(x1, x2, y1, y2);
                break;
            case MotionEvent.ACTION_UP:
                x2 = (int) event.getRawX();
                y2 = (int) event.getRawY();
                toggleButton(x2, y2);

                timeList = new ArrayList<>();
                treeMap = new TreeMap();

                List<TimeDataHelper> listTDH;
                Set<Integer> keys = treeMap.keySet();
                String startTime;
                String endTime;
                for (Integer key : keys) {
                    listTDH = treeMap.get(key);
                    startTime = listTDH.get(0).getHour() + ":" + listTDH.get(0).getMinutes() + ":00";
                    endTime = listTDH.get(listTDH.size() - 1).getHour() + ":" + listTDH.get(listTDH.size() - 1).getMinutes() + ":00";
                    Log.d("grecha11", "time : " + startTime + " - " + endTime);
                }
                break;
        }
        return true;
    }

    /**
     * 4 cases of coordinates:
     * [top-left corner -> bottom-right corner] or
     * [top-right corner -> bottom-left corner] or
     * [bottom-left corner -> top-right corner] or
     * [bottom-right corner -> top-left corner]
     *
     * @param x  top-left pixel of view's x
     * @param y  top-left pixel of view's y
     * @param x1 ACTION_DOWN x
     * @param x2 ACTION_UP x
     * @param y1 ACTION_DOWN y
     * @param y2 ACTION_UP y
     * @return
     */
    static boolean isPointWithin(int x, int y, int x1, int x2, int y1, int y2) {

        return ((x <= x2 && x >= x1 && y <= y2 && y >= y1) ||
                (x >= x2 && x <= x1 && y <= y2 && y >= y1) ||
                (x >= x2 && x <= x1 && y >= y2 && y <= y1) ||
                (x <= x2 && x >= x1 && y >= y2 && y <= y1));
    }

    private boolean isPointOnView(int dotX, int viewTopY, int viewBottomY, int x1, int y1, int x2, int y2){
        return (isCoordBetween(dotX, x1, x2) && (isCoordBetween(y1, viewTopY, viewBottomY) || isCoordBetween(y2, viewTopY, viewBottomY)));
    }

    private boolean isCoordBetween(int x, int x1, int x2){
        return (x >= x1 && x <= x2) || (x <= x1 && x >= x2);
    }

    /**
     * Method, checking child view if it is within coordinates
     *
     * @param x1 ACTION_DOWN x
     * @param x2 ACTION_UP x
     * @param y1 ACTION_DOWN y
     * @param y2 ACTION_UP y
     */
    private void toggleSelectedButtons(int x1, int x2, int y1, int y2) {
        //TODO Very large construction of inner operators, try to divide and encapsulate part of logic in SmallHourView and TimeGridLayout
        for (int i = 0; i < dayLayout.getChildCount(); i++) {
            View childOne = dayLayout.getChildAt(i);
            if (childOne instanceof TimeGridLayout) {
                TimeGridLayout tgl = (TimeGridLayout) childOne;
                for (int k = 0; k < tgl.getChildCount(); k++) {
                    View childTwo = tgl.getChildAt(k);
                    if (childTwo instanceof SmallHourView) {
                        SmallHourView shv = (SmallHourView) childTwo;
                        int[] hourViewLocation = new int[2];
                        shv.getLocationOnScreen(hourViewLocation);
                        int hourViewTopY = hourViewLocation[1];
                        int hourViewBottomY = hourViewTopY + shv.getHeight();
                        for (int j = 0; j < shv.getChildCount(); j++) {
                            View childThree = shv.getChildAt(j);
                            if (childThree instanceof LinearLayout) {
                                LinearLayout llParent = (LinearLayout) childThree;
                                for (int n = 0; n < llParent.getChildCount(); n++) {
                                    View childFour = llParent.getChildAt(n);
                                    if (childFour instanceof LinearLayout) {
                                        LinearLayout llChild = (LinearLayout) childFour;
                                        for (int m = 0; m < llChild.getChildCount(); m++) {
                                            View childFive = llChild.getChildAt(m);
                                            if (childFive instanceof CheckableButton) {
                                                CheckableButton btn = (CheckableButton) childFive;
                                                int[] loc = new int[2];
                                                btn.getLocationOnScreen(loc);
                                                int x = loc[0];
                                                int y = loc[1];
                                                if (isPointWithin(x + (btn.getWidth()/2), y, x1, x2, y1, y2) ||
                                                    isPointOnView(x + (btn.getWidth()/2), hourViewTopY, hourViewBottomY, x1, y1, x2, y2)) {
                                                    btn.toggle(startEventTime);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void toggleButton(int x, int y) {
        //TODO Very large construction of inner operators, try to divide and encapsulate part of logic in SmallHourView and TimeGridLayout
        for (int i = 0; i < dayLayout.getChildCount(); i++) {
            View childOne = dayLayout.getChildAt(i);
            if (childOne instanceof TimeGridLayout) {
                TimeGridLayout tgl = (TimeGridLayout) childOne;
                for (int k = 0; k < tgl.getChildCount(); k++) {
                    View childTwo = tgl.getChildAt(k);
                    if (childTwo instanceof SmallHourView) {
                        SmallHourView shv = (SmallHourView) childTwo;

                        int[] hourViewLocation = new int[2];
                        shv.getLocationOnScreen(hourViewLocation);
                        int x1 = hourViewLocation[0];
                        int x2 = x1 + shv.getWidth();
                        int y1 = hourViewLocation[1];
                        int y2 = y1 + shv.getHeight();

                        if (!(isCoordBetween(x, x1, x2) && isCoordBetween(y, y1, y2))) continue;

                        for (int j = 0; j < shv.getChildCount(); j++) {
                            View childThree = shv.getChildAt(j);
                            if (childThree instanceof LinearLayout) {
                                LinearLayout llParent = (LinearLayout) childThree;
                                for (int n = 0; n < llParent.getChildCount(); n++) {
                                    View childFour = llParent.getChildAt(n);
                                    if (childFour instanceof LinearLayout) {
                                        LinearLayout llChild = (LinearLayout) childFour;
                                        for (int m = 0; m < llChild.getChildCount(); m++) {
                                            View childFive = llChild.getChildAt(m);
                                            if (childFive instanceof CheckableButton) {
                                                CheckableButton btn = (CheckableButton) childFive;
                                                int[] loc = new int[2];
                                                btn.getLocationOnScreen(loc);
                                                int x3 = loc[0];
                                                int x4 = x3 + btn.getWidth();
                                                if (isCoordBetween(x, x3, x4)) {
                                                    btn.toggle(startEventTime);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public List<TimeProfiles> getTimeProfilesList() {
        timeList = new ArrayList<>();
        treeMap = new TreeMap();
        TimeDataHelper tdh = null;
        //TODO Very large construction of inner operators, try to divide and encapsulate part of logic in SmallHourView and TimeGridLayout
        for (int i = 0; i < dayLayout.getChildCount(); i++) {
            View childOne = dayLayout.getChildAt(i);
            if (childOne instanceof TimeGridLayout) {
                TimeGridLayout tgl = (TimeGridLayout) childOne;
                for (int k = 0; k < tgl.getChildCount(); k++) {
                    View childTwo = tgl.getChildAt(k);
                    if (childTwo instanceof SmallHourView) {
                        SmallHourView shv = (SmallHourView) childTwo;
                        for (int j = 0; j < shv.getChildCount(); j++) {
                            View childThree = shv.getChildAt(j);
                            if (childThree instanceof LinearLayout) {
                                LinearLayout llParent = (LinearLayout) childThree;
                                for (int n = 0; n < llParent.getChildCount(); n++) {
                                    View childFour = llParent.getChildAt(n);
                                    if (childFour instanceof LinearLayout) {
                                        LinearLayout llChild = (LinearLayout) childFour;
                                        for (int m = 0; m < llChild.getChildCount(); m++) {
                                            View childFive = llChild.getChildAt(m);
                                            if (childFive instanceof CheckableButton) {
                                                CheckableButton btn = (CheckableButton) childFive;
                                                if (btn.isChecked()) {
                                                    tdh = new TimeDataHelper(shv.getTag(R.id.TAG_HOUR).toString(), btn.getTag(R.id.TAG_MINUTES).toString());
                                                    tdh.setInterval(interval);
                                                    timeList.add(tdh);
                                                    treeMap.put(interval, timeList);
                                                } else {
                                                    ++interval;
                                                    timeList = new ArrayList<>();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        List<TimeDataHelper> listTDH;
        List<String> result;
        Set<Integer> keys = treeMap.keySet();
        String startTime;
        String endTime;
        List<TimeProfiles> timeExistent = new ArrayList<TimeProfiles>(Arrays.asList(time));
        List<TimeProfiles> toRemove = new ArrayList<TimeProfiles>();

        //clear timeprofiles of this day
        for (TimeProfiles t : timeExistent) {
            if (t.getDayOfWeek().equalsIgnoreCase(dayOfWeekEn)) {
                toRemove.add(t);
            }
        }
        timeExistent.removeAll(toRemove);
        List<TimeProfiles> timeProfiles = new ArrayList<>();
        TimeProfiles timeDay;
        for (Integer key : keys) {
            listTDH = treeMap.get(key);
            startTime = listTDH.get(0).getHour() + ":" + listTDH.get(0).getMinutes() + ":00";
            endTime = formatEndTime(listTDH.get(listTDH.size() - 1).getHour(), listTDH.get(listTDH.size() - 1).getMinutes());
            Time start = new Time();
            Time end = new Time();
            start.setTime(startTime);
            end.setTime(endTime);
            timeDay = new TimeProfiles();
            timeDay.setDayOfWeek(getDayEn().toLowerCase());
            timeDay.setStartTime(start);
            timeDay.setEndTime(end);
            timeExistent.add(timeDay);
        }
        return timeExistent;
    }

    public String getDay() {
        return dayOfWeek;
    }

    public String getDayEn() {
        return dayOfWeekEn;
    }

    public String formatEndTime(String hh, String mm) {
        String endTime;
        int tmpMins = Integer.valueOf(mm);
        int tmpHrs = Integer.valueOf(hh);
        if (tmpMins < 45) {
            tmpMins += 15;
        } else {
            tmpMins = 0;
            if (tmpHrs < 23) {
                ++tmpHrs;
            } else {
                tmpMins = 59;
            }
        }
        String mins = String.format("%02d", tmpMins);
        String hrs = String.format("%02d", tmpHrs);

        endTime = hrs + ":" + mins + ":00";
        return endTime;
    }

    public void clearTimeProfileUI(boolean checkAll){
        for (int i = 0; i < dayLayout.getChildCount(); i++) {
            View childOne = dayLayout.getChildAt(i);
            if (!(childOne instanceof TimeGridLayout)) continue;

            TimeGridLayout tgl = (TimeGridLayout) childOne;
            for (int k = 0; k < tgl.getChildCount(); k++) {
                View childTwo = tgl.getChildAt(k);
                if (!(childTwo instanceof SmallHourView)) continue;

                SmallHourView hourView = (SmallHourView) childTwo;

                hourView.setDefaultState(checkAll);
            }
        }
    }

    public void initTimeProfileUI(TimeProfiles[] time) {
        for (int p = 0; p < time.length; p++) {
            TimeProfiles timeProfile = time[p];
            if (timeProfile.getDayOfWeek().equals(dayOfWeekEn.toLowerCase())) {
                String startTime = timeProfile.getStartTime().getTime();
                String endTime = timeProfile.getEndTime().getTime();
                String startHrString = startTime.substring(0, startTime.indexOf(":", 0));
                String startMinString = startTime.substring(startTime.indexOf(":", 0) + 1, startTime.lastIndexOf(":"));
                String endHrString = endTime.substring(0, startTime.indexOf(":", 0));
                String endMinString = endTime.substring(startTime.indexOf(":", 0) + 1, startTime.lastIndexOf(":"));
                int startHr = Integer.valueOf(startHrString);
                int startMin = Integer.valueOf(startMinString);
                int endHr = Integer.valueOf(endHrString);
                int endMin = Integer.valueOf(endMinString);
                for (int i = 0; i < dayLayout.getChildCount(); i++) {
                    View childOne = dayLayout.getChildAt(i);
                    if (childOne instanceof TimeGridLayout) {
                        TimeGridLayout tgl = (TimeGridLayout) childOne;
                        for (int k = 0; k < tgl.getChildCount(); k++) {
                            View childTwo = tgl.getChildAt(k);
                            if (!(childTwo instanceof SmallHourView)) continue;

                            SmallHourView shv = (SmallHourView) childTwo;

                            int currentHr = Integer.valueOf(shv.getTag(R.id.TAG_HOUR).toString());

                            if (startHr > currentHr && currentHr > endHr) continue;

                            for (int j = 0; j < shv.getChildCount(); j++) {
                                View childThree = shv.getChildAt(j);
                                if (!(childThree instanceof LinearLayout)) continue;
                                LinearLayout llParent = (LinearLayout) childThree;
                                for (int n = 0; n < llParent.getChildCount(); n++) {
                                    View childFour = llParent.getChildAt(n);
                                    if (!(childFour instanceof LinearLayout)) continue;
                                    LinearLayout llChild = (LinearLayout) childFour;

                                    selectPartsOfHourView(llChild, currentHr, startHr, startMin, endHr, endMin);
                                }

                            }

                        }
                    }
                }
            }
        }
    }

    private void selectPartsOfHourView(LinearLayout parent,int currentHr, int startHr, int startMin, int endHr, int endMin){
        CheckableButton btn;

        int startTime = startHr * 60 + startMin;
        int endTime = endHr * 60 + endMin;
        int currentTime = currentHr * 60;

        if (startTime <= (currentTime + 0) && (currentTime + 0) < endTime) {
            btn = (CheckableButton) parent.findViewWithTag("00");
            btn.setChecked(true);
        }

        if (startTime <= (currentTime + 15) && (currentTime + 15) < endTime) {
            btn = (CheckableButton) parent.findViewWithTag("15");
            btn.setChecked(true);
        }

        if (startTime <= (currentTime + 30) && (currentTime + 30) < endTime) {
            btn = (CheckableButton) parent.findViewWithTag("30");
            btn.setChecked(true);
        }

        if (startTime <= (currentTime + 45) && (currentTime + 45) < endTime){
            btn = (CheckableButton) parent.findViewWithTag("45");
            btn.setChecked(true);
        }
    }
}