package android.itransition.com.wedge.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseModel implements Serializable {
    @JsonProperty
    private int code;
    @JsonProperty
    private boolean success;

    public int getCode() {
        return code;
    }

    public boolean isSuccess() {
        return success;
    }

}
