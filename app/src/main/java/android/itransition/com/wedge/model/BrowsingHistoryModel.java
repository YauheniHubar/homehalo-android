package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.Reports;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 13.09.2014.
 */
public class BrowsingHistoryModel extends BaseModel implements Parcelable {

    private boolean success;
    private int code;
    @JsonProperty("browsingRequests")
    private Reports[] browsingRequests;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean getSuccess(){
        return success;
    }

    public void setCode(int code){
        this.code = code;
    }

    public  int getCode(){
        return code;
    }

    public  void setBrowsingRequests(Reports[] history){
        this.browsingRequests = history;
    }

    public Reports[] getBrowsingRequests(){
        return browsingRequests;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelableArray(browsingRequests, 1);
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeInt(code);

    }

    public static final Creator<BrowsingHistoryModel> CREATOR = new Creator<BrowsingHistoryModel>() {

        public BrowsingHistoryModel createFromParcel(Parcel in) {

            return new BrowsingHistoryModel(in);
        }

        public BrowsingHistoryModel[] newArray(int size) {
            return new BrowsingHistoryModel[size];
        }
    };

    // constructor for reading data from Parcel
    public BrowsingHistoryModel(Parcel parcel) {
        success = parcel.readByte() == 1;
        code = parcel.readInt();
        browsingRequests= parcel.createTypedArray(Reports.CREATOR);

    }

    public BrowsingHistoryModel(){

    }


}
