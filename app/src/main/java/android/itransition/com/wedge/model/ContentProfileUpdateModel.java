package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.ContentProfile;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by y.drobysh on 16.12.2014.
 */
public class ContentProfileUpdateModel extends BaseModel {


    @JsonProperty
    private boolean success;
    @JsonProperty
    private int code;

    public ContentProfile getContentProfile() {
        return contentProfile;
    }

    @JsonProperty("contentProfile")
    private ContentProfile contentProfile;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean getSuccess(){
        return success;
    }

    public void setCode(int code){
        this.code = code;
    }

    public  int getCode(){
        return code;
    }
}
