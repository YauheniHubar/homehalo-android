package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.BrowsingHistory;
import android.itransition.com.wedge.entity.DevicesEntity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class DeviceModel extends  BaseModel {

    @JsonProperty("success")
    private boolean success;
    @JsonProperty("code")
    private int code;
    @JsonProperty("devices")
    private List<DevicesEntity> devices;
    @JsonProperty("device")
    private DevicesEntity device;
    @JsonProperty("summary")
    private BrowsingHistory[] summary;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean getSuccess(){
        return success;
    }

    public void setCode(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }

    public void setDevices(List<DevicesEntity> devices){
        this.devices = devices;
    }

    public List<DevicesEntity> getDevices(){
        return devices;
    }

    public void setDevice(DevicesEntity device){
        this.device = device;
    }

    public DevicesEntity  getDevice(){
        return device;
    }

    public void setSummary(BrowsingHistory[] summary){
        this.summary = summary;
    }

    public BrowsingHistory[]  getBrowsingTraffic(){
        return summary;
    }

}
