package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.DeviceOwner;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by p.gulevich on 15.07.2015.
 */
public class DeviceOwnerModel extends  BaseModel{
    @JsonProperty("success")
    private boolean success;
    @JsonProperty("code")
    private int code;
    @JsonProperty("deviceOwner")
    private DeviceOwner deviceOwner;

    public DeviceOwner getDeviceOwner() {
        return deviceOwner;
    }

    public void setDeviceOwner(DeviceOwner deviceOwner) {
        this.deviceOwner = deviceOwner;
    }

    @Override
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
