package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.DeviceOwner;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class DeviceOwnersModel extends  BaseModel implements Parcelable{

    @JsonProperty("success")
    private boolean success;
    @JsonProperty("code")
    private int code;
    @JsonProperty("deviceOwners")
    private List<DeviceOwner> deviceOwners;
    @JsonProperty("deviceOwner")
    private DeviceOwner deviceOwner;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean getSuccess(){
        return success;
    }

    public void setCode(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }

    public void setDeviceOwners(List<DeviceOwner> deviceOwners){
        this.deviceOwners = deviceOwners;
    }

    public List<DeviceOwner> getDeviceOwners(){
        return deviceOwners;
    }

    public void setDeviceOwner(DeviceOwner deviceOwner){
        this.deviceOwner = deviceOwner;
    }

    public DeviceOwner getDeviceOwner(){
        return deviceOwner;
    }



    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(code);
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeTypedList(deviceOwners);
        parcel.writeParcelable(deviceOwner, 1);
    }

    public static final Creator<DeviceOwnersModel> CREATOR = new Creator<DeviceOwnersModel>() {

        public DeviceOwnersModel createFromParcel(Parcel in) {

            return new DeviceOwnersModel(in);
        }

        public DeviceOwnersModel[] newArray(int size) {
            return new DeviceOwnersModel[size];
        }
    };

    // constructor for reading data from Parcel
    public DeviceOwnersModel(Parcel parcel) {
        success = parcel.readByte() == 1;
        code = parcel.readInt();
        deviceOwners = new ArrayList<>();
        parcel.readTypedList(deviceOwners, DeviceOwner.CREATOR);
        deviceOwner =(DeviceOwner) parcel.readParcelable(DeviceOwner.class.getClassLoader());
    }

    public DeviceOwnersModel(){

    }


    @Override
    public String toString() {
        if (deviceOwners != null) {
            return deviceOwners.toString();
        }
        if (deviceOwner != null) {
            return deviceOwner.toString();
        }
        return "success : " + success;
    }
}
