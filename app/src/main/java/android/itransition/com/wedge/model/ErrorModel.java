package android.itransition.com.wedge.model;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

/**
 * Created by y.drobysh on 11.12.2014.
 */
public class ErrorModel {

    @JsonProperty
    boolean success;

    @JsonProperty
    String code;

    List<String> messages;
    /*
    parse error messages
     */
    @JsonProperty("error")
    public void setMessages(Map<String, Object> foo) {
        try {
            if (foo != null) {
                messages = (List<String>) foo.get("messages");
            }
        } catch (ClassCastException ex) {
            Log.i("ErrorModel", "exception during category icon parse", ex);
        }
    }


    public boolean isSuccess() {
        return success;
    }

    public String getCode() {
        return code;
    }

    public List<String> getMessages() {
        return messages;
    }

}
