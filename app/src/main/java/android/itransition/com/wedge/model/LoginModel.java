package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.User;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 04.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginModel extends BaseModel {

    @JsonProperty("accessToken")
    private String accessToken;
    @JsonProperty("user")
    private User user;

    public void setAccessToken(String accessToken){
        this.accessToken = accessToken;
    }

    public  String getAccessToken(){
        return accessToken;
    }

    public void setUser(User user){
        this.user = user;
    }

    public  User getUser(){
        return user;
    }

    public LoginModel(){

    }

}
