package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.NotificationsEntity;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 04.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationModel extends BaseModel implements Parcelable {


    @JsonProperty("success")
    private boolean success;
//    @JsonProperty("accessToken")
//    private String accessToken;
    @JsonProperty("notifications")
    private NotificationsEntity[] notifications;


    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean getSuccess(){
        return success;
    }

//    public void setAccessToken(String accessToken){
//        this.accessToken = accessToken;
//    }
//
//    public  String getAccessToken(){
//        return accessToken;
//    }

    public void setNotifications(NotificationsEntity[] notifications){
        this.notifications = notifications;
    }

    public  NotificationsEntity[] getNotifications(){
        return notifications;
    }


    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
//        parcel.writeString(accessToken);
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeParcelableArray(notifications, 1);

    }

    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {

        public NotificationModel createFromParcel(Parcel in) {

            return new NotificationModel(in);
        }

        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };

    // constructor for reading data from Parcel
    public NotificationModel(Parcel parcel) {
        success = parcel.readByte() == 1;
//        accessToken = parcel.readString();
        parcel.readTypedArray(notifications, NotificationsEntity.CREATOR);
    }

    public NotificationModel(){

    }

}
