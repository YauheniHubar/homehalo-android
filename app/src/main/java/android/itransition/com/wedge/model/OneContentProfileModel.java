package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.ContentProfile;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 13.09.2014.
 */
public class OneContentProfileModel extends BaseModel implements Parcelable {
    private boolean success;
    private int code;
    @JsonProperty("contentProfile")
    private ContentProfile profiles;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean getSuccess(){
        return success;
    }

    public void setCode(int code){
        this.code = code;
    }

    public  int getCode(){
        return code;
    }

    public  void setProfiles(ContentProfile profiles){
        this.profiles = profiles;
    }

    public ContentProfile getProfiles(){
        return profiles;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(profiles, 1);
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeInt(code);

    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {

        public UserModel createFromParcel(Parcel in) {

            return new UserModel(in);
        }

        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    // constructor for reading data from Parcel
    public OneContentProfileModel(Parcel parcel) {
        success = parcel.readByte() == 1;
        code = parcel.readInt();
        profiles = parcel.readParcelable(ContentProfile.class.getClassLoader());

    }

    public OneContentProfileModel(){

    }


}
