package android.itransition.com.wedge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by i.grechishchev on 12.02.2016.
 * itransition 2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PopularBlacklist extends BaseModel {
    private int id;
    private String title;
    private String categoryName;
    private int categoryId;
    private String icon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
