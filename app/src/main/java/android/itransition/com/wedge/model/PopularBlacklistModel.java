package android.itransition.com.wedge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by i.grechishchev on 12.02.2016.
 * itransition 2016
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PopularBlacklistModel extends BaseModel {
    private List<PopularBlacklist> categories;

    public List<PopularBlacklist> getCategories() {
        return categories;
    }

    public void setCategories(List<PopularBlacklist> categories) {
        this.categories = categories;
    }

}
