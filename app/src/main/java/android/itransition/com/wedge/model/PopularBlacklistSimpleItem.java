package android.itransition.com.wedge.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by i.grechishchev on 16.02.2016.
 * itransition 2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PopularBlacklistSimpleItem extends BaseModel implements Parcelable {
    @JsonProperty
    private int contentCategory;
    @JsonProperty
    private int deviceOwner;
    @JsonProperty
    private boolean isParentBlocked;

    public int getContentCategory() {
        return contentCategory;
    }

    public void setContentCategory(int contentCategory) {
        this.contentCategory = contentCategory;
    }

    public int getDeviceOwner() {
        return deviceOwner;
    }

    public void setDeviceOwner(int deviceOwner) {
        this.deviceOwner = deviceOwner;
    }

    public boolean isParentBlocked() {
        return isParentBlocked;
    }

    public void setIsParentBlocked(boolean isParentBlocked) {
        this.isParentBlocked = isParentBlocked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(contentCategory);
        parcel.writeInt(deviceOwner);
    }

    public static final Creator<PopularBlacklistSimpleItem> CREATOR = new Creator<PopularBlacklistSimpleItem>() {

        public PopularBlacklistSimpleItem createFromParcel(Parcel in) {

            return new PopularBlacklistSimpleItem(in);
        }

        public PopularBlacklistSimpleItem[] newArray(int size) {
            return new PopularBlacklistSimpleItem[size];
        }
    };

    public PopularBlacklistSimpleItem(Parcel parcel) {
        contentCategory = parcel.readInt();
        deviceOwner = parcel.readInt();
    }

    public PopularBlacklistSimpleItem() {
        //Deafult constructor
    }
}
