package android.itransition.com.wedge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by i.grechishchev on 16.02.2016.
 * itransition 2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PopularBlacklistSimpleItemModel extends BaseModel{
    private List<PopularBlacklistSimpleItem> categories;

    public List<PopularBlacklistSimpleItem> getCategories() {
        return categories;
    }

    public void setCategories(List<PopularBlacklistSimpleItem> blacklistedCategories) {
        this.categories = blacklistedCategories;
    }
}
