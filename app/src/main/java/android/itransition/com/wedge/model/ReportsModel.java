package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.Reports;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by e.kazimirova on 05.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportsModel extends BaseModel implements Parcelable {

    private boolean success;
    private int code;
    @JsonProperty("browsingRequests")
    private Reports[] reports;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean getSuccess(){
        return success;
    }

    public void setCode(int code){
        this.code = code;
    }

    public  int getCode(){
        return code;
    }

    public  void setReports(Reports[] reports){
        this.reports = reports;
    }

    public Reports[] getReports(){
        return reports;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelableArray(reports, 1);
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeInt(code);

    }

    public static final Creator<ReportsModel> CREATOR = new Creator<ReportsModel>() {

        public ReportsModel createFromParcel(Parcel in) {

            return new ReportsModel(in);
        }

        public ReportsModel[] newArray(int size) {
            return new ReportsModel[size];
        }
    };

    // constructor for reading data from Parcel
    public ReportsModel(Parcel parcel) {
        success = parcel.readByte() == 1;
        code = parcel.readInt();
        reports=(Reports[]) parcel.readParcelableArray(Reports.class.getClassLoader());

    }

    public ReportsModel(){

    }


}
