package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.OwnerAccess;
import android.itransition.com.wedge.entity.TimeExtensions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by y.drobysh on 12.01.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeExtensionModel extends BaseModel {
    @JsonProperty
    public TimeExtensions timeExtension;
    @JsonProperty
    public OwnerAccess access;
}
