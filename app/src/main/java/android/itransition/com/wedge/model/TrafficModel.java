package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.TrafficEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by y.drobysh on 10.01.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrafficModel extends BaseModel {

    @JsonProperty
    public List<TrafficEntity> traffic;
}
