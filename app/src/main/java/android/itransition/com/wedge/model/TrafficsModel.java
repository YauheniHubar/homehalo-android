package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.TrafficsEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by i.grechishchev on 18.08.2015.
 * itransition 2015
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrafficsModel extends BaseModel{

    @JsonProperty
    public List<TrafficsEntity> traffics;

    public List<TrafficsEntity> getTraffics() {
        return traffics;
    }

    public void setTraffics(List<TrafficsEntity> traffics) {
        this.traffics = traffics;
    }
}
