package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.UrlTrafficEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by i.grechishchev on 30.07.2015.
 * itransition 2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlTrafficModel extends BaseModel{

    @JsonProperty
    public List<UrlTrafficEntity> results;

    public List<UrlTrafficEntity> getResults() {
        return results;
    }
}
