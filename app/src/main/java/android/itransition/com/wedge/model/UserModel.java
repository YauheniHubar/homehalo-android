package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.User;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * Created by e.kazimirova on 05.09.2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel extends BaseModel implements Parcelable {

    private boolean success;
    private int code;
    private User user;

    public void setSuccess(boolean success){
        this.success = success;
    }

    public boolean getSuccess(){
        return success;
    }

    public void setCode(int code){
        this.code = code;
    }

    public  int getCode(){
        return code;
    }

    public  void setUser(User user){
        this.user = user;
    }

    public User getUser(){
        return user;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(user, 1);
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeInt(code);

    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {

        public UserModel createFromParcel(Parcel in) {

            return new UserModel(in);
        }

        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    // constructor for reading data from Parcel
    public UserModel(Parcel parcel) {
        success = parcel.readByte() == 1;
        code = parcel.readInt();
        user = parcel.readParcelable(User.class.getClassLoader());

    }

    public UserModel(){

    }


}
