package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.UrlListEntity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class WhiteListModel extends BaseModel {

    @JsonProperty("urlWhitelist")
    private List<UrlListEntity> urls;



    public void setWhiteListUrl(List<UrlListEntity> urls){
        this.urls = urls;
    }

    public List<UrlListEntity> getWhiteListUrl(){
        return urls;
    }

}
