package android.itransition.com.wedge.model;

import android.itransition.com.wedge.entity.UrlListEntity;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by y.drobysh on 13.01.2015.
 */
public class WhiteUrlModel extends BaseModel {

    @JsonProperty("urlWhitelist")
    public UrlListEntity whiteUrl;
}
