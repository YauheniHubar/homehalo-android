package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.entity.ContentCategories;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by i.grechishchev on 02.08.2015.
 * itransition 2015
 */
public class AddCategoriesRequest extends BaseRequest<BaseModel>{

    private final String END_POINT = "/api/users/%d/device-owners/%d/add-categories";
    private long userId;
    private long deviceOwnerId;
    private ContentCategories[] categories;
    private HttpEntity<String> requestEntity;
    boolean remove;

    public AddCategoriesRequest(Context context, long deviceOwnerId, ContentCategories[] categories, boolean remove) {
        super(BaseModel.class, context);

        this.userId = Settings.getUserId();
        this.deviceOwnerId = deviceOwnerId;
        this.categories = categories;
        this.remove = remove;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());

        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", userId);
            jsonObject.put("deviceOwnerId", deviceOwnerId);
            jsonObject.put("remove", remove);

            JSONObject insideObject = new JSONObject();
            JSONArray array = new JSONArray();
            for (int i = 0; i < categories.length; i++) {
                insideObject = new JSONObject();
                insideObject.put("id", categories[i].getId());
                array.put(insideObject);
            }
            jsonObject.put("categories", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestEntity = new HttpEntity<>(jsonObject.toString(), httpHeaders);

        this.setRequestEntity(requestEntity);
    }



    @Override
    public BaseModel loadDataFromNetwork() throws Exception {
        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + String.format(END_POINT, userId, deviceOwnerId)).buildUpon();
        BaseModel model = makeRequest(HttpMethodEnum.post, uriBuilder, BaseModel.class, requestEntity);
        return model;
    }

    public  String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + String.format(END_POINT, userId, deviceOwnerId)).buildUpon().toString();
    }
}
