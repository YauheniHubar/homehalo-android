package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.model.DeviceOwnerModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by i.grechishchev on 15.04.2015.
 */
public class AddUserRequest extends BaseRequest<DeviceOwnerModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER = "/device-owners";
    private long id;
    private HttpEntity<String> requestEntity;

    public AddUserRequest(Context context, long id, String title, long contentProfile) {
        super(DeviceOwnerModel.class, context);

        this.id = id;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());

        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", title);
            jsonObject.put("status", "resume");
            jsonObject.put("contentProfile", contentProfile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestEntity = new HttpEntity<>(jsonObject.toString(), httpHeaders);

        this.setRequestEntity(requestEntity);
    }



    @Override
    public DeviceOwnerModel loadDataFromNetwork() throws Exception {
        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER).buildUpon();
        DeviceOwnerModel model = makeRequest(HttpMethodEnum.post, uriBuilder, DeviceOwnerModel.class, requestEntity);

        DeviceOwnersDataSource dataSource = new DeviceOwnersDataSource(WedgeApplication.mainSource);
        try {
            WedgeApplication.mainSource.database.beginTransaction();
            DeviceOwner owner = model.getDeviceOwner();
            int k = dataSource.getDeviceOwnersCount();
            dataSource.createDeviceOwner(owner, k);
            WedgeApplication.mainSource.database.setTransactionSuccessful();
            Settings.setNeedUpdateDeviceOwners(false);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            WedgeApplication.mainSource.database.endTransaction();
        }
        return model;
    }

    public  String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER).buildUpon().toString();
    }
}
