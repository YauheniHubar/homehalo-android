package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class AttachDeviceRequest extends BaseRequest<BaseModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_SECOND_PART="/wedge";
    private HttpEntity<String> requestEntity;
    private long userId;

    public AttachDeviceRequest(Context context, long userId, String serialNumber) {
        super(BaseModel.class, context);
        this.userId = userId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        JSONObject js = new JSONObject();
        try {
            js.put("serialNumber", serialNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestEntity = new HttpEntity<String>(js.toString(), httpHeaders);
        this.setRequestEntity(requestEntity);
    }


    @Override
    public BaseModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.URL_HOST + END_POINT+userId+END_POINT_SECOND_PART).buildUpon();
        return makeRequest(HttpMethodEnum.post, uriBuilder, BaseModel.class, requestEntity);
    }

    public String getUrl(){
        return Uri.parse(Settings.URL_HOST + END_POINT+userId+END_POINT_SECOND_PART).buildUpon().toString();
    }
}