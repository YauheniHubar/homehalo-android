package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.BuildConfig;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;
import android.util.Log;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.ankuda
 * Date: 18.10.13
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseRequest<RESULT> extends SpringAndroidSpiceRequest<RESULT> {


    public abstract String getUrl();
    public  String getRequestEntity(){
        return request.toString();
    }

    public String logData;

    public  void setRequestEntity(HttpEntity<?> request){
        this.request = request;
    }

    public enum HttpMethodEnum{
        post("post"), get("get"), put("put"), delete("delete"), auth("auth"), upload ("upload");

        String httpMethod;

        HttpMethodEnum(String arg) {
            if (arg.length() == 0) {
                this.httpMethod = "get";
            } else {
                this.httpMethod = arg;
            }
        }

        public String getHttpMethod() {
            return this.httpMethod;
        }
    }

    public Context getContext() {
        return context;
    }

    private Context context;


    public BaseRequest(Class<RESULT> clazz, Context context) {
        super(clazz);
        this.context = context;
    }




    private HttpEntity<?> request;
    protected RESULT makeRequest(HttpMethodEnum method, Uri.Builder uriBuilder, Class<RESULT> responseType, HttpEntity<?> request) {

        String url;

        getRestTemplate().setMessageConverters(getMessageConverters());

        url = uriBuilder.build().toString();
        this.request = request;

        if (Settings.LOG) {
            if (method != null)  Log.i("REQUEST", method.toString() + ":   " + url);
            if (request != null) Log.i("REQUEST", request.toString());
            logData = method != null ? method.toString() : ""
                    + ":   " + url + "\n" +
                    request != null ? request.toString() : "";
        }

        switch (method) {

            case get:
                return getForObject(url, request, responseType);
            case post:
                return postForObject(url, request, responseType);

            case delete:
                return deleteForObject(url, request, responseType);
            case put:
                return putForObject(url, request, responseType);
            case auth:
                return authGetForObject (url, request, responseType);
            default:
                return getForObject(url, request, responseType);
        }

    }

    public <RESULT> RESULT putForObject(String url, HttpEntity<?> request, Class<RESULT> responseType, Object... uriVariables) throws RestClientException {

        ResponseEntity<RESULT> response = getRestTemplate().exchange(url, HttpMethod.PUT,request, responseType);

        RESULT resultContainer = response.getBody();
        if (BuildConfig.DEBUG) {
            Log.d("Request response put", response.getBody().toString());
        }
        return resultContainer;
    }

    public <RESULT> RESULT postForObject(String url, HttpEntity<?> request, Class<RESULT> responseType, Object... uriVariables) throws RestClientException {
//        Log.e("REQUEST!!!!", request.toString());
        ResponseEntity<RESULT> response = getRestTemplate().exchange(url, HttpMethod.POST, request, responseType);

        RESULT resultContainer = response.getBody();


        return resultContainer;
    }

    public <RESULT> RESULT deleteForObject(String url, HttpEntity<?> request, Class<RESULT> responseType, Object... uriVariables) throws RestClientException {

       ResponseEntity<RESULT> response = getRestTemplate().exchange(url, HttpMethod.DELETE,request, responseType);

        RESULT resultContainer = response.getBody();


        return resultContainer;
    }



    public <RESULT> RESULT getForObject(String url, HttpEntity<?> request, Class<RESULT> responseType, Object... uriVariables) throws RestClientException {

        Log.d("ololo", "trololo");
        ResponseEntity<RESULT> response = getRestTemplate().exchange(url, HttpMethod.GET, request, responseType);

        if (BuildConfig.DEBUG) {
            Log.d("Request response get", response.getBody().toString());
        }

        return response.getBody();
    }

    public <RESULT> RESULT authGetForObject(String url, HttpEntity<?> request, Class<RESULT> responseType) throws RestClientException {

        ResponseEntity<RESULT> response = getRestTemplate().exchange(url, HttpMethod.GET, request, responseType);


        RESULT resultContainer = response.getBody();

        return resultContainer;
    }




    protected List<HttpMessageConverter<?>> getMessageConverters() {
        List<MediaType> types = new ArrayList<MediaType>();

        types.add(new MediaType("text", "html", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
        types.add(new MediaType("application", "json", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
        types.add(new MediaType("multipart","form-data", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
        types.add(new MediaType("application", "octet-stream", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));

        MappingJackson2HttpMessageConverter gsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        gsonHttpMessageConverter.setSupportedMediaTypes(types);

        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
        stringHttpMessageConverter.setSupportedMediaTypes(types);

        ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
        byteArrayHttpMessageConverter.setSupportedMediaTypes(types);

        List<HttpMessageConverter<?>> list = new ArrayList<HttpMessageConverter<?>>();
        list.add(new StringHttpMessageConverter());
        list.add(gsonHttpMessageConverter);
        list.add( byteArrayHttpMessageConverter);
        list.add(new FormHttpMessageConverter());
        return list;
    }

}
