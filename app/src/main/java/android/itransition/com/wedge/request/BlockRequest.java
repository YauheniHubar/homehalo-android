package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by y.drobysh on 23.01.2015.
 */
public class BlockRequest extends BaseRequest<DeviceOwnersModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER = "/device-owners/";
    private long idDeviceOwner;
    private HttpEntity<String> requestEntity;

    public BlockRequest(Context context, long idDeviceOwner, String title, long time,
                        CreateTimeUser blockStartedAt, long contentProfileId,
                        TimeProfiles[] timeProfileses, CreateTimeUser createdAt,
                        CreateTimeUser homeworkStartedAt, Long homeworkModeDuration, HomeWorkCategories[] homeWorkCategorieses) {
        super(DeviceOwnersModel.class, context);

        this.idDeviceOwner = idDeviceOwner;
        DeviceOwnersDataSource dataSource = new DeviceOwnersDataSource(WedgeApplication.mainSource);
        DeviceOwner owner = dataSource.getDeviceOwnerById(idDeviceOwner);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("id", idDeviceOwner);
            jsonObject.put("title", title);
            jsonObject.put("status", time > 0 ? "suspend" : "resume");

            JSONObject insideObject = new JSONObject();
            if (createdAt != null) {
                insideObject = new JSONObject();
                insideObject.put("dateTime", createdAt.getDateTime());
                insideObject.put("timezone", createdAt.getTimeZone());
            }
            jsonObject.put("createdAt", insideObject);

            jsonObject.put("time", time);
            jsonObject.put("blockStartedAt", blockStartedAt.getDateTime());

            jsonObject.put("contentProfile", contentProfileId);

            JSONArray array = new JSONArray();
            for (int i = 0; i < timeProfileses.length; i++) {
                insideObject = new JSONObject();
                insideObject.put("startTime", timeProfileses[i].getStartTime().getTime());
                insideObject.put("endTime", timeProfileses[i].getEndTime().getTime());
                insideObject.put("dayOfWeek", timeProfileses[i].getDayOfWeek());
                array.put(insideObject);
            }
            jsonObject.put("timeProfiles", array);

            if(owner.getHomeworkModeDuration()!=null) {
                jsonObject.put("homeworkModeStartedAt", homeworkStartedAt.getDateTime());
                jsonObject.put("homeworkModeDuration", homeworkModeDuration);
            }
            JSONArray categories = new JSONArray();
            for (int i = 0; i < homeWorkCategorieses.length; i++) {
                insideObject = new JSONObject();
//                insideObject.put("id",homeWorkCategorieses[i].getId());
//                insideObject.put("categoryName", homeWorkCategorieses[i].getCategoryName());
                insideObject.put("categoryId", homeWorkCategorieses[i].getCategoryId());
                insideObject.put("title", homeWorkCategorieses[i].getTitle());
                categories.put(insideObject);
            }
            jsonObject.put("homeworkCategories", categories);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = jsonObject.toString();
        Log.e("grch11","Block request: "+request);
        requestEntity = new HttpEntity<String>(request, httpHeaders);
        this.setRequestEntity(requestEntity);

    }

    @Override
    public String getUrl() {
        return null;
    }

    @Override
    public DeviceOwnersModel loadDataFromNetwork() throws Exception {
        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT + Settings.getUserId()
                + END_POINT_AFTER + idDeviceOwner).buildUpon();
        DeviceOwnersModel model = makeRequest(HttpMethodEnum.put, uriBuilder, DeviceOwnersModel.class, requestEntity);
        Log.d("grch11","device owner: "+model.getDeviceOwner().getTitle() + ", hwDur: " + model.getDeviceOwner().getHomeworkModeDuration());

        return model;
    }
}
