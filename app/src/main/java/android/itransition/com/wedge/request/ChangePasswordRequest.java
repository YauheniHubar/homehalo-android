package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class ChangePasswordRequest extends BaseRequest<BaseModel>{

    private final String END_POINT = "/api/users/";
    private final String AFTER = "/password";
    private long userId;
    private HttpEntity<String> requestEntity;

    public ChangePasswordRequest(Context context, long userId, String currentPass, String password) {
        super(BaseModel.class, context);

        this.userId = userId;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("currentPassword", currentPass);
            jsonObject.put("password", password);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = jsonObject.toString();
        request = request.replaceAll("\\\\/", "/");
        requestEntity = new HttpEntity<String>(request, httpHeaders);this.setRequestEntity(requestEntity);
    }

    @Override
    public BaseModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.URL_HOST + END_POINT+userId+
        AFTER).buildUpon();
        return makeRequest(HttpMethodEnum.put, uriBuilder, BaseModel.class, requestEntity);
    }



    public String getUrl(){
        return  Uri.parse(Settings.URL_HOST + END_POINT).buildUpon().toString();
    }
}