package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.ContentProfileDataSource;
import android.itransition.com.wedge.database.datesource.MainSource;
import android.itransition.com.wedge.entity.ContentProfile;
import android.itransition.com.wedge.model.ContentProfilesModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;


public class ContentProfilesRequest extends BaseRequest<ContentProfilesModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER = "/content-profiles";
    private long id;
    private HttpEntity<String> requestEntity;

    public ContentProfilesRequest(Context context, long id) {
        super(ContentProfilesModel.class, context);

        this.id = id;


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);

    }



    @Override
    public ContentProfilesModel loadDataFromNetwork() throws Exception {
        this.setRequestEntity(requestEntity);
        Uri.Builder uriBuilder = Uri.parse(Settings.URL_HOST + END_POINT+id+END_POINT_AFTER).buildUpon();
        ContentProfilesModel model = makeRequest(HttpMethodEnum.get, uriBuilder, ContentProfilesModel.class, requestEntity);

        MainSource source = WedgeApplication.mainSource;
        try {
            ContentProfileDataSource dataSource = new ContentProfileDataSource(source);
            source.database.beginTransaction();
            dataSource.deleteContentProfileAll();
            ContentProfile[] list = model.getProfiles();
            for(int i=0; i< list.length; i++) {
                dataSource.createContentProfile(list[i]);
            }
            source.database.setTransactionSuccessful();
            Settings.setNeedUpdateContentProfile(false);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            source.database.endTransaction();
        }

        return model;
    }

    public String getUrl(){
        return Uri.parse(Settings.URL_HOST + END_POINT+id+END_POINT_AFTER).buildUpon().toString();
    }
}