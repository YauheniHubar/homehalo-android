package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.OneContentProfileModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class CreateContentProfileRequest extends BaseRequest<OneContentProfileModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_SECOND_PART="/content-profiles";
    private HttpEntity<String> requestEntity;
    private long userId;
    private long deviceId;


    public CreateContentProfileRequest(Context context, String title, long userId) {
        super(OneContentProfileModel.class, context);
        this.userId = userId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        JSONObject object = new JSONObject();
        try {
            object.put("title", title);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestEntity = new HttpEntity<String>(object.toString(), httpHeaders);
        this.setRequestEntity(requestEntity);
    }



    @Override
    public OneContentProfileModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId+END_POINT_SECOND_PART).buildUpon();
        return makeRequest(HttpMethodEnum.post, uriBuilder, OneContentProfileModel.class, requestEntity);
    }

    public String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId+END_POINT_SECOND_PART+deviceId).buildUpon().toString();
    }
}