package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.DeviceOwnerModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by p.gulevich on 22.07.2015.
 */
public class DeleteDeviceOwnerRequest extends BaseRequest<DeviceOwnerModel> {
    private final String END_POINT = "/api/users/%d/device-owners/%d";

    private long mOwnerId;

    public DeleteDeviceOwnerRequest(Context context, long ownerId) {
        super(DeviceOwnerModel.class, context);
        this.mOwnerId = ownerId;
    }

    @Override
    public String getUrl() {
        return Uri.parse(Settings.getUrlHost() + String.format(END_POINT, Settings.getUserId(), mOwnerId)).buildUpon().toString();
    }

    @Override
    public DeviceOwnerModel loadDataFromNetwork() throws Exception {
        HttpEntity<String> requestEntity = makeRequest();

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost() + String.format(END_POINT, Settings.getUserId(), mOwnerId)).buildUpon();
        return makeRequest(HttpMethodEnum.delete, uriBuilder, DeviceOwnerModel.class, requestEntity);
    }

    private HttpEntity<String> makeRequest() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(httpHeaders);
    }
}
