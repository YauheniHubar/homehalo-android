package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.DeviceModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class DeviceDeleteRequest extends BaseRequest<DeviceModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_SECOND_PART="/devices/";
    private HttpEntity<String> requestEntity;
    private long userId;
    private long deviceId;

    public DeviceDeleteRequest(Context context, long userId, long deviceId) {
        super(DeviceModel.class, context);
        this.userId = userId;
        this.deviceId = deviceId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }


    @Override
    public DeviceModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.URL_HOST + END_POINT+userId+END_POINT_SECOND_PART+deviceId).buildUpon();
        return makeRequest(HttpMethodEnum.delete, uriBuilder, DeviceModel.class, requestEntity);
    }

    public String getUrl(){
        return Uri.parse(Settings.URL_HOST + END_POINT+userId+END_POINT_SECOND_PART+deviceId).buildUpon()
                .toString();
    }
}