package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.List;

/**
 * Created by y.drobysh on 16.02.2015.
 */
public class DeviceOwnersInfoRequest extends BaseRequest<DeviceOwnersModel> {

    public DeviceOwnersInfoRequest(Context context) {
        super(DeviceOwnersModel.class, context);
    }

    private final String END_POINT = "/api/users/%d/device-owners?short=1";

    @Override
    public DeviceOwnersModel loadDataFromNetwork() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(httpHeaders);

        String url = Settings.getUrlHost().trim() + String.format(END_POINT, Settings.getUserId());

        Uri.Builder uriBuilder = Uri.parse(url).buildUpon();
        DeviceOwnersModel model = makeRequest(HttpMethodEnum.get, uriBuilder, DeviceOwnersModel.class, requestEntity);


        try {
            DeviceOwnersDataSource dataSource = new DeviceOwnersDataSource(WedgeApplication.mainSource);
            WedgeApplication.mainSource.database.beginTransaction();
            List<DeviceOwner> owners = model.getDeviceOwners();
            for (int k = 0; k < owners.size(); k++) {
                dataSource.updateDeviceOwner(owners.get(k));
            }
            model.setDeviceOwners(dataSource.getAllDeviceOwners());
            WedgeApplication.mainSource.database.setTransactionSuccessful();
            Settings.setNeedUpdateDeviceOwners(false);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            WedgeApplication.mainSource.database.endTransaction();
        }

        return model;
    }

    public String getUrl() {
        return Uri.parse(Settings.getUrlHost().trim() + String.format(END_POINT, Settings.getUserId())).buildUpon().toString();
    }
}
