package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.List;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class DeviceOwnersRequest extends BaseRequest<DeviceOwnersModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER = "/device-owners";
    private boolean fromCache;

    public DeviceOwnersRequest(Context context) {
        super(DeviceOwnersModel.class, context);
    }

    public DeviceOwnersRequest(Context context, boolean fromCache) {
        super(DeviceOwnersModel.class, context);
        this.fromCache = fromCache;
    }

    @Override
    public DeviceOwnersModel loadDataFromNetwork() throws Exception {

        DeviceOwnersDataSource dataSource = new DeviceOwnersDataSource(WedgeApplication.mainSource);

        DeviceOwnersModel model = null;
        List<DeviceOwner> cachedOwners = null;
        if (fromCache) {
            cachedOwners = dataSource.getAllDeviceOwners();

            model = new DeviceOwnersModel();
            model.setSuccess(true);
            model.setDeviceOwners(cachedOwners);
        }

        if (!fromCache || cachedOwners.size() == 0) {
            HttpEntity<String> requestEntity = makeRequest();

            Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost() + END_POINT + Settings.getUserId() + END_POINT_AFTER).buildUpon();
            model = makeRequest(HttpMethodEnum.get, uriBuilder, DeviceOwnersModel.class, requestEntity);

            try {

                WedgeApplication.mainSource.database.beginTransaction();
                List<DeviceOwner> owners = model.getDeviceOwners();
                dataSource.deleteDeviceOwnerAll();
                for (int k = 0; k < owners.size(); k++) {
                    dataSource.createDeviceOwner(owners.get(k), k);
                }
                WedgeApplication.mainSource.database.setTransactionSuccessful();
                Settings.setNeedUpdateDeviceOwners(false);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                WedgeApplication.mainSource.database.endTransaction();
            }
        }

        return model;
    }

    private HttpEntity<String> makeRequest() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(httpHeaders);
    }

    public String getUrl() {
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT + Settings.getUserId() + END_POINT_AFTER).buildUpon().toString();
    }
}