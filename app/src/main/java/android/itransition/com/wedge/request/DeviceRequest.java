package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceDataSource;
import android.itransition.com.wedge.model.DeviceModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class DeviceRequest extends BaseRequest<DeviceModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_SECOND_PART="/devices";
    private HttpEntity<String> requestEntity;

    public DeviceRequest(Context context) {
        super(DeviceModel.class, context);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }

    @Override
    public DeviceModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT + Settings.getUserId() + END_POINT_SECOND_PART).buildUpon();
        DeviceModel result = makeRequest(HttpMethodEnum.get, uriBuilder, DeviceModel.class, requestEntity);

        try {
            DeviceDataSource datasource = new DeviceDataSource(WedgeApplication.mainSource);
            datasource.open();
            WedgeApplication.mainSource.database.beginTransaction();
            datasource.deleteDevicesAll();
            for (int i = 0; i < result.getDevices().size(); i++) {
                datasource.createDevices(result.getDevices().get(i));
            }
            WedgeApplication.mainSource.database.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            WedgeApplication.mainSource.database.endTransaction();
        }
        return result;
    }

    public String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT + Settings.getUserId() + END_POINT_SECOND_PART).buildUpon().toString();
    }
}