package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceDataSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.model.DeviceModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class DeviceUpdateRequest extends BaseRequest<DeviceModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_SECOND_PART="/devices/";
    private HttpEntity<String> requestEntity;
    private long userId;
    private long deviceId;

    public DeviceUpdateRequest(Context context, long userId, long deviceId,
                               String title, boolean isEnabled, boolean isFavourite,
                               boolean isOnline, String macAddress,
                               CreateTimeUser createTimeUser,
                               long deviceOwner) {
        super(DeviceModel.class, context);
        this.userId = userId;
        this.deviceId = deviceId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        JSONObject object = new JSONObject();
        try {
            object.put("id", deviceId);
            object.put("title", title);
            object.put("isEnabled", isEnabled);
            object.put("isOnline", isOnline);
            object.put("isFavorite", isFavourite);
            object.put("macAddress", macAddress);
            JSONObject objectInside = new JSONObject();
            if(createTimeUser!=null) {
                objectInside.put("dateTime", createTimeUser.getDateTime());
                objectInside.put("timezone", createTimeUser.getTimeZone());
            }
            object.put("createdAt", objectInside);
            object.put("deviceOwner", deviceOwner);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestEntity = new HttpEntity<String>(object.toString(), httpHeaders);
        this.setRequestEntity(requestEntity);
    }



    @Override
    public DeviceModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId+END_POINT_SECOND_PART+deviceId).buildUpon();
        DeviceModel deviceModel = makeRequest(HttpMethodEnum.put, uriBuilder, DeviceModel.class, requestEntity);

        DeviceDataSource dataSource = new DeviceDataSource(WedgeApplication.mainSource);

        if (deviceModel.getDevice() != null) dataSource.updateDevice(deviceModel.getDevice());

        return deviceModel;
    }

    public String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId+END_POINT_SECOND_PART+deviceId).buildUpon().toString();
    }
}