package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.TimeExtensionModel;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Date;

/**
 * Created by y.drobysh on 20.01.2015.
 */
public class GetAccessByExtension extends BaseRequest<TimeExtensionModel> {

    private final String END_POINT = "/api/users/%d/device-owners/%d/access";
    private final Date startedAt;
    private final long duration, terId, deviceOwnerId;

    public GetAccessByExtension(Context context, long deviceOwnerId, long duration, long terId, Date startedAt) {
        super(TimeExtensionModel.class, context);
        this.duration = duration;
        this.terId = terId;
        this.deviceOwnerId = deviceOwnerId;
        this.startedAt = startedAt;
    }

    @Override
    public String getUrl() {
        return null;
    }

    @Override
    public TimeExtensionModel loadDataFromNetwork() throws Exception {
        StringBuilder url = new StringBuilder(String.format(END_POINT, Settings.getUserId(), deviceOwnerId));
        url.append("?duration=").append(duration);
        url.append("&terId=").append(terId);
        url.append("&status=").append("accepted");


        if (startedAt != null) {
            String format = Utils.API_DATE_FORMAT.format(startedAt);
            url.append("&startedAt=").append(format);
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(httpHeaders);

        Uri.Builder builder = Uri.parse(Settings.getUrlHost().trim()
                + url.toString()).buildUpon();

        TimeExtensionModel model = makeRequest(HttpMethodEnum.get, builder, TimeExtensionModel.class, requestEntity);

        return model;
    }
}
