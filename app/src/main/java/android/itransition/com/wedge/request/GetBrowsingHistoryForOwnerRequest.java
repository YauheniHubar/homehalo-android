package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.TrafficModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by y.drobysh on 10.01.2015.
 */
public class GetBrowsingHistoryForOwnerRequest extends BaseRequest<TrafficModel> {
    private final String END_POINT = "/api/users/%d/device-owners/%d/browsing-traffic?time=%d";
    private final long deviceOwnerId;
    private long sec;

    public GetBrowsingHistoryForOwnerRequest(Context context, long deviceOwnerId, long sec) {
        super(TrafficModel.class, context);
        this.deviceOwnerId = deviceOwnerId;
        this.sec = sec;
    }

    @Override
    public String getUrl() {
        return null;
    }

    @Override
    public TrafficModel loadDataFromNetwork() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() +
                String.format(END_POINT, Settings.getUserId(), deviceOwnerId, sec)).buildUpon();

        TrafficModel result = makeRequest(HttpMethodEnum.get, uriBuilder, TrafficModel.class, new HttpEntity<String>(httpHeaders));

        return result;
    }
}
