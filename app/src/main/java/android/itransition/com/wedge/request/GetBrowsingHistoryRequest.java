package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.DeviceModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class GetBrowsingHistoryRequest extends BaseRequest<DeviceModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_SECOND_PART="/device-owner-browsing-summary";
    private final String QUERY_DATE="time";
    private long seconds;
    private HttpEntity<String> requestEntity;
    private long userId;

    public GetBrowsingHistoryRequest(Context context, long userId, long sec) {
        super(DeviceModel.class, context);
        this.userId = userId;
        this.seconds = sec;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);


        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }


    @Override
    public DeviceModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId+END_POINT_SECOND_PART
         + "?" + QUERY_DATE + "=" + seconds).buildUpon();
        return makeRequest(HttpMethodEnum.get, uriBuilder, DeviceModel.class, requestEntity);
    }

    public String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId+END_POINT_SECOND_PART).buildUpon().toString();
    }
}