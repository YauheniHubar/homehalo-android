package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.CategoriesContentProfilesModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class GetContentCategoriesRequest extends BaseRequest<CategoriesContentProfilesModel>{

    private final String END_POINT = "/api/content-categories";
    private HttpEntity<String> requestEntity;

    public GetContentCategoriesRequest(Context context) {
        super(CategoriesContentProfilesModel.class, context);


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }

    @Override
    public CategoriesContentProfilesModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon();
        return makeRequest(HttpMethodEnum.get, uriBuilder, CategoriesContentProfilesModel.class, requestEntity);
    }



    public String getUrl(){
        return  Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon().toString();
    }
}