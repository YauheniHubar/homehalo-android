package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.PopularBlacklistModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by i.grechishchev on 12.02.2016.
 * itransition 2016
 */
public class GetPopularBlacklistRequest extends BaseRequest<PopularBlacklistModel> {
    private final String END_POINT = "/api/custom-content-categories";

    public GetPopularBlacklistRequest(Context context) {
        super(PopularBlacklistModel.class, context);
    }


    @Override
    public PopularBlacklistModel loadDataFromNetwork() throws Exception {
        HttpEntity<String> requestEntity = makeRequest();

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost() + END_POINT).buildUpon();
        return makeRequest(BaseRequest.HttpMethodEnum.get, uriBuilder, PopularBlacklistModel.class, requestEntity);
    }

    private HttpEntity<String> makeRequest() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(httpHeaders);
    }

    @Override
    public String getUrl() {
        return Uri.parse(END_POINT).buildUpon().toString();
    }
}
