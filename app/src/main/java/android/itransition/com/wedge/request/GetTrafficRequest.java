package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.TrafficsModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by i.grechishchev on 18.08.2015.
 * itransition 2015
 */
public class GetTrafficRequest extends BaseRequest<TrafficsModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_SECOND_PART="/device-owners/browsing-traffics";
    private final String QUERY_DATE="time";
    private long seconds;
    private HttpEntity<String> requestEntity;
    private long userId;

    public GetTrafficRequest(Context context, long userId, long sec) {
        super(TrafficsModel.class, context);
        this.userId = userId;
        this.seconds = sec;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);


        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }


    @Override
    public TrafficsModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId+END_POINT_SECOND_PART
                + "?" + QUERY_DATE + "=" + seconds).buildUpon();
        return makeRequest(HttpMethodEnum.get, uriBuilder, TrafficsModel.class, requestEntity);
    }

    public String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId+END_POINT_SECOND_PART).buildUpon().toString();
    }
}
