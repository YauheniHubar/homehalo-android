package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.UrlTrafficModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by i.grechishchev on 30.07.2015.
 * itransition 2015
 */
public class GetUrlsBrowsingTrafficRequest extends BaseRequest<UrlTrafficModel> {
    private final String END_POINT = "/api/users/%d/device-owners/%d/urls-browsing-traffic?period=%d&status=";

    private long mOwnerId;
    private int period;
    private String status;

    public GetUrlsBrowsingTrafficRequest(Context context, long ownerId, int period, String status) {
        super(UrlTrafficModel.class, context);
        this.mOwnerId = ownerId;
        this.period = period;
        this.status = status;
    }

    @Override
    public UrlTrafficModel loadDataFromNetwork() throws Exception {

        HttpEntity<String> requestEntity = makeRequest();

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost() + String.format(END_POINT, Settings.getUserId(), mOwnerId, period) + status).buildUpon();
        return makeRequest(BaseRequest.HttpMethodEnum.get, uriBuilder, UrlTrafficModel.class, requestEntity);

    }

    private HttpEntity<String> makeRequest() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(httpHeaders);
    }

    @Override
    public String getUrl() {
        return Uri.parse(Settings.getUrlHost() + String.format(END_POINT, Settings.getUserId(), mOwnerId, period, status)).buildUpon().toString();
    }
}
