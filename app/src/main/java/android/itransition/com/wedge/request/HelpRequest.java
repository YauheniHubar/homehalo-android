package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;


public class HelpRequest extends BaseRequest<String>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER = "/help-requests";
    private long id;
    private HttpEntity<String> requestEntity;

    public HelpRequest(Context context, long id, String message, String email) {
        super(String.class, context);

        this.id = id;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());

        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("message", message);
            jsonObject.put("email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestEntity = new HttpEntity<String>(jsonObject.toString(), httpHeaders);

        this.setRequestEntity(requestEntity);
    }



    @Override
    public String loadDataFromNetwork() throws Exception {
        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER).buildUpon();
        return makeRequest(HttpMethodEnum.post, uriBuilder, String.class, requestEntity);
    }

    public  String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER).buildUpon().toString();
    }
}