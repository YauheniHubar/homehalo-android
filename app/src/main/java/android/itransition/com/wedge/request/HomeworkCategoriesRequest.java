package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.HomeworkCategoriesModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class HomeworkCategoriesRequest extends BaseRequest<HomeworkCategoriesModel>{

    private final String END_POINT = "/api/homework-categories";
    private HttpEntity<String> requestEntity;
    private long userId;
    private long deviceOwnerId;
    private int id=0;


    public HomeworkCategoriesRequest(Context context) {
        super(HomeworkCategoriesModel.class, context);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);


        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);

    }



    @Override
    public HomeworkCategoriesModel loadDataFromNetwork() throws Exception {
            Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT)
                    .buildUpon();
            return makeRequest(HttpMethodEnum.get, uriBuilder, HomeworkCategoriesModel.class, requestEntity);

    }

    public String getUrl(){

            return Uri.parse(Settings.getUrlHost().trim() + END_POINT)
                    .buildUpon().toString();


    }
}