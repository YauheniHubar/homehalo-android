package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.model.LoginModel;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.GcmHelper;
import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class LoginRequest extends BaseRequest<LoginModel>{

    private final String END_POINT = "/api/authentication";
    private String userName;
    private String password;
    private final String type;
    private boolean delete;
    private HttpEntity<String> requestEntity;

    public LoginRequest(Context context, String userName, String password,
                        String type, boolean delete) {
        super(LoginModel.class, context);
        Log.e("QA", "LoginRequest");
        this.userName = userName;
        this.password = password;
        this.type = type;
        this.delete = delete;
    }

    @Override
    public LoginModel loadDataFromNetwork() throws Exception {
        Log.e("QA", "loadDataFromNetwork");
        HttpHeaders httpHeaders = new HttpHeaders();
        JSONObject jsonObject = new JSONObject();
        if(!delete) {
            String push = GcmHelper.getRegistrationId(getContext());
            if (push == null) {
                push = GcmHelper.registerDevice(getContext(), 1);
            }
            if (push == null)
                throw new RuntimeException("The google services aren't available. Please, try again later");



            try {
                httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
                httpHeaders.set("Connection", "Close");
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);

                jsonObject.put("deviceType", type);
                jsonObject.put("deviceToken", push);
                jsonObject.put("username", userName);
                jsonObject.put("password", password);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
            httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
            httpHeaders.set("Accept-Language", Settings.getLanguage());
            httpHeaders.set("X-Mobile-App", Settings.getPlatform());
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        }

        if(!delete) {
            requestEntity = new HttpEntity<String>(jsonObject.toString(), httpHeaders);
        } else {
            requestEntity = new HttpEntity<String>(httpHeaders);
        }
        this.setRequestEntity(requestEntity);


        if(!delete) {
            Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon();
            LoginModel loginModel = makeRequest(HttpMethodEnum.post, uriBuilder, LoginModel.class, requestEntity);
            WedgeApplication.currentUser = loginModel.getUser();
            return loginModel;
        } else {
            WedgeApplication.currentUser = null;
            Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon();
            return makeRequest(HttpMethodEnum.delete, uriBuilder, LoginModel.class, requestEntity);
        }
    }

    public String getUrl(){
        if(!delete) {
            return Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon().toString();
        } else {
            return Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon().toString();
        }
    }
}
