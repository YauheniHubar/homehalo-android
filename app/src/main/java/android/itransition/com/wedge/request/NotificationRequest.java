package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.NotificationModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class NotificationRequest extends BaseRequest<NotificationModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER="/notifications?isViewed=0";
    private HttpEntity<String> requestEntity;
    private long id;

    public NotificationRequest(Context context, long id) {
        super(NotificationModel.class, context);

        this.id = id;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }

    @Override
    public NotificationModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER).buildUpon();
        return makeRequest(HttpMethodEnum.get, uriBuilder, NotificationModel.class, requestEntity);
    }

    public String getUrl(){
        return  Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER).buildUpon().toString();
    }
}