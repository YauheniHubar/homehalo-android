package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.ReportsModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class ReportsRequest extends BaseRequest<ReportsModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER="/devices/";
    private final String END_POINT_AFTER_BROWSING_HISTORY = "/browsing-requests?page=1&limit=30";
    private HttpEntity<String> requestEntity;//http://wedge.demohoster.com/api/users/2/devices/62/browsing-requests?page=1&limit=30
    private long id;
    private long deviceId;

    public ReportsRequest(Context context, long id, long deviceId) {
        super(ReportsModel.class, context);

        this.id = id;
        this.deviceId = deviceId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);

        this.setRequestEntity(requestEntity);
    }


    @Override
    public ReportsModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER+
            deviceId+END_POINT_AFTER_BROWSING_HISTORY).buildUpon();
        return makeRequest(HttpMethodEnum.get, uriBuilder, ReportsModel.class, requestEntity);
    }

    public String getUrl(){
        return  Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER+
                deviceId+END_POINT_AFTER_BROWSING_HISTORY).buildUpon().toString();
    }
}