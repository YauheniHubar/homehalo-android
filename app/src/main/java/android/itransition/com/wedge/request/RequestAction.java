package android.itransition.com.wedge.request;


import android.itransition.com.wedge.model.BaseModel;

/**
 * Created with IntelliJ IDEA.
 * User: s.ankuda
 * Date: 29.11.13
 * Time: 12:27
 * To change this template use File | Settings | File Templates.
 */
public interface RequestAction {

    public void updateViewAfterSuccessfulAction(BaseModel model);
    public void performFailRequestAction(int action);

}