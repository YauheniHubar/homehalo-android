package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class ResetRequest extends BaseRequest<BaseModel>{

    private final String END_POINT = "/api/password-reset";
    private HttpEntity<String> requestEntity;

    public ResetRequest(Context context, String email) {
        super(BaseModel.class, context);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestEntity = new HttpEntity<String>(jsonObject.toString(), httpHeaders);

        this.setRequestEntity(requestEntity);
    }



    @Override
    public BaseModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon();
        return makeRequest(HttpMethodEnum.post, uriBuilder, BaseModel.class, requestEntity);
    }
    public  String getUrl(){
        return  Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon().toString();
    }
}