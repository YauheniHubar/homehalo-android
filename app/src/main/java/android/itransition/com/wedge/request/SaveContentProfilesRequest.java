package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.model.ContentProfileUpdateModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.ArrayList;


public class SaveContentProfilesRequest extends BaseRequest<ContentProfileUpdateModel>{
    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER = "/content-profiles/";
    private long id;
    private long contentId;
    private HttpEntity<String> requestEntity;

    public SaveContentProfilesRequest(Context context, long userId, long contentId, String title,
                                      boolean isDefault, boolean isDeletable, CreateTimeUser createdAt,
                                      ArrayList<Integer> contentCategories) {
        super(ContentProfileUpdateModel.class, context);

        this.id = userId;
        this.contentId = contentId;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());

        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        JSONObject js = new JSONObject();
        try {
            js.put("id", contentId);
            js.put("title", title);
            js.put("isDefault", isDefault);
            js.put("isDeletable", isDeletable);
            JSONObject insideJs = new JSONObject();
            insideJs.put("dateTime", createdAt.getDateTime());
            insideJs.put("timezone", createdAt.getTimeZone());
            js.put("createdAt", insideJs);
            JSONArray array = new JSONArray();
            for(int i=0; i<contentCategories.size(); i++) {
                insideJs = new JSONObject();
                insideJs.put("categoryId", contentCategories.get(i));
                array.put(insideJs);
            }
            js.put("contentCategories", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String request = js.toString();
        request = request.replaceAll("\\\\/", "/");

        requestEntity = new HttpEntity<String>(request, httpHeaders);
        this.setRequestEntity(requestEntity);
    }



    @Override
    public ContentProfileUpdateModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+
                END_POINT_AFTER+contentId).buildUpon();
        return makeRequest(HttpMethodEnum.put, uriBuilder, ContentProfileUpdateModel.class, requestEntity);
    }

    public  String getUrl(){
        return   Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+
                END_POINT_AFTER+contentId).buildUpon().toString();
    }
}