package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.PopularBlacklistSimpleItemModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.List;

/**
 * Created by i.grechishchev on 12.02.2016.
 * itransition 2016
 */
public class SendBlacklistCategoriesRequest extends BaseRequest<PopularBlacklistSimpleItemModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_MIDDLE = "/device-owners/";
    private final String END_POINT_END = "/blacklisted-categories";
    private long idDeviceOwner;
    private HttpEntity<String> requestEntity;

    public SendBlacklistCategoriesRequest(Context context, long idDeviceOwner, List<Integer> ids) {
        super(PopularBlacklistSimpleItemModel.class, context);

        this.idDeviceOwner = idDeviceOwner;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONArray array = new JSONArray();
        JSONObject insideObject;
        try {
            for (int contentCategory : ids) {
                insideObject = new JSONObject();
                insideObject.put("deviceOwner", idDeviceOwner);
                insideObject.put("contentCategory", contentCategory);
                array.put(insideObject);
            }
//            jsonObject.put(array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = array.toString();
        Log.e("grch11", "blacklist request: " + request);
        requestEntity = new HttpEntity<String>(request, httpHeaders);
        this.setRequestEntity(requestEntity);

    }

    @Override
    public String getUrl() {
        return null;
    }

    @Override
    public PopularBlacklistSimpleItemModel loadDataFromNetwork() throws Exception {
        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT + Settings.getUserId()
                + END_POINT_MIDDLE + idDeviceOwner + END_POINT_END).buildUpon();
        PopularBlacklistSimpleItemModel model = makeRequest(HttpMethodEnum.post, uriBuilder, PopularBlacklistSimpleItemModel.class, requestEntity);

        return model;
    }
}