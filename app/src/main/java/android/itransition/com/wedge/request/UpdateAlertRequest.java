package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.entity.DataInformationDeviceOwner;
import android.itransition.com.wedge.entity.NotificationsEntity;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 16.09.2014.
 */
public class UpdateAlertRequest extends BaseRequest<BaseModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER = "/notifications/";//http://wedge.demohoster.com/api/users/2/notifications/86
    private long idUser;
    private long id;
    private String type;
    private HttpEntity<String> requestEntity;


    public UpdateAlertRequest(Context context, long idUser,
                              NotificationsEntity notificationsEntity) {
        super(BaseModel.class, context);

        this.idUser = idUser;
        this.id = notificationsEntity.getId();
        this.type = notificationsEntity.getType();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();

        DataInformationDeviceOwner deviceOwnerInfo = notificationsEntity.getData().getDataInformationDeviceOwner();
        try {

            jsonObject.put("id", notificationsEntity.getId());
            jsonObject.put("text", notificationsEntity.getText());
            jsonObject.put("title", notificationsEntity.getTitle());
            jsonObject.put("type", notificationsEntity.getType());
            jsonObject.put("isViewed", true);

            if (notificationsEntity.getType().trim().toLowerCase().equals("time_extension_request")) {
                JSONObject createdAt = new JSONObject();
                createdAt.put("dateTime", notificationsEntity.getCreatedAt().getDateTime());
                createdAt.put("timezone", notificationsEntity.getCreatedAt().getTimeZone());
                jsonObject.put("createdAt", createdAt);

                JSONObject data = new JSONObject();


                data.put("duration", notificationsEntity.getData().getDuration());
                data.put("status", notificationsEntity.getData().getStatus());
                data.put("id", notificationsEntity.getData().getId());
                data.put("terId", notificationsEntity.getData().getTerId());
                data.put("createdAt", notificationsEntity.getData().getCreatedAt());

                JSONObject deviceOwner = new JSONObject();
                deviceOwner.put("id", deviceOwnerInfo.getId());
                deviceOwner.put("content_profile", deviceOwnerInfo.getContentProfile());
                deviceOwner.put("user", deviceOwnerInfo.getUser());
                deviceOwner.put("title", deviceOwnerInfo.getTitle());
//                deviceOwner.put("created_at", deviceOwnerInfo.getCreatedAt());
                deviceOwner.put("updated_at", deviceOwnerInfo.getUpdatedAt());
                deviceOwner.put("status", deviceOwnerInfo.getStatus());
                deviceOwner.put("time", deviceOwnerInfo.getTime());
                deviceOwner.put("is_default", deviceOwnerInfo.getDefault());
                deviceOwner.put("homework_mode_duration",
                        deviceOwnerInfo.getHomework_mode_duration());
                deviceOwner.put("homework_mode_started_at",
                        deviceOwnerInfo.getHomework_mode_started_at());
                deviceOwner.put("block_started_at",
                        deviceOwnerInfo.getBlock_started_at());
                data.put("deviceOwner", deviceOwner);

                jsonObject.put("data", data);

            } else if (type.trim().toLowerCase().equals("new_device_attached")) {
                JSONObject insideObject = new JSONObject();
                insideObject.put("dateTime", notificationsEntity.getCreatedAt().getDateTime());
                insideObject.put("timezone", notificationsEntity.getCreatedAt().getTimeZone());
                jsonObject.put("createdAt", insideObject);

                //put data
                insideObject = new JSONObject();
                insideObject.put("created_at", notificationsEntity.getData().getCreated_At());
                insideObject.put("title", notificationsEntity.getData().getTitle());
                insideObject.put("user", notificationsEntity.getData().getUser());
                insideObject.put("mac_address", notificationsEntity.getData().getMacAddress());
                insideObject.put("is_online", notificationsEntity.getData().getOnline());
                insideObject.put("is_favourite", notificationsEntity.getData().getFavourite());
                insideObject.put("is_enabled", notificationsEntity.getData().getEnabled());
                insideObject.put("id", notificationsEntity.getData().getId());
                insideObject.put("device_owner", deviceOwnerInfo.getId());

                JSONObject insideObjectDeviceOwner = new JSONObject();
                insideObjectDeviceOwner.put("user", deviceOwnerInfo.getUser());
                insideObjectDeviceOwner.put("updated_at", deviceOwnerInfo.getUpdatedAt());
                insideObjectDeviceOwner.put("title", deviceOwnerInfo.getTitle());
                insideObjectDeviceOwner.put("time", deviceOwnerInfo.getTime());
                insideObjectDeviceOwner.put("status", deviceOwnerInfo.getStatus());
                insideObjectDeviceOwner.put("is_default", deviceOwnerInfo.getDefault());
                insideObjectDeviceOwner.put("id", deviceOwnerInfo.getId());
                insideObjectDeviceOwner.put("homework_mode_started_at", deviceOwnerInfo.getHomework_mode_started_at());
                insideObjectDeviceOwner.put("homework_mode_duration", deviceOwnerInfo.getHomework_mode_duration());
//                insideObjectDeviceOwner.put("created_at", deviceOwnerInfo.getCreatedAt());
                insideObjectDeviceOwner.put("content_profile", deviceOwnerInfo.getContentProfile());
                insideObjectDeviceOwner.put("block_started_at", deviceOwnerInfo.getBlock_started_at());
                insideObject.put("deviceOwner", insideObjectDeviceOwner);

                jsonObject.put("data", insideObject);

            } else if (notificationsEntity.getType().toLowerCase().trim().equals("whitelist_request")) {

                JSONObject insideObject = new JSONObject();
                insideObject.put("dateTime", notificationsEntity.getCreatedAt().getDateTime());
                insideObject.put("timezone", notificationsEntity.getCreatedAt().getTimeZone());
                jsonObject.put("createdAt", insideObject);

                //put data
                insideObject = new JSONObject();
                insideObject.put("created_at", notificationsEntity.getData().getCreated_At());
                insideObject.put("id", notificationsEntity.getData().getId());
                insideObject.put("status", notificationsEntity.getData().getStatus());
                insideObject.put("url", notificationsEntity.getData().getUrl());
                insideObject.put("zveloRating", notificationsEntity.getData().getZveloRating());

                JSONObject insideObjectDeviceOwner = new JSONObject();
                insideObjectDeviceOwner.put("user", deviceOwnerInfo.getUser());
                insideObjectDeviceOwner.put("block_started_at", deviceOwnerInfo.getBlock_started_at());
                insideObjectDeviceOwner.put("updated_at", deviceOwnerInfo.getUpdatedAt());
                insideObjectDeviceOwner.put("title", deviceOwnerInfo.getTitle());
                insideObjectDeviceOwner.put("time", deviceOwnerInfo.getTime());
                insideObjectDeviceOwner.put("status", deviceOwnerInfo.getStatus());
                insideObjectDeviceOwner.put("is_default", deviceOwnerInfo.getDefault());
                insideObjectDeviceOwner.put("id", deviceOwnerInfo.getId());
                insideObjectDeviceOwner.put("homework_mode_started_at", deviceOwnerInfo.getHomework_mode_started_at());
                insideObjectDeviceOwner.put("homework_mode_duration", deviceOwnerInfo.getHomework_mode_duration());
//                insideObjectDeviceOwner.put("created_at", deviceOwnerInfo.getCreatedAt());
                insideObjectDeviceOwner.put("content_profile", deviceOwnerInfo.getContentProfile());
                insideObject.put("deviceOwner", insideObjectDeviceOwner);

                jsonObject.put("data", insideObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String request = jsonObject.toString();
        request = request.replaceAll("\\\\/", "/");

        requestEntity = new HttpEntity<String>(request, httpHeaders);
        this.setRequestEntity(requestEntity);

    }


    @Override
    public BaseModel loadDataFromNetwork() throws Exception {
        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT + idUser +
                END_POINT_AFTER + id).buildUpon();


        return makeRequest(HttpMethodEnum.put, uriBuilder, BaseModel.class, requestEntity);
    }

    public String getUrl() {
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT + idUser +
                END_POINT_AFTER + id).buildUpon().toString();
    }


}