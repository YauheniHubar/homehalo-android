package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.HomeWorkCategories;
import android.itransition.com.wedge.entity.TimeProfiles;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 16.09.2014.
 */
public class UpdateDeviceRequest extends BaseRequest<DeviceOwnersModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER="/device-owners/";
    private long idUser;
    private long idDeviceOwner;
    private HttpEntity<String> requestEntity;


    public UpdateDeviceRequest(Context context, long idUser, long idDeviceOwner, String title, String status, long time,
                               CreateTimeUser blockStartedAt, CreateTimeUser homeworkModeStartedAt, long homeworkModeDuration,
                               long contentProfileId, TimeProfiles[] timeProfileses,
                               HomeWorkCategories[] homeWorkCategorieses, CreateTimeUser createTimeUser, CreateTimeUser updatedAt) {
        super(DeviceOwnersModel.class, context);

        this.idUser = idUser;
        this.idDeviceOwner = idDeviceOwner;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", title);
            jsonObject.put("status", status);
            jsonObject.put("id", idDeviceOwner);


            JSONObject insideObject = new JSONObject();
            if (createTimeUser != null) {
                insideObject.put("dateTime", createTimeUser.getDateTime());
                insideObject.put("timezone", createTimeUser.getTimeZone());
            }
            jsonObject.put("createdAt", insideObject);
            if(blockStartedAt!=null) {
                jsonObject.put("blockStartedAt", blockStartedAt.getDateTime());
                jsonObject.put("time", time);
            } else {
                jsonObject.put("blockStartedAt",null);
                jsonObject.put("time", null);
            }
            //TODO  в блок реквест
            if(homeworkModeStartedAt!=null) {
                jsonObject.put("homeworkModeStartedAt", homeworkModeStartedAt.getDateTime());
                jsonObject.put("homeworkModeDuration", homeworkModeDuration);
            }
            jsonObject.put("contentProfile", contentProfileId);
            JSONArray array = new JSONArray();
            for (int i = 0; i < timeProfileses.length; i++) {
                insideObject = new JSONObject();
               // insideObject.put("id", timeProfileses[i].getId());
                insideObject.put("startTime", timeProfileses[i].getStartTime().getTime());
                insideObject.put("endTime", timeProfileses[i].getEndTime().getTime());
                insideObject.put("dayOfWeek", timeProfileses[i].getDayOfWeek());
                array.put(insideObject);
            }
            jsonObject.put("timeProfiles", array);
            array = new JSONArray();
            for (int i = 0; i < homeWorkCategorieses.length; i++) {
                insideObject = new JSONObject();
                insideObject.put("id",homeWorkCategorieses[i].getId());
                insideObject.put("title", homeWorkCategorieses[i].getTitle());
                insideObject.put("categoryName", homeWorkCategorieses[i].getCategoryName());
                insideObject.put("categoryId", homeWorkCategorieses[i].getCategoryId());
                array.put(insideObject);
            }
            jsonObject.put("homeworkCategories", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = jsonObject.toString();
        request = request.replaceAll("\\\\/", "/");

        requestEntity = new HttpEntity<String>(request, httpHeaders);
        this.setRequestEntity(requestEntity);

    }

    @Override
    public DeviceOwnersModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+idUser+END_POINT_AFTER+idDeviceOwner).buildUpon();
        DeviceOwnersModel model = makeRequest(HttpMethodEnum.put, uriBuilder, DeviceOwnersModel.class, requestEntity);

        try {
            if (model.getDeviceOwner() != null) {
                DeviceOwnersDataSource source = new DeviceOwnersDataSource(WedgeApplication.mainSource);
                source.deleteDeviceOwner(model.getDeviceOwner().getId());
                source.createDeviceOwner(model.getDeviceOwner(), source.getDeviceOwnerById(model.getDeviceOwner().getId()).getOrderId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return model;
    }


    public String getUrl(){
        return   Uri.parse(Settings.getUrlHost().trim() + END_POINT+idUser+END_POINT_AFTER+idDeviceOwner).buildUpon()
                .toString();
    }

}