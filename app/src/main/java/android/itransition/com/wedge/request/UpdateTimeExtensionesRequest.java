package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;


public class UpdateTimeExtensionesRequest extends BaseRequest<BaseModel>{

    private final String END_POINT = "/api/users/";
    private final String END_POINT_AFTER = "/device-owners/";
    private final String END_POINT_AFTER_TIME_EXTENSION = "/time-extension/";
    private long id;
    private long deviceOwnerId;
    private  long timeExtensionId;

    private HttpEntity<String> requestEntity;

    public UpdateTimeExtensionesRequest(Context context, long id, long deviceOwneId,
                                        long timeExtensionId, String status,
                                        long duration, long terId) {
        super(BaseModel.class, context);

        this.id = id;
        this.deviceOwnerId = deviceOwneId;
        this.timeExtensionId = timeExtensionId;
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", status);
            jsonObject.put("duration", duration);
            jsonObject.put("terId", terId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }



    @Override
    public BaseModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER+
                deviceOwnerId+END_POINT_AFTER_TIME_EXTENSION+timeExtensionId).buildUpon();
        return makeRequest(HttpMethodEnum.put, uriBuilder, BaseModel.class, requestEntity);
    }

    public String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+id+END_POINT_AFTER+
                deviceOwnerId+END_POINT_AFTER_TIME_EXTENSION+timeExtensionId).buildUpon().toString();
    }
}