package android.itransition.com.wedge.request;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.model.UserModel;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class UserRequest extends BaseRequest<UserModel>{

    private final String END_POINT = "/api/users/";
    private HttpEntity<String> requestEntity;
    private long userId;

    public UserRequest(Context context, long userId) {
        super(UserModel.class, context);
        this.userId = userId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }



    @Override
    public UserModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId).buildUpon();
        UserModel userModel = makeRequest(HttpMethodEnum.get, uriBuilder, UserModel.class, requestEntity);

        if (userModel.getSuccess() && userModel.getUser() != null &&
                userModel.getUser().getId() == Settings.getUserId()) {
            WedgeApplication.currentUser = userModel.getUser();
        }

        return userModel;
    }

    public String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+userId).buildUpon().toString();
    }
}