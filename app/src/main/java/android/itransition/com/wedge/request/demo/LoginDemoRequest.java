package android.itransition.com.wedge.request.demo;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.model.LoginModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Consts;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.GcmHelper;
import android.net.Uri;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by y.drobysh on 28.01.2015.
 */
public class LoginDemoRequest extends BaseRequest<LoginModel> {

    private static final String END_POINT = "/api/demo-users";

    public LoginDemoRequest(Context context) {
        super(LoginModel.class, context);
    }

    @Override
    public LoginModel loadDataFromNetwork() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        String push = GcmHelper.getRegistrationId(getContext());
        if (push == null) {
            push = GcmHelper.registerDevice(getContext(), 1);
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deviceType", Consts.GCM_TYPE);
        jsonObject.put("deviceToken", push);

        HttpEntity<String> requestEntity = new HttpEntity<>(jsonObject.toString(), httpHeaders);
        this.setRequestEntity(requestEntity);

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT).buildUpon();
        LoginModel result = makeRequest(HttpMethodEnum.post, uriBuilder, LoginModel.class, requestEntity);

        WedgeApplication.currentUser = result.getUser();
        Settings.setXAccessToken(result.getAccessToken());
        Settings.setDemoMode(true);
        Settings.setUserId(result.getUser().getId());
        Settings.setSkipButtonFlag(result.getUser().isEmailStepSkipButtonFlag());

        return result;
    }

    @Override
    public String getUrl() {
        return null;
    }
}
