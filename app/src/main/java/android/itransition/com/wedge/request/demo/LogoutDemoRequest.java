package android.itransition.com.wedge.request.demo;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by y.drobysh on 28.01.2015.
 */
public class LogoutDemoRequest extends BaseRequest<BaseModel> {

    public static final String HEADER_SECRET_TOKEN = "X-Secret-Token";
    public static final String HEADER_ACCESS_TOKEN = "X-Access-Token";

    private static final String END_POINT = "/api/demo-users/";

    public LogoutDemoRequest(Context context) {
        super(BaseModel.class, context);
    }

    @Override public BaseModel loadDataFromNetwork() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HEADER_SECRET_TOKEN, Settings.getXSecretToken());
        httpHeaders.set(HEADER_ACCESS_TOKEN, Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT + Settings.getUserId()).buildUpon();
        BaseModel result = makeRequest(HttpMethodEnum.delete, uriBuilder, BaseModel.class, requestEntity);

        WedgeApplication.currentUser = null;
        Settings.setDemoMode(false);
        Settings.setXAccessToken("");
        Settings.setUpdatingAlerts(false);
        Settings.setNeedUpdateContentProfile(true);
        Settings.setUpdatingContentProfileCategories(false);
        Settings.setUpdatingHW(false);
        Settings.setNeedUpdateDeviceOwners(true);
        Settings.setUpdatingDevices(false);
        Settings.setUpdatinWedge(false);

        return result;
    }

    @Override
    public String getUrl() {
        return null;
    }
}
