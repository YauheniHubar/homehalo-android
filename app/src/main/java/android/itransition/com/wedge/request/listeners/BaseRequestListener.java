package android.itransition.com.wedge.request.listeners;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.activity.BuyActivty;
import android.itransition.com.wedge.activity.LoginActivity;
import android.itransition.com.wedge.entity.User;
import android.itransition.com.wedge.model.ErrorModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Settings;
import android.itransition.com.wedge.utils.Utils;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.apache.http.HttpResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: s.ankuda
 * Date: 18.10.13
 * Time: 19:07
 * To change this template use File | Settings | File Templates.
 */
abstract public class BaseRequestListener<RESULT> implements RequestListener<RESULT> {

    Context context;
    private boolean showToast = true;
    private boolean redirectOn403 = true;
    private ProgressDialog dialog;
    protected BaseRequest request;
    protected boolean isHideDialog;

    public BaseRequestListener() {

    }

    public void setRedirectOn403(boolean value) {
        redirectOn403 = value;
    }

    public ProgressDialog getDialog(){
        return dialog;
    }
    public BaseRequestListener(Context context, boolean showToast) {
        this.context = context;
        this.showToast = showToast;
        buildDialog(context, null);
    }

    public BaseRequestListener(Context context, final BaseRequest request) {
        this.context = context;
        this.request = request;

        buildDialog(context, request);
    }

    public BaseRequestListener(Context context, final BaseRequest request, boolean isHideDialog) {
        this.context = context;
        this.request = request;
        this.isHideDialog = isHideDialog;

        buildDialog(context, request);
    }

    public void buildDialog(Context context, final BaseRequest request) {
        if (this.context != null && !isHideDialog) {
            dialog = new ProgressDialog(context);
            dialog.setProgressStyle(ProgressDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setMessage("Loading...");
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    if (request!=null && !request.isCancelled()) {
                        request.cancel();
                    }
                }
            });
            dialog.show();
        }
    }


    public void onRequestFailure(SpiceException e) {
        if (dialog != null) {
            dialog.dismiss();
        }

        String message = e.getLocalizedMessage();
        if(e.getCause() instanceof HttpClientErrorException) {
            HttpClientErrorException exception = (HttpClientErrorException)e.getCause();
            if (exception.getStatusCode().equals(HttpStatus.FORBIDDEN) && redirectOn403) {
                goToLoginScreen();
            } else {
                String bodyAsString = exception.getResponseBodyAsString();
                Log.i("Error Response", bodyAsString);
                try {
//                    if (Settings.CRASH_REPORTS) {
//                        Crashlytics.log("Http_Client_Error bodyAsString: " + bodyAsString + "\n\n" + request == null ? "" : request.logData);
//                        Crashlytics.logException(e);
//                    }
                    ObjectMapper mapper = new ObjectMapper();
                    ErrorModel model = mapper.readValue(bodyAsString, ErrorModel.class);
                    List<String> errors = model.getMessages();
                    if (errors != null && errors.size() > 0) {
                        message = errors.get(0);
                    }
                } catch (Exception ee) {

                }
            }
        } else if (e.getCause() instanceof NoNetworkException) {
            message = context.getString(R.string.error_connection);
        } else if (e.getCause() instanceof HttpServerErrorException) {
            message = "Internal server error";
//            if (Settings.CRASH_REPORTS) {
//                Crashlytics.log("Logged exception on server side");
//                Crashlytics.logException(e);
//            }
        } else {
            message = context.getString(R.string.some_error);
        }

        if (showToast && context != null) Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private void goToLoginScreen() {
        if (context != null && redirectOn403) {
            User user = WedgeApplication.currentUser;
            if (user != null && user.getSubscription() != null) {
                if (!user.getSubscription().isActive()) {
                    checkForPayment(user);
            }} else {
                Settings.setXAccessToken("");
                Intent i = new Intent(context, LoginActivity.class);
                if (Settings.isDemoMode()) {
                    Settings.setDemoMode(false);
                }
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                Utils.makeToast(context.getString(R.string.access_error));
                showToast = false;
            }
        }
    }

    private void checkForPayment(User user) {
//        User user = WedgeApplication.currentUser;
        if (!user.getSubscription().isActive()) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle(R.string.error);
            alert.setMessage(R.string.subscription_expired);
            alert.setPositiveButton(R.string.renew, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(context, BuyActivty.class));
                }
            });
            alert.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        }
    }

    public void onRequestSuccess(RESULT o) {

        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public static String GetText(InputStream in) {
        String text = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            text = sb.toString();
        } catch (Exception ex) {

        } finally {
            try {

                in.close();
            } catch (Exception ex) {
            }
        }
        return text;
    }

    public static String GetText(HttpResponse response) {
        String text = "";
        try {
            text = GetText(response.getEntity().getContent());
        } catch (Exception ex) {
        }
        return text;
    }

    public void setShowToast(boolean value) {
        showToast = value;
    }

    public void setHideDialog(boolean isHideDialog) {
        this.isHideDialog = isHideDialog;
    }
}























































