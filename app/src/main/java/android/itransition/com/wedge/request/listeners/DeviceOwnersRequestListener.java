package android.itransition.com.wedge.request.listeners;

import android.content.Context;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.RequestAction;

import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class DeviceOwnersRequestListener extends BaseRequestListener<DeviceOwnersModel> {


    private RequestAction action;

    public DeviceOwnersRequestListener(Context context, RequestAction action, BaseRequest.HttpMethodEnum method,
                                       String url, BaseRequest request) {
        super(context, request);
        this.action = action;

    }

    public DeviceOwnersRequestListener(Context context, RequestAction action, BaseRequest request, boolean isHideDialog) {
        super(context, request, isHideDialog);
        this.action = action;

    }


    @Override
    public void onRequestFailure(SpiceException e) {
        super.onRequestFailure(e);

        this.action.performFailRequestAction(0);

    }

    @Override
    public void onRequestSuccess(DeviceOwnersModel standartModel) {
        super.onRequestSuccess(standartModel);

        action.updateViewAfterSuccessfulAction(standartModel);
    }

}