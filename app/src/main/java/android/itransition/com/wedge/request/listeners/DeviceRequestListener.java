package android.itransition.com.wedge.request.listeners;

import android.content.Context;
import android.itransition.com.wedge.model.DeviceModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.RequestAction;

import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by e.kazimirova on 05.09.2014.
 */
public class DeviceRequestListener extends BaseRequestListener<DeviceModel> {


    private Context context;
    private RequestAction action;

    public DeviceRequestListener (Context context, RequestAction action, BaseRequest.HttpMethodEnum method,
                                  String url, BaseRequest request) {
        super(context, request);
        this.context = context;
        this.action = action;

    }


    @Override
    public void onRequestFailure(SpiceException e) {
        super.onRequestFailure(e);
        this.action.performFailRequestAction(0);

    }

    @Override
    public void onRequestSuccess(DeviceModel standartModel) {
        super.onRequestSuccess(standartModel);
        action.updateViewAfterSuccessfulAction(standartModel);
    }

}