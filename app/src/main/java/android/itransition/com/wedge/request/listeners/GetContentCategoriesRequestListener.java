package android.itransition.com.wedge.request.listeners;

import android.content.Context;
import android.itransition.com.wedge.model.CategoriesContentProfilesModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.RequestAction;

import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class GetContentCategoriesRequestListener extends BaseRequestListener<CategoriesContentProfilesModel> {


    private Context context;
    private RequestAction action;

    public GetContentCategoriesRequestListener(Context context, RequestAction action, BaseRequest.HttpMethodEnum method,
                                               String url, BaseRequest request) {
        super(context, request);
        this.context = context;
        this.action = action;

    }


    @Override
    public void onRequestFailure(SpiceException e) {
        super.onRequestFailure(e);

        this.action.performFailRequestAction(0);

    }

    @Override
    public void onRequestSuccess(CategoriesContentProfilesModel standartModel) {
        super.onRequestSuccess(standartModel);
        action.updateViewAfterSuccessfulAction(standartModel);
    }

}
