package android.itransition.com.wedge.request.listeners;

import android.content.Context;
import android.itransition.com.wedge.model.LoginModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.itransition.com.wedge.settings.Settings;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.octo.android.robospice.persistence.exception.SpiceException;


public class LoginRequestListener extends BaseRequestListener<LoginModel> {

    private RequestAction action;

    public  LoginRequestListener(Context context, RequestAction action,  BaseRequest.HttpMethodEnum method,
                                 String url, BaseRequest request) {
        super(context, request);
        this.action = action;
        setRedirectOn403(false);
    }


    @Override
    public void onRequestFailure(SpiceException e){
        super.onRequestFailure(e);

        this.action.performFailRequestAction(0);

    }

    @Override
    public void onRequestSuccess(LoginModel loginModel) {
        super.onRequestSuccess(loginModel);
        action.updateViewAfterSuccessfulAction(loginModel);
        try {
            Settings.setXAccessToken(loginModel.getAccessToken());
            Settings.setTimeZone(loginModel.getUser().getWedge().getLastOnlineAt().getTimeZone());
        } catch (NullPointerException ex){}
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

}
