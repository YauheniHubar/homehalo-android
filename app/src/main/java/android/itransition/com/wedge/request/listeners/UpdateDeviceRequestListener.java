package android.itransition.com.wedge.request.listeners;

import android.content.Context;
import android.itransition.com.wedge.model.DeviceOwnersModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.request.RequestAction;
import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by e.kazimirova on 16.09.2014.
 */
public class UpdateDeviceRequestListener extends BaseRequestListener<DeviceOwnersModel> {


    private Context context;
    private RequestAction action;

    public UpdateDeviceRequestListener(Context context, RequestAction action, BaseRequest.HttpMethodEnum method,
                                       String url, BaseRequest request) {
        super(context, request);
        this.context = context;
        this.action = action;

    }


    @Override
    public void onRequestFailure(SpiceException e) {

        super.onRequestFailure(e);
        Log.e("grch11", "Request fail: "+e.getMessage());
        this.action.performFailRequestAction(0);

    }

    @Override
    public void onRequestSuccess(DeviceOwnersModel standartModel) {
        super.onRequestSuccess(standartModel);
        Log.e("grch11", "Request success: " + String.valueOf(standartModel));
//        context.startActivity(Intent.createChooser(Utils.sendLoagcatMail(), "Send email..."));
//        Utils.sendLogcatMail();
        action.updateViewAfterSuccessfulAction(standartModel);
    }

}