package android.itransition.com.wedge.request.timeextension;

import android.content.Context;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by y.drobysh on 09.01.2015.
 */
public class DeleteTimeExtensionRequest extends BaseRequest<BaseModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_DEVICE_OWNER = "/device-owners/";
    private final String END_POINT_URL_WHITELIST = "/time-extensions/";
    private final long deviceOwner;
    private final long extensionId;

    public DeleteTimeExtensionRequest(Context context, long deviceOwner, long extensionId) {
        super(BaseModel.class, context);
        this.deviceOwner = deviceOwner;
        this.extensionId = extensionId;
    }

    @Override
    public String getUrl() {
        return getUrlBuilder().toString();
    }

    Uri.Builder getUrlBuilder() {
        return Uri.parse(Settings.URL_HOST + END_POINT + Settings.getUserId() +
                END_POINT_DEVICE_OWNER + deviceOwner + END_POINT_URL_WHITELIST + extensionId)
                .buildUpon();
    }

    @Override
    public BaseModel loadDataFromNetwork() throws Exception {

        Uri.Builder url = getUrlBuilder();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        BaseModel baseModel = makeRequest(HttpMethodEnum.delete, url, BaseModel.class, new HttpEntity<String>(httpHeaders));

        return baseModel;
    }
}
