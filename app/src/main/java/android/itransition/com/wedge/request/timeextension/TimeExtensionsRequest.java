package android.itransition.com.wedge.request.timeextension;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.model.TimeExtensionModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class TimeExtensionsRequest extends BaseRequest<TimeExtensionModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_DEVICE_OWNER = "/device-owners/";
    private final String END_POINT_URL_WHITELIST = "/time-extensions";
    private final long duration;
    private long deviceOwnerId;
    private long id;


    public TimeExtensionsRequest(Context context, long deviceOwnerId, long duration) {
        super(TimeExtensionModel.class, context);
        this.duration = duration;
        this.deviceOwnerId = deviceOwnerId;
    }

    public TimeExtensionsRequest(Context context, long deviceOwnerId, long duration, long id) {
        this(context, deviceOwnerId, duration);
        this.id = id;
    }

    @Override
    public TimeExtensionModel loadDataFromNetwork() throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(Settings.URL_HOST).append(END_POINT).append(Settings.getUserId())
                .append(END_POINT_DEVICE_OWNER).append(deviceOwnerId)
                .append(END_POINT_URL_WHITELIST);

        HttpMethodEnum method = HttpMethodEnum.post;

        if (id != 0) {
            url.append("/" + id);
            method = HttpMethodEnum.put;
        }

        Uri.Builder uriBuilder = Uri.parse(url.toString())
                .buildUpon();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("duration", duration);
            jsonObject.put("status", "accepted");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpEntity<String> requestEntity = new HttpEntity<String>(jsonObject.toString(), httpHeaders);

        TimeExtensionModel response = makeRequest(method, uriBuilder, TimeExtensionModel.class, requestEntity);

        try {
            DeviceOwnersDataSource source = new DeviceOwnersDataSource(WedgeApplication.mainSource);
            if (response.timeExtension != null) {
                source.createTimeExtension(response.timeExtension, deviceOwnerId);
            }
            DeviceOwner deviceOwner = source.getDeviceOwnerById(deviceOwnerId);
            if (deviceOwner != null) {
                deviceOwner.setHasAccessUntil(response.access.getHasAccessUntil());
                deviceOwner.setHasNoAccessUntil(response.access.getHasNoAccessUntil());
                deviceOwner.setHasAccessUntilLabel(response.access.getHasAccessUntilLabel());
                source.updateDeviceOwner(deviceOwner);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public String getUrl(){
        return Uri.parse(Settings.URL_HOST + END_POINT+ Settings.getUserId() +
                END_POINT_DEVICE_OWNER+deviceOwnerId+END_POINT_URL_WHITELIST+id)
                .buildUpon().toString();
    }
}