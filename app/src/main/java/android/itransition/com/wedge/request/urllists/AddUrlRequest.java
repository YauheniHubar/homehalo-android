package android.itransition.com.wedge.request.urllists;

import android.content.Context;
import android.itransition.com.wedge.database.datesource.UrlListDataSource;
import android.itransition.com.wedge.model.WhiteUrlModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class AddUrlRequest extends BaseRequest<WhiteUrlModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_DEVICE_OWNER = "/device-owners/";
    private final String END_POINT_URL_WHITELIST_SECOND = "/url-whitelists";
    private HttpEntity<String> requestEntity;
    private long deviceOwnerId;


    public AddUrlRequest(Context context, long deviceOwnerId, String url) {
        super(WhiteUrlModel.class, context);

        this.deviceOwnerId = deviceOwnerId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", "accepted");
            jsonObject.put("url", url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String request = jsonObject.toString();
        request = request.replaceAll("\\\\/", "/");
        requestEntity = new HttpEntity<String>(request, httpHeaders);
        this.setRequestEntity(requestEntity);

    }

    @Override
    public WhiteUrlModel loadDataFromNetwork() throws Exception {
        //http://wedge.demohoster.com/api/users/38/device-owners/70/url-whitelists
        Uri.Builder uriBuilder = Uri.parse(Settings.URL_HOST + END_POINT + Settings.getUserId() +
                END_POINT_DEVICE_OWNER + deviceOwnerId + END_POINT_URL_WHITELIST_SECOND)
                .buildUpon();
        WhiteUrlModel result = makeRequest(HttpMethodEnum.post, uriBuilder, WhiteUrlModel.class, requestEntity);

        try {
            if (result.isSuccess()) {
                UrlListDataSource.createUrl(result.whiteUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public String getUrl(){
            //http://wedge.demohoster.com/api/users/38/device-owners/70/url-whitelists
            return Uri.parse(Settings.URL_HOST + END_POINT + Settings.getUserId() +
                    END_POINT_DEVICE_OWNER + deviceOwnerId + END_POINT_URL_WHITELIST_SECOND)
                    .buildUpon().toString();
    }
}