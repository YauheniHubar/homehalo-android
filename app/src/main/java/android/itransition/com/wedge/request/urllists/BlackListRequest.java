package android.itransition.com.wedge.request.urllists;

import android.content.Context;
import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.database.datesource.DeviceOwnersDataSource;
import android.itransition.com.wedge.database.datesource.UrlListDataSource;
import android.itransition.com.wedge.entity.UrlListEntity;
import android.itransition.com.wedge.model.BlackUrlModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class BlackListRequest extends BaseRequest<BlackUrlModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_DEVICE_OWNERS = "/device-owners/";
    private final String END_POINT_BLACK_LIST = "/url-blacklists";

    private final Context context;
    private final HttpMethodEnum action;
    private UrlListEntity url;


    /**
     *
     * @param context
     * @param action post for create, put for update, delete for delete
     * @param url
     */
    public BlackListRequest(Context context, HttpMethodEnum action, UrlListEntity url) {
        super(BlackUrlModel.class, context);
        this.context = context;
        this.action = action;
        this.url = url;
    }

    @Override
    public BlackUrlModel loadDataFromNetwork() throws Exception {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);





        HttpEntity<String> stringHttpEntity = null;
        Uri.Builder uriBuilder = Uri.parse(getUrl()).buildUpon();


        JSONObject jsonObject;
        switch (action) {
            case post:
                jsonObject = new JSONObject();
                jsonObject.put("url", url.getUrl());
                stringHttpEntity = new HttpEntity<>(jsonObject.toString(), httpHeaders);
                break;
            case delete:
                uriBuilder.appendEncodedPath(String.valueOf(url.getId()));
                stringHttpEntity = new HttpEntity<>(httpHeaders);
                break;
            case put:
                jsonObject = new JSONObject();
                uriBuilder.appendEncodedPath(String.valueOf(url.getId()));
                jsonObject.put("url", url.getUrl());
                stringHttpEntity = new HttpEntity<>(jsonObject.toString(), httpHeaders);
                break;
        }

        BlackUrlModel result = makeRequest(action, uriBuilder, BlackUrlModel.class, stringHttpEntity);

        try {
            if (result.isSuccess()) {
                DeviceOwnersDataSource dataSource = new DeviceOwnersDataSource(WedgeApplication.mainSource);
                if (action == HttpMethodEnum.post && result.urlBlacklist != null) {
                    result.urlBlacklist.setBlack(true);
                    UrlListDataSource.createUrl(result.urlBlacklist);
                } else if (action == HttpMethodEnum.delete) {
                    UrlListDataSource.deleteUrl(url);
                } else if (action == HttpMethodEnum.put && result.urlBlacklist != null) {
                    result.urlBlacklist.setBlack(true);
                    UrlListDataSource.updateUrl(result.urlBlacklist);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public String getUrl() {
        return Settings.URL_HOST + END_POINT + Settings.getUserId() + END_POINT_DEVICE_OWNERS +
                url.getDeviceOwner() + END_POINT_BLACK_LIST;
    }
}
