package android.itransition.com.wedge.request.urllists;

import android.content.Context;
import android.itransition.com.wedge.database.datesource.UrlListDataSource;
import android.itransition.com.wedge.entity.UrlListEntity;
import android.itransition.com.wedge.model.BaseModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class DeleteUrlRequest extends BaseRequest<BaseModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_DEVICE_OWNER = "/device-owners/";
    private final String END_POINT_URL_WHITELIST = "/url-whitelists/";
    private HttpEntity<String> requestEntity;
    private long deviceOwnerId;
    private long urlWhiteListId;

    public DeleteUrlRequest(Context context, long deviceOwnerId, long urlWhiteListId) {
        super(BaseModel.class, context);

        this.urlWhiteListId = urlWhiteListId;
        this.deviceOwnerId = deviceOwnerId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        requestEntity = new HttpEntity<String>(httpHeaders);
        this.setRequestEntity(requestEntity);
    }



    @Override
    public BaseModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.URL_HOST + END_POINT+Settings.getUserId()+
                END_POINT_DEVICE_OWNER+deviceOwnerId+END_POINT_URL_WHITELIST+urlWhiteListId)
                .buildUpon();
        BaseModel result = makeRequest(HttpMethodEnum.delete, uriBuilder, BaseModel.class, requestEntity);

        if (result.isSuccess()) {
            try {
                UrlListEntity entity = new UrlListEntity();
                entity.setId(urlWhiteListId);
                UrlListDataSource.deleteUrl(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public String getUrl(){
        return Uri.parse(Settings.URL_HOST + END_POINT+Settings.getUserId()+
                END_POINT_DEVICE_OWNER+deviceOwnerId+END_POINT_URL_WHITELIST+urlWhiteListId)
                .buildUpon().toString();
    }
}