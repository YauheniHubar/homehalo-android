package android.itransition.com.wedge.request.urllists;

import android.content.Context;
import android.itransition.com.wedge.database.datesource.UrlListDataSource;
import android.itransition.com.wedge.model.WhiteUrlModel;
import android.itransition.com.wedge.request.BaseRequest;
import android.itransition.com.wedge.settings.Settings;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * Created by e.kazimirova on 04.09.2014.
 */
public class UpdateUrlRequest extends BaseRequest<WhiteUrlModel> {

    private final String END_POINT = "/api/users/";
    private final String END_POINT_DEVICE_OWNER = "/device-owners/";
    private final String END_POINT_URL_WHITELIST = "/url-whitelists/";
    private HttpEntity<String> requestEntity;
    private long deviceOwnerId;
    private long urlWhiteListId;

    public UpdateUrlRequest(Context context, long deviceOwnerId, long urlWhiteListId, String url, String status) {
        super(WhiteUrlModel.class, context);

        this.urlWhiteListId = urlWhiteListId;
        this.deviceOwnerId = deviceOwnerId;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("X-Secret-Token", Settings.getXSecretToken());
        httpHeaders.set("X-Access-Token", Settings.getXAccessToken());
        httpHeaders.set("Accept-Language", Settings.getLanguage());
        httpHeaders.set("X-Mobile-App", Settings.getPlatform());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", status);
            jsonObject.put("url", url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestEntity = new HttpEntity<String>(jsonObject.toString(), httpHeaders);
        this.setRequestEntity(requestEntity);
    }



    @Override
    public WhiteUrlModel loadDataFromNetwork() throws Exception {

        Uri.Builder uriBuilder = Uri.parse(Settings.getUrlHost().trim() + END_POINT+ Settings.getUserId() +
                END_POINT_DEVICE_OWNER+deviceOwnerId+END_POINT_URL_WHITELIST+urlWhiteListId)
                .buildUpon();
        WhiteUrlModel result = makeRequest(HttpMethodEnum.put, uriBuilder, WhiteUrlModel.class, requestEntity);

        if (result.isSuccess()) {
            try {
                UrlListDataSource.updateUrl(result.whiteUrl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public String getUrl(){
        return Uri.parse(Settings.getUrlHost().trim() + END_POINT+ Settings.getUserId() +
                END_POINT_DEVICE_OWNER+deviceOwnerId+END_POINT_URL_WHITELIST+urlWhiteListId)
                .buildUpon().toString();
    }
}