package android.itransition.com.wedge.robospice;

import com.octo.android.robospice.Jackson2SpringAndroidSpiceService;

import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;

/**
 * Created by y.drobysh on 18.12.2014.
 */
public class ApiService extends Jackson2SpringAndroidSpiceService {

    @Override
    public RestTemplate createRestTemplate() {
        RestTemplate restTemplate = super.createRestTemplate();

        HostnameVerifier verifier = new NullHostnameVerifier();
        MySimpleClientHttpRequestFactory factory = new MySimpleClientHttpRequestFactory(verifier);
        restTemplate.setRequestFactory(factory);


        return restTemplate;
    }



}


