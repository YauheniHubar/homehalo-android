package android.itransition.com.wedge.settings;

/**
 * Created by e.kazimirova on 19.09.2014.
 */
public class Consts {

    public static final String GCM_SENDER_ID = "852728098874";


    public static final String IS_WHITE_LIST = "is_white_list";
    public static final String IS_UNLIMITED_ACCESS = "is_unlimited_access";
    public static final String NEEDS_NEW_PIN = "needs_new_pin";
    public static final String GCM_PREFERENCES = "gcm";
    public static final String GCM_TYPE = "rms_push_notifications.os.android.gcm";

    public static String EDIT_PIN = "edit_pin";
    public  static String IS_DEVICE_OWNER="is_device_owner";
    public static String USERS_ID="users_id";
    public static String USERS_TITLE = "users_title";
    public static String FROM_CONFIG= "from_config";
    public static String USER  = "user";
    public static String DEVICES= "devices";
    public static String DEVICE_ID = "device_id";
    public static String IS_ALERTS = "is_alerts";
    public static String DEVICE_TITLE = "title";
    public static String MAC_ADDRESS = "mac_address";
    public static String MANUFACTURER_TITLE = "manufacturer_title";
    public static String DEVICE_OWNER_ID = "device_owner_id";
    public static String DEVICE_STATUS = "status";
    public static String HOMEWORK_MODE_DURATION = "homeworkModeDuration";
    public static String TIME = "time";
    public static String CONTENT_PROFILE_TITLE ="contentProfileTitle";
    public static String BLOCK_STARTED_AT = "blockStartedAt";
    public static String HOMEWORK_MODE_STARTED_AT = "homeworkModeStartedAt";
    public static String CONTENT_PROFILE_ID = "contentProfileId";
    public static String CONTENT_CATEGORIES = "contentCategories";
    public static String CONTENT_PROFILE_IS_DEFAULT = "content_profile_is_default";
    public static String CONTENT_PROFILE_IS_DELETABLE = "content_profile_is_deletable";
    public static String TIME_PROFILESES = "timeProfileses";
    public static String TIME_EXTENSIONSES_CREATE = "timeExtensionsesCreate";
    public static String TIME_EXTENSIONSES_ID = "timeExtensionsesId";
    public static String TIME_EXTENSION = "timeExtension";
    public static String TIME_EXTENSIONSES_DURATION = "timeExtensionsesDuration";
    public static String TIME_EXTENSIONSES_TER_ID = "timeExtensionsesTerId";
    public static String TIME_EXTENSIONSES_STATUS = "timeExtensionsesStatus";
    public static String TIME_EXTENSIONSES_COUNT = "timeExtensionsesCount";
    public static String HOMEWORK_CATEGORIES = "homeWorkCategorieses";
    public static String CREATED_AT = "createdAt";
    public static String WHITE_LIST_URL = "white_list_url";
    public static String DAY_SPINNER = "day_spinner";
    public static String DAY_NUMBER = "day_number";
    public static String FILENAME = "pin_code";
    public static String BLOCKED = "blocked";
    public static String NOTIFICATION_ENTRY = "notification_entry";
    public static String OWNER_NAME = "device_owner_name";
    public static String STATUS_ACCEPTED = "accepted";
    public static String STATUS_DENIED = "denied";
    public static String ID_DAY = "id_day";



    public static String PIN_FROM_BACKGROUND = "PIN_FROM_BACKGOUND";

}
