package android.itransition.com.wedge.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.itransition.com.wedge.BuildConfig;

import java.util.Locale;

/**
 * Created by e.kazimirova on 02.09.2014.
 */
public class Settings {

    private static final String KEY_PIN = "key_pin";
    private static SharedPreferences sPrefs;
    public static String DATE="date";
    public static String KEY_NAME = "key_name";
    public static String X_SECRET_TOKEN = "x_secret_token";
    public static String X_ACCESS_TOKEN="x_access_token";
    public static String CURRENT_USER_ID = "current_user_id";
    public static String HAS_WEDGE = "has_wedge";
    public static String KEY_EMAIL = "key_email";
    public static String KEY_TIMEZONE = "timezone";
    public static String KEY_UPDATING_CONTENT_PROFILE  = "updating_content_profile";
    public static String KEY_UPDATING_DEVICES  = "updating_devices";
    public static String KEY_UPDATING_DEVICE_OWNER  = "updating_device_owner";
    public static String KEY_UPDATING_HOMEWORK_CATEGORIES  = "updating_homework_categories";
    public static String KEY_UPDATING_WEDGE = "updating_wedge";
    public static String KEY_UPDATING_CONTENT_CATEGORIES="updating_content_categories";
    public static String KEY_COUNT_ALERTS = "count_alerts";
    public static String KEY_ALERTS = "alerts";
    public static String KEY_WAS_LOGGED = "already_was_started";
    public static String KEY_DEMO_MODE = "key_demo_mode";
    public static String KEY_NOTIFICATION_ID = "key_notification";
    public static String KEY_NEEDS_PROMO = "key_needs_promo";
    public static String KEY_CANCEL_ALARM = "key_cancel_alarm";
    public static String KEY_SKIP_BUTTON_FLAG = "emailStepSkipButtonFlag";
    public static String ANDROID = "android";

//    public static final String URL_HOST = "http://54.76.6.48";
    public static final String URL_HOST = "https://homehalo.net";
    public static final String URL_SETTINGS = "https://www.homehalo.net/webpanel/#/profile";
//    public static final String URL_HOST = "http://52.17.11.221";

    public static final boolean LOG = BuildConfig.LOG;
    public static final boolean CRASH_REPORTS = BuildConfig.CRASH_REPORTS;

    public  static void setsPrefs(Context context){
        sPrefs = context.getSharedPreferences(
                "android.itransition.com.wedge", Context.MODE_PRIVATE);
    }

    public static SharedPreferences getsPrefs(){
        return sPrefs;
    }

    public static void setName(String name){
        getsPrefs().edit().putString(KEY_NAME, name).commit();
    }

    public static String getName(){
        return getsPrefs().getString(KEY_NAME, "");
    }

    public static void setEmail(String email){
        getsPrefs().edit().putString(KEY_EMAIL, email).commit();
    }

    public static String getEmail(){
        return getsPrefs().getString(KEY_EMAIL, "");
    }

    public static void setTimeZone(String email){
        getsPrefs().edit().putString(KEY_TIMEZONE, email).commit();
    }

    public static String getTimeZone(){
        return getsPrefs().getString(KEY_TIMEZONE, "UTC");
    }

    public static void setXSecretToken(String secretToken){
        getsPrefs().edit().putString(X_SECRET_TOKEN, secretToken).commit();
    }

    public static String getXSecretToken(){
        return getsPrefs().getString(X_SECRET_TOKEN, "");
    }

    public static void setXAccessToken(String accessToken){
        getsPrefs().edit().putString(X_ACCESS_TOKEN, accessToken).apply();
    }

    public static String getXAccessToken(){
        return getsPrefs().getString(X_ACCESS_TOKEN, "");
    }

    public static void setUserId(long userId){
        getsPrefs().edit().putLong(CURRENT_USER_ID, userId).commit();
    }

    public static long getDate(){
        return getsPrefs().getLong(DATE, 0);
    }

    public static void setDate(long date){
        getsPrefs().edit().putLong(DATE, date).commit();
    }

    public static long getUserId(){
        return getsPrefs().getLong(CURRENT_USER_ID, 0);
    }


    public static boolean hasFullAccess(){
        return getsPrefs().getBoolean(HAS_WEDGE, false);
    }

    public static void setWedge(boolean hasWedge){
        getsPrefs().edit().putBoolean(HAS_WEDGE, hasWedge).commit();
    }


    public static boolean needUpdateContentProfile(){
        return getsPrefs().getBoolean(KEY_UPDATING_CONTENT_PROFILE, false);
    }

    public static void setNeedUpdateContentProfile(boolean update){
        getsPrefs().edit().putBoolean(KEY_UPDATING_CONTENT_PROFILE,update).commit();
    }

    public static boolean getUpdatingDevices(){
        return getsPrefs().getBoolean(KEY_UPDATING_DEVICES, false);
    }

    public static void setUpdatingDevices(boolean update){
        getsPrefs().edit().putBoolean(KEY_UPDATING_DEVICES,update).commit();
    }
    public static boolean isNeedUpdateDeviceOwner() {
        return getsPrefs().getBoolean(KEY_UPDATING_DEVICE_OWNER, true);
    }

    public static void setNeedUpdateDeviceOwners(boolean update){
        getsPrefs().edit().putBoolean(KEY_UPDATING_DEVICE_OWNER,update).apply();
    }

    public static boolean getUpdatingHW(){
        return getsPrefs().getBoolean(KEY_UPDATING_HOMEWORK_CATEGORIES, false);
    }

    public static void setUpdatingHW(boolean update){
        getsPrefs().edit().putBoolean(KEY_UPDATING_HOMEWORK_CATEGORIES,update).commit();
    }


    public static boolean getUpdatingWedge(){
        return getsPrefs().getBoolean(KEY_UPDATING_WEDGE, false);
    }

    public static void setUpdatinWedge(boolean update){
        getsPrefs().edit().putBoolean(KEY_UPDATING_WEDGE,update).commit();
    }

    public static boolean getUpdatingContentProfileCategories(){
        return getsPrefs().getBoolean(KEY_UPDATING_CONTENT_CATEGORIES, false);
    }
    public  static void setUpdatingContentProfileCategories(boolean update){
        getsPrefs().edit().putBoolean(KEY_UPDATING_CONTENT_CATEGORIES,update).commit();
    }

    public static boolean getUpdatingAlerts(){
        return getsPrefs().getBoolean(KEY_ALERTS, false);
    }
    public  static void setUpdatingAlerts(boolean update){
        getsPrefs().edit().putBoolean(KEY_ALERTS,update).commit();
    }

    public static int getCountAlerts(){
        return getsPrefs().getInt(KEY_COUNT_ALERTS, 0);
    }

    public  static void setCountAlerts(int count){
        getsPrefs().edit().putInt(KEY_COUNT_ALERTS, count).commit();
    }

    public static String getUrlHost() {
        return URL_HOST;
    }
    public static String getUrlSettings() {
        return URL_SETTINGS;
    }


    public static boolean isNeedShowDemoScreen() {
        boolean wasLogged = getsPrefs().getBoolean(KEY_WAS_LOGGED, false);
        return wasLogged;
    }
    public static void disableDemoScreen() {
        getsPrefs().edit().putBoolean(KEY_WAS_LOGGED, true).apply();
    }

    public static int getNotificationCount() {
        return getsPrefs().getInt(KEY_NOTIFICATION_ID, 0);
    }

    public static String getPin() {
        return getsPrefs().getString(KEY_PIN, "");
    }

    public static void setPin(String val) {
        getsPrefs().edit().putString(KEY_PIN, val).apply();
    }

    public static void setDemoMode(boolean value) {
        getsPrefs().edit().putBoolean(KEY_DEMO_MODE, value).apply();
    }

    public static boolean isDemoMode() {
        return getsPrefs().getBoolean(KEY_DEMO_MODE, false);
    }
    public static void saveNotificationCount(int val) {
        getsPrefs().edit().putInt(KEY_NOTIFICATION_ID, val).apply();
    }

    public static void setFirstRun(boolean value) {
        getsPrefs().edit().putBoolean(KEY_NEEDS_PROMO, value).apply();
    }

    public static boolean isFirstRun() {
        return getsPrefs().getBoolean(KEY_NEEDS_PROMO, true);
    }

    public static void setCancelAlarmManager(boolean value) {
        getsPrefs().edit().putBoolean(KEY_CANCEL_ALARM, value).apply();
    }

    public static boolean isAlarmCanceled() {
        return getsPrefs().getBoolean(KEY_CANCEL_ALARM, false);
    }

    public static void setSkipButtonFlag(int value) {
        getsPrefs().edit().putInt(KEY_SKIP_BUTTON_FLAG, value).apply();
    }

    public static int getSipButtonFlag() {
        return getsPrefs().getInt(KEY_SKIP_BUTTON_FLAG, 0);
    }

    public static String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public static String getPlatform() {
        return ANDROID;
    }

}
