package android.itransition.com.wedge.utils;

import android.itransition.com.wedge.WedgeApplication;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.DeviceOwner;
import android.itransition.com.wedge.entity.DevicesEntity;
import android.itransition.com.wedge.entity.TimeExtensions;
import android.util.Log;
import android.util.Pair;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by y.drobysh on 10.02.2015.
 */
public class DeviceOwnerManager {



    public enum OwnerAccessState {
        HasAccess, NoAccess, Blocked, HomeWork, Extended, Online, Offline, UnlimitedAccess;
    }
    private DeviceOwnerManager() {}

    public static long getBlockRemainMin(DeviceOwner owner) {
        long blockDurationSec = owner.getTime();
        CreateTimeUser block = owner.getBlockStartedAt();

        if (blockDurationSec == 0 || block == null || block.getDateTime() == null
                || block.getTimeZone() == null || block.getDate() == null) {
            return -1;
        }

        Date blockStartTime = block.getDate();
        Date now;
        if (WedgeApplication.currentUser.getWedge() != null) {
            now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
        } else {
            now = new DateTime().toDate();
        }

        long remainMin = (long) Math.ceil((blockStartTime.getTime() + blockDurationSec * 1000 - now.getTime()) / (1000 * 60));

        return remainMin;
    }


    public static long getHomeWorkRemainMin(DeviceOwner owner) {
        long hwDurationSec;
        if (owner.getHomeworkModeDuration() == null) {
            return -1;
        } else {
            hwDurationSec = owner.getHomeworkModeDuration();
        }
        CreateTimeUser hwStartedAt = owner.getHomeworkModeStartedAt();

        if (hwDurationSec == 0 || hwStartedAt == null || hwStartedAt.getDateTime() == null
                || hwStartedAt.getTimeZone() == null || hwStartedAt.getDate() == null) {
            return -1;
        }

        Date hwStartTime = hwStartedAt.getDate();
        Date now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));

        long remainMin = (long) Math.ceil((hwStartTime.getTime() + hwDurationSec * 1000 - now.getTime()) / (1000 * 60));

        return remainMin;
    }


    public static long getTimeExtensionRemainMin(DeviceOwner owner) {
        TimeExtensions extension = owner.getTimeExtension();
        if (extension == null || extension.getStartedAt() == null || extension.getStartedAt().getDate() == null
                || extension.getStartedAt().getTimeZone() == null) {
            return -1;
        }

        Date hwStartTime = extension.getStartedAt().getDate();
        Date now;
        if (WedgeApplication.currentUser.getWedge() != null) {
            now = Utils.getNow(Utils.getWedgeTimeZone(WedgeApplication.currentUser.getWedge()));
        } else {
            now = new DateTime().toDate();
        }
        long remainMin;
        if (hwStartTime.after(now)) {
            remainMin = extension.getDuration() / 60;
        } else {
            remainMin = (long) Math.ceil((hwStartTime.getTime() + extension.getDuration() * 1000 - now.getTime()) / (1000 * 60));
        }

        return remainMin;
    }

    public static boolean hasAccessNow(DeviceOwner owner) {
        CreateTimeUser startedAt = owner.getHasAccessUntil();

        if (startedAt == null || startedAt.getDateTime() == null
                || startedAt.getTimeZone() == null || startedAt.getDate() == null) {
            return false;
        }

        Date hwStartTime = startedAt.getDate();
        Date now = Utils.getNow(startedAt.getTimeZone());

        long remainMin = (long) Math.ceil((hwStartTime.getTime() - now.getTime()) / (1000 * 60));

        return remainMin > 0;
    }

    public static OwnerAccessState getOwnerAccessState(DeviceOwner owner) {

        return getOwnerAccessStateWithTime(owner).first;
    }

    /**
     * returns device owner state with remain time in which he must be in this state ????/
     * @param owner
     * @return pair containing access state and remain minutes in this state
     */
    public static Pair<OwnerAccessState, Long> getOwnerAccessStateWithTime(DeviceOwner owner) {
        long remainTime;
        if (owner.getHasAccessUntil()== null){
            /*
                the user has no Access so the field with additional info available -  hasNoAccessUntil, hasNoAccessUntilLabel, isNoAccessUnlimited
             */

            if (owner.getHasNoAccessUntil()!= null
                    && !owner.isNoAccessUnlimited()
                    && !owner.isAccessUnlimited()
                    && owner.getBlockStartedAt()==null){
                return new Pair<>(OwnerAccessState.NoAccess, 0L);
            }else if (owner.getHasNoAccessUntil()!= null
                    && !owner.isNoAccessUnlimited()
                    && !owner.isAccessUnlimited()
                    && owner.getBlockStartedAt().getDateTime() != null
                    && getBlockRemainMin(owner) > 0){
                remainTime = getBlockRemainMin(owner);
                return new Pair<>(OwnerAccessState.Blocked, remainTime);
            }

        } else if (owner.getHasAccessUntil()!= null && owner.getHasAccessUntil().getDateTime() != null){

        }


        if (!owner.isAccessUnlimited() && owner.getHasAccessUntil()== null){
            return new Pair<>(OwnerAccessState.NoAccess, 0L);
        } else if (owner.isAccessUnlimited()&& (owner.getHomeworkModeDuration() == null)){
            return new Pair<>(OwnerAccessState.UnlimitedAccess, 0L);
        }

        if ((remainTime = getBlockRemainMin(owner)) > 0) {
            Log.d("grecha","remainTime:"+remainTime);

            return new Pair<>(OwnerAccessState.Blocked, remainTime);

        } else if ((remainTime = getHomeWorkRemainMin(owner)) > 0) {

            return new Pair<>(OwnerAccessState.HomeWork, remainTime);
        } else if ((remainTime = getTimeExtensionRemainMin(owner)) > 0) {

            return new Pair<>(OwnerAccessState.Extended, remainTime);
        } else if (!DeviceOwnerManager.hasAccessNow(owner)) {      //haven't access now

            return new Pair<>(OwnerAccessState.NoAccess, 0L);
        }
        return new Pair<>(OwnerAccessState.HasAccess, 0L);
    }

    /**
     *
     * @param device
     * @return device network state - Offline or Online
     */
    public static OwnerAccessState getOwnerNetworkStatus(DevicesEntity device){

        return device.isOnline()? OwnerAccessState.Online : OwnerAccessState.Offline;
    }

}
