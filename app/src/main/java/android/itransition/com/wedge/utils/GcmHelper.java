package android.itransition.com.wedge.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.itransition.com.wedge.settings.Consts;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

/**
 * Created by y.drobysh on 23.01.2015.
 */
public class GcmHelper {

    private static final String TAG = "GcmHelper";

    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";

    private static Object lock = new Object();

    private GcmHelper() {
    }


    public static String getRegistrationId(Context context) {
        String registrationId;
        synchronized (lock) {
            final SharedPreferences prefs = getGcmPreferences(context);
            registrationId = prefs.getString(PROPERTY_REG_ID, "");
            if (TextUtils.isEmpty(registrationId)) {
                Log.i(TAG, "Registration not found.");
                return "";
            }
            int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
            int currentVersion = getAppVersion(context);
            if (registeredVersion != currentVersion) {
                Log.i(TAG, "App version changed.");
                return "";
            }
        }
        return registrationId;
    }

    private static SharedPreferences getGcmPreferences(Context context) {
        return context.getSharedPreferences(Consts.GCM_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static String registerDevice(Context context) {
        return registerDevice(context, 5);
    }
    public static String registerDevice(Context context, int attempts) {
        synchronized (lock) {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
            while (attempts > 0) {
                try {
                    String regid = gcm.register(Consts.GCM_SENDER_ID);
                    Log.d(TAG, "Device registered, registration ID=" + regid);
                    storeRegistrationId(context, regid);
                    return regid;
                } catch (IOException ex) {
                    Log.d(TAG, "Error :" + ex.getMessage());
                }
                attempts--;
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) { }
            }
        }
        return null;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion + "\n" + " regId = " + regId);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

}
