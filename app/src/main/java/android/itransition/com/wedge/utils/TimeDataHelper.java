package android.itransition.com.wedge.utils;

/**
 * Created by i.grechishchev on 08.07.2015.
 * itransition 2015
 */
public class TimeDataHelper {
    String hour;
    String minutes;
    int interval;

    public TimeDataHelper(String hour, String minutes) {
        this.minutes = minutes;
        this.hour = hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public String getHour() {
        return hour;
    }

    public String getMinutes() {
        return minutes;
    }

    public int getInterval() {
        return interval;
    }
}
