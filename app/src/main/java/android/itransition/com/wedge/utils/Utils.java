package android.itransition.com.wedge.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.itransition.com.wedge.R;
import android.itransition.com.wedge.entity.CreateTimeUser;
import android.itransition.com.wedge.entity.WedgeEntity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by y.drobysh on 09.12.2014.
 */
public class Utils {

    private static Context context;
    public static void init(Context ctx) {
        context = ctx;
    }

    public static SimpleDateFormat API_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final String HOUR_MINUTES_DELIMITER = ":";

    public static void makeToast(String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void makeToast(int stringId) {
        Toast.makeText(context, stringId, Toast.LENGTH_SHORT).show();
    }

    public static boolean isOnline(Context ctx) {
        ConnectivityManager cm =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static int dp2px(int dp, Context ctx) {
        		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                        ctx.getResources().getDisplayMetrics());
         	}

    public static void hideSoftKeyboard(Context context, View v) {
        InputMethodManager inputMethodManager = (InputMethodManager)  context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context, View v) {
        InputMethodManager inputMethodManager = (InputMethodManager)  context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
    }

    public static int roundHours(int progressInMin) {
        if ((progressInMin / 60) >= 2 ) {
            progressInMin = (progressInMin / 60) * 60;
        }
        return progressInMin;
    }

    public static void updateSliderText(Context context, int progressInMin, long timeDuration, TextView textView) {
        int h = progressInMin / 60;
        String sliderText = null;
        if (progressInMin > 0) {
            if (h > 0) {
                /*if (h >= 2) {
                    sliderText = h + context.getString(R.string.circle_slider_hours);
                } else */if (progressInMin - h * 60 != 0) {
                    sliderText =  h + HOUR_MINUTES_DELIMITER + (progressInMin - h * 60)
                            +  context.getString(R.string.circle_slider_hours);
                } else {
                    sliderText = h + context.getString(R.string.circle_slider_hour);
                }
            } else {
                sliderText =  progressInMin + context.getString(R.string.circle_slider_minutes);
            }
        } else if (progressInMin <= 0 && timeDuration >= 0) {
            sliderText = "0 " + context.getString(R.string.circle_slider_minutes);
        }
        if (sliderText != null) textView.setText(sliderText);
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static Date getNow(String blockZone) {
        Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone(blockZone));
        Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone(blockZone));
        cal2.clear();
        cal2.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
        cal2.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
        cal2.set(Calendar.DATE, cal1.get(Calendar.DATE));
        cal2.set(Calendar.HOUR_OF_DAY, cal1.get(Calendar.HOUR_OF_DAY));
        cal2.set(Calendar.MINUTE, cal1.get(Calendar.MINUTE));
        return cal2.getTime();
    }

    public static Date getNowJoda(String blockZone) {
        DateTime utc = new DateTime(DateTimeZone.UTC);
        DateTimeZone tz = DateTimeZone.forID(blockZone);
        DateTime losAngelesDateTime = utc.toDateTime(tz);
        return losAngelesDateTime.toLocalDateTime().toDate();
    }

    public static boolean isPeriodActive(CreateTimeUser time, long durationSec) {
        if (durationSec <= 0 || time.getDate() == null || time.getTimeZone() == null)   return false;

        Date now = getNow(time.getTimeZone());
        Date end = new Date(time.getDate().getTime() + durationSec * 1000);
        return end.after(now);
    }

    public static CharSequence getTimeString(long remainTimeMin) {
        if (remainTimeMin > 60) {
            long hours = remainTimeMin / 60;
            long mins = remainTimeMin % 60;
            return context.getResources()
                    .getQuantityString(R.plurals.plural_for_hour, (int) hours, hours) + " " +
                    context.getResources()
                            .getQuantityString(R.plurals.plural_for_min, (int) mins, mins);
        }
        return context.getResources()
                .getQuantityString(R.plurals.plural_for_min, (int) remainTimeMin, remainTimeMin);
    }

    public static int getDisplayHeight(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int getDisplayWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static void sendLogcatMail(){
        // save logcat in file
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Wedge_logs");
        myDir.mkdirs();
        String command = "logcat -d *:W";

        File outputFile = new File(myDir,
                "wedge_logs.txt");
        String []cmd = new String[]{"logcat", "-f", outputFile.getAbsolutePath(), "-v", "time", "*:E"};
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //send file using email
        Log.e("grch11", "====================== saved logs =======================");
//        Intent emailIntent = new Intent(Intent.ACTION_SEND);
//        String to[] = {"i.grechishchev@itransition.com"};
//        emailIntent .putExtra(Intent.EXTRA_EMAIL, to);
//        // the attachment
//        emailIntent .putExtra(Intent.EXTRA_STREAM, outputFile.getAbsolutePath());
//        // the mail subject
//        emailIntent .putExtra(Intent.EXTRA_SUBJECT, "logs");
//        startActivity(Intent.createChooser(emailIntent , "Send email..."));
//        return emailIntent;
//        LogsToFile.getInstance().generateLog("W", "logsTest", null);
    }

    public static String getWedgeTimeZone(WedgeEntity wedgeEntitiy) {
        if (wedgeEntitiy != null && wedgeEntitiy.getTimezone() != null) {
            return wedgeEntitiy.getTimezone();
        } else {
            return "GMT";
        }
    }

}
